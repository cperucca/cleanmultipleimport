
# -*- coding: utf-8 -*-

from xml.dom import minidom
import xml.etree.ElementTree as ET
import codecs
import operator
import os.path
import stat
import shutil
import glob
import json
from datetime import datetime, timedelta
import ast
import logging

# definizione dei namespaces per parsaqre gli atom
namespaces = { 'atom':'{http://www.w3.org/2005/Atom}',
               'dcterms' : '{http://purl.org/dc/terms/}',
                'mam' : '{http://www.vizrt.com/2010/mam}',
                'age' : '{http://purl.org/atompub/age/1.0}',
                'opensearch' : '{http://a9.com/-/spec/opensearch/1.1/}',
                'vaext' : '{http://www.vizrt.com/atom-ext}',
                'app' : '{http://www.w3.org/2007/app}',
                'vdf' : '{http://www.vizrt.com/types}',
                'metadata' : '{http://xmlns.escenic.com/2010/atom-metadata}',
                #'': '{http://www.w3.org/1999/xhtml}',
                'playout' : '{http://ns.vizrt.com/ardome/playout}' }

logger = logging.getLogger('pyAPIDB')

for entry in namespaces:
        #logger.debug(entry, namespaces[entry])
        ET._namespace_map[namespaces[entry].replace('{','').replace('}','')] = entry

def Aggiungi_Items_al_Db( archive_name, archive_dict ):

        logger.debug(' ------------------ INIT Aggiungi_Items_al_Db ------------')
        if len(archive_dict) == 0 :
                return
        old_dict = {}
        if os.path.isfile( archive_name ):
                adesso = datetime.utcnow()
                shutil.copy( archive_name, archive_name + "_" + adesso.strftime("%s") )
                try :
                        logger.debug('AGGIUNGO a  ARCHIVE_NAME ' + archive_name)
                        fin = codecs.open( archive_name, 'r', 'utf-8' )
                        old_dict = json.load( fin )
                        logger.debug(old_dict)
                        fin.close()
                        logger.debug('---------------------- AGGIUNTO AD ARCHIVIO ------')
                except Exception as e:
                        logger.debug('PROBLEMI CON IL DB in Aggiungi_Items_al_Db : ' + str(e))
                        pass

        for key,value in archive_dict.iteritems():
                old_dict[ key ] = value


        fout = codecs.open( archive_name, 'w', 'utf-8')
        json.dump( old_dict, fout )
        fout.close()


def Azzera_Db( db_name ):

	result = True

	logger.debug(' ---- INIT ----- Azzera_Db : ' + db_name)
	#if len(lista) == 0 :
		#return

	#adesso = datetime.utcnow()

	lista = {}
	#shutil.copy( db_name, db_name + "_" + adesso.strftime("%s") )
	try :
		fout = codecs.open( db_name, 'w', 'utf-8')
		json.dump( lista, fout )
		fout.close()
	except Exception as e:
		logger.error('PROBLEMI CON IL DB in Azzera_Db: ' + str(e))
		logger.debug('PROBLEMI CON IL DB')
		result = False
		pass

	logger.debug(' ---- END ----- Azzera_Db : ' + db_name)
	return result



def Scrivi_Items_Db( db_name, lista ):


	result = True

	logger.debug(' ---- INIT ----- Scrivi_Items_Db : ' + db_name)
	#if len(lista) == 0 :
	#return

	#adesso = datetime.utcnow()

	#shutil.copy( db_name, db_name + "_" + adesso.strftime("%s") )
	try :
		fout = codecs.open( db_name, 'w', 'utf-8')
		json.dump( lista, fout )
		fout.close()
	except Exception as e:
		logger.error('PROBLEMI CON IL DB in Scrivi_Items_Db: ' + str(e))
		logger.debug('PROBLEMI CON IL DB')
		result = False
		pass

	logger.debug(' ---- END ----- Scrivi_Items_Db : ' + db_name)
	return result


def Prendi_Items_Db( db_name ):

	logger.debug(' ----INIT----- Prendi_Items_Db : ' + db_name)
	result_dict = {}
	try :
		#logger.debug('leggo DB_NAME ' + db_name)
		fin = codecs.open( db_name, 'r', 'utf-8' )
		result_dict = json.load( fin )
		logger.debug(result_dict)
		logger.debug( ' -----------------------------------------------------------------------------CLAD -------------------')
		fin.close()
	except Exception as e:
		logger.error('PROBLEMI CON IL DB in Prendi_Items_Db: ' + str(e))
		logger.debug('PROBLEMI CON IL DB')
		pass

	logger.debug(' ----END----- Prendi_Items_Db : ' + db_name)

	return result_dict

def printList( dict_content ):


	for key, value in dict_content.iteritems():
		logger.debug(' Key -----> ' + key)
		logger.debug(' Value -----> ' + value)
		logger.debug('------------------------- fine ' + key + '-----------------------')



if __name__ == "__main__":

	#print( Scrivi_Items_Db( '/home/perucccl/Webservices/PRODUCTION/ImportKeyFramesTraffic/Resources/_ImportKeyFramesDb_', dict ))
	print( Prendi_Items_Db( '/home/perucccl/Webservices/PRODUCTION/ImportKeyFramesTraffic/Resources/_ImportKeyFramesDb_' ))
	print( Azzera_Db( '/home/perucccl/Webservices/PRODUCTION/ImportKeyFramesTraffic/Resources/_ImportKeyFramesDb_' ))
	print( Prendi_Items_Db( '/home/perucccl/Webservices/PRODUCTION/ImportKeyFramesTraffic/Resources/_ImportKeyFramesDb_' ))

	exit(0)

