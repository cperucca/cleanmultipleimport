# -*- coding: utf-8 -*-


import logging
import logging.config

import os
import shutil
import json
import glob
import time
from pathlib import Path


import MAM.MamServices as MamServices
import ECE.XmlServices as XmlServices
import ECE.SolrServices as SolrServices
import Resources.pySettaVars as pySettaVars
import DBApi.API_DB as API_DB


# These two lines enable debugging at httplib level (requests->urllib3->http.client)
# You will see the REQUEST, including HEADERS and DATA, and RESPONSE with HEADERS but without DATA.
# The only thing missing will be the response.body which is not logged.
try:
    import http.client as http_client
except ImportError:
    # Python 2
    import httplib as http_client
http_client.HTTPConnection.debuglevel = 5
requests_log = logging.getLogger("requests.packages.urllib3")
requests_log.setLevel(logging.DEBUG)
requests_log.propagate = True



if __name__ == "__main__":



	'''
	with open('listadictMamId.txt', 'r') as filehandle:
	    dictMamId = json.load(filehandle)

	dictMamToEce = {}

	print( 'listadictMamId = ' + str(len(dictMamId)))
	# qui adesso ho un dict con id:mamUrn
	for key,value in dictMamId.items():
		if value in dictMamToEce:
			dictMamToEce[value].append(key)
		else:
			dictMamToEce[value] = [ key ]

	countSingoli = 0
	for key, value in dictMamToEce.items():
		print( key,value)
		if len(value)==1:
			countSingoli = countSingoli + 1  
	print(countSingoli)
	print(len(dictMamToEce ))
	with open('listadictMamToEce.txt', 'w') as filehandle:
	    json.dump(dictMamToEce, filehandle)
		
	with open('listaAsset.txt', 'r') as filehandle:
	    listaAsset = json.load(filehandle)
	print( 'listaAsset = ' + str(len(listaAsset)))
	listaId = []
	for lis in listaAsset:
		id = lis['id'].split(':')[-1]
		listaId.append(id)
	with open('listaId.txt', 'w') as filehandle:
	    json.dump(listaId, filehandle)

	with open('listaId.txt', 'r') as filehandle:
	    listaId = json.load(filehandle)
	print( 'listaId = ' + str(len(listaId)))
	with open('listadictMamToEce.txt', 'r') as filehandle:
	    dictMamToEce = json.load(filehandle)
	print( 'dictMamToEce = ' + str(len(dictMamToEce)))

	countSenza = 0
	for key, value in dictMamToEce.items():
		print( key,value)
		for val in value:
			if val in listaId:
				print( 'TROVATOOOO')
		countSenza = countSenza+1
		print('------------------------- ' + key)

	print(countSenza)
	

	with open('listadictRealMamToEce.txt', 'r') as filehandle:
	    listaReal = json.load(filehandle)
	print( 'listaReal = ' + str(len(listaReal)))

	daSalvare = []
	daEliminare = []
	for key, value in listaReal.items():
		print( key,value)
		print( value['real'], value['fromEce'])
		for val in value['fromEce']:
			if val == value['real']:
				print('salvo')
				daSalvare.append(val)
			else:
				print('cancello')
				daEliminare.append(val)
	
	print('daSalvare = ' + str(len(daSalvare)))
	print('daEliminare = ' + str(len(daEliminare)))
	with open('listaSalvare.txt', 'w') as filehandle:
	    json.dump(daSalvare, filehandle)
	with open('listaEliminare.txt', 'w') as filehandle:
	    json.dump(daEliminare, filehandle)

	'''
	with open('listaError.txt', 'r') as filehandle:
	    listaError = json.load(filehandle)
	print( 'listaError = ' + str(len(listaError)))
	with open('listaEliminare.txt', 'r') as filehandle:
	    listaEliminare = json.load(filehandle)
	print( 'listaEliminare = ' + str(len(listaEliminare)))
	
	listaRealError = []
	for lis in listaError:
		
		if lis in listaEliminare:
			print('trovato : ' + lis )
			continue
		else:
			listaRealError.append( lis )


	
	print( 'listaRealError = ' + str(len(listaRealError)))



























