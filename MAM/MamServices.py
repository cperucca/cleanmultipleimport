
import os 

# -*- coding: utf-8 -*-

import logging
import urllib.request, urllib.error, urllib.parse
import base64
from xml.dom import minidom
import xml.etree.ElementTree as ET
import codecs
import operator
import os.path
import stat
import shutil
import glob
import json
from datetime import datetime, timedelta
import ast
import os
import copy
from http.client import HTTPSConnection
from base64 import b64encode
import sys
import re


# These two lines enable debugging at httplib level (requests->urllib3->http.client)
# You will see the REQUEST, including HEADERS and DATA, and RESPONSE with HEADERS but without DATA.
# The only thing missing will be the response.body which is not logged.
try:
    import http.client as http_client
except ImportError:
    # Python 2
    import httplib as http_client
http_client.HTTPConnection.debuglevel = 5
requests_log = logging.getLogger("requests.packages.urllib3")
requests_log.setLevel(logging.DEBUG)
requests_log.propagate = True

# definizione dei namespaces per parsare gli atom
namespaces = { 'atom':'{http://www.w3.org/2005/Atom}',
	       'dcterms' : '{http://purl.org/dc/terms/}',
		'mam' : '{http://www.vizrt.com/2010/mam}',
		'age' : '{http://purl.org/atompub/age/1.0}',
		'opensearch' : '{http://a9.com/-/spec/opensearch/1.1/}',
		'vaext' : '{http://www.vizrt.com/atom-ext}',
		'app' : '{http://www.w3.org/2007/app}',
		'vdf' : '{http://www.vizrt.com/types}',
		'metadata' : '{http://xmlns.escenic.com/2010/atom-metadata}',
		#'': '{http://www.w3.org/1999/xhtml}',
		'playout' : '{http://ns.vizrt.com/ardome/playout}' }

# importa il pySettaVars
import Resources.pySettaVars as pySettaVars

logger = logging.getLogger('pyMam')

for entry in namespaces:
	#logger.debug(entry, namespaces[entry])
	ET._namespace_map[namespaces[entry].replace('{','').replace('}','')] = entry

def getJsonRundown( mamId ):
	print ( '------------------------ inizia getJsonRundown -------------- ' )
	# per esempio http://rsis-zp-mam-blackhole/mam-video-api/rundown/full/c8ce483bf45babecc08681f854de1961
	urn =  'rundown/full'
	logger.debug ( '------------------------ inizia getJsonRundown -------------- ' )
	result= None 

	try:
		link = os.environ['MAM_SERVER']  + urn + '/' + str(mamId)
		print( 'link : ' + link  )
		logger.debug( 'link : ' + link  )
		
		request = urllib.request.Request(url=link, method='GET', headers={'User-Agent': 'Foo bar'})
		resultResponse = urllib.request.urlopen(request)
		#print ( resultResponse )
		str_response = resultResponse.read().decode('utf-8')
		#print(str_response)
	
		response = json.loads(str_response)
		#print(response)
		result = response

	except urllib.error.HTTPError as e:
		print(' EXCEPTIOOOONNNNNN - ritorno none !!!! ' + str(e))
		logger.debug(' EXCEPTIOOOONNNNNN - ritorno none !!!! ' + str(e))
		return [ False, str(e) ]

	print ( '------------------------ END getJsonRundown -------------- ' )
	logger.debug ( '------------------------ END getJsonRundown -------------- ' )

	return [ True, result ]

def getCmsUrnFromRundown( mamId ):
	
	print( '------------------------ INIT getCmsFromRundown -------------- 4: '  + mamId)
	logger.debug( '------------------------ INIT getCmsFromRundown -------------- 4 : ' + mamId )
	result = ''

	try:
		[ success, val ] = getJsonRundown( mamId )
		print('success di getJsonRundown = ' + str(success))
		#print('val di getJsonRundown = ' + str(val))
		if not success:
			logger.error('in getCmsFromRundown la getJsonRundown mi ha tornato False ! ')
			return 'error'
		if 'cms' in val and 'urn' in val['cms']:
			result = val['cms']['urn'].split(':')[-1]
		else:
			print('Hmmmm')
			logger.debug('Hmmmm')
		
	except Exception as e:
		logger.error('ERROR: Except in getCmsUrnFromRundown  = ' + str(e))
		return  ''

	print( '------------------------ END getCmsFromRundown -------------- ' )
	logger.debug( '------------------------ END getCmsFromRundown -------------- ' )
	return result


if __name__ == "__main__":

	os.environ['MAM_SERVER'] = 'http://rsis-zp-mam-blackhole/mam-video-api/'
	
	dictRealMamToEce = {}
	with open('listadictMamToEce.txt', 'r') as filehandle:
	    dictMamToEce = json.load(filehandle)
	for key,value in dictMamToEce.items():
		val = getCmsUrnFromRundown( key )
		print(val)
		dictRealMamToEce[ key ] = {'real': val, 'fromEce' : value }

	with open('listadictRealMamToEce.txt', 'w') as filehandle:
	    json.dump(dictRealMamToEce, filehandle)
	exit(0)

	val = getCmsUrnFromRundown( 'c8ce483bf45babecc08681f854d92a6f' )
	print(val)
	
