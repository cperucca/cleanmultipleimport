import os

# IMPORT_SECTION: 9718 corrisponde a LA1->Film e Telefilm
PROD_Environment_Variables = {'MAM_SERVER':'http://rsis-zp-mam-blackhole/mam-video-api/','ECE_MODEL':'http://internal.publishing.production.rsi.ch/webservice/publication/rsi/escenic/model/','IMPORT_SECTION': '9718','FILE_LOG_DIR':'/home/perucccl/Webservices/PRODUCTION/cleanMultipleImport/LOGS/FILES/','AUDIO_CREATE_URL':'http://internal.publishing.production.rsi.ch/webservice/escenic/section/__CREATE_SECTION__/content-items', 'AUDIO_RESOURCE_TEMPLATE':'/home/perucccl/Webservices/PRODUCTION/cleanMultipleImport/Resources/template_mamtranscodableaudio.xml','LOCK_URL' : 'http://internal.publishing.production.rsi.ch/webservice/escenic/lock/article/','LOCK_ID' : '11868353' ,'REST_POST_URL': 'http://publishing.rsi.ch/rsi-api/intlay/mockup/importkeyframe/keyframes.json','REST_GET_URL': 'https://www.rsi.ch/rsi-api/intlay/mockup/importkeyframe/keyframes.json', 'IMPORT_ARCHIVE_DIR' : '/mnt/rsi_transcoded/import/digas/imported/', 'IMPORT_DIR' : '/mnt/rsi_transcoded/import/digas/','VERSION' : '3.2','ECE_USER' : 'TSMM', 'ECE_PWD':'8AKjwWXiWAFTxb2UM3pZ', 'ECE_SERVER' : 'http://internal.publishing.production.rsi.ch/webservice/escenic/content/','ECE_BINARY' : 'http://internal.publishing.production.rsi.ch/webservice/escenic/binary',  'ECE_SECTION' : 'http://internal.publishing.production.rsi.ch/webservice/escenic/section/','UPDATE_FILE' : '/home/perucccl/APICoreXPROD/_cambiamento_','ARCHIVE_NAME' : '/home/perucccl/APICoreXPROD/Resources/_LivestreamingArchive_','LOCK_RESOURCE_DIR':'/home/perucccl/Webservices/PRODUCTION/cleanMultipleImport/Resources/','DB_NAME' : '/home/perucccl/Webservices/PRODUCTION/cleanMultipleImport/Resources/_cleanMultipleImportServiceDb_', 'http_proxy': 'http://gateway.zscloud.net:10268', 'LESSOPEN': '||/usr/bin/lesspipe.sh %s', 'SSH_CLIENT': '146.159.126.207 57239 22', 'SELINUX_USE_CURRENT_RANGE': '', 'L OGNAME': 'perucccl', 'USER': 'perucccl', 'HOME': '/home/perucccl', 'PATH': '/usr/local/bin:/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/sbin:/home/perucccl/bin', 'LANG': 'en_US.UTF-8', 'TERM': 'xterm', 'SHELL': '/bin/bash', 'SHLVL': '1', 'G_BROKEN_FILENAMES': '1', 'HISTSIZE' : '1000', 'https_proxy': 'http://gateway.zscloud.net:10268', 'SELINUX_ROLE_REQUESTED': '', '_': '/usr/bin/python', 'SSH_CONNECTION': '146.159.126.207 57239 10.72.112.35 22', 'SSH_TTY': '/dev/pts/1', 'HOSTNAME': 'rsis-prod-web1.media.int', 'SELINUX_LEVEL_REQUESTED': '', ' HISTCONTROL': 'ignoredups', 'no_proxy': 'rsis-zp-mam-blackhole,rsis-zp-mongo1,localhost,127.0.0.1,.media.int,rsis-tifone-t1,rsis-tifone-t2,rsis-tifone-t,rsis-tifone-01,rsis-tifone-02,.rsi.ch,10.102.7.38:8180,10.101.8.27:8180,.twitter.com', 'MAIL': '/var/spool/mail/perucccl', 'LS_COLORS': 'rs=0:di=01;34:ln=0 1;36:mh=00:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:mi=01;05;37;41:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=34;42:st=37;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arj=01;31:*.taz=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.zip=01;31:*.z=01;31: *.Z=01;31:*.dz=01;31:*.gz=01;31:*.lz=01;31:*.xz=01;31:*.bz2=01;31:*.tbz=01;31:*.tbz2=01;31:*.bz=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.rar=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.jpg=01;35:*.jpeg=01;35:*.gif=01;35:*.bmp=01;35:*.  pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob =01;35:*.qt=01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.axv=01;35:*.anx=01;35:*.ogv=01;35:*.ogx=01;35:*.aac=01;36:*.a u=01;36:*.flac=01;36:*.mid=01;36:*.midi=01;36:*.mka=01;36:*.mp3=01;36:*.mpc=01;36:*.ogg=01;36:*.ra=01;36:*.wav=01;36:*.axa=01;36:*.oga=01;36:*.spx=01;36:*.xspf=01;36:'}
TEST_Environment_Variables = {'MAM_SERVER':'http://rsis-bs-mam-blackhole/mam-video-api/','ECE_MODEL':'http://internal.publishing.staging.rsi.ch/webservice/publication/rsi/escenic/model/','IMPORT_SECTION': '9718','AUDIO_CREATE_URL':'http://internal.publishing.staging.rsi.ch/webservice/escenic/section/__CREATE_SECTION__/content-items', 'AUDIO_RESOURCE_TEMPLATE':'/home/perucccl/Webservices/STAGING/cleanMultipleImport/Resources/template_mamtranscodableaudio.xml','LOCK_URL' : 'http://internal.publishing.production.rsi.ch/webservice/escenic/lock/article/','LOCK_ID' : '11868353' ,'REST_POST_URL': 'http://publishing.production.rsi.ch/rsi-api/intlay/mockup/importkeyframe/keyframes.json','REST_GET_URL': 'http://publishing.production.rsi.ch/rsi-api/intlay/mockup/importkeyframe/keyframes.json', 'IMPORT_ARCHIVE_DIR' : '/mnt/rsi_transcoded/import/digas/imported/archived/',  'IMPORT_DIR' : '/mnt/rsi_transcoded/import/digas/imported/','VERSION' : '3.2','ECE_USER' : 'TSMM', 'ECE_PWD':'8AKjwWXiWAFTxb2UM3pZ', 'ECE_SERVER' : 'http://internal.publishing.production.rsi.ch/webservice/escenic/content/','ECE_BINARY' : 'http://internal.publishing.staging.rsi.ch/webservice/escenic/binary', 'ECE_SECTION' : 'http://internal.publishing.production.rsi.ch/webservice/escenic/section/', 'UPDATE_FILE' : '/home/perucccl/STAGING/APICoreX/_cambiamento_','ARCHIVE_NAME' : '/home/perucccl/STAGING/APICoreX/Resources/_LivestreamingArchive_','LOCK_RESOURCE_DIR':'/home/perucccl/Webservices/STAGING/cleanMultipleImport/Resources/','DB_NAME' : '/home/perucccl/Webservices/STAGING/cleanMultipleImport/Resources/_cleanMultipleImportDb_', 'http_proxy': 'http://gateway.zscloud.net:10268', 'LESSOPEN': '||/usr/bin/lesspipe.sh %s', 'SSH_CLIENT': '146.159.126.207 57239 22', 'SELINUX_USE_CUR RENT_RANGE': '', 'LOGNAME': 'perucccl', 'USER': 'perucccl', 'HOME': '/home/perucccl', 'PATH': '/usr/local/bin:/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/sbin:/home/perucccl/bin', 'LANG': 'en_US.UTF-8', 'TERM': 'xterm', 'SHELL': '/bin/bash', 'SHLVL': '1', 'G_BROKEN_FILENAME S': '1', 'HISTSIZE': '1000', 'https_proxy': 'http://gateway.zscloud.net:10268', 'SELINUX_ROLE_REQUESTED': '', '_': '/usr/bin/python', 'SSH_CONNECTION': '146.159.126.207 57239 10.72.112.35 22', 'SSH_TTY': '/dev/pts/1', 'HOSTNAME': 'rsis-prod-web1.media.int', 'SELINUX_LEVE L_REQUESTED': '', 'HISTCONTROL': 'ignoredups', 'no_proxy': 'rsis-bs-mam-blackhole*,rsis-zp-mongo1,localhost,127.0.0.1,.media.int,rsis-tifone-t1,rsis-tifone-t2,rsis-tifone-t,.rsi.ch,10.102.7.38:8180,10.101.8.27:8180,.twitter.com', 'MAIL': '/var/spool/mail/perucccl', 'LS_COLORS': 'rs=0:di=01;34:ln=01;36:mh=00: pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:mi=01;05;37;41:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=34;42:st=37;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arj=01;31:*.taz=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.zip=01;31:*.z=01;31:*.Z=01;31:* .dz=01;31:*.gz=01;31:*.lz=01;31:*.xz=01;31:*.bz2=01;31:*.tbz=01;31:*.tbz2=01;31:*.bz=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.rar=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.jpg=01;35:*.jpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:* .pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob=01;35:*.qt =01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.axv=01;35:*.anx=01;35:*.ogv=01;35:*.ogx=01;35:*.aac=01;36:*.au=01;36:*.f lac=01;36:*.mid=01;36:*.midi=01;36:*.mka=01;36:*.mp3=01;36:*.mpc=01;36:*.ogg=01;36:*.ra=01;36:*.wav=01;36:*.axa=01;36:*.oga=01;36:*.spx=01;36:*.xspf=01;36:'} 


LOGS_CONFIG_FILE = '/home/perucccl/Webservices/PRODUCTION/cleanMultipleImport/LOGS/conf.cleanMultipleImport_logging.conf'

LOGGING_FILE = ''
PROD_LOGGING_FILE = '/home/perucccl/Webservices/PRODUCTION/cleanMultipleImport/LOGS/cleanMultipleImport_PROD.log'
TEST_LOGGING_FILE = '/home/perucccl/Webservices/STAGING/cleanMultipleImport/LOGS/cleanMultipleImport_TEST.log'

if __name__ == "__main__":

        logger = logging.getLogger('pyAPICoreX')
        logger.debug('PRIMA DI SETTARE ENVIRONMENT')
        logger.debug('ENV : ')
        logger.debug(os.environ)

        #for param in os.environ.keys():
            #logger.debug("%20s %s" % (param,os.environ[param]))
            #dict_env[ param ] = os.environ[param]

        logger.debug('---------------------------------')

        logger.debug('verifico se la variabile _APICoreXEnv_ e settata')
        if '_APICoreXEnv_' in os.environ:
                if 'PRODUCTION' in os.environ['_APICoreXEnv_']:
                        logger.debug('setto ENV di PROD')
                else:
                        logger.debug('Variabile _APICoreXEnv_ su valore diverso da \'PRODUCTION\'')
                        logger.debug('setto ENV di TEST')

        else:
                logger.debug('setto ENV di TEST')


        exit(0)
        for param in Environment_Variables.keys():
            #logger.debug("%20s %s" % (param,dict_env[param]))
            os.environ[param] = Environment_Variables[ param ]


        logger.debug('DOPO AVER SETTATO ENVIRONMENT')
        logger.debug('ENV : ')
        logger.debug(os.environ)



