# -*- coding: utf-8 -*-


import logging
import logging.config

import os
import shutil
import json
import glob
import time
from pathlib import Path


import MAM.MamServices as MamServices
import ECE.XmlServices as XmlServices
import ECE.SolrServices as SolrServices
import Resources.pySettaVars as pySettaVars
import DBApi.API_DB as API_DB


# These two lines enable debugging at httplib level (requests->urllib3->http.client)
# You will see the REQUEST, including HEADERS and DATA, and RESPONSE with HEADERS but without DATA.
# The only thing missing will be the response.body which is not logged.
try:
    import http.client as http_client
except ImportError:
    # Python 2
    import httplib as http_client
http_client.HTTPConnection.debuglevel = 5
requests_log = logging.getLogger("requests.packages.urllib3")
requests_log.setLevel(logging.DEBUG)
requests_log.propagate = True


def SettaEnvironment( debug ):

	if (debug):
		print( 'PRIMA DI SETTARE ENVIRONMENT' )
	if (debug):
		print( 'ENV : ' )
	if (debug):
		print( os.environ )

	if (debug):
		print( '---------------------------------' )

	Environment_Variables = {}

	print( 'verifico il setting della variabile _cleanMultipleImportEnv_ ' )
	if '_cleanMultipleImportEnv_' in os.environ:
		if 'PRODUCTION' in os.environ['_cleanMultipleImportEnv_']:
			print( 'variabile _cleanMultipleImportEnv_ ha valore \'PRODUCTION\'' )
			print( 'setto ENV di PROD' )
			Environment_Variables = pySettaVars.PROD_Environment_Variables
			pySettaVars.LOGGING_FILE  = pySettaVars.PROD_LOGGING_FILE
		else:
			print( 'variabile _cleanMultipleImportEnv_ ha valore : ' + os.environ['_cleanMultipleImportEnv_'] )
			print( 'diverso da \'PRODUCTION\'' )
			print( 'setto ENV di TEST' )
			Environment_Variables = pySettaVars.TEST_Environment_Variables
			pySettaVars.LOGGING_FILE  = pySettaVars.TEST_LOGGING_FILE

	else:
		print( 'variabile _cleanMultipleImportEnv_ non trovata: setto ENV di TEST' )
		Environment_Variables = pySettaVars.TEST_Environment_Variables


	for param in Environment_Variables.keys():
		#print( "%20s %s" % (param,dict_env[param]) )
		os.environ[param] = Environment_Variables[ param ]


	if (debug):
		print( 'DOPO AVER SETTATO ENVIRONMENT' )
	if (debug):
		print( 'ENV : ' )
	if (debug):
		print( os.environ )

# questa e per settare le veriabili di ambiente altrimenti come cron non andiamo da nessuna parte
SettaEnvironment( False )

print( '\n\nCLAD  log configuration file : ' +  pySettaVars.LOGS_CONFIG_FILE + '\n' )
logging.config.fileConfig(pySettaVars.LOGS_CONFIG_FILE, disable_existing_loggers=False, defaults={'logfilename': pySettaVars.LOGGING_FILE})
logger = logging.getLogger('pyCleanMultipleImport')
print( ' logging file : \t\t' + logger.handlers[0].baseFilename + '\n' )
logger.debug( 'logger -> logging file : \t\t' + logger.handlers[0].baseFilename + '\n' )




def Prendi_Lista( workingDir ):

	result = {}
	
	lista_files = glob.glob( workingDir + '*.mp3' )
	lista_files = lista_files + glob.glob( workingDir + '*.MP3' )
	
	# dove trovo filenames del tipo : 07.30_R1SPORT_VOCE_LUSTRINELLI_08.09.20_OK.MP3
	result = lista_files

	return result



def Importa_Img( lista_immagini ):

	logger.debug('------------------------ INIT Importa_Img -------------- ')

	result = {}
	lista_rimaste = {}
	lista_archivio = []

	section = os.environ['IMPORT_SECTION']
	
	# dove trovo filenames del tipo : GP765997_P00000011_1.jpg
	# con ancora tutto il path
	# e a me interessano il primo campo = LegacyId
	# e il numero prima del . che rappresenta quanti ce ne sono per quel LegacyId

	for legacy, img in lista_immagini.iteritems():
		# in img ho la lista di file_path che passo per upload del binary
		for file_path in img:
			logger.debug( ' file path = ' + file_path )
		
			binary = pyPutContentInECE.UploadBinary( file_path )
			if not binary[0] :
				# non son riuscito a fare la load del file ....
				# continuo senza cambiare img con id
				if legacy in lista_rimaste:
					lista_rimaste[legacy].append(file_path)
				else:
					lista_rimaste[legacy] = [file_path]
				continue
				
			else:

				# ho fatto upload binary e in binary[1] ho la location url del binary
				# da passare alla creazione del content picture
				result_crea = pyPutContentInECE.Create_Img( binary[1] , legacy, section )
				
				if not result_crea[0]:
					# con quel binary non sono riuscito a costruire un asset immagine
					if legacy in lista_rimaste:
						lista_rimaste[legacy].append(file_path)
					else:
						lista_rimaste[legacy] = [file_path]
					continue
				else:
					# ho creato img ed e andato tutto bene
					if legacy in result:
						result[legacy].append(result_crea[1])
					else:
						result[legacy] = [ result_crea[1] ]
				
					# e andato tutto bene posso spostarla nella directory di archiviazione
					# e la muovo in archived
					Sposta_Img( file_path )

	logger.debug( ' result = ' + str(result))
	logger.debug( ' rimaste = ' + str(lista_rimaste))
	logger.debug('------------------------ END Importa_Img -------------- ')

	return [ result , lista_rimaste ]

def spostaAudio(  file_path ):
	
	logger.debug('------------------------ INIT spostaAudio -------------- ')
	try:
		file_name = os.path.basename( file_path )
		logger.debug( "mv " + file_path + " to -> " + os.environ['IMPORT_ARCHIVE_DIR'] + file_name )
		shutil.move( file_path,  os.environ['IMPORT_ARCHIVE_DIR'] + file_name )
		
	except Exception as e:
		logger.error('ERROR: EXCEPT in spostaAudio  = ' + str(e))
		logger.error('ERROR: EXCEPT in spostaAudio per immagine = ' + file_path)
		pass

	logger.debug('------------------------ END spostaAudio -------------- ')

def prendiSize( fileName ):
	result = -1

	try:
		result = Path( fileName ).stat().st_size
	
	except Exception as e:
		logger.error('ERROR: EXCEPT in prendiSize  = ' + str(e))
		logger.error('ERROR: EXCEPT in prendiSize per audio = ' + fileName)
		return -1

	return result

def verificaDimensioneAudio( lista_audio, items_da_db ):
	
	result = {}
	tmpDb = {}

	try:
		for lis in lista_audio:
			# ne prendo la dimensione attuale
			sizeNow = prendiSize( lis )
			if lis not in items_da_db:
				# e' la prima volta che lo incontro ne scrivo la dimensione nel DB
				tmpDb[ lis ] = sizeNow
			else:
				# ho gia' trovato questo nome e verifico la dimensione
				if sizeNow == items_da_db[ lis ]:
					# la dimensione e' uguale assumo che abbiano finito di scrivere
					# lo metto nella lista da importare e non lo metto piu' nel DB
					result[ lis ] = sizeNow
				else:
					# devo metterlo nel nuovo DB con la dimensione nuova
					tmpDb[ lis ] = sizeNow
	except Exception as e:
		logger.error('ERROR: EXCEPT in verificaDimensioneAudio  = ' + str(e))
		return [ {}, items_da_db ]

	return [ result, tmpDb ]

def prendiMamId( id ):
	result = ''

	try:
		[ success, assetTree ] = XmlServices.getEceId( id )
		if not success:
			logger.error('ERROR: EXCEPT in prendiMamId  = ' + str(e))
			return None
		# quindi ne prendo il field mamUrn
		valoreMamUrn = XmlServices.PrendiField( assetTree, 'mamUrn')
		print(valoreMamUrn)
		result = valoreMamUrn.split(':')[-1]

			
	except Exception as e:
		logger.error('ERROR: EXCEPT in prendiMamId  = ' + str(e))
		return None

	return result
	
def prendiMamId( id ):
	result = ''

	logger.debug( '------------------------ INIT prendiMamId -------------- : ' + id )
	try:
		[ success, assetTree ] = XmlServices.getEceId( id )
		if not success:
			logger.error('ERROR: EXCEPT in prendiMamId  = ' + str(e))
			return None
		# quindi ne prendo il field mamUrn
		valoreMamUrn = XmlServices.PrendiField( assetTree, 'mamUrn')
		print(valoreMamUrn)
		logger.debug(valoreMamUrn)
		result = valoreMamUrn.split(':')[-1]

			
	except Exception as e:
		logger.error('ERROR: EXCEPT in prendiMamId  = ' + str(e))
		return None

	logger.debug( '------------------------ END prendiMamId -------------- : ' + id )
	return result
	




def prendiListaMamId( listaAsset ):
	result = {}

	logger.debug( '------------------------ INIT prendiListaMamId -------------- : ' )
	try:
		count = 0
		for lis in listaAsset:
			# in lis ho un dict nella forma :  {"id": "article:13954168"}
			# da cui devo prendere il valore di id
			id = lis['id'].split(':')[-1]
			result[ id ] = prendiMamId( id )
			logger.debug(' count : ' + str(count) + ' - id -> ' + id )
			count = count + 1 
			
	except Exception as e:
		logger.error('ERROR: EXCEPT in prendiListaMamId  = ' + str(e))
		return {}


	logger.debug( '------------------------ END prendiListaMamId -------------- : ' )
	return result
	
def creaListaRelazionati( dictMamId ):
	resLegit = []
	resDelete = []
	resError = []
	logger.debug( '------------------------ INIT creaListaRelazionati -------------- : ' )

	try:
		for key,value in dictMamId.items():
			# in key ho ECE id 
			# in value la mamUrb da massare alle mam-video-apis
			id = key
			mamUrn = value
			# controllo se non aveva mamUrn
			if mamUrn is None:
				logger.warning('WARNINIG: trovato mamUrn == None per id ' + str(id))
				continue

			logger.debug('per ECEid = '+ str(id) + ' che ha mamUrn = ' + str(mamUrn))
			cmsDb = MamServices.getCmsUrnFromRundown( mamUrn )
			if 'error' in cmsDb:
				resError.append( id )
				logger.debug('appendo a Error : ' + id )
			logger.debug(cmsDb)
			if cmsDb in id:
				resLegit.append( id )
				logger.debug('appendo a Legit : ' + id )
			else:
				resDelete.append( id )
				logger.debug('appendo a Delete : ' + id )
			
	except Exception as e:
		logger.error('ERROR: EXCEPT in creaListaRelazionati  = ' + str(e))
		return [ [], [], [] ]

	logger.debug( '------------------------ END creaListaRelazionati -------------- : ' )

	return [ resLegit, resDelete, resError ]

if __name__ == "__main__":

	print( '-\n' )
	#print( os.environ )

	# questa e per settare le veriabili di ambiente altrimenti come cron non andiamo da nessuna parte
	SettaEnvironment( False )

	print( '\n\n log configuration file : ' +  pySettaVars.LOGS_CONFIG_FILE + '\n' )
	logging.config.fileConfig(pySettaVars.LOGS_CONFIG_FILE, disable_existing_loggers=False, defaults={'logfilename': pySettaVars.LOGGING_FILE})
	logger = logging.getLogger('pyCleanMultipleImport')
	print( ' logging file : \t\t' + logger.handlers[0].baseFilename + '\n' )
	logger.debug( ' logging file : \t\t' + logger.handlers[0].baseFilename + '\n' )

	'''
	lista_merge =  {'werwer': ['333333'] , 'GP765666': ['11892743']}
	lista_old =  {'GP765666': ['11866666', '123123123'], 'werwer' : ['11866666', 'wefwerwe']}
	Merge_Liste(lista_merge, lista_old )
	exit(0)
	print( os.environ['REST_POST_URL']  )
	print( GetRestJson.PostJson( lista_merge, os.environ['REST_POST_URL'] ) )
	exit(0)

	img_result = [ False, '' ]
	img_result = pyPutContentInECE.UploadBinary( '/mnt/rsi_import/keyframe_traffic/archived/CP966669_P00000011_1.jpg')
	if not img_result[0]:
		logger.error(' Non riesco ad uploadare la immagine: esco e lavoro la prossima volta ')
		exit(0)
	#img_bin = 'http://internal.publishing.production.rsi.ch/webservice/escenic/binary/-1/2019/6/18/15/6cc0413d-3d50-4453-b551-359afe397d5a.bin'

	crea_result = pyPutContentInECE.Create_Img( img_result[-1], 'titolo della Immagine', '5909' )
	print( crea_result )
	exit(0)

	'''
	logger.debug( '------------ INIT ---------- pyCleanMultipleImport.py ---------------------' )

	
	'''
	# devo prendere le cose di tipo 
	type = 'mamProgramme'
	# pubblicate dal giorno tot
	zuluDateFrom = '2021-04-02T00:00:00Z'
	# pubblicate al giorno tot
	zuluDateTo = '2021-04-03T00:00:00Z'
	# nella sezione root
	sectionId = '4'
	listaAsset = []
	# chiamando i servizi Solr
	listaAsset = SolrServices.solrPrendiContent( sectionId, type, zuluDateFrom, zuluDateTo )
	with open('listaAsset.txt', 'w') as filehandle:
	    json.dump(listaAsset, filehandle)
	'''
	
	
	with open('listaAsset.txt', 'r') as filehandle:
	    listaAsset = json.load(filehandle)

	logger.debug( 'listaAsset = ' + str(len(listaAsset)))

	'''
	#listaAsset = [ {"id": "article:13954012"}]
	# per ogni asset nella lista ne devo  prendere il mam_id
	# e la metto in un dict
	dictMamId = {}
	dictMamId = prendiListaMamId( listaAsset )
	with open('listadictMamId.txt', 'w') as filehandle:
	    json.dump(dictMamId, filehandle)

	'''
	with open('listadictMamId.txt', 'r') as filehandle:
	    dictMamId = json.load(filehandle)

	logger.debug( 'listadictMamId = ' + str(len(dictMamId)))
	# qui adesso ho un dict con id:mamUrn
	# e devo chiamare la mam-video API per vedere che ECE id corrisponde a quella mamUrn nel DB
	listaLegit = []
	listaDelete = []
	listaError = []
	'''
	[ listaLegit , listaDelete, listaError ]= creaListaRelazionati( dictMamId )
	print( listaLegit, listaDelete, listaError )
	with open('listaLegit.txt', 'w') as filehandle:
	    json.dump(listaLegit, filehandle)
	with open('listaDelete.txt', 'w') as filehandle:
	    json.dump(listaDelete, filehandle)
	with open('listaError.txt', 'w') as filehandle:
	    json.dump(listaError, filehandle)
	with open('listaLegit.txt', 'r') as filehandle:
	    listaLegit = json.load( filehandle)
	logger.debug( 'listaLegit = ' + str(len(listaLegit)))
	with open('listaDelete.txt', 'r') as filehandle:
	    listaDelete = json.load(filehandle)
	logger.debug( 'listaDelete = ' + str(len(listaDelete)))
	with open('listaError.txt', 'r') as filehandle:
	    listaError = json.load(filehandle)
	logger.debug( 'listaError = ' + str(len(listaError)))
	'''
	
	with open('listaEliminare.txt', 'r') as filehandle:
	    listaEliminare = json.load(filehandle)
	logger.debug( 'listaEliminare = ' + str(len(listaEliminare)))
	
	count = 0
	for lis in sorted(listaEliminare):
		count = count + 1
		print( '- ' + str(count) + ' - ' + 'cancello lis : ' + lis )
		SolrServices.deleteId( lis ) 
		time.sleep(0.75)
	
	logger.debug( '------------ END ---------- pyCleqanMultipleImport.py ---------------------' )
