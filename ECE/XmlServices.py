import os 

# -*- coding: utf-8 -*-

import logging
import urllib.request, urllib.error, urllib.parse
import base64
from xml.dom import minidom
import xml.etree.ElementTree as ET
import codecs
import operator
import os.path
import stat
import shutil
import glob
import json
from datetime import datetime, timedelta
import ast
import os
import copy
from http.client import HTTPSConnection
from base64 import b64encode
import sys
import re

# definizione dei namespaces per parsare gli atom
namespaces = { 'atom':'{http://www.w3.org/2005/Atom}',
	       'dcterms' : '{http://purl.org/dc/terms/}',
		'mam' : '{http://www.vizrt.com/2010/mam}',
		'age' : '{http://purl.org/atompub/age/1.0}',
		'opensearch' : '{http://a9.com/-/spec/opensearch/1.1/}',
		'vaext' : '{http://www.vizrt.com/atom-ext}',
		'app' : '{http://www.w3.org/2007/app}',
		'vdf' : '{http://www.vizrt.com/types}',
		'metadata' : '{http://xmlns.escenic.com/2010/atom-metadata}',
		#'': '{http://www.w3.org/1999/xhtml}',
		'playout' : '{http://ns.vizrt.com/ardome/playout}' }

_UPDATE_FUNC_ = 0
_CREATE_FUNC_ = 1
_SISTEMA_FUNC_ = 2

# importa il pySettaVars
import Resources.pySettaVars as pySettaVars

logger = logging.getLogger('pyECE')

for entry in namespaces:
	#logger.debug(entry, namespaces[entry])
	ET._namespace_map[namespaces[entry].replace('{','').replace('}','')] = entry

def Dump_Link ( link, filename ):

	auth = '%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])
	base64string = b64encode(auth.encode())
	base64string = base64string.decode("ascii")
	headers = { 'Authorization' : 'Basic %s' %  base64string }

	print (link )
	print (filename)

	try:
		request = urllib.request.Request(link, headers=headers)
		resultResponse = urllib.request.urlopen(request)
		xml = minidom.parse(resultResponse)
		fout = codecs.open(filename, 'w', 'utf-8')
		fout.write( xml.toxml() )
		fout.close()

	except Exception as e:
		print ( 'PROBLEMI in Dump_Link ' + str(e) )
		pass 
	return

def Dump_ET_Link ( idStr, filename ):

	auth = '%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])
	base64string = b64encode(auth.encode())
	base64string = base64string.decode("ascii")
	headers = { 'Authorization' : 'Basic %s' %  base64string }

	link = os.environ['ECE_SERVER']  + str(idStr)

	print (idStr )
	print (link )
	print (filename)
	try:
		request = urllib.request.Request(link, headers=headers)
		resultResponse = urllib.request.urlopen(request)
		tree = ET.parse(resultResponse)
		#print ( ET.tostring( tree.getroot() ))
		tree.write(filename)

	except Exception as e:
		print ( 'PROBLEMI in Dump_ET_Link : ' + str(e) )
		pass

	return


def Put_Link( link, filename ):
	
	logger.debug('------------------------ inizia Put_Link -------------- ')

	auth = '%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])
	base64string = b64encode(auth.encode())
	base64string = base64string.decode("ascii")
	headers = { 'Authorization' : 'Basic %s' %  base64string }
	file = open(filename)
	dati = file.read()

	request = urllib.request.Request(link, data=dati)

	request.add_header("Authorization", "Basic %s" % base64string)
	request.add_header('If-Match', '*')
	request.add_header('Content-Type', 'application/atom+xml')
	request.get_method = lambda: 'PUT'
	result = urllib.request.urlopen(request)

	#logger.debug(result.read())
	return
	xml = minidom.parse(result)
	fout = codecs.open(filename, 'w', 'utf-8')
	fout.write( xml.toxml() )
	fout.close()


	return

def Put_IdProd( idx, filename ):

	logger.debug('------------------------ inizia Put_Id -------------- ')
	try:
		auth = '%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])
		base64string = b64encode(auth.encode())
		base64string = base64string.decode("ascii")
		headers = { 'Authorization' : 'Basic %s' %  base64string }

		file = open(filename)
		dati = file.read()
		link = os.environ['ECE_SERVER']  + str(idx)

		prod_ece = 'http://internal.publishing.production.rsi.ch/webservice/escenic/content/'
		link = prod_ece  + str(idx)
		request = urllib.request.Request(link, data=dati)

		request.add_header("Authorization", "Basic %s" % base64string)
		request.add_header('If-Match', '*')
		request.add_header('Content-Type', 'application/atom+xml')
		request.get_method = lambda: 'PUT'
		result = urllib.request.urlopen(request)

		logger.debug('Put_Id' )
		logger.debug(result.read())
		
	
	except Exception as e:
		print ( 'PROBLEMI in Put_Id : ' + str(e) )
		return False

	logger.debug('------------------------ finisce Put_Id -------------- ')
	return True




def Put_Id( idx, filename ):

	print('------------------------ inizia Put_Id -------------- ')
	try:
		auth = '%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])
		base64string = b64encode(auth.encode())
		base64string = base64string.decode("ascii")

		headers = { 'Authorization' : 'Basic %s' % base64string,
				'If-Match': '*',
				'Content-Type': 'application/atom+xml'
			}    

		file = open(filename)
		dati = file.read()
		dati = ( dati ).encode()
		link = os.environ['ECE_SERVER']  + str(idx)
		print ( link )

		request = urllib.request.Request(url=link, data=dati, headers=headers, method='PUT')
		resultResponse = urllib.request.urlopen(request)

		print ( resultResponse.status )
		print ( resultResponse.headers )
		#print ( resultResponse.headers.split('rsi.ch/webservice/escenic/content/' )[-1] )
		print ( resultResponse.reason )
		
	
	except Exception as e:
		print ( 'PROBLEMI in Put_Id : ' + str(e) )
		return False

	logger.debug('------------------------ finisce Put_Id -------------- ')
	return True





#Dump_Link("http://internal.publishing.production.rsi.ch/webservice/escenic/content/8709154", '8709154.xml')
#Put_Link("http://internal.publishing.production.rsi.ch/webservice/escenic/content/8568391", '../_cambiamento_')
#Dump_Link("http://internal.publishing.production.rsi.ch/webservice/escenic/content/8568391", '8568391.xml')
#Dump_Link("http://internal.publishing.production.rsi.ch/webservice/escenic/content/8730301", '8730301.xml')
#Dump_Link("http://internal.publishing.production.rsi.ch/webservice/escenic/content/8675248", 'gallery_8675248.xml')
#Dump_Link("http://internal.publishing.production.rsi.ch/webservice/escenic/content/8948557", 'gallery_8948557.xml')
#Dump_Link("http://internal.publishing.production.rsi.ch/webservice/escenic/content/9013267", '9013267.xml')
#Dump_ET_Link('8845033', '8845033_et.xml')
#exit(0)

#Dump_Link("http://internal.publishing.production.rsi.ch/webservice/escenic/section/22742", 'jonas_section')
#Dump_Link("http://localhost/webservice/escenic/section/ROOT/subsections", 'jonas_1')
#exit(0)
#Dump_Link("http://internal.publishing.production.rsi.ch/webservice/escenic/section/7963/subsections", 'jonas_tvs')
#Dump_Link("http://internal.publishing.production.rsi.ch/webservice/escenic/section/8599/subsections", 'jonas_tvsplayer')
#Dump_Link("http://internal.publishing.production.rsi.ch/webservice/escenic/section/4/subsections", 'jonas_rsi')


def Dump_Rows ( link, filename ):

	logger.debug(' in Dump Rows ')

	#base64string = base64.encodestring('%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD']45')).replace('\n', '')
	base64string = base64.encodestring('%s:%s' % ('admin', 'admin')).replace('\n', '')

	request = urllib.request.Request(link)
	request.add_header("Authorization", "Basic %s" % base64string)
	result = urllib.request.urlopen(request)
	
	tree = ET.parse(result)


	totresult = int(tree.find( namespaces['opensearch'] + 'totalResults').text)
	items_per_page = int(tree.find( namespaces['opensearch'] + 'itemsPerPage').text)

	logger.debug('  per le ROWS : totalResults %s itemsPerPage %s ' % (totresult , items_per_page))

	request = urllib.request.Request(link)
	request.add_header("Authorization", "Basic %s" % base64string)
	result_xml = urllib.request.urlopen(request)
	xml = minidom.parse(result_xml)
	fout = codecs.open(filename, 'a', 'utf-8')
	fout.write( xml.toxml() )
	if  totresult > items_per_page:
		#logger.debug(' giro sui next e prev ')
		# devi  girare sui next per prendere gli altri
		for x in range(int(float(totresult)/float(items_per_page))):
			link_next = PrendiLink(tree, 'next')
			#logger.debug(link_next)
			request = urllib.request.Request(link_next )

			request.add_header("Authorization", "Basic %s" % base64string)
			result = urllib.request.urlopen(request)
			tree = ET.parse( result )
			request = urllib.request.Request(link_next)
			request.add_header("Authorization", "Basic %s" % base64string)
			result_xml = urllib.request.urlopen(request)
			xml = minidom.parse(result_xml)
			fout = codecs.open(filename, 'a', 'utf-8')
			fout.write( xml.toxml() )
			fout.close()

	exit(0)

	return

def PrendiState( entry , rel):

	list =  entry.findall(namespaces['app'] + "control/" + namespaces['vaext'] + 'state')
	
	for idx , lis in enumerate(list):
		#logger.debug(idx, lis)
		#logger.debug(lis.attrib['name'])
		return lis.attrib['name']
	return None

def PrendiEdited_Time( entry , rel):

	list =  entry.findall(namespaces['app'] + "edited/" )
	
	#logger.debug(list)

	for idx , lis in enumerate(list):
		#logger.debug(idx, lis.text)
		#logger.debug(lis.attrib['name'])
		return lis.text

	return None



def PrendiSezione( entry , rel):

	list =  entry.findall(namespaces['atom'] + "link")
	
	#logger.debug(list)

	for idx , lis in enumerate(list):
		#logger.debug(idx, lis)
		#logger.debug(lis.attrib['rel'])
		if rel in lis.attrib['rel']:
			#logger.debug(lis.attrib['href'])
			#logger.debug(lis.attrib['title'])
			return lis.attrib['title']
	return None

def PrendiTipo( entry ):

	list =  entry.findall(namespaces['atom'] + "content")
	for lis in list:
		#logger.debug(lis)
		payload =  lis.findall(namespaces['vdf'] + "payload")
		tipo = payload[0].attrib['model'].split('/')[-1]
		return tipo

	return None


def PrendiPayload( entry , rel):

	list =  entry.findall(namespaces['atom'] + "payload")
	
	logger.debug(len(list))

	for idx , lis in enumerate(list):
		logger.debug(idx, lis)
		logger.debug(lis.attrib['rel'])
		if rel in lis.attrib['rel']:
			logger.debug(lis.attrib['href'])
			logger.debug(lis.attrib['title'])
			#return lis.attrib['href']
	return None

def prendiGroup( entry , rel):

	result = []
	for idx , lis in enumerate(list):
		#logger.debug(idx, lis)
		#logger.debug(lis.attrib['rel'])
		if rel in lis.attrib['rel']:
			result.append( lis )
	return result

def prendiListaRelated( entry , rel):

	result = []
	list =  entry.findall(namespaces['atom'] + "link")
	for idx , lis in enumerate(list):
		#logger.debug(idx, lis)
		#logger.debug(lis.attrib['rel'])
		if rel in lis.attrib['rel']:
			result.append( lis )
	return result



def prendiPVideoIdDaRel( listaRel ):

	#print (lis)
	for entry in listaRel:
		payload =  entry.findall(namespaces['vdf'] + "payload")
		for pay in payload:
			if 'mamProgrammeVideo' in pay.attrib['model']:
				#print('CLAD2')
				fields = pay.findall(namespaces['vdf'] + "field")
				#print (entry.attrib[namespaces['dcterms'] + 'identifier'])
				#print (entry.attrib['href'])
				return entry.attrib[namespaces['dcterms'] + 'identifier']

	return ''

def ordinaEditorial( listaRel ):
	
	print ( '------------------------ INIT ordinaEditorial -------------- ' )
	# questa ritorna una stringa con la lista delle related ordinata
	# per il valore che trova in custom
	order = {}
	result = ''
	count  = 0
	primo = True
	for res in listaRel:
		# in res ho la relazione  
		val = res.get(namespaces['metadata'] +'group')
		if 'EDITORIALKEYFRAMES' in val  and primo:
			print  (ET.tostring(res))
			order[ count ] = listaRel[-1]
			primo = False
		else:
			print  (ET.tostring(res))
			order[ count ] = res
		count += 1

	print ( order ) 
	for i in sorted (order.keys()) :  
		#print(i)
		print  (ET.tostring(order[ i ] ))
		#exit(0)
		result = result + str(ET.tostring(order[ i ]), 'utf-8' )

	print ( '------------------------ END ordinaEditorial -------------- ' )
	return result
		

def aggiungiEditorialList( entry,listaRel ):
	
	print ( '------------------------ INIT aggiungiEditorialList -------------- ' )
	# questa ritorna una stringa con la lista delle related ordinata
	# per il valore che trova in custom
	order = {}
	result = ''
	count  = 0
	for res in listaRel[:-1]:
		# in res ho la relazione  
		val = res.get(namespaces['metadata'] +'group')
		if 'EDITORIALKEYFRAMES' in val:
			listaRel.pop( count )
		count +=1

	for rel in listaRel:
		#print(i)
		print  (ET.tostring(rel ))
		#exit(0)
		entry.append(rel)

	print ( '------------------------ END aggiungiEditorialList -------------- ' )
	return entry
		

def ordinaListaRelated( listaRel ):
	
	print ( '------------------------ INIT ordinaListaRelated -------------- ' )
	# questa ritorna una stringa con la lista delle related ordinata
	# per il valore che trova in custom
	order = {}
	result = ''
	for res in listaRel:
		# in res ho la relazione 
		#print ( prendiFieldCustom( res, 'custom' ) )
		order[ int(prendiFieldCustom( res, 'custom' )) ] = res

	#print ( order ) 
	for i in sorted (order.keys()) :  
		#print(i)
		#print  (ET.tostring(order[ i ] ))
		#exit(0)
		result = result + str(ET.tostring(order[ i ]), 'utf-8' )

	print ( '------------------------ END ordinaListaRelated -------------- ' )
	return result
		
def rimuoviListaRelated( entry , rel):
	print ( '------------------------ INIT rimuoviListaRelated -------------- ' )
	# questa rimuove dal xml la lista delle relazioni 

	list =  entry.findall(namespaces['atom'] + "link")
	for idx,lis in enumerate(list):
		print(idx, lis)
		#logger.debug(lis.attrib['rel'])
		if rel in lis.attrib['rel']:
			print('rimuovo qualcosa')
			entry.remove( lis )
	print ( '------------------------ END rimuoviListaRelated -------------- ' )
	return entry
	
def aggiungiListaRelated( entry , listaRel):
	print ( '------------------------ INIT aggiungiListaRelated -------------- ' )
	
	order = {}
	result = ''
	for res in listaRel:
		# in res ho la relazione 
		#print ( prendiFieldCustom( res, 'custom' ) )
		order[ int(prendiFieldCustom( res, 'custom' )) ] = res

	#print ( order ) 
	for i in sorted (order.keys()) :  
		#print(i) 
		#print  (ET.tostring(order[ i ] ))
		#exit(0)
		entry.append(order[ i ])

	print ( '------------------------ END aggiungiListaRelated -------------- ' )
	return entry
	

def sistemaOffsetAndPVId( listaRel , startTimeInMs, pvId):
	print ( '------------------------ INIT sistemaOffsetAndPVId -------------- ' )
	# questa deve sistemare il campo tc_offset di tutti quelli nella
	# lista related

	for res in listaRel:
		# in res ho la relazione 
		eceId = prendiSegmentIdDaRel( res )
		print( eceId)
		# e su questo eceId vado a sistemare il tc_offset
		if len(eceId) > 0:
			sistemaTcOffsetAndPVId( eceId, startTimeInMs, pvId) 
		

	print ( '------------------------ END sistemaOffsetAndPVId -------------- ' )
	return entry
	


def PrendiLinkRelated( entry , rel):

	list =  entry.findall(namespaces['atom'] + "link")
	
	#logger.debug(len(list))

	for idx , lis in enumerate(list):
		#logger.debug(idx, lis)
		#logger.debug(lis.attrib['rel'])
		if rel in lis.attrib['rel']:
			#logger.debug(lis.attrib['href'])
			#logger.debug(lis.attrib['title'])
			return lis
			
	return None


def PrendiLinkRelatedId( entry , rel):

	list =  entry.findall(namespaces['atom'] + "link")
	
	#logger.debug(len(list))

	for idx , lis in enumerate(list):
		#logger.debug(idx, lis)
		#logger.debug(lis.attrib['rel'])
		if rel in lis.attrib['rel']:
			#logger.debug(lis.attrib['href'])
			#logger.debug(lis.attrib['title'])
			return lis.attrib['href'].split('/')[-1]
			
	return None

def Trova_KeyFrame( Id_xx ):


	logger.debug(' ------------------- INIZIO Trova_KeyFrame ------------------------- ')
	
	base64string = base64.encodestring('%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])).replace('\n', '')

	link = os.environ['ECE_SERVER']  + str(Id_xx)

	request = urllib.request.Request(link)


	request.add_header("Authorization", "Basic %s" % base64string)
	try:
		result = urllib.request.urlopen(request)
	

		tree = ET.parse(result)

		id_keyframe =  PrendiLinkKeyframe( tree , 'related')




	except urllib.error.HTTPError as e:
		logger.debug(' EXCEPTIOOOONNNNNN - ritorno none !!!! ')
		return [ u'None' , u'None']

	logger.debug(' ------------------- FINE Trova_KeyFrame ------------------------- ')
	return id_keyframe

def PrendiLinkKeyframe( entry , rel):
	

	list =  entry.findall(namespaces['atom'] + "link")

        #logger.debug(len(list))
	id_keyframe = ''
	for idx , lis in enumerate(list):
		#logger.debug(idx, lis)
		#logger.debug(lis.attrib['rel'])
		if rel in lis.attrib['rel']:
			#for idx2, attr in enumerate(lis.attrib):
				#logger.debug(idx2, attr)
			logger.debug(lis.attrib[namespaces['metadata'] + 'group'])
			group =  lis.attrib[namespaces['metadata'] + 'group']
 
			if 'KEYFRAMES' in group:
				# ho trovato un keyframe
				_payload = lis.findall( namespaces['vdf'] + "payload")
				logger.debug(_payload[0].attrib['model'])
				if 'EDITORIAL' in group:
				
					logger.debug(lis.attrib['href'])
					logger.debug(lis.attrib['title'])
					#  se editorial ritorno con ID
					return lis.attrib['href'].split('/')[-1]
				else:
					# se normale me lo ricordo ma cerco editorial
					id_keyframe = lis.attrib['href'].split('/')[-1] 

	return id_keyframe


def PrendiLinkLeadId( entry , rel):
	
	# al momento prende sempre e solo la prima
	# e assume che sia una immagine

	list =  entry.findall(namespaces['atom'] + "link")

	#logger.debug(len(list))

	for idx , lis in enumerate(list):
		#logger.debug(idx, lis)
		#logger.debug(lis.attrib['rel'])
		if rel in lis.attrib['rel']:
			#for idx2, attr in enumerate(lis.attrib):
				#logger.debug(idx2, attr)
			#logger.debug(lis.attrib[namespaces['metadata'] + 'group'])
			group =  lis.attrib[namespaces['metadata'] + 'group']
		if 'lead' in group:
				# ho trovato una relazione in lead
				# devo verificare che sia una picture
				_payload = lis.findall( namespaces['vdf'] + "payload")
				#logger.debug(_payload[0].attrib['model'])
				if 'picture' in _payload[0].attrib['model']:
				
					#logger.debug(lis.attrib['href'])
					#logger.debug(lis.attrib['title'])
					# e qui ne prendo ID
					return lis.attrib['href'].split('/')[-1]
				else:
					# devo prendere la immagine del video
					# altrimenti il video stesso
					logger.debug('Trovato VIDEO ? ')
					# devo prendere l id della picture che mi interessa
					# poi dovro prendere il valore del video stesso e buttarlo
					# su
					id_keyframe = Trova_KeyFrame( lis.attrib['href'].split('/')[-1] )
					return id_keyframe

	return None


def PrendiLink( entry , rel):

	list =  entry.findall(namespaces['atom'] + "link")
	
	#logger.debug(len(list))

	for idx , lis in enumerate(list):
		#logger.debug(idx, lis)
		#logger.debug(lis.attrib['rel'])
		if rel in lis.attrib['rel']:
			#logger.debug(lis.attrib['href'])
			#logger.debug(lis.attrib['title'])
			
			return lis.attrib['href']
	return None

def prendiSegmentIdDaRel( entry ):

	#print (lis)
	payload =  entry.findall(namespaces['vdf'] + "payload")
	for pay in payload:
		if 'mamSegmentedProgrammeVideo' in pay.attrib['model']:
			#print('CLAD2')
			fields = pay.findall(namespaces['vdf'] + "field")
			#print (entry.attrib[namespaces['dcterms'] + 'identifier'])
			#print (entry.attrib['href'])
			return entry.attrib[namespaces['dcterms'] + 'identifier']

	return ''

def prendiFieldCustom( entry , rel):

	#print (lis)
	payload =  entry.findall(namespaces['vdf'] + "payload")
	for pay in payload:
		fields = pay.findall(namespaces['vdf'] + "field")
		#print (fields)

		for idx , fiel in enumerate(fields):
			#print(idx, fiel)
			#print(fiel.attrib['name'])
			if rel in fiel.attrib['name']:
				if not (fiel.find(namespaces['vdf'] + "value") is None) :
					return fiel.find(namespaces['vdf'] + "value").text
				else:
					return 0

	return 0

def prendiBodyVal( entry , rel):

	list =  entry.findall(namespaces['atom'] + "content")
	for lis in list:
		#print (lis)
		payload =  lis.findall(namespaces['vdf'] + "payload")
		for pay in payload:
			fields = pay.findall(namespaces['vdf'] + "field")
			#print (fields)
			for idx , fiel in enumerate(fields):
				#print(idx, fiel)
				#print(fiel.attrib['name'])
				
				if 'body' in fiel.attrib['name']:
					#print('trovato body')
					elem = fiel.find(namespaces['vdf'] + "value" )
					#print (elem)
					#print (ET.tostring( elem, encoding='unicode'))
					return ET.tostring( elem, encoding='unicode')

	return ''



def prendiField( entry , rel):

	list =  entry.findall(namespaces['atom'] + "content")
	for lis in list:
		#print (lis)
		payload =  lis.findall(namespaces['vdf'] + "payload")
		for pay in payload:
			fields = pay.findall(namespaces['vdf'] + "field")
			#print (fields)

			for idx , fiel in enumerate(fields):
				#print(idx, fiel)
				#print(fiel.attrib['name'])
				if rel in fiel.attrib['name']:
					if not (fiel.find(namespaces['vdf'] + "value") is None) :
						return fiel.find(namespaces['vdf'] + "value").text
					else:
						return ''

	return ''



def PrendiField( entry , rel):

	list =  entry.findall(namespaces['atom'] + "content")
	for lis in list:
		#print (lis)
		payload =  lis.findall(namespaces['vdf'] + "payload")
		for pay in payload:
			fields = pay.findall(namespaces['vdf'] + "field")
			#print (fields)

			for idx , fiel in enumerate(fields):
				#print(idx, fiel)
				#print(fiel.attrib['name'])
				if rel in fiel.attrib['name']:
					if not (fiel.find(namespaces['vdf'] + "value") is None) :
						return fiel.find(namespaces['vdf'] + "value").text
					else:
						return None

	return None

def PrendiFieldSocial( entry , rel):

	list =  entry.findall(namespaces['atom'] + "content")
	for lis in list:
		#logger.debug(lis)
		payload =  lis.findall(namespaces['vdf'] + "payload")
		for pay in payload:
			fields = pay.findall(namespaces['vdf'] + "field")
			#logger.debug(fields)

			for idx , fiel in enumerate(fields):
				#logger.debug(idx, fiel)
				#logger.debug(fiel.attrib['name'])
				if rel in fiel.attrib['name']:
					if not (fiel.find(namespaces['vdf'] + "value") is None) :
						return fiel.find(namespaces['vdf'] + "value").text
					else:
						return None

	return None




def processElem(elem):

	if elem.text is not None:
		logger.debug(elem.text)
	for child in elem:
		processElem(child)
		if child.tail is not None:
			logger.debug(' scrivo la coda ' )
			logger.debug(child.tail)


def PrendiFieldTest( entry , rel):

	list =  entry.findall(namespaces['atom'] + "content")
	for lis in list:
		#logger.debug(lis)
		payload =  lis.findall(namespaces['vdf'] + "payload")
		for pay in payload:
			fields = pay.findall(namespaces['vdf'] + "field")
			#logger.debug(fields)

			for idx , fiel in enumerate(fields):
				logger.debug(idx, fiel)
				logger.debug(fiel.attrib['name'])
				if rel in fiel.attrib['name']:
					logger.debug(' trovato ' + rel )
					if not (fiel.find(namespaces['vdf'] + "value") is None) :
						val =  fiel.find(namespaces['vdf'] + "value")
						processElem( val )
						return fiel.find(namespaces['vdf'] + "value").text
					else:
						return 'None'

	return 'None'


def RiempiListaStream( entry ):
	
	result = []

	logger = logging.getLogger('pySub_ECE')
	logger.debug(' ---------- INIT RiempiListaStream -------------- ')
	_list = entry.findall(namespaces['vdf'] + "list")
	logger.debug(_list)
	if len( _list ) > 0 :
		_list = _list[0]
	else:
		return []

	payload =  _list.findall(namespaces['vdf'] + "payload")
	for pay in payload:
		fields = pay.findall(namespaces['vdf'] + "field")
		#logger.debug(fields)

		for idx , fiel in enumerate(fields):
			#logger.debug(idx, fiel)
			#logger.debug(fiel.attrib['name'])
			if not (fiel.find(namespaces['vdf'] + "value") is None) :
				#logger.debug( fiel.find(namespaces['vdf'] + "value").text)
				result.append( fiel.find(namespaces['vdf'] + "value").text )

	#logger.debug(result)
	return result
	

def PrendiStream( entry, ref ):


	stream = ''

	list =  entry.findall(namespaces['atom'] + "content")
	for lis in list:
		#logger.debug(lis)
		payload =  lis.findall(namespaces['vdf'] + "payload")
		for pay in payload:
			fields = pay.findall(namespaces['vdf'] + "field")
			#logger.debug(fields)

			for idx , fiel in enumerate(fields):
				#logger.debug(idx, fiel)
				#logger.debug(fiel.attrib['name'])
				if ref in fiel.attrib['name'] :
					
					#logger.debug(' trovato attr stream ' )
					if not (fiel.find(namespaces['vdf'] + "value") is None) :
						#logger.debug(' trovato stream ' )
						stream =  fiel.find(namespaces['vdf'] + "value").text
						return stream
					else:
						#logger.debug("trovato stream Nullo"  )
						#logger.debug("lo setto sul valore 12"  )
						stream = "12"

	
	return stream

	
def PrendiListaStream( entry ,rel ):
	
	
	lista_stream = []

	list =  entry.findall(namespaces['atom'] + "content")
	for lis in list:
		#logger.debug(lis)
		payload =  lis.findall(namespaces['vdf'] + "payload")
		for pay in payload:
			fields = pay.findall(namespaces['vdf'] + "field")
			#logger.debug(fields)

			for idx , fiel in enumerate(fields):
				#logger.debug(idx, fiel)
				#logger.debug(fiel.attrib['name'])
				if rel in fiel.attrib['name'] and not 'audio' in fiel.attrib['name'] :
					

					lista_stream = RiempiListaStream( fiel )
					break
				
	#logger.debug(' lista stream ' )
	#logger.debug(lista_stream)

	return lista_stream


def PrendiContinousLive( entry ):

	logger = logging.getLogger('pySub_ECE')
	logger.debug(' ---------- INIT PrendiContinousLive -------------- ')

	continuous_live = 'false'
	continuous_live = PrendiField( entry, 'continuous_live')
	if continuous_live is None or 'false' in continuous_live:
		continuous_live = 'false'
	else:
		if 'true' in continuous_live:
			continuous_live = 'true'
		
	logger.debug(' continuous_live : ' + str(continuous_live))

	logger.debug(' ---------- FINE PrendiContinousLive -------------- ')
	return continuous_live

def PrendiMinNotifica( accountJson ):

	logger = logging.getLogger('pySub_ECE')
	logger.debug(' ---------- INIT PrendiMin Notifica -------------- ')
	# metto result come data tra un anno
	minData = datetime.now()  + timedelta( days=365 )
	result = ''

	platforms = ['fb' , 'yt' ]
	for plat in platforms:
		account = accountJson[ plat + '-livestreaming-account' ]
		if len( account ) < 1 :
			continue
		logger.debug( account )
		for key,valueUp in account.items():
			#print ( key, valueUp )
			value = valueUp['SocialDetails']
			if ( not 'notificato' in value or not value['notificato'] ) and  minData > datetime.strptime( value['notificaTime'],  '%Y-%m-%dT%H:%M:%SZ' ):
				minData =  datetime.strptime( value['notificaTime'] ,  '%Y-%m-%dT%H:%M:%SZ' )
				#print ( minData )
				result = value['notificaTime']
	return result



def SistemaEncoders( accountJson ):

	logger = logging.getLogger('pySub_ECE')
	logger.debug(' ---------- INIT Sistema_Encoder -------------- ')

	platforms = ['fb' , 'yt' ]
	for plat in platforms:
		account = accountJson[ plat + '-livestreaming-account' ]
		for key,valueUp in account.items():
			# print ( key, valueUp )
			value = valueUp['SocialDetails']

			if 'encoder' not in value:
				# sono LA1
				logger.debug('Incoming Stream : la1')
				value['encoder'] = '12'

			value['encoders'] = [ False, value['encoder'] ]

			if '12' in value['encoder']:
				# sono LA1
				logger.debug('Incoming Stream : la1')
				enco = 'la1'
			else:
				if '13' in value['encoder'] :
					# sono LA2
					logger.debug('Incoming Stream : la2')
					enco = 'la2'
				else:
					logger.debug('Incoming Stream : enc' + value['encoder'] )
					enco = 'enc' + value['encoder']
			
			value['incoming_stream'] = enco

			if ( 'useOtherEncoder' in value )  and ('true' in value['useOtherEncoder']):
				# sono usato encoder extra
				value['encoders'] = [ True, value['extraStream'] ]
				value['incoming_stream'] = 'custom'

		logger.debug(' ---------- FINE Sistema_Encoder -------------- ')

	return accountJson 




def PrendiEncoders( entry ):

	logger = logging.getLogger('pySub_ECE')
	logger.debug(' ---------- INIT Prendi_Encoder -------------- ')
	# la prendo comunque al massimo sara vuota
	stream = PrendiStream( entry , 'stream' )
	logger.debug( 'Stream : ' + stream )
	# e inizializzo la lista_extra
	lista_extra = []


	# verifico se sono usato encoder extra
	customEncoder = PrendiField( entry, 'useOtherEncoder')
	if customEncoder is None or 'false' in customEncoder:
		customEncoder = False
	else:
		if 'true' in customEncoder:
			customEncoder = True
		
	if customEncoder:
		logger.debug(' prendo gli encoder extra')
		lista_extra = PrendiExtraStream( entry )
		logger.debug(lista_extra)
		
		return [ True, lista_extra ]
		

	#logger.debug(' stream : ' + str(stream))
	#logger.debug(' lista_extra : ' + str(lista_extra))

	logger.debug(' ---------- FINE Prendi_Encoder -------------- ')
	return [ False, stream ] 


def PrendiTimeCtrl( entry ):

	
	activation = PrendiActivation(entry, ' activation_time')
	#logger.debug('activation ' + activation)
	published = PrendiPublished( entry, 'updated')
	#logger.debug('published ' + published)
	expires = PrendiExpires( entry, 'expiration' )
	#logger.debug('expires ' + expires)

	return [ activation, published, expires ]

def PrendiPublished( entry , name):

	list =  entry.findall(namespaces['atom'] + name )
	for lis in list:
		#logger.debug(lis.text)
		return lis.text

	return None




def PrendiExpires( entry , rel):

	list =  entry.findall(namespaces['age'] + "expires")
	for lis in list:
		#logger.debug(lis.text)
		return lis.text

	return None



def PrendiActivation( entry , rel):

	list =  entry.findall(namespaces['dcterms'] + "available")
	for lis in list:
		#logger.debug(lis.text)
		return lis.text

	return None


def CambiaBodyFile( filein ):

	print( '--------------- INIT CambiaBodyFile ---------------------')
	rimpiazza = {'<html:':'<', '</html:':'</','xmlns:html=':'xmlns=' }

	rep = dict((re.escape(k), v) for k, v in rimpiazza.items()) 
	pattern = re.compile("|".join(rep.keys()))

	linesOk = ""
	with open(filein) as infile:
		lines = infile.readlines()
		#print (lines )
		for line in lines:
			txtLine = pattern.sub(lambda m: rep[re.escape(m.group(0))], line )
			print(" ------ " +  txtLine )
		linesOk = linesOk + " " + txtLine

	
	print ( linesOk)

	infile.close()
	with open(filein, 'w') as outfile:
		for lineout in lines:
			outfile.write(lineout)	
	outfile.close()
	print( '--------------- END CambiaBodyFile ---------------------')
	

def CambiaState( entry , value):

	list =  entry.findall(namespaces['app'] + "control/" + namespaces['vaext'] + 'state')
	
	#logger.debug(' CambiaState ------ ')
	#logger.debug(list)

	for idx , lis in enumerate(list):
		#logger.debug(idx, lis)
		lis.text = value
		return entry

	#logger.debug(' CambiaState ------ ')
	return None



def CambiaField( entry , rel, value):

	list =  entry.findall(namespaces['atom'] + "content")
	for lis in list:
		payload =  lis.findall(namespaces['vdf'] + "payload")
		for pay in payload:
			fields = pay.findall(namespaces['vdf'] + "field")
			#logger.debug(fields)

			for idx , fiel in enumerate(fields):
				#logger.debug(idx, fiel)
				#logger.debug(fiel.attrib['name'])
				if rel in fiel.attrib['name']:
					if fiel.find(namespaces['vdf'] + "value") is None:
						fiel.append(ET.Element(namespaces['vdf'] + "value"))
					fiel.find(namespaces['vdf'] + "value").text = value
					#logger.debug(fiel.attrib['name'], fiel.find(namespaces['vdf'] + "value").text)
					return entry

	return entry

def Change_Item_Content( title, description, alttext, leadtext, tree ):


	if leadtext is not None:
		# allora devo riempirlo con il leadtext
		logger.debug("leadtext is not None = " + leadtext)
		CambiaField( tree, "alttext", leadtext )
		return tree
	#logger.debug("alttext vuoto")
	else:
		if description is not None:
			# allora devo riempirlo con la descrizione
			logger.debug("description is not None = " + description)
			CambiaField( tree, "alttext", description )
			return tree
		else:
			# allora devo riempirlo con il titolo
			logger.debug("riempio con il titolo = " + title)
			CambiaField( tree, "alttext", title )
			return tree

	return tree


def Get_Item_Content( Id_xx ):

	base64string = base64.encodestring('%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])).replace('\n', '')
	link = os.environ['ECE_SERVER']  + str(Id_xx)
	request = urllib.request.Request(link)
	request.add_header("Authorization", "Basic %s" % base64string)
	try:
		result = urllib.request.urlopen(request)
		tree = ET.parse(result)
		#tree.write('x.xml')
		#ET.dump(tree)
		tipo = PrendiTipo( tree )

		logger.debug(tipo)

		return

		if 'keyframe' in tipo:
			#se e' un keyframe si chiama name
			campo_titolo = "name"
		else:
			#altrimenti si chiama title
			campo_titolo = "title"
		title =  PrendiField( tree, campo_titolo)
		logger.debug(title)
		description =  PrendiField( tree, "description")
		logger.debug(description)
		alttext =  PrendiField( tree, "alttext")
		logger.debug(alttext)
		leadtext =  PrendiField( tree, "leadtext")
		logger.debug(leadtext)
		#programmeId =  PrendiField( tree, "programmeId")
		#logger.debug(programmeId)

		result.close()
	except urllib.error.HTTPError as e:
		logger.debug(' EXCEPTIOOOONNNNNN - ritorno none !!!! ')
		return [ u'None' , u'None', u'None']


	return [ title , description, alttext, leadtext, tree ]


def Prendi_Url( Id_xx ):
		
	base64string = base64.encodestring('%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])).replace('\n', '')

	link = os.environ['ECE_SERVER']  + str(Id_xx)

	request = urllib.request.Request(link)

	request.add_header("Authorization", "Basic %s" % base64string)
	try:
		result = urllib.request.urlopen(request)
	

		tree = ET.parse(result)
		#tree.write('prendi_URL_ok.xml')
		#ET.dump(tree)
		Url =  PrendiLink( tree, "alternate")
		#print ('URL = ' +  Url )

		result.close()
	except urllib.request.HTTPError as e:
		logger.debug(' EXCEPTIOOOONNNNNN - ritorno none !!!! ')
		return [ u'None' , u'None', u'None']
		
	
	return Url

def Check_Data_Range( adesso, Id_xx ):

	# questa server per verificare di essere nel range tra activation
	# e expiration 
	# verifico anche la publishing perche talvolta e piu alta della activation date

	[ activation, published, expires ] =  Prendi_Social_TimeCtrl(Id_xx)
	# logger.debug(activation, published, expires)

	data_limite = adesso
	active = False

	# logger.debug(' adesso = ' + str(adesso))

	# in caso non sia settata alcuna data di attivazione la Prendi_Social_TimeCtrl
	# mi torna None per il campo activate 
	if activation is None:
		# mettendo il seguente aassegnamento sono sicuro poi di confrontare
		# la data attuale con la data  di pubblicazione e dovrei passare sempre
		active = True
	else :
		data_limite = datetime.strptime( activation, '%Y-%m-%dT%X.000Z' )

	# logger.debug('data_limite = ' + str(data_limite))
	
	if adesso >= data_limite:	
		# logger.debug(' Data attuale nel Range ')
		return True
	
	# logger.debug(' Data attuale FUORI Range ')
	return False

	



def Prendi_Social_TimeCtrl( Id_xx ):
	
	flagDiRitorno = {}
		
	base64string = base64.encodestring('%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])).replace('\n', '')

	link = os.environ['ECE_SERVER']  + str(Id_xx)

	request = urllib.request.Request(link)


	request.add_header("Authorization", "Basic %s" % base64string)
	try:
		result = urllib.request.urlopen(request)
	

		tree = ET.parse(result)
		timeCtrl = PrendiTimeCtrl( tree )

		result.close()
	except urllib.error.HTTPError as e:
		logger.debug(' EXCEPTIOOOONNNNNN - ritorno none !!!! ')
		return [ None , None]
		
	
	return timeCtrl

def PrendiExtraStream( entry ):

	result = []
	list =  entry.findall(namespaces['atom'] + "content")
	for lis in list:
		#logger.debug(lis)
		payload =  lis.findall(namespaces['vdf'] + "payload")
		for pay in payload:
			fields = pay.findall(namespaces['vdf'] + "field")
			#logger.debug(fields)

			for idx , fiel in enumerate(fields):
				#logger.debug(idx, fiel)
				#logger.debug(fiel.attrib['name'])
				if 'extraStream' == fiel.attrib['name']:
					#logger.debug('passo di qui')
					# qui ho preso il field fb-livestreaming-account
					# e adesso devo prendere tutti gli account che ho messo
					_list = fiel.findall(namespaces['vdf'] + "list")
					if len(_list) > 0:
						_list = _list[0]
					else:
						return False
					stre_payloads =  _list.findall(namespaces['vdf'] + "payload")
					#logger.debug('stre_payloads ')
					#logger.debug(stre_payloads)
					for stre in stre_payloads:
						#logger.debug('giro _payloads')
						#logger.debug(stre)
						stre_field = stre.find(namespaces['vdf'] + "field")
						#logger.debug(stre_field.attrib['name'])
						if 'extraStream' in stre_field.attrib['name']:
							if not (stre_field.find(namespaces['vdf'] + "value") is None) :
								#logger.debug(stre_field.find(namespaces['vdf'] + "value").text)
								result.append( stre_field.find(namespaces['vdf'] + "value").text )
								#result.append(acc_field.find(namespaces['vdf'] + "value").text)

	return result




def VerificaExtraStream( entry ):

	result = []
	list =  entry.findall(namespaces['atom'] + "content")
	for lis in list:
		#logger.debug(lis)
		payload =  lis.findall(namespaces['vdf'] + "payload")
		for pay in payload:
			fields = pay.findall(namespaces['vdf'] + "field")
			#logger.debug(fields)

			for idx , fiel in enumerate(fields):
				#logger.debug(idx, fiel)
				#logger.debug(fiel.attrib['name'])
				if 'extraStream' == fiel.attrib['name']:
					#logger.debug('passo di qui')
					# qui ho preso il field fb-livestreaming-account
					# e adesso devo prendere tutti gli account che ho messo
					_list = fiel.findall(namespaces['vdf'] + "list")
					if len(_list) > 0:
						_list = _list[0]
					else:
						return False
					stre_payloads =  _list.findall(namespaces['vdf'] + "payload")
					#logger.debug('stre_payloads ')
					#logger.debug(stre_payloads)
					for stre in stre_payloads:
						#logger.debug('giro _payloads')
						#logger.debug(stre)
						stre_field = stre.find(namespaces['vdf'] + "field")
						#logger.debug(stre_field.attrib['name'])
						if 'extraStream' in stre_field.attrib['name']:
							if not (stre_field.find(namespaces['vdf'] + "value") is None) :
								#logger.debug(stre_field.find(namespaces['vdf'] + "value").text)
								if 'ch-lh.akamaihd' in stre_field.find(namespaces['vdf'] + "value").text:
									return True
								#result.append(acc_field.find(namespaces['vdf'] + "value").text)

	return False



def VerificaGeo( entry ):

	result = []

	list =  entry.findall(namespaces['atom'] + "content")
	for lis in list:
		#logger.debug(lis)
		payload =  lis.findall(namespaces['vdf'] + "payload")
		for pay in payload:
			fields = pay.findall(namespaces['vdf'] + "field")
			#logger.debug(fields)

			for idx , fiel in enumerate(fields):
				#logger.debug(idx, fiel)
				#logger.debug(fiel.attrib['name'])
				if 'stream' == fiel.attrib['name']:
					#logger.debug('passo di qui')
					# qui ho preso il field fb-livestreaming-account
					# e adesso devo prendere tutti gli account che ho messo
					_list = fiel.findall(namespaces['vdf'] + "list")
					if len( _list ) > 0 :
						_list = _list[0]
					else:
						return False
					stre_payloads =  _list.findall(namespaces['vdf'] + "payload")
					#logger.debug('stre_payloads ' )
					#logger.debug(stre_payloads)
					for stre in stre_payloads:
						#logger.debug('giro _payloads')
						#logger.debug(stre)
						stre_field = stre.find(namespaces['vdf'] + "field")
						#logger.debug(stre_field.attrib['name'])
						if 'stream' in stre_field.attrib['name']:
							if not (stre_field.find(namespaces['vdf'] + "value") is None) :
								#logger.debug(stre_field.find(namespaces['vdf'] + "value").text)
								if 'ch-lh.akamaihd' in stre_field.find(namespaces['vdf'] + "value").text:
									#logger.debug( ' Ritorno GEO = True ' )
									return True
								#result.append(acc_field.find(namespaces['vdf'] + "value").text)	

	return False

def PrendiAccounts( entry , type ):

	result = {}
	account = {}
	list =  entry.findall(namespaces['atom'] + "content")
	for lis in list:
		#logger.debug(lis)
		payload =  lis.findall(namespaces['vdf'] + "payload")
		for pay in payload:
			fields = pay.findall(namespaces['vdf'] + "field")
			#logger.debug(fields)

			for idx , fiel in enumerate(fields):
				#logger.debug(idx, fiel)
				#logger.debug(fiel.attrib['name'])
				if type in fiel.attrib['name']:
					#logger.debug('passo di qui')
					# qui ho preso il field fb-livestreaming-account
					# e adesso devo prendere tutti gli account che ho messo
					_list = fiel.findall(namespaces['vdf'] + "list")
					if len( _list ) < 1 :
						return result
						
					else:

						# faccio cosi perche la lista e unica
						_list = _list[0]
						acc_payloads =  _list.findall(namespaces['vdf'] + "payload")
						
						#print ( ' acc_payloads lungo : ' + str( len ( acc_payloads)) )
						for acc in acc_payloads:
							#print ( '----------------------------giro _payloads' )
							acc_fblivesocencchan = acc.find(namespaces['vdf'] + "field")
							acc_fields = acc_fblivesocencchan.findall(namespaces['vdf'] + "field")
							#print ( ' acc_payloads lungo : ' + str( len ( acc_payloads)) )
							
							for acc_f in acc_fields:
								if not (acc_f.find(namespaces['vdf'] + "value") is None) :
									#print (  acc_f.attrib['name']  + ' = ' + acc_f.find(namespaces['vdf'] + "value").text )
									account[ acc_f.attrib['name'] ] =  acc_f.find(namespaces['vdf'] + "value").text	

							result[ account['livesocialid']] =   {'SocialDetails' : account}
							account = {}
	return result

def PrendiSocialTime( entry ):


	start = ''
	end = ''

	list =  entry.findall(namespaces['atom'] + "content")
	for lis in list:
		#logger.debug(lis)
		payload =  lis.findall(namespaces['vdf'] + "payload")
		for pay in payload:
			fields = pay.findall(namespaces['vdf'] + "field")
			#logger.debug(fields)

			for idx , fiel in enumerate(fields):
				#logger.debug(idx, fiel)
				#logger.debug(fiel.attrib['name'])
				if 'socialStartTime' in fiel.attrib['name'] :
					if not (fiel.find(namespaces['vdf'] + "value") is None) :
						# ho trovato il time nella forma 
						# <vdf:value>2017-04-24T10:30:00Z</vdf:value>
						start =  fiel.find(namespaces['vdf'] + "value").text
	
				if 'socialEndTime' in fiel.attrib['name'] :
					if not (fiel.find(namespaces['vdf'] + "value") is None) :
						# ho trovato il time nella forma 
						# <vdf:value>2017-04-24T10:30:00Z</vdf:value>
						end =  fiel.find(namespaces['vdf'] + "value").text



	return [ start, end ]



def PrendiTime( entry ):


	start = ''
	end = ''

	list =  entry.findall(namespaces['atom'] + "content")
	for lis in list:
		#logger.debug(lis)
		payload =  lis.findall(namespaces['vdf'] + "payload")
		for pay in payload:
			fields = pay.findall(namespaces['vdf'] + "field")
			#logger.debug(fields)

			for idx , fiel in enumerate(fields):
				#logger.debug(idx, fiel)
				#logger.debug(fiel.attrib['name'])
				if 'startTime' in fiel.attrib['name'] :
					if not (fiel.find(namespaces['vdf'] + "value") is None) :
						# ho trovato il time nella forma 
						# <vdf:value>2017-04-24T10:30:00Z</vdf:value>
						start =  fiel.find(namespaces['vdf'] + "value").text
	
				if 'endTime' in fiel.attrib['name'] :
					if not (fiel.find(namespaces['vdf'] + "value") is None) :
						# ho trovato il time nella forma 
						# <vdf:value>2017-04-24T10:30:00Z</vdf:value>
						end =  fiel.find(namespaces['vdf'] + "value").text


	return [ start, end ]



def SistemaYtAccounts( lista_yt_accounts ):

	result = []
	#da ytl-account-rsi
	#devono diventare una lista cosi ['testare_e_bello', 'linea_rossa', 'patti_chiari', 'rsi_sport', 'rsi_news'] 
	for value in lista_yt_accounts:
		if value is not None:
			# e qui prendo solo la parte descrittiva della chiave dell account
			result.append(value.split('account-')[-1])


	return result

def SistemaTimeOld( Item_Json ):

	logger = logging.getLogger('pySub_ECE')
	logger.debug(" ---------------------- INIZIO SistemaTempo   ------------------ ")


	# prende il piu' basso tra i tempi di inizio e terminazione
	# prendendo in considerazione activation e starttime per inizio
	# e expiratione e endTime per la fine

	# dato che poi FB vuole il tempo timezonato aggiundo la timedifference
	delta = datetime.now() - datetime.utcnow()

	start = Item_Json['startTime']
	end = Item_Json['endTime']

	if ((Item_Json['notificaTime'] is None) or len(Item_Json['notificaTime'])< 1 ) :
		Item_Json['setNotifTime'] = False
		notif = datetime.strftime(datetime.strptime( Item_Json['published'], '%Y-%m-%dT%X.000Z' ) - delta,'%Y-%m-%dT%H:%M:%SZ' )
	else:
		Item_Json['setNotifTime'] = True
		notif = Item_Json['notificaTime']
		

	logger.debug(' sistema tempo : start, end, notifica ')
	logger.debug(start +' '+ end +' '+ notif)

	if (not (Item_Json['activation'] is None) ) :
		activa = datetime.strptime( Item_Json['activation'], '%Y-%m-%dT%X.000Z' )
		_tmp_start = datetime.strptime( Item_Json['startTime'],'%Y-%m-%dT%H:%M:%SZ' )
		if activa > _tmp_start:
			start = datetime.strftime( Item_Json['activation'], '%Y-%m-%dT%H:%M:%SZ' )
	
	if (not (Item_Json['expiration'] is None) ) :
		expira = datetime.strptime( Item_Json['expiration'], '%Y-%m-%dT%X.000Z' )
		_tmp_end = datetime.strptime( Item_Json['endTime'],'%Y-%m-%dT%H:%M:%SZ' )
		if  expira < _tmp_end:
			end = datetime.strftime( Item_Json['expiration'], '%Y-%m-%dT%H:%M:%SZ' )



	start = datetime.strptime( start,'%Y-%m-%dT%H:%M:%SZ' ) + delta
	start = datetime.strftime( start, '%Y-%m-%dT%H:%M:%SZ')
	end = datetime.strptime( end,'%Y-%m-%dT%H:%M:%SZ' ) + delta
	end = datetime.strftime( end, '%Y-%m-%dT%H:%M:%SZ')
	notif = datetime.strptime( notif,'%Y-%m-%dT%H:%M:%SZ' ) + delta
	notif = datetime.strftime( notif, '%Y-%m-%dT%H:%M:%SZ')

	logger.debug(' sistema tempo : start, end, notifica ')
	logger.debug(start +' '+ end +' '+ notif)
	# ritorno nella forma
	# <vdf:value>2017-04-24T10:30:00Z</vdf:value>
	
	logger.debug(" ---------------------- END SistemaTempo   ------------------ ")
	return [ start, end, notif ]


def SistemaTime( Item_Json ):

	logger = logging.getLogger('pySub_ECE')
	logger.debug(" ---------------------- INIZIO SistemaTempo   ------------------ ")
	logger.debug( Item_Json )

	# prende il piu' basso tra i tempi di inizio e terminazione
	# prendendo in considerazione activation e starttime per inizio
	# e expiratione e endTime per la fine

	# dato che poi FB vuole il tempo timezonato aggiundo la timedifference
	delta = datetime.now() - datetime.utcnow()
	logger.debug( ' delta = ' + str(delta))
	start = Item_Json['startTime']
	end = Item_Json['endTime']

	# sistemo il tempo di notifica dei vari account
	platforms = ['fb' , 'yt' ]
	for plat in platforms:
		account = Item_Json[ plat + '-livestreaming-account' ]
		for key,valueUp in account.items():
			logger.debug( key )
			logger.debug( valueUp )
			value = valueUp['SocialDetails']
			logger.debug( value )

			if ((value['notificaTime'] is None) or len(value['notificaTime'])< 1 ) :
				value['setNotifTime'] = False
				# value['notificaTime'] = datetime.strftime(datetime.strptime( Item_Json['published'], '%Y-%m-%dT%X.000Z' ) - delta,'%Y-%m-%dT%H:%M:%SZ' )
				value['notificaTime'] = datetime.strftime(datetime.strptime( Item_Json['published'], '%Y-%m-%dT%X.000Z' ) + delta,'%Y-%m-%dT%H:%M:%SZ' )
			else:
				logger.debug( 'PASSSO DI QUI ! ' )
				logger.debug( value['notificaTime'] )
				value['setNotifTime'] = True
				value['notificaTime'] = datetime.strftime(datetime.strptime( value['notificaTime'], '%Y-%m-%dT%H:%M:%SZ' ) + delta,'%Y-%m-%dT%H:%M:%SZ' )
				logger.debug( value['notificaTime'] )

	if (not (Item_Json['activation'] is None) ) :
		activa = datetime.strptime( Item_Json['activation'], '%Y-%m-%dT%X.000Z' )
		_tmp_start = datetime.strptime( Item_Json['startTime'],'%Y-%m-%dT%H:%M:%SZ' )
		if activa > _tmp_start:
			start = datetime.strftime( activa, '%Y-%m-%dT%H:%M:%SZ' )
	
	if (not (Item_Json['expiration'] is None) ) :
		expira = datetime.strptime( Item_Json['expiration'], '%Y-%m-%dT%X.000Z' )
		_tmp_end = datetime.strptime( Item_Json['endTime'],'%Y-%m-%dT%H:%M:%SZ' )
		if  expira < _tmp_end:
			end = datetime.strftime( expira, '%Y-%m-%dT%H:%M:%SZ' )

	start = datetime.strptime( start,'%Y-%m-%dT%H:%M:%SZ' ) + delta
	start = datetime.strftime( start, '%Y-%m-%dT%H:%M:%SZ')
	end = datetime.strptime( end,'%Y-%m-%dT%H:%M:%SZ' ) + delta
	end = datetime.strftime( end, '%Y-%m-%dT%H:%M:%SZ')

	logger.debug(' sistema tempo : start, end ')
	logger.debug(start +' '+ end )
	# ritorno nella forma
	# <vdf:value>2017-04-24T10:30:00Z</vdf:value>
	
	logger.debug(" ---------------------- END SistemaTempo   ------------------ ")
	return [ start, end ]

def SistemaEncodersOld( Tab_Json ):

	# devo guardare se hanno scelto encoder 12 o 13 che sono mappati su LA1 e LA2
	# altrimenti sono = custom

	
	logger = logging.getLogger('pySub_ECE')

	logger.debug( " ---------------------- END SistemaEncoders   ------------------ ")
	logger.debug( Tab_Json['encoders'])
	
	enco  = Tab_Json['encoders']
	logger.debug('Encoders = ' + str( enco))

	if enco[0] : 
		logger.debug('Incoming Stream : custom')
		return 'custom'

	if '12' in enco:
		# sono LA1
		logger.debug('Incoming Stream : la1')
		return 'la1'
	else:
		if '13' in enco:
			# sono LA2
			logger.debug('Incoming Stream : la2')
			return 'la2'
		else:
			logger.debug('Incoming Stream : enc' + enco[-1] )
			return 'enc' + enco[-1]
	

	return Null

def SistemaResto( accountJson ):
	
	logger = logging.getLogger('pySub_ECE')
	logger.debug(' ---------- INIT SistemaResto -------------- ')

	platforms = ['fb' , 'yt' ]

	for plat in platforms:
		account = accountJson[ plat + '-livestreaming-account' ]
		for key,valueUp in account.items():
			#print ( key, valueUp )
			value = valueUp['SocialDetails']

			if 'isGeoBlocked' not in value or value['isGeoBlocked'] is None :
				value['isGeoBlocked'] = 'false'
			if 'continuousLive' not in value or value['continuousLive'] is None :
				value['continuousLive'] = 'false'

			if 'notificato' not in value or value['notificato'] is None :
				value['notificato'] = False

			# CLAD CORONA
			# fix for CORONAVID
			# devo mettere il continuous SEMPRE a false altrimenti scoppia
			value['continuousLive'] = 'false'
			# CLAD CORONA


	logger.debug(' ---------- FINE  SistemaResto -------------- ')

	return accountJson


def SistemaTitolo( accountJson ):
	
	logger = logging.getLogger('pySub_ECE')
	logger.debug(' ---------- INIT SistemaTitolo -------------- ')

	platforms = ['fb' , 'yt' ]

	for plat in platforms:
		account = accountJson[ plat + '-livestreaming-account' ]
		for key,valueUp in account.items():
			#print ( key, valueUp )
			value = valueUp['SocialDetails']

			if 'videoTitle' not in value or value['videoTitle'] is None or len( value['videoTitle'] ) < 1 :
				# devo mettere il titolo dell Evento
				if not ( accountJson['title'] is None ) :
					value['videoTitle'] = accountJson['title'] 
				else:
					value['videoTitle'] = ''


			if 'videoDesc' not in value or value['videoDesc'] is None or len( value['videoDesc'] ) < 1 :
				# devo mettere il titolo dell Evento
				if not ( accountJson['subtitle'] is None ) :
					value['videoDesc'] = accountJson['subtitle'] 
				else:
					value['videoDesc'] = ''

	logger.debug(' ---------- FINE  SistemaTitolo -------------- ')

	return accountJson

def RemoveLiveId( socialAccounts, liveId ) :

	result = socialAccounts
	# devo cancellare il liveId richiesto 
	# dopo aver verificato che non ci siano VideoDetails o WowzaDetails
	# e se ci sono cancellarli  
	value = socialAccounts[ liveId ]
	if 'VideoDetails' in value:
		# CLAD TODO
		logger.debug( ' Dovrei spegnere il video di : ' + liveId  + ' e cancellarne la entry ')
		# e per spegnerlo magari far passare una lista indietro di entry da spegnere 
		# al Controlla_Flag_Social ?
		# o magari solo flaggare il socialAccount anziche cancellarlo e 
		# TBD
	if 'WowzaDetails' in value:
		# CLAD TODO
		logger.debug( ' Dovrei spegnere il wowza di : ' + liveId  + ' e cancellarne la entry ')
	
		
	return result

def MergeSocialFlags( socialNew, socialOld ) :

	logger.debug(' ---------- INIT  MergeSocialFlags -------------- ')
	logger.debug(' socialNew : ')
	logger.debug(socialNew)
	logger.debug(' socialOld : ')
	logger.debug(socialOld)

	# se non 'e ancora stata inizializzata cioe non arriva dal DB
	# non devo rinfrescarla ma settarla
	if len( socialOld ) < 1 :
		result = socialNew
		return result

	# in caso contrario devo passare al merge
	# il cui compito e' aggiornare i valori dele social  Flags
	# che arrivano da ECE senza cancellare gli eventuali valori
	# di video o wowza

	
	# aggiungo verifica che non sia stata cancellata una entry dalla scheda
	# quindi giro su quello vecchio e verifico che ci siano TUTTE le entry 
	# aka livesocialid
	for key,value in socialOld.items(): 
		if 'livestreaming-account' in key:
			new_accounts = {}
			# siamo nel dict dei broadcast di fb o yt
			# quindi in fb-livestreaming-account oppure
			# yt-livestreaming-account
			for liveId, detailsvalues in value.items():
				# qui in liveId ho gli id unici che devo cercare nel socialNew
				if not liveId in socialNew[ key ]:
					# non trovato in quello nuovo quindi lo hanno tolto
					# devo cancellarlo da quello vecchio 
					logger.debug( ' hanno cancellato -> ' + liveId )
					RemoveLiveId( value , liveId )
				else:
					new_accounts[ liveId ] = value[ liveId ]

			socialOld[ key ] = new_accounts
					
	#logger.debug( ' new Accounts : ' + str(socialOld[  key ] ))
					
	result = socialOld
	#print ( 'socialNew : ' + str(socialNew) )
	#print ( 'socialOld : ' + str(socialOld) )
	# DEBUG CLAD  da verificare

	
	for key,value in socialNew.items(): 
		if 'livestreaming-account' in key:
			# siamo nel dict dei broadcast di fb o yt
			for broad,broadValue in value.items():
				logger.debug(' chiave dell account : ' + broad)
				logger.debug(' value : ' + str( broadValue ) )
				# devo trovare il posto giusto di quella vecchia
				# se non ha ancora nulla gli copia dentro la chiave 
				# come se niente fosse 
				if not broad in result[key]:
					result[key][broad]  = broadValue
				else:
					
					result[ key ][broad]['SocialDetails'] = broadValue['SocialDetails'] 
			continue
		result[ key ] = value

	logger.debug(' ---------- END MergeSocialFlags -------------- ')
	logger.debug(result)
	logger.debug(' ---------- END MergeSocialFlags -------------- ')


	return result


def Prendi_Flag_Social_New( Id_xx ):
	
	logger = logging.getLogger('pySub_ECE')
	logger.debug(' ---------- INIT Prendi_Flag_Social -------------- ')
	lista_value_social = ['title','subtitle']


	base64string = base64.encodestring('%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])).replace('\n', '')
	#link = os.environ['ECE_SERVER']  + str(Id_xx)
	# cambiato per andare a prendere pub2 senza pwd
	print ( os.environ['ECE_SERVER'] )
	link = os.environ['ECE_SERVER']  + str(Id_xx)
	print ( link )
	#print ( 'Clad' )
	request = urllib.request.Request(link)
	request.add_header("Authorization", "Basic %s" % base64string)

	Tab_Json = {}
	try:
	
		result = urllib.request.urlopen(request)
		#print ( result.read()  )

		tree = ET.parse(result)
		#tree.write('11464970.xml')
		#ET.dump(tree)
		#exit(0)

		for val in lista_value_social:
			Tab_Json[val] =  PrendiField( tree, val)

		# Prendo anche  lo stato dell asset per sapere se e' stato 
		# spubblicato
		Tab_Json['state'] = PrendiState( tree , 'dummy')

		# e adesso prendo lo start e end time
		[ Tab_Json['startTime'], Tab_Json['endTime'] ] = PrendiTime( tree )

		# e adesso prendo lo start e end time
		[ Tab_Json['socialStartTime'], Tab_Json['socialEndTime'] ] = PrendiSocialTime( tree )

		if len(Tab_Json['socialStartTime']) > 1 :
			logger.debug( 'setto Start = SocialStart' )
			Tab_Json['startTime'] = Tab_Json['socialStartTime']

		if len(Tab_Json['socialEndTime']) > 1 :
			logger.debug( 'setto End = SocialEnd' )
			Tab_Json['endTime'] = Tab_Json['socialEndTime']

		# e adesso prendo i check times = activation expire ...
		[ Tab_Json['activation'], Tab_Json['published'], Tab_Json['expiration']] = PrendiTimeCtrl( tree )



		# e adesso prendo gli account fi facebook a cui spedirlo
		Tab_Json['fb-livestreaming-account'] = PrendiAccounts( tree , 'fblivesocencchan')


		# e adesso prendo gli account fi facebook a cui spedirlo
		# qui sistemo gli account di YT
		Tab_Json['yt-livestreaming-account'] = PrendiAccounts( tree , 'ioutubelivesocencchan')


		# sistemo i tempi per avere t_start e t_end
		[ Tab_Json['t_start'], Tab_Json['t_end'] ] = SistemaTime( Tab_Json )

		# sistemo gli encoder
		Tab_Json  = SistemaEncoders( Tab_Json )


		Tab_Json = SistemaTitolo( Tab_Json )
		Tab_Json = SistemaResto( Tab_Json )


		logger.debug('ID : ' + str(Id_xx) + ' = ' +  str(Tab_Json))
		logger.debug(' ---------- FINE Prendi_Flag_Social -------------- ')
		result.close()
	except urllib.error.HTTPError as e:
		logger.debug(' EXCEPTIOOOONNNNNN - ritorno none !!!! ')
		logger.debug(' ---------- FINE Prendi_Flag_Social -------------- ')
		return [ u'None' , u'None', u'None']
	return Tab_Json


def Prendi_Section_Parameters( Id_xx ):
	
	logger = logging.getLogger('pySub_ECE')
	logger.debug(' ---------- INIT Prendi_Section_Parameters -------------- ')
	lista_value_social = [ 'title','subtitle', 'fbVideoTitle', 'fbVideoDesc', 'ytVideoTitle','ytVideoDesc' ]


	base64string = base64.encodestring('%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])).replace('\n', '')
	#link = os.environ['ECE_SERVER']  + str(Id_xx)
	# cambiato per andare a prendere pub2 senza pwd
	print ( os.environ['ECE_SECTION'] )

	link = os.environ['ECE_SECTION']  + str(Id_xx)
	logger.debug( link )
	#print ( 'Clad' )
	request = urllib.request.Request(link)
	request.add_header("Authorization", "Basic %s" % base64string)

	num_posts = 0
	account = ''
	nomefile = ''
	path = ''

	try:
	
		result = urllib.request.urlopen(request)
		#print ( result.read()  )

		tree = ET.parse(result)
		#tree.write('22742.xml')
		#ET.dump(tree)
		print ( 'scritto' )
		#return
		parametri = PrendiParam( tree )
		print ( parametri )
		parametri = parametri.splitlines()
		for para in parametri:
			if 'mostSocial.account' in para:
				print ( 'trovato account' )
				account = para.split('=')[-1]
			if 'mostSocial.num_posts' in para:
				print ( 'trovato num_posts' )
				num_posts = para.split('=')[-1]
			if 'mostSocial.nomeFile' in para:
				print ( 'trovato num_posts' )
				nomefile = para.split('=')[-1]
			if 'mostSocial.path' in para:
				print ( 'trovato num_posts' )
				path = para.split('=')[-1]
		
		

		logger.debug(' ---------- FINE Prendi_Section_Parameters -------------- ')
		result.close()
	except urllib.error.HTTPError as e:
		logger.debug(' EXCEPTIOOOONNNNNN - ritorno none !!!! ')
		logger.debug(' ---------- FINE Prendi_Section_Parameters -------------- ')

		return { u'num_posts' : u'None' , u'account' : u'None', u'nomefile': u'None', u'path': u'None'}
		
	
	return { u'num_posts' : num_posts , u'account' : account , u'nomefile' : nomefile, u'path' : path }


def PrendiParam( entry ):

	rel = 'sectionParameters'

	list =  entry.findall(namespaces['atom'] + "content")
	for lis in list:
		#logger.debug(lis)
		#print ( lis )
		payload =  lis.findall(namespaces['vdf'] + "payload")
		for pay in payload:
			fields = pay.findall(namespaces['vdf'] + "field")
			#logger.debug(fields)
			#print ( fields )

			for idx , fiel in enumerate(fields):
				#print ( idx, fiel )
				#print ( fiel.attrib['name'] )
				if rel in fiel.attrib['name']:
					if not (fiel.find(namespaces['vdf'] + "value") is None) :
						return fiel.find(namespaces['vdf'] + "value").text
					else:
						return None

	return None







def Prendi_Altro_Content( Id_xx ):

	logger.debug(' INIT Prendi_Altro_Content ')
	
	contentDiRitorno = {}
		
	base64string = base64.encodestring('%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])).replace('\n', '')

	link = os.environ['ECE_SERVER']  + str(Id_xx)

	request = urllib.request.Request(link)


	request.add_header("Authorization", "Basic %s" % base64string)
	try:
		result = urllib.request.urlopen(request)
	

		tree = ET.parse(result)
		#logger.debug(ET._namespace_map)
		#tree.write('rimpingua_content_con_img.xml')
		#ET.dump(tree)
		#logger.debug(key)

		# come fatto qui sotto posso prendere tutti i fields
		title =  PrendiField( tree, 'title' )
		subtitle = PrendiField( tree, 'subtitle' )
		leadtext = PrendiField( tree, 'leadtext' )
		logger.debug(title, subtitle , leadtext)
		# e title e subtitle li metto direttamente al posto giusto

		#body =   PrendiFieldTest( tree, 'body')
		#logger.debug(body)
		#logger.debug(' fine body ')


		#logger.debug(PrendiField( tree, 'leadtext' ))
		#logger.debug(PrendiLinkRelatedId( tree, 'related'))
		#logger.debug(PrendiLinkLeadId( tree, 'related'))
		
		tipo = PrendiTipo( tree )
		if 'short' in tipo:
			link_alla_url = 'http://www.rsi.ch/web/ultimora/?a=' + Id_xx
		else:
			#link_alla_url = Prendi_Url( Id_xx )
			link_alla_url = 'http://www.rsi.ch/g/' + Id_xx

		link_alla_url = link_alla_url.replace('publishing','www')

		rel_id = PrendiLinkLeadId( tree, 'related')
		if not( rel_id is None):

	
			'''

			# eventualmente per prendere la caption
			related_content = PrendiLinkRelated( tree, 'related')

			lista =  related_content.findall(namespaces['vdf'] + "payload")
			logger.debug(len(lista))
			for lis in lista:
				logger.debug(lis.attrib)
			logger.debug(related_content)
			exit(0)

			''' 
			caption = ''




			url = 'https://www.rsi.ch/rsi-api/resize/image/BASE_FREE/' + rel_id
			contentDiRitorno = { 'link' : link_alla_url,
					'picture':url,
					'name' : title, 
					'description':subtitle,
					'caption':caption}
		else:
			url = None
			contentDiRitorno = { 'link' : link_alla_url,
					'picture':url,
					'name' : title, 
					'description':subtitle,
					'caption':''}
		
		result.close()
	except urllib.error.HTTPError as e:
		logger.debug(' EXCEPTIOOOONNNNNN - ritorno none !!!! ')
		return [ u'None' , u'None']
		
	
	logger.debug(contentDiRitorno)
	return contentDiRitorno 

def Cambia_Flag_Social( Id_xx , UpdateFlags ):
	
		
	logger.debug(' --------------------------- init Cambia_Flag_Social ----------------------')
	base64string = base64.encodestring('%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])).replace('\n', '')

	link = os.environ['ECE_SERVER']  + str(Id_xx)

	request = urllib.request.Request(link)


	request.add_header("Authorization", "Basic %s" % base64string)
	try:
		result = urllib.request.urlopen(request)
	

		tree = ET.parse(result)
		#tree.write('8568391.xml')
		# esempio di modifica del file xml 
		#tmp_tree =  CambiaField( tree, "facebookText", 'CAMBIATOOOO' )
		#tree = tmp_tree
		#tmp_tree =  CambiaField( tree, "facebookAltTitle", 'INSERITOOOOOO' )
		#tree = tmp_tree
		#tree.write('8568391_cambiato.xml')
		#exit(0)
		
		#ET.dump(tree)
		for key, value in UpdateFlags.items():

			# fix per non modificare i campi data
			# perche se non ha alcun valore io ci sbatterei dentro None 
			# al posto di niente
			if 'Time' in key:
				continue
			# invece cosi non li cambio per niente e mi ritrovo
			# magicamente i valori che erano settati prima.
			

			logger.debug(key, value)
			if value is None:
				value = 'None'
			else:
				if  isinstance( value, ( int ) ):
					logger.debug(value)
					value = str( value )
				
			if 'None' in value:
				continue
				

			# come fatto qui sotto posso prendere tutti i fields
			tmp_tree =  CambiaField( tree, key, value )
			tree = tmp_tree

		logger.debug('Dump CLAD')
		#NON CAMBIAAAAAAA
		#tree.write('8568391_cambiato.xml')
		result.close()
		logger.debug(' --------------------------- fine Cambia_Flag_Social ----------------------')
		return tree

	except urllib.error.HTTPError as e:
		logger.debug(' EXCEPTIOOOONNNNNN - ritorno none !!!! ')
		return [ u'None' , u'None', u'None']
		
	
	logger.debug(' --------------------------- fine Cambia_Flag_Social ----------------------')
	return []


def Cambia_MonitorProd( Id_xx , html_body ):
	
		
	logger.debug(' --------------------------- init Cambia_Monitor ----------------------')
	base64string = base64.encodestring('%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])).replace('\n', '')

	link = os.environ['ECE_SERVER']  + str(Id_xx)
	prod_ece = 'http://internal.publishing.production.rsi.ch/webservice/escenic/content/'
	link = prod_ece  + str(Id_xx)

	request = urllib.request.Request(link)


	request.add_header("Authorization", "Basic %s" % base64string)
	try:
		result = urllib.request.urlopen(request)
	

		tree = ET.parse(result)
		#tree.write('10378235.xml')
		# esempio di modifica del file xml 
		#cambio = '&lt;div&gt; Tabella Per i Valentinis ... e tutti i picchetti riuniti &lt;\/div&gt;'
		cambio = 'CAMBIATOOOO  DEFAULT CLAD'
		cambio = html_body

		tmp_tree =  CambiaField( tree, "code", cambio )
		tree = tmp_tree
		#tmp_tree =  CambiaField( tree, "facebookAltTitle", 'INSERITOOOOOO' )
		#tree = tmp_tree
		#tree.write('10378235_cambiato.xml')
		#exit(0)
		
		#logger.debug('Dump CLAD')
		#NON CAMBIAAAAAAA
		#tree.write('8568391_cambiato.xml')
		result.close()
		logger.debug(' --------------------------- fine Cambia_Monitor ----------------------')
		return tree

	except urllib.error.HTTPError as e:
		logger.debug(' EXCEPTIOOOONNNNNN - ritorno none !!!! ')
		return [ u'None' , u'None', u'None']
		
	
	logger.debug(' --------------------------- fine Cambia_Monitor ----------------------')
	return []



def Cambia_Monitor( Id_xx , html_body ):
	
		
	logger.debug(' --------------------------- init Cambia_Monitor ----------------------')
	base64string = base64.encodestring('%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])).replace('\n', '')

	link = os.environ['ECE_SERVER']  + str(Id_xx)

	request = urllib.request.Request(link)


	request.add_header("Authorization", "Basic %s" % base64string)
	try:
		result = urllib.request.urlopen(request)
	

		tree = ET.parse(result)
		#tree.write('10378235.xml')
		# esempio di modifica del file xml 
		#cambio = '&lt;div&gt; Tabella Per i Valentinis ... e tutti i picchetti riuniti &lt;\/div&gt;'
		cambio = 'CAMBIATOOOO  DEFAULT CLAD'
		cambio = html_body

		tmp_tree =  CambiaField( tree, "code", cambio )
		tree = tmp_tree
		#tmp_tree =  CambiaField( tree, "facebookAltTitle", 'INSERITOOOOOO' )
		#tree = tmp_tree
		#tree.write('10378235_cambiato.xml')
		#exit(0)
		
		#logger.debug('Dump CLAD')
		#NON CAMBIAAAAAAA
		#tree.write('8568391_cambiato.xml')
		result.close()
		logger.debug(' --------------------------- fine Cambia_Monitor ----------------------')
		return tree

	except urllib.error.HTTPError as e:
		logger.debug(' EXCEPTIOOOONNNNNN - ritorno none !!!! ')
		return [ u'None' , u'None', u'None']
		
	
	logger.debug(' --------------------------- fine Cambia_Monitor ----------------------')
	return []


def Rimpingua_Content( lista_content ):

	logger.debug(' ----------------------- inizia Rimpingua_Content -----------------------')
	
	result_list = []

	count = 0
	for lis in lista_content:
		#logger.debug(' - ' + str(count) + ' -- ' + lis['id'][8:] + ' --- ')
		count = count + 1
		lis['lead'] = Prendi_Altro_Content(lis['id'][8:])
		lis['link'] = 'www.rsi.ch/g/'+   lis['id'][8:]
		logger.debug(lis['lead'])
		# prima la lista da_fare la aggiungevo qui .....
		# adesso per motivi di performance  ho spostato sotto il rimpingua content
		# che faccio solo su quelle che devo postare 
		# e la lista da_fare la aggiungo prima
		#lis['da_fare'] = []
		result_list.append( lis )
		
	return result_list

	logger.debug(' ----------------------- fine Rimpingua_Content -----------------------')

def Aggiungi_Items_al_Db( archive_name, archive_dict ):
	
	if len(archive_dict) == 0 :
		return
	old_dict = {}
	if os.path.isfile( archive_name ):
		adesso = datetime.utcnow()
		shutil.copy( archive_name, archive_name + "_" + adesso.strftime("%s") )
		try :
			logger.debug('AGGIUNGO a  ARCHIVE_NAME ' + archive_name)
			fin = codecs.open( archive_name, 'r', 'utf-8' )
			old_dict = json.load( fin )
			logger.debug(old_dict)
			fin.close()
		except:
			logger.debug('PROBLEMI CON IL DB')
			pass


	for key,value in archive_dict.items():
		old_dict[ key ] = value


	fout = codecs.open( archive_name, 'w', 'utf-8')
	json.dump( old_dict, fout )
	fout.close()



def Scrivi_Items_Db( db_name, lista ):

	
	
	#if len(lista) == 0 :
		#return

	#adesso = datetime.utcnow()

	#shutil.copy( db_name, db_name + "_" + adesso.strftime("%s") )

	fout = codecs.open( db_name, 'w', 'utf-8')
	json.dump( lista, fout )
	fout.close()

def Prendi_Items_Db( db_name ):

	result_dict = {}
	try :
		#logger.debug('leggo DB_NAME ' + db_name)
		fin = codecs.open( db_name, 'r', 'utf-8' )
		result_dict = json.load( fin )
		#logger.debug(result_dict)
		fin.close()
	except:
		logger.debug('PROBLEMI CON IL DB')
		pass

	return result_dict

def Aggiungi_Items_da_Db( db_name, dict_content):

	# deve aggiungere tutti e solo gli item che ci non nel Db e che mancano
	# alla query del Solr cioe non sono stati modificati nell'ultimo minuto

	#logger.debug(dict_content)
	result = dict_content

	for key,value in dict_content.items():
		dict_content[ key ]['FBStreamingDetails'] = {}
		dict_content[ key ]['YTStreamingDetails'] = {}
	
	# quindi prendo quelli del db
	dict_db = Prendi_Items_Db( db_name )
	logger.debug('  totale content preso dal DB        =  ' + str(len(dict_db)))
	# e verifico che non siano gia' presenti nella dict_content
	for key, value in dict_db.items():
		#logger.debug(key)
		#logger.debug(value)
		#logger.debug()
		#logger.debug(value['SocialFlags'])
		#logger.debug(value['ECEDetails'])
		
		_appendo = True
		if key in dict_content:
			# questo non lo aggiungo
			_appendo = False
			# se esistono gia i video details allora li copia nelle nuove info che arrivano da ECE
			# magari qualcuno ha cambiato qualche valore di time e dobbiamo riverificare tutto
			# in base a quell id ma non perdere i videodetail e neppure i wowza details
			if 'FBStreamingDetails' in value:
				dict_content[ key ]['FBStreamingDetails'] = value['FBStreamingDetails']
			if 'YTStreamingDetails' in value:
				dict_content[ key ]['YTStreamingDetails'] = value['YTStreamingDetails']
					
		if _appendo:
			result[ key ] = value
	
	return result

def printList( dict_content ): 


	for key, value in dict_content.items():
		logger.debug(' Key -----> ' + key)
		if 'ECEDetails' in value :
			logger.debug('----  ECEDetails : '	)
			logger.debug(value['ECEDetails'])
		if 'SocialFlags' in value :
			logger.debug('----  SocialFlags : '	)
			logger.debug(value['SocialFlags'])
		if 'FBStreamingDetails' in value and len(value['FBStreamingDetails']) > 0:
			logger.debug('----  FBStreamingDetails : '	)
			for keystream,valuestrem in value['FBStreamingDetails'].items():
				logger.debug('Details of : ' + keystream)
				logger.debug(valuestrem)
		if 'YTStreamingDetails' in value and len(value['YTStreamingDetails']) > 0:
			logger.debug('----  YTStreamingDetails : '	)
			for keystream,valuestrem in value['YTStreamingDetails'].items():
				logger.debug('Details of : ' + keystream)
				logger.debug(valuestrem)
		logger.debug('------------------------- fine ' + key + ' -----------------------\n')
	


def printIDS( dict_content ):


	lista = []
	for key, value in dict_content.items():
		lista.append( str(key) )
	logger.info( str(lista) )
	

def Controlla_Flag_Social( lista_content ):
	

	logger.info('----------------------- inizia Controlla_Flag_Social -----------------------')
	#logger.debug(' lunghezza lista iniziale : ' + str(len(lista_content)))
	logger.debug(lista_content)
	#logger.debug(' da fare : ')


	# questa deve preparare la lista di quelle da fare adesso e quelle da scrivere nel DB
	# e quelli da fare sono quelli da aprire o chiudere
	# quindi che hanno appena passato lo starttime
	# o che hanno appena passato endTime
	
	da_fare_list_NOTIFICA = {}
	da_fare_list_ON = {}
	da_fare_list_OFF = {}
	da_fare_list_PREPARA = {}

	
	# cosi considero anche eventuali cambiamenti di daysaving time 
	adesso = datetime.now()
	# per controllo su data emissione mi server il tempo attuale
	logger.debug('prendo tempo attuale in utc ' + str(adesso))

	# CLAD se sono in production setto una var per togliere il 9845290
	_PROD_ = False
	if '_APICoreXEnv_' in os.environ:
		if 'PRODUCTION' in os.environ['_APICoreXEnv_']:
			_PROD_ = True

	count = 0
	#spinner = pySpin.Spinner()
	#spinner.start()
	# ... some long-running operations
	# time.sleep(3)
	for key,value in lista_content.items():
		# giro su tutti gli itemspresi sia da Solr che dal mio db
		logger.debug(' ----- ' + str(count) + ' ------ ' + key + ' ------- ')

		# CLAD se sono in production TOLGO il 9845290
		if _PROD_ and '9845290' in  key:
			logger.debug( ' SONO in PROD -> Rimuovo item 9845290' )
			continue

	
		count = count + 1

		# qui prendo le social flags
		# se arrivo dal db le riprendo cmq nel caso qualcuno avesse cambiato qualcosa nell'ultimo
		# minuto 
		# 12.04.2019 nuovo content type : non basta copiarle sopra le altre
		# perche non sono piu separati i contenuti.
		# devo andare dentro e fare una merge
		# value['SocialFlags'] =   Prendi_Flag_Social_New( key )
		logger.debug( ' value -> ' + str(value ) )

		# DEBUG CLAD  da verificare
		value['SocialFlags'] =   MergeSocialFlags( Prendi_Flag_Social_New( key ), value['SocialFlags'] )
		# DEBUG CLAD 
		

		#logger.debug('debug delle social flags')
		#logger.debug('SocialFlags')
		#for k,v in value['SocialFlags'].items():
			#print ( k,v )

		#exit(0)
		# qui metto il controllo che sia ancora published
		# altrimenti salto
		if not ( value['SocialFlags']['state'] == 'published' ) :
			logger.debug(' ESCO perche non PUBLISHED')
			continue
	

		# qui metto il controllo che ci sia almeno un account
		# a cui spedirlo altrimenti salto
		if len(value['SocialFlags']['fb-livestreaming-account'] ) < 1 and len(value['SocialFlags']['yt-livestreaming-account'] ) < 1:
			logger.debug(' ESCO perche non ci sono le FlagSocial settate')
			continue
	

		# i casi sono tre
		if adesso < datetime.strptime( value['SocialFlags'] ['t_start'],  '%Y-%m-%dT%H:%M:%SZ' ):
			# devo verificare che non ci sia settata la flag setNotifTime
			# in caso positivo devo verificare il valore della t_notifica
			# e se almeno uno dei canali ha la notifica minore di adesso 
			# metto il tutto in lista prepara
			if adesso > datetime.strptime( PrendiMinNotifica( value['SocialFlags']) ,  '%Y-%m-%dT%H:%M:%SZ' ):
				logger.debug( '-----------> : ' + key +' --> Ho data notifica minore di timeNow ')
				# devo preparare il video e in caso ci sia notifica mandarla
				da_fare_list_PREPARA[ key ] = value
				# la metto da_scrivere
				# ci sara' da fare qualcosa 
			else:
			
				logger.debug( '-----------> : ' + key +' -->  Ho data notifica MAGGIORE di timeNow ')
				da_fare_list_NOTIFICA[ key ] = value
				# devo mettere il video nel DB per poi prepararlo in seguito
				# la metto da_scrivere
		else:
			if adesso > datetime.strptime( value['SocialFlags'] ['t_end'],  '%Y-%m-%dT%H:%M:%SZ' ):
				# devo spegnere
				da_fare_list_OFF[ key ] = value
			else:
				if len(value['SocialFlags']['fb-livestreaming-account']) > 0:

					if 'VideoDetails' in value['SocialFlags']['fb-livestreaming-account'].values()[0]:
						if value['SocialFlags']['fb-livestreaming-account'].values()[0]['VideoDetails']['status'] == 'LIVE_STOPPED':
							continue
				else:
					logger.debug('QUI DOVREI AGGIUNGERE LA PREPARAZIONE avendo passato la data di inizio ma non essendo stato preparato ? \r\n')

				if len(value['SocialFlags']['yt-livestreaming-account']) > 0:
					if 'VideoDetails' in value['SocialFlags']['yt-livestreaming-account'].values()[0]:
						if value['SocialFlags']['yt-livestreaming-account'].values()[0]['VideoDetails']['LivebroadcastDetails']['status']['lifeCycleStatus'] == 'complete':
							continue
				else:
					logger.debug('QUI DOVREI AGGIUNGERE LA PREPARAZIONE avendo passato la data di inizio ma non essendo stato preparato ? \r\n')


				# devo accendere oppure e gia accesa
				da_fare_list_ON[ key ] = value
				# e cmq ci sara qualcosa da fare

		

        #spinner.stop()
	logger.info(' lunghezza lista PREPARA  : ' + str(len(da_fare_list_PREPARA)) + '\t\t\t\t--PREPARA')
	print (IDS( da_fare_list_PREPARA ) )
	print (List (da_fare_list_PREPARA ) )
	logger.info(' lunghezza lista ON  : ' + str(len(da_fare_list_ON)) + '\t\t\t\t\t--ON')
	print (IDS( da_fare_list_ON ) )
	print (List ( da_fare_list_ON ) )
	logger.info(' lunghezza lista OFF  : ' + str(len(da_fare_list_OFF)) + '\t\t\t\t\t--OFF')
	print (IDS( da_fare_list_OFF ) )
	print (List ( da_fare_list_OFF ) )
	logger.info(' lunghezza lista NOTIFICA  : ' + str(len(da_fare_list_NOTIFICA)) + '\t\t\t\t--NOTIFICA')
	print (IDS( da_fare_list_NOTIFICA ) )
	print (List ( da_fare_list_NOTIFICA ) )
	logger.info(' ----------------------- fine Controlla_Flag_Social -----------------------')
	return [ da_fare_list_PREPARA, da_fare_list_ON, da_fare_list_OFF, da_fare_list_NOTIFICA ]
		
		
def UpdateMonitorProd( id_monitor, html_body ):

	logger.debug(' ----------------------- inizia UpdateMonitor -----------------------')
	logger.debug(' su id =  ' + id_monitor )
	
	count = 0

	flagsdiritorno =  Cambia_MonitorProd( id_monitor, html_body )

	# CLADDDDDD #

	logger.debug(' tornato in UpdateMonitor da Cambia _Flag_Social ')
	logger.debug(PrendiState( flagsdiritorno, 'dummy' ))
	flagsdiritorno =  CambiaState(flagsdiritorno, "published")

	nome_file = os.environ['UPDATE_FILE']

	try:
		os.remove(nome_file)
		logger.debug(' remove finto')
	except:
		pass

	flagsdiritorno.write(nome_file)
	# brutto fix : cambiamo <html: e </html: direttamente nel file
	CambiaBodyFile( nome_file)

	Put_IdProd( id_monitor, nome_file)

	logger.debug(' ----------------------- fine UpdateMonitor CLAD -----------------------')
	return 
	
		
def UpdateMonitor( id_monitor, html_body ):

	logger.debug(' ----------------------- inizia UpdateMonitor -----------------------')
	logger.debug(' su id =  ' + id_monitor )
	
	count = 0

	flagsdiritorno =  Cambia_Monitor( id_monitor, html_body )

	# CLADDDDDD #

	logger.debug(' tornato in UpdateMonitor da Cambia _Flag_Social ')
	logger.debug(PrendiState( flagsdiritorno, 'dummy' ))
	flagsdiritorno =  CambiaState(flagsdiritorno, "published")

	nome_file = os.environ['UPDATE_FILE']
	try:
		os.remove(nome_file)
		logger.debug(' remove finto')
	except:
		pass

	flagsdiritorno.write(nome_file)
	# brutto fix : cambiamo <html: e </html: direttamente nel file
	CambiaBodyFile( nome_file)

	Put_Id( id_monitor, nome_file)

	logger.debug(' ----------------------- fine UpdateMonitor CLAD -----------------------')
	return 
		
	
		
def Update_Flag_Social( lista_content ):

	logger.debug(' ----------------------- inizia Update_Flag_Social -----------------------')
	logger.debug(' lunghezza lista iniziale : ' + str(len(lista_content)))
	
	result_list = []

	nome_file = os.environ['UPDATE_FILE']
	count = 0
	for lis in lista_content:
		logger.debug(' - ' + str(count) + ' -- ' + lis['id'][8:] + ' --- ')
		count = count + 1
		logger.debug(lis['social_flags'])

		# il valore di ritorno del forceResend DEVE SEMPRE essere a false
		lis['social_flags']['tw-forceResend'] = 'false'
		lis['social_flags']['fb-forceResend'] = 'false'

	
		flagsdiritorno =  Cambia_Flag_Social(lis['id'][8:], lis['social_flags'])

		# CLADDDDDD #

		logger.debug(' tornato in Update_Flag da Cambia _Flag_Social ')
		logger.debug(PrendiState( flagsdiritorno, 'dummy' ))
		flagsdiritorno =  CambiaState(flagsdiritorno, "published")

		try:
			os.remove(nome_file)
			logger.debug(' remove finto')
		except:
			pass

		flagsdiritorno.write(nome_file)
		# brutto fix : cambiamo <html: e </html: direttamente nel file
		CambiaBodyFile( nome_file)

		Put_Id( lis['id'][8:], nome_file)

	logger.debug(' ----------------------- fine Update CLAD Flag_Social -----------------------')
	return result_list
				
def putIdFromStr( idx, xmlStr ):

	print('------------------------ inizia putIdFromStr -------------- ')
	logger.debug('------------------------ inizia putIdFromStr -------------- ')
	try:
		auth = '%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])
		base64string = b64encode(auth.encode())
		base64string = base64string.decode("ascii")

		headers = { 'Authorization' : 'Basic %s' % base64string,
				'If-Match': '*',
				'Content-Type': 'application/atom+xml'
			}    

		dati = xmlStr
		dati = ( dati ).encode()
		link = os.environ['ECE_SERVER']  + str(idx)
		print ( link )
		logger.debug ( link )

		request = urllib.request.Request(url=link, data=dati, headers=headers, method='PUT')
		resultResponse = urllib.request.urlopen(request)

		print ( resultResponse.status )
		logger.debug ( resultResponse.status )
		print ( resultResponse.headers )
		logger.debug ( resultResponse.headers )
		#print ( resultResponse.headers.split('rsi.ch/webservice/escenic/content/' )[-1] )
		print ( resultResponse.reason )
		logger.debug ( resultResponse.reason )
		
	
	except Exception as e:
		print ( 'PROBLEMI in putIdFromStr : ' + str(e) )
		logger.debug ( 'PROBLEMI in putIdFromStr : ' + str(e) )
		return False

	logger.debug('------------------------ finisce putIdFromStr -------------- ')
	return True

		
def putId( idx, filename ):

	print('------------------------ inizia putId -------------- ')
	logger.debug('------------------------ inizia putId -------------- ')
	try:
		auth = '%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])
		base64string = b64encode(auth.encode())
		base64string = base64string.decode("ascii")

		headers = { 'Authorization' : 'Basic %s' % base64string,
				'If-Match': '*',
				'Content-Type': 'application/atom+xml'
			}    

		file = open(filename)
		dati = file.read()
		
		print( dati)
		dati = ( dati ).encode()
		print( dati)
		link = os.environ['ECE_SERVER']  + str(idx)
		print ( link )
		logger.debug ( link )


		request = urllib.request.Request(url=link, data=dati, headers=headers, method='PUT')
		resultResponse = urllib.request.urlopen(request)

		print ( resultResponse.status )
		logger.debug ( resultResponse.status )
		print ( resultResponse.headers )
		logger.debug ( resultResponse.headers )
		#print ( resultResponse.headers.split('rsi.ch/webservice/escenic/content/' )[-1] )
		print ( resultResponse.reason )
		logger.debug ( resultResponse.reason )
		
	
	except Exception as e:
		print ( 'PROBLEMI in putId : ' + str(e) )
		logger.debug ( 'PROBLEMI in putId : ' + str(e) )
		return False

	logger.debug('------------------------ finisce putId -------------- ')
	return True
		
def putToState( eceId, state ):

	print('------------------------ inizia putToState -------------- ')
	logger.debug('------------------------ inizia putToState -------------- ')
	successGetId = -1
	try:
		[ successGetId, programmeEceId  ] = getEceId(  eceId )



		nome_file = os.environ['UPDATE_FILE']
		print ( nome_file)
		tree4Ece.write(nome_file)
		if not putId( eceId, nome_file):
			raise Exception("Errore in putId")	
		else:
			print ( " updatato : " + eceId )

	
	except Exception as e:
		print ( 'PROBLEMI in putToState : ' + str(e) )
		logger.debug ( 'PROBLEMI in putToState : ' + str(e) )
		return False

	logger.debug('------------------------ finisce putToState -------------- ')
	print('------------------------ finisce putToState -------------- ')
	return True



def postLockId( idx ):

	print('------------------------ inizia postLockId -------------- ')
	try:
		auth = '%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])
		base64string = b64encode(auth.encode())
		base64string = base64string.decode("ascii")

		headers = { 'Authorization' : 'Basic %s' % base64string,
				'If-Match': '*',
				'Content-Type': 'application/atom+xml'
			}    

		file = open(os.environ['LOCK_RESOURCE_DIR'] + os.environ['LOCK_NAME'])
		dati = file.read()
		dati = ( dati ).encode()
		link = os.environ['LOCK_URL']  + str(idx)
		print ( link )

		request = urllib.request.Request(url=link, data=dati, headers=headers, method='POST')
		resultResponse = urllib.request.urlopen(request)

		print ( resultResponse.status )
		print ( resultResponse.headers )
		#print ( resultResponse.headers.split('rsi.ch/webservice/escenic/content/' )[-1] )
		# in location ho una url tipo : 
		# http://internal.publishing.staging.rsi.ch/webservice/escenic/lock/article/13080332/private/3pf5fm
		print ( resultResponse.reason )
		
	
	except Exception as e:
		print ( 'PROBLEMI in postLockId : ' + str(e) )
		return False

	logger.debug('------------------------ finisce postLockId -------------- ')
	return True

		
def deleteId( idx ):

	print('------------------------ inizia deleteId -------------- ')
	logger.debug('------------------------ inizia deleteId -------------- ')
	try:
		auth = '%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])
		base64string = b64encode(auth.encode())
		base64string = base64string.decode("ascii")

		headers = { 'Authorization' : 'Basic %s' % base64string,
				'If-Match': '*',
				'Content-Type': 'application/atom+xml'
			}    

		link = os.environ['ECE_SERVER']  + str(idx)
		print ( link )
		logger.debug ( link )

		request = urllib.request.Request(url=link, headers=headers, method='DELETE')
		resultResponse = urllib.request.urlopen(request)

		print ( resultResponse.status )
		logger.debug ( resultResponse.status )
		print ( resultResponse.headers )
		logger.debug ( resultResponse.headers )
		print ( resultResponse.reason )
		logger.debug ( resultResponse.reason )
		
	
	except Exception as e:
		logger.debug ( 'PROBLEMI in deleteId : ' + str(e) )
		print ( 'PROBLEMI in deleteId : ' + str(e) )
		return False

	logger.debug('------------------------ finisce deleteId -------------- ')
	return True






def flatten(d, parent_key='', sep='_'):
	items = []
	for k, v in d.items():
		new_key = parent_key + sep + k if parent_key else k
		try:
			items.extend(flatten(v, new_key, sep=sep).items())
		except:
			items.append((new_key, v))

	return dict(items)

def flatterRundown( jsonRundown ):
	# funzione che fa il parsing dell xml del rundown e ne restituisce un json con
	# i campi e i loro valori
	# quando poi si passera al json si presume che venga uguale
	result = []

	for jrun in jsonMpRundown:
		try:
			#print ( jsonMpRundown )
			result.append(flatten(jrun))
		except:
			logger.warning("problemi in flatterMpRundown")
			return []

	return result
		
def mapMptoEce( jsonFlatRundown, fieldsMamProgramme ):

	print ( '------------------------ INIT mapMptoEce -------------- ' )
	logger.debug ( '------------------------ INIT mapMptoEce -------------- ' )
	count = 1
	newDict = {}

	for lis,value in fieldsMamProgramme.items():
		#print ( lis,value )

		# questa qui sotto trova OGNI occorrenza della sottostringa lis nelle chiavi
		# del json mpJsonFlatRundown - anche forse troppo per quello che ci serve
		# res = dict(filter(lambda item: lis in item[0], mpJsonFlatRundown.items())) 
		# print (ing result   )
		# print (("Key-Value pair for substring keys : " + str(res))  )

		# come workaround facciamo che aggiungere schedule_broadcast_ a tutte le chiavi
		# per vedere se ci sono nell'altro

		if lis in jsonFlatRundown:
			#print ( jsonFlatRundown[ lis ], type( jsonFlatRundown[lis]) )
			if jsonFlatRundown[ lis ] is None or ( isinstance( jsonFlatRundown[lis], str )  and len(jsonFlatRundown[ lis ] ) < 1 ): 
				newDict[ value ] = ''
			else:
				# il casino sui tipi qui sotto perche ECE accetta solo stringhe
				if isinstance( jsonFlatRundown[ lis ] ,str):
					# aggiunto controllo per evitare passaggi di string = null
					if 'null' in jsonFlatRundown[ lis ] and len(jsonFlatRundown[ lis ] )<5:
						newDict[value] = ''
					else:
						newDict[ value ] = jsonFlatRundown[ lis ] 
				else:
					if isinstance( jsonFlatRundown[ lis ] , bool):
						if jsonFlatRundown[ lis ] :
							#print ( 'bool' )
							newDict[ value ] = 'true'
						else:
							newDict[ value ] = 'false'
					else:
						if isinstance( jsonFlatRundown[ lis ]  ,int):
							#print ( 'int' )
							newDict[ value ] = str( jsonFlatRundown[lis ])										
						else:
							if isinstance( jsonFlatRundown[ lis ]  ,dict):
								#print ( 'dic --------------------t' )
								tmpval = str( jsonFlatRundown[lis ])										
								tmpval =  tmpval.replace('True','true').replace('False','false') 
								newDict[ value ] =  tmpval
		else:
			# per evitare che passino i valori di default
			newDict[value] = ''

		print ( lis +' ::' + newDict[ value ]  )
		logger.debug( lis  +' :: ' + newDict[ value ] )
	# prendo anche i valori per la section giusta passando channel 
	# e serie_titlepress a prendiSectionId
	#print (newDict)
	#logger.debug (newDict)
	#if '__DA_PASSARE_A_BRAND__' in newDict:
		#print ( newDict['__DA_PASSARE_A_BRAND__'] )
	#if '__CHANNEL_MAMPROGRAMME__' in newDict:
		#print ( newDict['__CHANNEL_MAMPROGRAMME__'].lower())

	logger.debug ( '------------------------ END mapMptoEce -------------- ' )
	print ( '------------------------ END mapMptoEce -------------- ' )
	return newDict

def pulisciXml( xmlStr ):

	# per togliere dalle balle le inutili intestazioni ( gia presenti nelle labels )
	# e avere gli id puliti
	# e cambiare i loghi
	result = ""

	daTogliere = {
		'rsi:escenic:mamProgramme:' : '',
		'rsi:escenic:programme:' : '',
		'rsi:escenic:mamProgrammeVideo:' : '',
		'rsi:escenic:programmeVideo:' : '',
		'rsi:escenic:mamTranscodableVideo:' : '',
		'rsi:escenic:transcodableVideo:' : '',
		'rsi:escenic:mamTranscodableAudio:' : '',
		'rsi:escenic:transcodableAudio:' : '',
		'rsi:escenic:mamSegmentedProgrammeVideo:' : '',
		'rsi:escenic:segmentedProgrammeVideo:' : '',
		'LOGO ROSSO' : 'logoRosso',
		'LOGO GIALLO' : 'logoGiallo',
		'RSI La1 - TG 20:00 ' : '',
		'RSI La1 - TG 12:30 ' : '',
		'RSI La1 - QUOTIDIANO ESTATE ' : '',
		'RSI La1 - QUOTIDIANO ' : '',
		'&' : '&amp;'
	}
	
	daTogliereOk = {
		'rsi:mp:louise:' : '',
		'rsi:mp:playlist:' : '',
		'rsi:mam:video:' : '',
		'rsi:mam:audio:' : '',
		'rsi:escenic:mamProgramme:' : '',
		'rsi:escenic:programme:' : '',
		'rsi:escenic:mamProgrammeVideo:' : '',
		'rsi:escenic:programmeVideo:' : '',
		'rsi:escenic:mamTranscodableVideo:' : '',
		'rsi:escenic:transcodableVideo:' : '',
		'rsi:escenic:mamTranscodableAudio:' : '',
		'rsi:escenic:transcodableAudio:' : '',
		'rsi:escenic:mamSegmentedProgrammeVideo:' : '',
		'rsi:escenic:segmentedProgrammeVideo:' : '',
		'LOGO ROSSO' : 'logoRosso',
		'LOGO GIALLO' : 'logoGiallo',
		'RSI La1 - TG 20:00 ' : '',
		'RSI La1 - TG 12:30 ' : '',
		'RSI La1 - QUOTIDIANO ESTATE ' : '',
		'RSI La1 - QUOTIDIANO ' : '',
		'&' : '&amp;'
	}
	
	try:
		# use these three lines to do the replacement
		rep = dict((re.escape(k), v) for k, v in daTogliere.items()) 
		pattern = re.compile("|".join(rep.keys()))
		text = pattern.sub(lambda m: rep[re.escape(m.group(0))], xmlStr)
		result = text

	except Exception as e:
		logger.warning( 'Problemi in pulisciXml : ' + str(e) )
		pass

	return result

def replaceFielsTemplate( mapJsonMpToEce, templateType ):
	result = ""

	# apro il template opportuno 
	fin = codecs.open( os.environ['RESOURCE_DIR'] + "template_" + templateType + ".xml" , 'r', 'utf-8' )
	eceTemplate = fin.read()
	fin.close()
	
	try:
		# use these three lines to do the replacement
		rep = dict((re.escape(k), v) for k, v in mapJsonMpToEce.items()) 
		pattern = re.compile("|".join(rep.keys()))
		text = pattern.sub(lambda m: rep[re.escape(m.group(0))], eceTemplate)
		#print ( text )
		result = text

	except Exception as e:
		logger.warning( 'Problemi in replaceFieldsTemplate : ' + str(e) )
		pass

	return result

def togliAccenti( accentata ):
	result = ''
	# qui dichiaro cosa devo cambiare
	repl = str.maketrans(
		"àáâãäèéêëìíîïòóôöùúûü",
		"aaaaaeeeeiiiioooouuuu"
	)

	try :
		result = accentata.translate(repl)
	except Exception as e:
		logger.warning( 'Problemi in togliAccenti : ' + str(e) )
		pass

	return result

def prendiHomeSectionDaProgramme( tree ):
	print ( '------------------------ INIT prendiHomeSectionDaProgramme -------------- ' )
	result = -1
	try:
		link = PrendiLinkRelated( tree, "home-section" )
		if not link  is None:
			#print ( link)
			#print ( link.attrib['href'].split('section/')[-1])
			# da mettere un po di controllo errori ....
			result =  link.attrib['href'].split('section/')[-1]

	except Exception as e:
		print ( 'PROBLEMI in prendiHomeSectionDaProgramme : ' + str(e) )
		return result

	print ( '------------------------ END prendiHomeSectionDaProgramme -------------- ' )
	return result

def prendiSectionDaProgramme( tree ):
	print ( '------------------------ INIT prendiSectionDaProgramme -------------- ' )
	# nuova versione che prende i valori dal padre per ricalcolarsela
	# per sistemare robe tipo falo->falo estate ....
	jsonForSection = {}
	
	result = -1

	tmpVal = prendiField( tree, 'channel')
	if tmpVal is None or len(tmpVal) < 1:
		return -1
	else:
		jsonForSection['channel'] = tmpVal

	tmpVal = prendiField( tree, 'parentSeries')
	if tmpVal is None or len(tmpVal) < 1:
		return -1
	else:
		jsonForSection['__DA_PASSARE_A_BRAND__'] = tmpVal

	tmpVal = prendiField( tree, 'episode_producttypedesc')
	if not (tmpVal is None ) and len(tmpVal) > 1:
		# questo non e' indispensabile
		# quindi se non c'e' non importa
		jsonForSection['__PRODUCTTYPEDESC_MAMPROGRAMME__'] = tmpVal

	result = prendiSectionId(jsonForSection)

	print ( '------------------------ END prendiSectionDaProgramme -------------- ' )
	return result



def prendiSectionId( jsonFlatMessage ):

	print ( '------------------------ INIT prendiSectionId -------------- ' )
	logger.debug ( '------------------------ INIT prendiSectionId -------------- ' )
	channel = 'la1'
	result = '15100'
	sectionName = ''
	productTypeDesc = ''

	print ( 'CLAD' )
	print ( jsonFlatMessage ) 

	if '__CHANNEL_MAMPROGRAMME__' in jsonFlatMessage:
		channel = jsonFlatMessage['__CHANNEL_MAMPROGRAMME__'].lower()
	if '__DA_PASSARE_A_BRAND__' in jsonFlatMessage:
		sectionName = jsonFlatMessage['__DA_PASSARE_A_BRAND__']
	if '__PRODUCTTYPEDESC_MAMPROGRAMME__' in jsonFlatMessage:
		productTypeDesc = jsonFlatMessage['__PRODUCTTYPEDESC_MAMPROGRAMME__']

	print ('channel : ' +  channel )
	print ('sectionName : ' +  sectionName )
	print ('productTypeDesc : ' +  productTypeDesc )

	# piccolo fix per TG Lis che deve andare nella section 22357 
	# Telegiornale lingua dei segni FIX
	if 'web1' in channel:
		if 'Telegiornale Lingua dei segni' in jsonFlatMessage['title']:
			print('FIX Telegiornale lingua dei segni')
			logger.debug('FIX Telegiornale lingua dei segni')
			return '22357'
	
	# metto i valori di default nel caso in cui il servizio di riconoscimento
	# sections mi restituisca 0
	#print ( 'CLAD' )
	#print ('channel : ' +  channel )

	if 'la1' in channel:
		#print( 'passo da la1')
		if 'Telefilm' in productTypeDesc:
			result = '14779'
		else:
			if 'Film' in productTypeDesc:
				result = '4127'
			else:
				# questo e id di altri programmi per La1
				result = '15097'
		
	else:
		#print( 'passo da la2')
		if 'Telefilm' in productTypeDesc:
			result = '14782'
		else:
			if 'Film' in productTypeDesc:
				result = '4553'
			else:
				# questo e id di altri programmi per La2
				result = '15100'

	#print ( 'CLAD  ' + result )

	# funzione per prendere il numero di sezione partendo dal nome 
	# trovato nel serie- titlepress
	# modificato come segue:
	# tutti i caratteri devono essere trasformati in minuscolo
	brand = sectionName.lower()
	# replace dei caratteri accentati con i caratteri semplici. es. Falò --> brand=falo**
	brand = togliAccenti( brand )
	# replace degli spazi con "-" es: Criminal minds --> brand=criminal-minds
	brand = brand.replace(' ', '-')
	# replace dei caratteri speciali con "-". es Grey's anatomy --> brand = grey-s-anatomy
	brand = brand.replace('\'','-').replace('\"', '-')
	print ( brand )
	logger.debug ( brand )
	

	# CLAD _DEBUG
	#brand = 'via-col-venti'
	#channel = 'la1'
	# e quindi devo passarla come parametro alla 
	# http://internal.publishing.rsi.ch/webservice-extensions/srg/sectionIdForBrand/?publicationName=rsi&channel=la1&brand=via-col-venti
	
	auth = '%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])
	base64string = b64encode(auth.encode())
	base64string = base64string.decode("ascii")

	link = os.environ['ECE_BRAND'] 
	# in cui devo poi rimpiazzare __BRAND__ e __CHANNEL__
	link = link.replace('__CHANNEL__', channel.lower()).replace('__BRAND__',brand)
	print ( link )
	logger.debug ( link )
	
	headers = { 'Authorization' : 'Basic %s' %  base64string }

	try:
		request = urllib.request.Request(link, headers=headers)
		resultResponse = urllib.request.urlopen(request)

		tree = ET.parse(resultResponse)
		#print ( ET.tostring( tree.getroot() ))
		list = tree.getroot().findall( namespaces['atom'] + "id" )
		#print  (list[0].text)
		#print  (list[0].text.split('.rsi.ch:8080/webservice/escenic/section/')[-1])
		resultRequest = list[0].text.split('.rsi.ch:8080/webservice/escenic/section/')[-1]
		# questa ritorna 0 se non aveva una section corrispondente a quel nome
		if len(resultRequest) > 1:
			# mi ha restituito un id valido di sezione e lo metto 
			# e lo metto in result al posto dell id di altri programmi
			result = resultRequest
			print ( 'result di prendiSectionId = ' + resultRequest )
			logger.debug ( 'result di prendiSectionId = ' + resultRequest )
			
	except Exception as e:
		print ( 'PROBLEMI in prendiSection : ' + str(e) )
		logger.debug ( 'PROBLEMI in prendiSection : ' + str(e) )
		return result
		
	print ( '------------------------ END prendiSectionId -------------- ' )
	logger.debug ( '------------------------ END prendiSectionId -------------- ' )
	return result


def prendiSectionIdOld( mapJsonMpToEce ):

	print ( '------------------------ INIT prendiSectionId -------------- ' )
	logger.debug ( '------------------------ INIT prendiSectionId -------------- ' )
	channel = 'la1'
	result = '15100'
	sectionName = ''
	productTypeDesc = ''

	print ( 'CLAD' )
	print ( mapJsonMpToEce ) 
	exit(0)

	if '__CHANNEL_MAMPROGRAMME__' in mapJsonMpToEce:
		channel = mapJsonMpToEce['__CHANNEL_MAMPROGRAMME__'].lower()
	if '__CHANNEL_MAMSEGMENTVIDEO__' in mapJsonMpToEce:
		channel = mapJsonMpToEce['__CHANNEL_MAMSEGMENTVIDEO__'].lower()
	if '__DA_PASSARE_A_BRAND__' in mapJsonMpToEce:
		sectionName = mapJsonMpToEce['__DA_PASSARE_A_BRAND__']
	if '__PRODUCTTYPEDESC_MAMPROGRAMME__' in mapJsonMpToEce:
		productTypeDesc = mapJsonMpToEce['__PRODUCTTYPEDESC_MAMPROGRAMME__']

	#print ( channel )
	#print ( sectionName )
	#print ( productTypeDesc )
	
	# metto i valori di default nel caso in cui il servizio di riconoscimento
	# sections mi erstituisca 0
	if 'la1' in channel:
		if 'Episode' in productTypeDesc:
			result = '14779'
		else:
			if 'Film' in productTypeDesc:
				result = '4127'
			else:
				# questo e id di altri programmi per La1
				result = '15097'
		
	else:
		if 'Episode' in productTypeDesc:
			result = '14782'
		else:
			if 'Film' in productTypeDesc:
				result = '4553'
			else:
				# questo e id di altri programmi per La2
				result = '15100'


	# funzione per prendere il numero di sezione partendo dal nome 
	# trovato nel serie- titlepress
	# modificato come segue:
	# tutti i caratteri devono essere trasformati in minuscolo
	brand = sectionName.lower()
	# replace dei caratteri accentati con i caratteri semplici. es. Falò --> brand=falo**
	brand = togliAccenti( brand )
	# replace degli spazi con "-" es: Criminal minds --> brand=criminal-minds
	brand = brand.replace(' ', '-')
	# replace dei caratteri speciali con "-". es Grey's anatomy --> brand = grey-s-anatomy
	brand = brand.replace('\'','-').replace('\"', '-')
	print ( brand )
	logger.debug ( brand )
	

	# CLAD _DEBUG
	#brand = 'via-col-venti'
	#channel = 'la1'
	# e quindi devo passarla come parametro alla 
	# http://internal.publishing.rsi.ch/webservice-extensions/srg/sectionIdForBrand/?publicationName=rsi&channel=la1&brand=via-col-venti
	
	auth = '%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])
	base64string = b64encode(auth.encode())
	base64string = base64string.decode("ascii")

	link = os.environ['ECE_BRAND'] 
	# in cui devo poi rimpiazzare __BRAND__ e __CHANNEL__
	link = link.replace('__CHANNEL__', channel.lower()).replace('__BRAND__',brand)
	print ( link )
	logger.debug ( link )
	
	headers = { 'Authorization' : 'Basic %s' %  base64string }

	try:
		request = urllib.request.Request(link, headers=headers)
		resultResponse = urllib.request.urlopen(request)

		tree = ET.parse(resultResponse)
		#print ( ET.tostring( tree.getroot() ))
		list = tree.getroot().findall( namespaces['atom'] + "id" )
		#print  (list[0].text)
		#print  (list[0].text.split('.rsi.ch:8080/webservice/escenic/section/')[-1])
		resultRequest = list[0].text.split('.rsi.ch:8080/webservice/escenic/section/')[-1]
		# questa ritorna 0 se non aveva una section corrispondente a quel nome
		if len(resultRequest) > 1:
			# mi ha restituito un id valido di sezione e lo metto 
			# e lo metto in result al posto dell id di altri programmi
			result = resultRequest
			print ( 'result = ' + resultRequest )
			logger.debug ( 'result = ' + resultRequest )
			
	except Exception as e:
		print ( 'PROBLEMI in prendiSection : ' + str(e) )
		logger.debug ( 'PROBLEMI in prendiSection : ' + str(e) )
		return result
		
	print ( '------------------------ END prendiSectionId -------------- ' )
	logger.debug ( '------------------------ END prendiSectionId -------------- ' )
	return result

#################################################
# 		GESTISCI			#
#################################################

def gestisciCreateUpdateTest( message, consumerPtr ):

	print ( '------------------------ inizia gestisciTest -------------- ' )
	# questo mi ritorna tutti gli xml gia' buoni da buttare su ECE
	print( consumerPtr.topic )
	print( consumerPtr.ecetypes[ str(list(consumerPtr.consumer.subscription() )[0] ) ] )
	resultBool = False
	resultJson = {}

	try:
		# metto nel message il contentType che sto creando
		message['contentType'] =  consumerPtr.ecetypes[ str(list(consumerPtr.consumer.subscription() )[0] ) ]

		# prendo il dict e verifico se contiene il campo cms
		# Delete Transcodable  programme keyframe
		if  ( 'cms' in message  and len( message['cms']) > 0 and 
			( 'programme' == message['contentType'] or 
			( 'programmeVideo' == message['contentType'] or
				'segmentedProgrammeVideo'  == message['contentType'] or
				'transcodableVideo'  == message['contentType'] or
				'transcodableAudio'  == message['contentType'] )  and 'asset' in message['cms'] )) :
				[ resultBool, resultJson ] = consumerPtr.ecefunction[ str(list(consumerPtr.consumer.subscription() )[0] ) ][ _UPDATE_FUNC_ ]( message, consumerPtr )
				print( 'FATTO UPDATE !! ')
		else:
			# oppure non lo contiene oppure == {} 
			# e allora parto con la produciProgrammeDaKafka
			[ resultBool, resultJson ] = consumerPtr.ecefunction[ str(list(consumerPtr.consumer.subscription() )[0] ) ][ _CREATE_FUNC_ ]( message, consumerPtr )
			
	except Exception as e:
		print ( 'PROBLEMI in gestisciProgrammeCreateUpdateMessageDaKafka : ' + str(e) )
		return [ False, { 'error' : str(e) } ]
		
	return [ resultBool, resultJson ] 



def gestisciDeleteMessageDaKafka( message ):

	print ( '------------------------ inizia gestisciDeleteMessageDaKafka -------------- ' )
	logger.debug ( '------------------------ inizia gestisciDeleteMessageDaKafka -------------- ' )
	# questo mi ritorna tutti gli xml gia' buoni da buttare su ECE
	resultBool = False
	resultJson = {}

	try:
		# prendo il dict e verifico se contiene il campo cms
		if 'cms' in message  and len( message['cms']) > 0:
			# e nel caso faccio una update 
			resultBool = deleteEceDaKafka( message )
		else:
			# oppure non lo contiene oppure == {} 
			resultBool = False
			
	except Exception as e:
		print ( 'PROBLEMI in gestisciDeleteMessageDaKafka : ' + str(e) )
		return [ False, { 'error' : str(e) } ]
		
	return [ resultBool, resultJson ] 


def gestisciCreateUpdateProgrammeVideoMessageDaKafka( message ):

	print ( '------------------------ inizia gestisciProgrammeVideoCreateUpdateMessageDaKafka -------------- ' )
	logger.debug ( '------------------------ inizia gestisciProgrammeVideoCreateUpdateMessageDaKafka -------------- ' )
	# questo mi ritorna tutti gli xml gia' buoni da buttare su ECE
	resultBool = False
	resultJson = {}

	try:
		# metto nel message il contentType che sto creando
		message['contentType'] = 'programmeVideo'

		# prendo il dict e verifico se contiene il campo cms
		if 'cms' in message  and len( message['cms']) > 0 and 'asset' in message['cms']:
			# e nel caso faccio una update 
			[ resultBool, resultJson ] = updateProgrammeVideoDaKafka( message )
		else:
			# oppure non lo contiene oppure == {} 
			# e allora parto con la produciProgrammeVideoDaKafka
			[ resultBool, resultJson ] = produciProgrammeVideoDaKafka( message )
			
	except Exception as e:
		print ( 'PROBLEMI in gestisciProgrammeVideoCreateUpdateMessageDaKafka : ' + str(e) )
		return [ False, { 'error' : str(e) } ]
		
	return [ resultBool, resultJson ] 


def gestisciCreateUpdateSegmentVideoMessageDaKafka( message ):

	print ( '------------------------ inizia gestisciSegmentVideoCreateUpdateMessageDaKafka -------------- ' )
	logger.debug ( '------------------------ inizia gestisciSegmentVideoCreateUpdateMessageDaKafka -------------- ' )
	# questo mi ritorna tutti gli xml gia' buoni da buttare su ECE
	resultBool = False
	resultJson = {}

	try:
		# metto nel message il contentType che sto creando
		message['contentType'] = 'segmentedProgrammeVideo'
		# prendo il dict e verifico se contiene il campo cms
		# prima era cosi if 'cms' in message  and len( message['cms']) > 0:
		# adesso esiste il campo asset in cms per fare  la update
		if 'cms' in message  and 'asset' in message['cms']:
			# e nel caso faccio una update 
			[ resultBool, resultJson ]  = updateSegmentVideoDaKafka( message )
		else:
			# oppure non lo contiene oppure == {} 
			# e allora parto con la produciSegmentVideoDaKafka
			[ resultBool, resultJson ] = produciSegmentVideoDaKafka( message )
			
	except Exception as e:
		print ( 'PROBLEMI in gestisciSegmentVideoCreateUpdateMessageDaKafka : ' + str(e) )
		return [ False, { 'error' : str(e) } ]
		
	return [ resultBool, resultJson ] 


def gestisciCreateUpdateTranscodableVideoMessageDaKafka( message ):

	print ( '------------------------ inizia gestisciCreateUpdateTranscodableVideoMessageDaKafka -------------- ' )
	logger.debug ( '------------------------ inizia gestisciCreateUpdateTranscodableVideoMessageDaKafka -------------- ' )
	# questo mi ritorna tutti gli xml gia' buoni da buttare su ECE
	resultBool = False
	resultJson = {}

	try:
		# metto nel message il contentType che sto creando
		message['contentType'] = 'transcodableVideo'

		# prendo il dict e verifico se contiene il campo cms
		if 'cms' in message  and len( message['cms']) > 0:
			# e nel caso faccio una update 
			[ resultBool, resultJson ]  = updateTranscodableVideoDaKafka( message )
		else:
			# oppure non lo contiene oppure == {} 
			# e allora parto con la produciTranscodableDaKafka
			[ resultBool, resultJson ] = produciTranscodableVideoDaKafka( message )
			
	except Exception as e:
		print ( 'PROBLEMI in gestisciCreateUpdateTranscodableVideoMessageDaKafka : ' + str(e) )
		return [ False, { 'error' : str(e) } ]
		
	return [ resultBool, resultJson ] 


def gestisciCreateUpdateTranscodableAudioMessageDaKafka( message ):

	print ( '------------------------ inizia gestisciCreateUpdateTranscodableAudioMessageDaKafka -------------- ' )
	logger.debug ( '------------------------ inizia gestisciCreateUpdateTranscodableAudioMessageDaKafka -------------- ' )
	# questo mi ritorna tutti gli xml gia' buoni da buttare su ECE
	resultBool = False
	resultJson = {}

	try:
		# metto nel message il contentType che sto creando
		message['contentType'] = 'transcodableAudio'

		# prendo il dict e verifico se contiene il campo cms
		if 'cms' in message  and len( message['cms']) > 0:
			# e nel caso faccio una update 
			[ resultBool, resultJson ]  = updateTranscodableAudioDaKafka( message )
		else:
			# oppure non lo contiene oppure == {} 
			# e allora parto con la produciTranscodableDaKafka
			[ resultBool, resultJson ] = produciTranscodableAudioDaKafka( message )
			
	except Exception as e:
		print ( 'PROBLEMI in gestisciCreateUpdateTranscodableAudioMessageDaKafka : ' + str(e) )
		return [ False, { 'error' : str(e) } ]
		
	return [ resultBool, resultJson ] 




def gestisciCreateUpdateProgrammeMessageDaKafka( message ):

	print ( '------------------------ inizia gestisciProgrammeCreateUpdateMessageDaKafka -------------- ' )
	# questo mi ritorna tutti gli xml gia' buoni da buttare su ECE
	resultBool = False
	resultJson = {}

	try:
		# metto nel message il contentType che sto creando
		message['contentType'] = 'programme'

		# prendo il dict e verifico se contiene il campo cms
		if 'cms' in message  and len( message['cms']) > 0:
			# e nel caso faccio una update 
			[ resultBool, resultJson ] = updateProgrammeDaKafka( message )
			print( 'FATTO UPDATE !! ')
		else:
			# oppure non lo contiene oppure == {} 
			# e allora parto con la produciProgrammeDaKafka
			[ resultBool, resultJson ] = produciProgrammeDaKafka( message )
			
	except Exception as e:
		print ( 'PROBLEMI in gestisciProgrammeCreateUpdateMessageDaKafka : ' + str(e) )
		return [ False, { 'error' : str(e) } ]
		
	return [ resultBool, resultJson ] 



def gestisciCreateUpdateKeyframeMessageDaKafka( message ):

	print ( '------------------------ inizia gestisciKeyframeCreateUpdateKeyframeMessageDaKafka -------------- ' )
	# questo mi ritorna tutti gli xml gia' buoni da buttare su ECE
	resultBool = False
	resultJson = {}

	try:
		# metto nel message il contentType che sto creando
		message['contentType'] = 'keyframe'

		# prendo il dict e verifico se contiene il campo cms
		if 'cms' in message  and len( message['cms']) > 0:
			# e nel caso faccio una update 
			[ resultBool, resultJson ] = updateKeyframeDaKafka( message )
			print( 'FATTO UPDATE !! ')
			exit(0)
		else:
			# oppure non lo contiene oppure == {} 
			# e allora parto con la produciKeyframeDaKafka
			[ resultBool, resultJson ] = produciKeyframeDaKafka( message )
			
	except Exception as e:
		print ( 'PROBLEMI in gestisciCreateUpdateKeyframeMessageDaKafka : ' + str(e) )
		return [ False, { 'error' : str(e) } ]
		
	print ( '------------------------ END gestisciKeyframeCreateUpdateKeyframeMessageDaKafka -------------- ' )
	return [ resultBool, resultJson ] 

#################################################
# 		GESTISCI			#
##################END############################

def returnFalse():
	return [ False, {} ] 

def getEceIdFromRelation( jsonIn ):
	result = None
	print( jsonIn )
	exit(0)
	if 'relationData' in jsonIn and 'chiave' in jsonIn['relationData']:
		result =  jsonIn['relationData']['chiave'].split(':')[-1]
		print (result)


	return result


def chiamaPubblica( resultJson ):

	print ( '------------------------ inizia chiamaPubblica -------------- ' )
	result = False
	# questa deve prendere l'ECE id da resultJson
	# e passarlo all url del servizio di pubblicazione
	if 'eceId' in resultJson:
		eceId = resultJson['eceId']
	print ( 'eceIOd = ' + eceId )
	if eceId  is None:
		return result
	try:
		link = os.environ['PUBLISH_ECE'] + eceId
		print( 'link : ' + link  )
		
		request = urllib.request.Request(url=link, method='POST')
		resultResponse = urllib.request.urlopen(request)
		print ( resultResponse.status )
	
	except Exception as e:
		print( 'PROBLEMI in chiamaPubblica: ' + str(e)  )
		return False

	print ( '------------------------ END chiamaPubblica -------------- ' )
	return result

def getDurationFromAws( awsJson ):
	result = ''
	if 'transcodedAssets' in awsJson and len(awsJson['transcodedAssets']) > 0 :
		result = awsJson['transcodedAssets'][0]['duration']
		result = str(int(result)/1000.0)

	return result


def getDurationFromTv( tVJson ):
	result = ''
	if 'variants' in tVJson and len(tVJson['variants']) > 0 :
		result = tVJson['variants'][0]['duration']
		result = str(int(result)/1000.0)

	return result




def getEceIdFromCmsJson( cmsJson ):
	print ( '------------------------ inizia getEceIdFromCmsJson -------------- ' )
	logger.debug ( '------------------------ inizia getEceIdFromCmsJson -------------- ' )
	result=[ None, None ]

	try:
		# prendo Ece id dal json
		# "urn":"urn:rsi:escenic:

		if  not 'urn' in cmsJson or len(cmsJson['urn']) < 17:
			print ( 'in getEceIdFromCmsJson non esiste cms[urn]')
			logger.debug ( 'in getEceIdFromCmsJson non esiste cms[urn]')
			return [ None, None ]

		eceId = cmsJson['urn'].split(':')[-1]
		link = os.environ['ECE_SERVER']  + eceId
		print( 'link : ' + link  )
		logger.debug( 'link : ' + link  )
		
		auth = '%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])
		base64string = b64encode(auth.encode())
		base64string = base64string.decode("ascii")

		headers = { 'Authorization' : 'Basic %s' % base64string,
				'Content-Type': 'application/atom+xml'
			}    

		request = urllib.request.Request(url=link, headers=headers, method='GET')
		resultResponse = urllib.request.urlopen(request)
		print ( resultResponse.status )
		logger.debug ( resultResponse.status )
		#print ( resultResponse.headers )
		#print ( resultResponse.headers )
		#print ( resultResponse.headers.split('rsi.ch/webservice/escenic/content/' )[-1] )
		#print ( resultResponse.reason )
		print ( resultResponse.getheader('Location') )
		logger.debug ( resultResponse.getheader('Location') )

		tree = ET.parse(resultResponse)
		print ( tree )
		logger.debug ( tree )
		result = tree
		#tree.write('newx.xml')
		#exit(0)
		#ET.dump(tree)
		#programmeId =  PrendiField( tree, "programmeId")
		#logger.debug(programmeId)

	except urllib.error.HTTPError as e:
		logger.debug(' EXCEPTIOOOONNNNNN - ritorno none !!!! ')
		return [ None, None ]

	print ( '------------------------ END getEceIdFromCmsJson -------------- ' )
	logger.debug ( '------------------------ END getEceIdFromCmsJson -------------- ' )

	return [ eceId, result ]


def getEceId( eceId ):
	print ( '------------------------ inizia getEceId -------------- ' )
	logger.debug ( '------------------------ inizia getEceId -------------- ' )
	result=[ False, None ]

	try:
		link = os.environ['ECE_SERVER']  + str(eceId)
		print( 'link : ' + link  )
		logger.debug( 'link : ' + link  )
		
		auth = '%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])
		base64string = b64encode(auth.encode())
		base64string = base64string.decode("ascii")

		headers = { 'Authorization' : 'Basic %s' % base64string,
				'Content-Type': 'application/atom+xml'
			}    

		request = urllib.request.Request(url=link, headers=headers, method='GET')
		resultResponse = urllib.request.urlopen(request)
		print ( resultResponse.status )
		logger.debug ( resultResponse.status )
		#print ( resultResponse.headers )
		#print ( resultResponse.headers )
		#print ( resultResponse.headers.split('rsi.ch/webservice/escenic/content/' )[-1] )
		#print ( resultResponse.reason )
		#print ( resultResponse.getheader('Location') )

		tree = ET.parse(resultResponse)
		#print ( tree )
		#print ( type(tree))
		result = tree
		#tree.write('newx.xml')
		#exit(0)
		#ET.dump(tree)

		#title =  PrendiField( tree, "title")
		#logger.debug(title)
		#programmeId =  PrendiField( tree, "programmeId")
		#logger.debug(programmeId)

	except urllib.error.HTTPError as e:
		print(' EXCEPTIOOOONNNNNN - ritorno none !!!! ')
		logger.debug(' EXCEPTIOOOONNNNNN - ritorno none !!!! ')
		return [ False, str(e) ]

	print ( '------------------------ END getEceId -------------- ' )
	logger.debug ( '------------------------ END getEceId -------------- ' )

	return [ True, result ]


def getEceLockId( eceId ):
	print ( '------------------------ inizia getEceLockId -------------- ' )
	result=[ False, None ]

	try:
		link = os.environ['LOCK_URL']  + str(eceId)
		print( 'link : ' + link  )
		
		auth = '%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])
		base64string = b64encode(auth.encode())
		base64string = base64string.decode("ascii")

		headers = { 'Authorization' : 'Basic %s' % base64string,
				'Content-Type': 'application/atom+xml'
			}    

		request = urllib.request.Request(url=link, headers=headers, method='GET')
		resultResponse = urllib.request.urlopen(request)
		print ( resultResponse.status )
		#print ( resultResponse.headers )
		#print ( resultResponse.headers )
		#print ( resultResponse.headers.split('rsi.ch/webservice/escenic/content/' )[-1] )
		#print ( resultResponse.reason )
		print ( resultResponse.getheader('Location') )

		tree = ET.parse(resultResponse)
		#print ( tree )
		result = tree
		#tree.write('newx.xml')
		#exit(0)
		#ET.dump(tree)

		#title =  PrendiField( tree, "title")
		#logger.debug(title)
		#programmeId =  PrendiField( tree, "programmeId")
		#logger.debug(programmeId)

	except urllib.error.HTTPError as e:
		logger.debug(' EXCEPTIOOOONNNNNN - ritorno none !!!! ')
		print(' EXCEPTIOOOONNNNNN - ritorno none !!!! ')
		return [ False, str(e) ]

	print ( '------------------------ END getEceLockId -------------- ' )

	return [ True, result ]


def prendiLock( entry ):
	rel = 'lock'

	list =  entry.findall(namespaces['atom'] + "link")
	
	#logger.debug(len(list))

	for idx , lis in enumerate(list):
		#logger.debug(idx, lis)
		#logger.debug(lis.attrib['rel'])
		if rel in lis.attrib['rel']:
			#logger.debug(lis.attrib['href'])
			#logger.debug(lis.attrib['title'])
			
			return lis.attrib['href']
	return None


def lockEceId( eceId ):

	print ( '------------------------ INIT lockEceId -------------- ' )
	result = ''

	# i passi per lockare una risorsa sono
	# 1. GET the resource you want to lock.
	eceOk = False
	[ eceOk, eceTree ] = getEceId( eceId )
	# in eceTree ho l albero parsificato del content richiesto
	
	if  not eceOk:
		return False
	# trovo la sua lock uri
	lockUri = prendiLock( eceTree )
	print ( lockUri )
	'''
	# 2. Optionally, GET the resource's public locks (if any) from the resource's lock collection URI.
	pubLockOk = False
	[ pubLockOk, pubLock ] = getEceLockId( eceId )
	if not pubLockOk :
		return False
	# qui ho l xml parsato dei lock 
	print ( pubLock )
	'''
	# 3. If the resource's existing public locks do not conflict with the lock you require, POST a lock request to the lock collection URI. If successful, the web service returns a private lock URI.



	# 4. PUT the modified version of the resource. In order for the PUT request to succeed you must include the private lock URI in its HTTP header.
	# 5. DELETE the private lock URI in order to release your lock.


	print ( '------------------------ END lockEceId -------------- ' )
	return result

def creaEntryDateExpires( entry, value ):

	# scritta perche' altrimenti non trovando la entry expires
	# non avremmo fatto la update
	# invece cosi aggiungiamo la entry 
	exp = ET.Element(namespaces['age'] + "expires")
	exp.text = value

	root = entry.getroot(  )
	root.insert( 1, exp )

	#entry.write('test_expires.xml')
	 
	return entry

def cambiaDateExpires( entry , dummyRel, value):

	list =  entry.findall(namespaces['age'] + "expires")
	if len( list ) < 1:
		# non ho trovato la entry : la creo io
		creaEntryDateExpires( entry, value )
	else:
		for lis in list:
			print  (lis.text )
			lis.text = value
	return entry

def prendiTitle( entry ):

	result = ''
	list =  entry.findall(namespaces['atom'] + "title")
	for lis in list:
		date_time_obj = datetime. strptime(lis.text, "%Y-%m-%dT%H:%M:%S.%fZ")
		return str(int(datetime.timestamp(date_time_obj)))
	
	return result


def prendiStartTimeInMs( entry ):

	result = ''
	result = prendiField( entry, 'startTimeInMs')
	return result




def prendiDateUpdate( entry ):

	result = ''
	list =  entry.findall(namespaces['atom'] + "updated")
	for lis in list:
		return lis.text
	
	return result



def cambiaDateUpdate( entry , dummyRel, value):

	list =  entry.findall(namespaces['atom'] + "updated")
	for lis in list:
		print  (lis.text )
		lis.text = value
	return entry


def cambiaField( entry , rel, value):

	list =  entry.findall(namespaces['atom'] + "content")
	for lis in list:
		payload =  lis.findall(namespaces['vdf'] + "payload")
		for pay in payload:
			fields = pay.findall(namespaces['vdf'] + "field")
			#logger.debug(fields)

			for idx , fiel in enumerate(fields):
				#logger.debug(idx, fiel)
				#logger.debug(fiel.attrib['name'])
				if rel in fiel.attrib['name'] and len( rel) == len(fiel.attrib['name']):
					if fiel.find(namespaces['vdf'] + "value") is None:
						fiel.append(ET.Element(namespaces['vdf'] + "value"))
					fiel.find(namespaces['vdf'] + "value").text = value
					#logger.debug(fiel.attrib['name'], fiel.find(namespaces['vdf'] + "value").text)
					return entry

	return entry

def replaceDateFields( tree, mapJsonMpToEce ):

	print ( '------------------------ INIT replaceDateFields -------------- ' )
	result = tree
	try: 
		if '__ECE_AGE_EXPIRES__' in mapJsonMpToEce and len(mapJsonMpToEce['__ECE_AGE_EXPIRES__']) > 0:
			# devo cambiare il campo in fondo al xml
			# prendendo il valore dal json che al momnento e' nella forma 
			# <age:expires>4000-01-01T12:40:06Z</age:expires>
			print ( '__ECE_AGE_EXPIRES__ : ' )
			eceExpires = mapJsonMpToEce['__ECE_AGE_EXPIRES__']
			eceExpires = eceExpires.split('<age:expires>')[-1].split('</age:expires>')[0]
			tmptree = cambiaDateExpires( tree, 'dummyexpires', eceExpires )
			tree = tmptree

		if '__ECE_UPDATED__' in mapJsonMpToEce and len(mapJsonMpToEce['__ECE_UPDATED__']) > 0: 
			print ( '__ECE_UPDATED__ : ' )

			# devo cambiare il campo in fondo al xml
			# prendendo il valore dal json che al momnento e' nella forma 
			# <updated>2020-06-20T12:40:06.000Z</updated>
			eceExpires = mapJsonMpToEce['__ECE_UPDATED__']
			eceExpires = eceExpires.split('<updated>')[-1].split('</updated>')[0]
			tmptree = cambiaDateUpdate( tree, 'dummyexpires', eceExpires )
			tree = tmptree
			result = tree

	except Exception as e:
		print ( 'PROBLEMI in replaceDateFields : ' + str(e) )
		return None

	print ( '------------------------ END replaceDateFields -------------- ' )
	return  result



def replaceFieldsTree( tree, mapJsonMpToEce, fieldsUpdate ):

	print ( '------------------------ INIT replaceFieldsTree -------------- ' )
	result = None
	
	# questa prende la mappa tra mp ed ECE e prenden do i valori dalla listaUpdate
	# crea un nuovo dict con chiave = campo del content type di ECE
	# e valore quello arrivato da aggiornare
	mapToUpdate = {}
	try: 
		for key, value in sorted(fieldsUpdate.items()):
			if value in mapJsonMpToEce:
				mapToUpdate[ key ] = mapJsonMpToEce[ value ]
			
		print ('mapToUpdate = ' + str(mapToUpdate))
		for key, value in mapToUpdate.items():
			print ( str(key) + ' :: ' + str(value) )
			logger.debug( str(key) + ' :: ' + str(value) )
			tmptree = cambiaField( tree, key, value )
			tree = tmptree
		result = tree

	except Exception as e:
		print ( 'PROBLEMI in replaceFieldsTree : ' + str(e) )
		pass

	print ( '------------------------ END replaceFieldsTree -------------- ' )
	return  result

def creaNomeFile( contentType ):

	dateTimeObj = datetime.now()
	timestampStr = dateTimeObj.strftime("%d.%m.%Y_%H.%M.%S.%f")
	result = os.environ['FILE_LOG_DIR'] + '/' + contentType + '_' + timestampStr + '.xml'
	return result
	

	
def deleteEceDaKafka( message ):

	print ( '------------------------ inizia deleteEceDaKafka -------------- ' )
	logger.debug ( '------------------------ inizia deleteEceDaKafka -------------- ' )
	# questo mi ritorna tutti gli xml gia' buoni da buttare su ECE
	result = False

	try:
	
	 	# posso passare direttamente il valore del json con ece id da 
		# cambiare perche se sono qui vuol dire che e presente
		eceId = None
		#
		[ eceId, treeToUpdate  ]= getEceIdFromCmsJson( message['cms'] )
		print ( 'tornato da getEceIdFromCmsJson ' )
		logger.debug ( 'tornato da getEceIdFromCmsJson ' )
		if eceId is None:
			return False

		if not deleteId( eceId):
			raise Exception("Errore in deleteId")	
		else:
			print ( " updatato : " + eceId )
			logger.debug ( " updatato : " + eceId )
			result = True

	except Exception as e:
		print ( 'PROBLEMI in deleteProgrammeDaKafka : ' + str(e) )
		logger.debug ( 'PROBLEMI in deleteProgrammeDaKafka : ' + str(e) )
		return False

	print ( '------------------------ END deleteEceDaKafka -------------- ' )
	logger.debug ( '------------------------ END deleteEceDaKafka -------------- ' )
	return result

#################################################
# 		UPDATE				#
#################################################
			
def updateAllTest( message , consumerPtr ):

	print ( '------------------------ inizia updateAllTest -------------- ' )
	logger.debug( '------------------------ inizia updateAllTest -------------- ' )
	result = [ False, {"error" : "Default ERROR" } ]

	try:
	
		# per fare update prima devo prendere il programme da cambiare
	 	# posso passare direttamente il valore del json con ece id da 
		# cambiare perche se sono qui vuol dire che e presente
		# e mettere in jsonForId quello giusto se e un programme oppure altro ( quindi con asset )
		if 'programme' == consumerPtr.ecetypes[ str(list(consumerPtr.consumer.subscription() )[0] ) ]:
			jsonForId = message['cms']
		else:
			jsonForId = message['cms']['asset']

		eceId = None
		[ eceId, treeToUpdate  ]= getEceIdFromCmsJson( jsonForId ) 

		if eceId is None:
			logger.warning( 'tornato da getEceIdFromCmsJson senza ID -> ERRORE ' )
			return [ False, {"error" : "Non trovato EceId di riferimento" } ]
		else:
			print ( 'tornato da getEceIdFromCmsJson ' )
			logger.debug( 'tornato da getEceIdFromCmsJson = ' + eceId )

		# prendo il dict e lo trasformo in stringa per pulirlo
		# dalle varie cose inutili e fare i cambiamenti del caso
		messageStr = json.dumps( message )
		xmlStr = pulisciXml( messageStr )
		print ( xmlStr )
		# e quindi lo riporto in json
		messageJson = json.loads( xmlStr )

		channel = 'la1'
		if 'channel' in messageJson:
			channel = messageJson['channel']
			
		jsonFlatMessage = flatten( messageJson )

		programme = None
		if 'cms_urn' in jsonFlatMessage and len( jsonFlatMessage['cms_urn']) > 0:
			print ( 'prendo ECE id' )
			logger.debug( 'prendo ECE id' )
			[ successGetId, programmeEceIdTree  ] = getEceId(  jsonFlatMessage['cms_urn'] )
		else:
			print ( 'in updateAllTest non trovo cms_urn' )
			logger.debug( 'in updateAllTest non trovo cms_urn' )

		successGetId = -1
		sectionId = -1
		if successGetId != -1:
			# qui prendo la sezione in cui si trova il suo 	programme
			sectionId = prendiSectionDaProgramme( programmeEceIdTree )
			# e qui ne prendo lo startTime come da MG-96
			#print (jsonFlatMessage)
			# e lo sbatto nel campo rights_activationDate cosi' sotto
			# nella sistemaJsonPerAll lo metteranno al posto giusto
			# programmeEceIdTree.write('./test_programmeEceIdTree.xml')
			jsonFlatMessage['rights_activationDate'] = prendiDateUpdate( programmeEceIdTree )
			print( 'jsonFlatMessage[rights_activationDate] = '  + str(jsonFlatMessage['rights_activationDate'] ))
			logger.debug( 'jsonFlatMessage[rights_activationDate] = '  + str(jsonFlatMessage['rights_activationDate'] ))

		if sectionId == -1:
			# qui costruisco l id della sezione in cui buttarlo
			sectionId = prendiSectionId( jsonFlatMessage )


		# ennesimo flusso di scelta della sectionId basato sulla source:
		# se la 'source' nel message e' uguale a ingest-system-sonaps deve
		# andare nella section 10630, se invece source == ingest-media - ingest-systems-cmm 
		# oppure ingest-media-aws deve andare nella 5
		sectionId = prendiSectionDaSource( jsonFlatMessage , sectionId )

		print ('sectionId = ' +  sectionId )
		logger.debug( 'sectionId = ' + sectionId )
		# in questo modo i json che arrivano dalla GUI con section gia settata
		# non vengono sovrascritti mentre a tutti gli altri viene settata la sezione 
		# appena calcolata
		if not 'section' in  jsonFlatMessage:
			jsonFlatMessage['section'] = sectionId
		# lo metto qui in esplicito cosi' poi non devo far casini
		# per chiamare la url di pubblicazione
		jsonFlatMessage['eceId'] = eceId
		# metto intero mesaggio per prendere poi il json aws direttamente da li 
		jsonFlatMessage['message'] = messageJson

		# 		UPDATE				#
		# 		ALLTEST				#

		# e qui devo chiamare quella giusto per ogni content-type
		jsonFlatMessage = consumerPtr.ecefunction[ str(list(consumerPtr.consumer.subscription() )[0] ) ][ _SISTEMA_FUNC_ ]( jsonFlatMessage )
		print ( jsonFlatMessage )
		logger.debug( jsonFlatMessage )

		mapJsonMpToEce = mapMptoEce( jsonFlatMessage, consumerPtr.updateFields[ str(list(consumerPtr.consumer.subscription() )[0] ) ] )
		print ( mapJsonMpToEce )
		logger.debug( mapJsonMpToEce )

		# segmented Programme Video
		tree4Ece = replaceFieldsTree( treeToUpdate, mapJsonMpToEce, listaUpdate.fieldsMamSegmentVideo )
		nome_file = creaNomeFile( message['contentType'] )
		#print ( nome_file)
		tree4Ece.write(nome_file)
		if not putId( eceId, nome_file):
			logger.error( " Errore in update di : " + eceId )
			raise Exception("Errore in putId")	
		else:
			print ( " updatato : " + eceId )
			logger.debug( " updatato : " + eceId )

	except Exception as e:
		print ( 'PROBLEMI in updateAllTest : ' + str(e) )
		logger.debug( 'PROBLEMI in updateAllTest : ' + str(e) )
		return [ False, {"error" : str(e) } ]

	print ( '------------------------ END updateAllTest -------------- ' )
	logger.debug( '------------------------ END updateAllTest -------------- ' )
	return [ True, jsonFlatMessage ]


	
def updateSegmentVideoDaKafka( message ):

	print ( '------------------------ inizia updateSegmentVideoDaKafka -------------- ' )
	# questo mi ritorna tutti gli xml gia' buoni da buttare su ECE
	result = [ False, {"error" : "Default ERROR" } ]

	try:
	
		# per fare update prima devo prendere il programme da cambiare
	 	# posso passare direttamente il valore del json con ece id da 
		# cambiare perche se sono qui vuol dire che e presente
		eceId = None
		[ eceId, treeToUpdate  ]= getEceIdFromCmsJson( message['cms']['asset'] )
		print ( 'tornato da getEceIdFromCmsJson ' )
		if eceId is None:
			return [ False, {"error" : "Non trovato EceId di riferimento" } ]

		# prendo il dict e lo trasformo in stringa per pulirlo
		# dalle varie cose inutili e fare i cambiamenti del caso
		messageStr = json.dumps( message )
		xmlStr = pulisciXml( messageStr )
		print ( xmlStr )
		# e quindi lo riporto in json
		messageJson = json.loads( xmlStr )

		channel = 'la1'
		if 'channel' in messageJson:
			channel = messageJson['channel']
			
		jsonFlatMessage = flatten( messageJson )

		successGetId = -1
		programme = None
		if 'cms_urn' in jsonFlatMessage and len( jsonFlatMessage['cms_urn']) > 0:
			[ successGetId, programmeEceIdTree  ] = getEceId(  jsonFlatMessage['cms_urn'] )
		else:
			print ( 'in produciSegmentVideoDaKafka non trovo cms_urn' )
			jsonFlatMessage['cms_urn'] = ''

		sectionId = -1

		if successGetId != -1:
			# qui prendo la sezione in cui si trova il suo programme
			sectionId = prendiSectionDaProgramme( programmeEceIdTree )
			

		if sectionId == -1:
			# qui costruisco l id della sezione in cui buttarlo
			sectionId = prendiSectionId( jsonFlatMessage )

		print ( sectionId )

		jsonFlatMessage['section'] = sectionId
		# lo metto qui in esplicito cosi' poi non devo far casini
		# per chiamare la url di pubblicazione
		jsonFlatMessage['eceId'] = eceId
		jsonFlatMessage = sistemaJsonPerSegmentedVideo( jsonFlatMessage )
		print ( jsonFlatMessage )

		# 		UPDATE				#
		# 		SEGMENTED			#

		mapJsonMpToEce = mapMptoEce( jsonFlatMessage, listaFields.fieldsMamSegmentVideo )
		print ( mapJsonMpToEce )

		# segmented Programme Video
		tree4Ece = replaceFieldsTree( treeToUpdate, mapJsonMpToEce, listaUpdate.fieldsMamSegmentVideo )
		if 'status' in jsonFlatMessage and 'ready' in jsonFlatMessage['status']:
			tree4Ece = CambiaState( tree4Ece, jsonFlatMessage['state'] )
		nome_file = creaNomeFile( message['contentType'] )
		print ( nome_file)
		tree4Ece.write(nome_file)
		if not putId( eceId, nome_file):
			raise Exception("Errore in putId")	
		else:
			print ( " updatato : " + eceId )

	except Exception as e:
		print ( 'PROBLEMI in updateSegmentVideoDaKafka : ' + str(e) )
		return [ False, {"error" : str(e) } ]

	print ( '------------------------ END updateSegmentVideoDaKafka -------------- ' )
	return [ True, jsonFlatMessage ]


	
def updateTranscodableAudioDaKafka( message ):

	print ( '------------------------ inizia updateTranscodableAudioDaKafka -------------- ' )
	# questo mi ritorna tutti gli xml gia' buoni da buttare su ECE
	result = [ False, {"error" : "Default ERROR" } ]

	try:
	
		# per fare update prima devo prendere il programme da cambiare
	 	# posso passare direttamente il valore del json con ece id da 
		# cambiare perche se sono qui vuol dire che e presente
		eceId = None
		[ eceId, treeToUpdate  ]= getEceIdFromCmsJson( message['cms']['asset'] )
		print ( 'tornato da getEceIdFromCmsJson ' )
		if eceId is None:
			return [ False, {"error" : "Non trovato EceId di riferimento" } ]

		# prendo il dict e lo trasformo in stringa per pulirlo
		# dalle varie cose inutili e fare i cambiamenti del caso
		messageStr = json.dumps( message )
		xmlStr = pulisciXml( messageStr )
		print ( xmlStr )
		# e quindi lo riporto in json
		messageJson = json.loads( xmlStr )

		channel = 'la1'
		if 'channel' in messageJson:
			channel = messageJson['channel']
			
		jsonFlatMessage = flatten( messageJson )

		successGetId = -1
		programme = None
		if 'cms_urn' in jsonFlatMessage and len( jsonFlatMessage['cms_urn']) > 0:
			print ( 'prendo ECE id' )
			[ successGetId, programmeEceIdTree  ] = getEceId(  jsonFlatMessage['cms_urn'] )
		else:
			print ( 'in produciTranscodableAudioDaKafka non trovo cms_urn' )
			jsonFlatMessage['cms_urn'] = ''

		sectionId = -1

		if successGetId != -1:
			# qui prendo la sezione in cui si trova il suo programme
			sectionId = prendiSectionDaProgramme( programmeEceIdTree )

		if sectionId == -1:
			# qui costruisco l id della sezione in cui buttarlo
			sectionId = prendiSectionId( jsonFlatMessage )

		print ( sectionId )
		


		jsonFlatMessage['section'] = sectionId
		# lo metto qui in esplicito cosi' poi non devo far casini
		# per chiamare la url di pubblicazione
		jsonFlatMessage['eceId'] = eceId
		# metto intero mesaggio per prendere poi il json aws direttamente da li 
		jsonFlatMessage['message'] = messageJson
		jsonFlatMessage = sistemaJsonPerTranscodableVideo( jsonFlatMessage )
		#print ( jsonFlatMessage )

		# 		UPDATE				#
		# 		TRANSAUDIO			#

		mapJsonMpToEce = mapMptoEce( jsonFlatMessage, listaFields.fieldsMamTranscodableVideo )
		print ( mapJsonMpToEce )

		tree4Ece = replaceFieldsTree( treeToUpdate, mapJsonMpToEce, listaUpdate.fieldsMamTranscodableVideo)
		# e adesso sistemo anche gli eventuali cambiamenti di update e expire date
		tree4Ece = replaceDateFields( tree4Ece, mapJsonMpToEce )
		if 'status' in jsonFlatMessage and 'ready' in jsonFlatMessage['status']:
			print( ' jsonFlatMessage[state] = ' + str(jsonFlatMessage['state'] ))
			tree4Ece = CambiaState( tree4Ece, jsonFlatMessage['state'] )
		nome_file = creaNomeFile( message['contentType'] )
		print ( nome_file)
		tree4Ece.write(nome_file)
		if not putId( eceId, nome_file):
			raise Exception("Errore in putId")	
		else:
			print ( " updatato : " + eceId )
			# e adesso devo pubblicarlo

		

	except Exception as e:
		print ( 'PROBLEMI in updateTranscodableAudioDaKafka : ' + str(e) )
		return [ False, {"error" : str(e) } ]

	print ( '------------------------ END updateTranscodableAudioDaKafka -------------- ' )
	return [ True, jsonFlatMessage ]




	
def updateTranscodableVideoDaKafka( message ):

	print ( '------------------------ inizia updateTranscodableVideoDaKafka -------------- ' )
	# questo mi ritorna tutti gli xml gia' buoni da buttare su ECE
	result = [ False, {"error" : "Default ERROR" } ]

	try:
	
		# per fare update prima devo prendere il programme da cambiare
	 	# posso passare direttamente il valore del json con ece id da 
		# cambiare perche se sono qui vuol dire che e presente
		eceId = None
		[ eceId, treeToUpdate  ]= getEceIdFromCmsJson( message['cms']['asset'] )
		print ( 'tornato da getEceIdFromCmsJson ' )
		if eceId is None:
			return [ False, {"error" : "Non trovato EceId di riferimento" } ]

		# prendo il dict e lo trasformo in stringa per pulirlo
		# dalle varie cose inutili e fare i cambiamenti del caso
		messageStr = json.dumps( message )
		xmlStr = pulisciXml( messageStr )
		print ( xmlStr )
		# e quindi lo riporto in json
		messageJson = json.loads( xmlStr )

		channel = 'la1'
		if 'channel' in messageJson:
			channel = messageJson['channel']
			
		jsonFlatMessage = flatten( messageJson )

		successGetId = -1
		programme = None
		if 'cms_urn' in jsonFlatMessage and len( jsonFlatMessage['cms_urn']) > 0:
			print ( 'prendo ECE id' )
			[ successGetId, programmeEceIdTree  ] = getEceId(  jsonFlatMessage['cms_urn'] )
		else:
			print ( 'in updateTranscodableVideoDaKafka non trovo cms_urn' )
			jsonFlatMessage['cms_urn'] = ''

		sectionId = -1

		if successGetId != -1:
			# qui prendo la sezione in cui si trova il suo programme
			sectionId = prendiSectionDaProgramme( programmeEceIdTree )

		if sectionId == -1:
			# qui costruisco l id della sezione in cui buttarlo
			sectionId = prendiSectionId( jsonFlatMessage )

		print ( sectionId )
		


		jsonFlatMessage['section'] = sectionId
		# lo metto qui in esplicito cosi' poi non devo far casini
		# per chiamare la url di pubblicazione
		jsonFlatMessage['eceId'] = eceId
		# metto intero mesaggio per prendere poi il json aws direttamente da li 
		jsonFlatMessage['message'] = messageJson
		jsonFlatMessage = sistemaJsonPerTranscodableVideo( jsonFlatMessage )
		#print ( jsonFlatMessage )

		mapJsonMpToEce = mapMptoEce( jsonFlatMessage, listaFields.fieldsMamTranscodableVideo )
		print ( mapJsonMpToEce )

		# 		UPDATE				#
		# 		TRANSVIDEO			#

		tree4Ece = replaceFieldsTree( treeToUpdate, mapJsonMpToEce, listaUpdate.fieldsMamTranscodableVideo)
		# e adesso sistemo anche gli eventuali cambiamenti di update e expire date
		tree4Ece = replaceDateFields( tree4Ece, mapJsonMpToEce )
		nome_file = creaNomeFile( message['contentType'] )
		print ( nome_file)
		if 'status' in jsonFlatMessage and 'ready' in jsonFlatMessage['status']:
			tree4Ece = CambiaState( tree4Ece, jsonFlatMessage['state'] )
			
		tree4Ece.write(nome_file)
		if not putId( eceId, nome_file):
			raise Exception("Errore in putId")	
		else:
			print ( " updatato : " + eceId )

	except Exception as e:
		print ( 'PROBLEMI in updateTranscodableVideoDaKafka : ' + str(e) )
		return [ False, {"error" : str(e) } ]

	print ( '------------------------ END updateTranscodableVideoDaKafka -------------- ' )
	return [ True, jsonFlatMessage ]




	
def updateProgrammeVideoDaKafka( message ):

	print ( '------------------------ inizia updateProgrammeVideoDaKafka -------------- ' )
	# questo mi ritorna tutti gli xml gia' buoni da buttare su ECE
	result = [ False, {"error" : "Default ERROR" } ]

	try:
	
		# per fare update prima devo prendere il programme da cambiare
	 	# posso passare direttamente il valore del json con ece id da 
		# cambiare perche se sono qui vuol dire che e presente
		eceId = None
		[ eceId, treeToUpdate  ]= getEceIdFromCmsJson( message['cms']['asset'] )
		print ( 'tornato da getEceIdFromCmsJson ' )
		if eceId is None:
			return [ False, {"error" : "Non trovato EceId di riferimento" } ]
			

		# prendo il dict e lo trasformo in stringa per pulirlo
		# dalle varie cose inutili e fare i cambiamenti del caso
		messageStr = json.dumps( message )
		xmlStr = pulisciXml( messageStr )
		print ( xmlStr )
		# e quindi lo riporto in json
		messageJson = json.loads( xmlStr )

		channel = 'la1'
		if 'channel' in messageJson:
			channel = messageJson['channel']
			
		jsonFlatMessage = flatten( messageJson )

		successGetId = -1
		programme = None
		if 'cms_urn' in jsonFlatMessage and len( jsonFlatMessage['cms_urn']) > 0:
			print ( 'prendo ECE id' )
			[ successGetId, programmeEceIdTree  ] = getEceId(  jsonFlatMessage['cms_urn'] )
		else:
			print ( 'in updateProgrammeVideoDaKafka non trovo cms_urn' )
			jsonFlatMessage['cms_urn'] = ''

		# mettere valore di Default
		sectionId = '15100'

		if successGetId != -1:
			# qui prendo la sezione in cui si trova il suo programme
			sectionId = prendiSectionDaProgramme( programmeEceIdTree )

		if sectionId == -1:
			# qui costruisco l id della sezione in cui buttarlo
			sectionId = prendiSectionId( jsonFlatMessage )

		print ( sectionId )
		

		# 		UPDATE				#
		# 		PROGRAMMEVIDEO			#

		jsonFlatMessage['section'] = sectionId
		# lo metto qui in esplicito cosi' poi non devo far casini
		# per chiamare la url di pubblicazione
		jsonFlatMessage['eceId'] = eceId
		# metto intero mesaggio per prendere poi il json aws direttamente da li 
		jsonFlatMessage['message'] = messageJson
		jsonFlatMessage = sistemaJsonPerProgrammeVideo( jsonFlatMessage )
		#print ( jsonFlatMessage )

		mapJsonMpToEce = mapMptoEce( jsonFlatMessage, listaFields.fieldsMamProgrammeVideo )
		print ( mapJsonMpToEce )

		# programme Video
		tree4Ece = replaceFieldsTree( treeToUpdate, mapJsonMpToEce, listaUpdate.fieldsMamProgrammeVideo)
		# e adesso sistemo anche gli eventuali cambiamenti di update e expire date
		tree4Ece = replaceDateFields( tree4Ece, mapJsonMpToEce )
		if 'status' in jsonFlatMessage and 'ready' in jsonFlatMessage['status']:
			tree4Ece = CambiaState( tree4Ece, jsonFlatMessage['state'] )
		nome_file = creaNomeFile( message['contentType'] )
		print ( nome_file)

		tree4Ece.write(nome_file)
		if not putId( eceId, nome_file):
			raise Exception("Errore in putId")	
		else:
			print ( " updatato : " + eceId )

	except Exception as e:
		print ( 'PROBLEMI in updateProgrammeVideoDaKafka : ' + str(e) )
		return [ False, {"error" : str(e) } ]

	print ( '------------------------ END updateProgrammeVideoDaKafka -------------- ' )
	return [ True, jsonFlatMessage ]


	
def updateProgrammeDaKafka( message ):

	print ( '------------------------ inizia updateProgrammeDaKafka -------------- ' )
	# questo mi ritorna tutti gli xml gia' buoni da buttare su ECE
	result = [ False, {"error" : "Default ERROR" } ]

	try:
	
		# per fare update prima devo prendere il programme da cambiare
	 	# posso passare direttamente il valore del json con ece id da 
		# cambiare perche se sono qui vuol dire che e presente
		eceId = None
		[ eceId, treeToUpdate  ]= getEceIdFromCmsJson( message['cms'] )
		print ( 'tornato da getEceIdFromCmsJson ' )
		if eceId is None:
			return [ False, {"error" : "Non trovato EceId di riferimento" } ]

		# adesso in treeToUpdate ho xml parsato del programme
		# giro su tutti i fields miei cambiandoli con i valori nuovi
		# usando la cambiaFields

		# prima per sapere che valori mettere

		# prendo il dict e lo trasformo in stringa per pulirlo
		# dalle varie cose inutili e fare i cambiamenti del caso
		messageStr = json.dumps( message )
		xmlStr = pulisciXml( messageStr )
		# e quindi lo riporto in json
		messageJson = json.loads( xmlStr )

		channel = 'la1'
		if 'channel' in messageJson:
			channel = messageJson['channel']
		
		jsonFlatMessage = flatten( messageJson )
		#print ( jsonFlatMessage )
		# lo metto qui in esplicito cosi' poi non devo far casini
		# per chiamare la url di pubblicazione
		jsonFlatMessage['eceId'] = eceId
		# metto intero mesaggio per prendere poi il json aws direttamente da li 
		jsonFlatMessage['message'] = messageJson
		jsonFlatMessage = sistemaJsonPerProgramme( jsonFlatMessage )

		mapJsonMpToEce = mapMptoEce( jsonFlatMessage, listaFields.fieldsMamProgramme )


		# 		UPDATE				#
		# 		PROGRAMME			#

		print ( mapJsonMpToEce )

		tree4Ece = replaceFieldsTree( treeToUpdate, mapJsonMpToEce, listaUpdate.fieldsMamProgramme )
		# e adesso sistemo anche gli eventuali cambiamenti di update e expire date
		tree4Ece = replaceDateFields( tree4Ece, jsonFlatMessage )
		nome_file = creaNomeFile( message['contentType'] )
		print ( nome_file)
		tree4Ece.write(nome_file)
		#CambiaBodyFile(nome_file)
		#exit(0)
		if not putId( eceId, nome_file):
			raise Exception("Errore in putId")	
		else:
			print ( " updatato : " + eceId )

	except Exception as e:
		print ( 'PROBLEMI in updateProgrammeDaKafka : ' + str(e) )
		return [ False, {"error" : str(e) } ]

	print ( '------------------------ END updateProgrammeDaKafka -------------- ' )
	return [ True, jsonFlatMessage ]

#################################################
# 		UPDATE				#
#################END#############################

def trasformaTcOffset( mIn ):

	result = None
	# questa deve  calcolare il tc_offset dal markIn
	millis = int(mIn)
	seconds=(millis/1000)%60
	seconds = int(seconds)
	minutes=(millis/(1000*60))%60
	minutes = int(minutes)
	hours=(millis/(1000*60*60))%24
	milliseconds = int((int(mIn)%1000)/100)
	# contanto che stiamo andando a 25 fps
	# e quindi ci vogliono 40 millisecond per frame
	frames = int(milliseconds/40)

	print ("%d:%d:%d:%d" % (hours, minutes, seconds,frames))

	result =  "%d:%d:%d:%d" % (hours, minutes, seconds,frames)

	return result


def trasformaMIn( mIn ):
	result = None
	# questa deve  calcolare il tc_offset dal markIn
	millis = int(mIn)/1000
	seconds=(millis/1000)%60
	seconds = int(seconds)
	minutes=(millis/(1000*60))%60
	minutes = int(minutes)
	hours=(millis/(1000*60*60))%24
	milliseconds = int((int(mIn)%1000)/100)

	print ("%d:%d:%d.%d" % (hours, minutes, seconds,milliseconds))

	result =  "%d:%d:%d.%d" % (hours, minutes, seconds,milliseconds)

	return result

def calcolaDuration( mIn, mOut ):
	result = None
	# questa deve  calcolare la duration dati in e out
	result = int(mOut) - int(mIn)
	result = result / 1000
	result = str( result )
	return result

def prendiSectionDaSource( jsonFlatMessage , sectionId ):

	lista = { 'ingest-systems-sonaps':'10630', 
		  'ingest-media':'5',
		  'ingest-systems-cmm':'5',
                  'ingest-media-aws':'5'
	}
	result = sectionId
	if 'source' not in jsonFlatMessage:
		return result
	else:
		if jsonFlatMessage['source'] in lista:
			return lista[ jsonFlatMessage['source'] ]

	return result

#################################################
# 		SISTEMA				#
#################################################

def sistemaJsonPerSegmentedVideo( jsonToClean ) :
	result = jsonToClean

	print ( '------------------------ INIT sistemaJsonPerSegmentedVideo-------------- ' )
	
	# sistemo il tempo
	if 'clips' in jsonToClean and len ( jsonToClean['clips'] ) > 0:
		clipsValues = jsonToClean['clips'][0]
		if 'markIn' in clipsValues :
			if 'startTimeInMs' in jsonToClean:
				result['tc_offset'] = trasformaTcOffset( int(clipsValues['markIn'] ) - int( jsonToClean['startTimeInMs']))
			else:
				result['tc_offset'] = '00:00:00:01'
			result['clips_markIn'] = clipsValues['markIn']

		if 'markOut' in clipsValues  and 'markIn' in clipsValues:
			result['duration'] = calcolaDuration( clipsValues['markIn'], clipsValues['markOut'])
	
	result = sistemaRightsDates( result, jsonToClean)

	# qui opero il mapping per lo status
	if 'status' in jsonToClean and not 'null' in jsonToClean['status']:
		result['status'] = jsonToClean['status'].replace('COMPLETED','ready').replace('FAILED','failed')
		if 'ready' in result['status']:
			jsonToClean['state'] = 'published'
		else:
			jsonToClean['state'] = 'draft'
	
	# metto a falso il fatto che sia un editorial keyframe
	jsonToClean['__IF_EDITORIAL__'] = False
	# e aggiungo tutta la trattazione dei keyframes
	result['__ECE_LINK_KEYFRAME__'] = preparaKeyframes( jsonToClean )
	print ( '------------------------ INIT sistemaJsonPerSegmentedVideo-------------- ' )
	return result


def sistemaJsonPerProgramme( jsonToClean ) :
	result = jsonToClean

	print ( '------------------------ INIT sistemaJsonPerProgramme-------------- ' )
	
	# sistemo il tempo
	
	# e aggiungo tutta la trattazione dello starttime
	if 'rights_activationDate' in jsonToClean and not 'null' in jsonToClean['rights_activationDate'] :
		print( 'trovato rights_activationDate' )
		timestamp = int(jsonToClean['rights_activationDate'])/1000.0
		value = datetime.utcfromtimestamp(timestamp)
		data = value.strftime("%Y-%m-%dT%H:%M:%SZ")
		result['__ECE_UPDATED__'] = '<updated>' + data  + '</updated>'
	elif 'dateTimes_startBroadcastPress' in jsonToClean and not 'null' in jsonToClean['dateTimes_startBroadcastPress'] :
		print( 'trovato dateTimes_startBroadcastPress' )
		print( 'lo metto nella publishDate' )
		result['__ECE_UPDATED__'] = '<updated>' + jsonToClean['dateTimes_startBroadcastPress']  + '</updated>'
	else:
		result['__ECE_UPDATED__'] = ''
	# e aggiungo tutta la trattazione del expire time
	if 'rights_expireDate' in jsonToClean and not 'null' in jsonToClean['rights_expireDate'] :
		print( 'trovato rights_expireDate' )
		timestamp = int(jsonToClean['rights_expireDate'])/1000.0
		value = datetime.utcfromtimestamp(timestamp)
		data = value.strftime("%Y-%m-%dT%H:%M:%SZ")
		result['__ECE_AGE_EXPIRES__'] = '<age:expires>' + data  + '</age:expires>'
	else:
		result['__ECE_AGE_EXPIRES__'] = ''


	if 'sourceSystem_louise_replica' in jsonToClean and jsonToClean['sourceSystem_louise_replica']:
		result['__STATE_MAMPROGRAMME__'] = 'draft'
	else:
		result['__STATE_MAMPROGRAMME__'] = 'published'

	print ( '------------------------ END sistemaJsonPerProgramme-------------- ' )
	return result



def sistemaJsonPerProgrammeVideo( jsonToClean ) :
	result = jsonToClean

	print ( '------------------------ INIT sistemaJsonPerProgrammeVideo -------------- ' )
	
	# sistemo il tempo
	if 'clips' in jsonToClean and len ( jsonToClean['clips'] ) > 0:
		clipsValues = jsonToClean['clips'][0]
		if 'markIn' in clipsValues :
			result['clips_markIn'] = clipsValues['markIn'] 

	result = sistemaRightsDates( result, jsonToClean)

	# qui opero il mapping per lo status
	if 'status' in jsonToClean and not 'null' in jsonToClean['status']:
		result['status'] = jsonToClean['status'].replace('COMPLETED','ready').replace('FAILED','failed')
		if 'ready' in result['status']:
			jsonToClean['state'] = 'published'
		else:
			jsonToClean['state'] = 'draft'

	if 'message' in jsonToClean:
		jsonForAws = jsonToClean['message']
		if 'transcoderMetadata' in jsonForAws and not 'null' in jsonForAws['transcoderMetadata']:
			# devo mettere il json nel campo transcoderMetadata
			result['transcoderMetadata'] = jsonForAws['transcoderMetadata']
			# devo aggiungere la duration che adesso deve essere calcolata
			# dal campo relativo della prima variante del video contenuto 
			# nel json aws
			result['duration'] = getDurationFromAws( jsonForAws['transcoderMetadata'] )
			print ( result )
			# e aggiungo tutta la trattazione dei keyframes
			result['__ECE_LINK_KEYFRAME__'] = preparaKeyframesProgrammeVideo( jsonToClean )
		else:
			result['transcoderMetadata'] = ''
	
	print ( result ) 
	
	print ( '------------------------ END sistemaJsonPerProgrammeVideo -------------- ' )
	return result


def sistemaJsonPerTranscodableVideo( jsonToClean ) :

	result = jsonToClean

	print ( '------------------------ INIT sistemaJsonPerTranscodableVideo -------------- ' )
	
	# sistemo il tempo
	'''
	tolto su consiglio del Cave 14.07
	if 'clips' in jsonToClean and len ( jsonToClean['clips'] ) > 0:
		clipsValues = jsonToClean['clips'][0]
		if 'markIn' in clipsValues :
			result['playMarkIn'] = trasformaMIn( clipsValues['markIn'] )

		if 'markOut' in clipsValues  and 'markIn' in clipsValues:
			result['duration'] = calcolaDuration( clipsValues['markIn'], clipsValues['markOut'])
	'''
	
	result = sistemaRightsDates( result, jsonToClean)

	# qui opero il mapping per lo status
	if 'status' in jsonToClean and not 'null' in jsonToClean['status']:
		result['status'] = jsonToClean['status'].replace('COMPLETED','ready').replace('FAILED','failed')
		if 'ready' in result['status']:
			jsonToClean['state'] = 'published'
		else:
			jsonToClean['state'] = 'draft'
			
	
	result['__BINARY_PATH_4_TV__'] = os.environ['BINARY_PATH_4_TV']

	if 'message' in jsonToClean:
		jsonForTv = jsonToClean['message']
		if 'transcoderMetadata' in jsonForTv and not 'null' in jsonForTv['transcoderMetadata']:
			# devo mettere il json nel campo transcoderMetadata
			result['transcoderMetadata'] = jsonForTv['transcoderMetadata']
			# devo agiungere la duration che adesso deve essere calcolata
			# dal campo relativo della prima variante del video contenuto 
			# nel json aws
			result['duration'] = getDurationFromTv( jsonForTv['transcoderMetadata'] )
			print ( result )
			# e aggiungo tutta la trattazione dei keyframes
			result['__ECE_LINK_KEYFRAME__'] = preparaKeyframesTranscodableVideo( jsonToClean )
		else:
			result['transcoderMetadata'] = ''
	
	print ( result ) 
	
	print ( '------------------------ END sistemaJsonPerTranscodableVideo -------------- ' )
	return result

#################################################
# 		SISTEMA				#
##################END############################
def sistemaRightsDates( result, jsonToClean ):
	
	# e aggiungo tutta la trattazione dello starttime
	if 'rights_activationDate' in jsonToClean and not 'null' in jsonToClean['rights_activationDate'] and len(jsonToClean['rights_activationDate'] ) > 0:
		if 'Z' in jsonToClean['rights_activationDate']:
			print( 'arrivata rights_activationDate dal Programme' )
			# mi e' arrivata la data gia nel formato giusto
			data = jsonToClean['rights_activationDate']
			print( data )
		else :
			print( 'trovato rights_activationDate' )
			timestamp = int(jsonToClean['rights_activationDate'])/1000.0
			print( timestamp )
			value = datetime.utcfromtimestamp(timestamp)
			data = value.strftime("%Y-%m-%dT%H:%M:%SZ")
			print( data )
		result['__ECE_UPDATED__'] = '<updated>' + data  + '</updated>'
		result['__ECE_DCTERMS_AVAILABLE__'] = '<dcterms:available>' + data  + '</dcterms:available>'
	else:
		result['__ECE_UPDATED__'] = ''
		result['__ECE_DCTERMS_AVAILABLE__'] = ''

	# e aggiungo tutta la trattazione del expire time
	if 'rights_expireDate' in jsonToClean and not 'null' in jsonToClean['rights_expireDate'] and len( jsonToClean['rights_expireDate']) > 0 :
		if 'Z' in jsonToClean['rights_expireDate']:
			print( 'arrivata rights_expireDate dal Programme' )
			# mi e' arrivata la data gia nel formato giusto
			data = jsonToClean['rights_expireDate'] 
			print( data )
		else:
			print( 'trovato rights_expireDate' )
			timestamp = int(jsonToClean['rights_expireDate'])/1000.0
			print( timestamp )
			value = datetime.utcfromtimestamp(timestamp)
			data = value.strftime("%Y-%m-%dT%H:%M:%SZ")
			print( data )
		result['__ECE_AGE_EXPIRES__'] = '<age:expires>' + data  + '</age:expires>'
	else:
		result['__ECE_AGE_EXPIRES__'] = ''

	return result



def preparaKeyframes( jsonToClean ):
	print ( '------------------------ INIT  preparaKeyframes -------------- ' )
	logger.debug ( '------------------------ INIT  preparaKeyframes -------------- ' )
	result = ''
	if 'awsKeyframe_url'in jsonToClean and not jsonToClean['awsKeyframe_url'] is None:
		# devi creare i keyframe nella sezione giusta
		print ( 'prendo : ' + jsonToClean['awsKeyframe_url'] )
		logger.debug ( 'prendo : ' + jsonToClean['awsKeyframe_url'] )
		# prendo la immagine da  S3
		resultBool = False
		eceId = -1
		[ resultBool, eceId ] = importaImgFromAws( jsonToClean['awsKeyframe_url'], jsonToClean['section'], 'KeyFrame_1_Title' )

		if not resultBool:
			return ''

		# apro il template opportuno 
		fin = codecs.open( os.environ['RESOURCE_DIR'] + "template_relation_keyframe" + ".xml" , 'r', 'utf-8' )
		eceTemplate = fin.read()
		fin.close()
		# a cui devo rimpiazzare __ECE_ID__ __ECE_SERVER__ ECE_MODEL __IF_EDITORIAL__
		rimpiazza = {}
		rimpiazza['__ECE_ID__'] =  eceId 
		rimpiazza['__ECE_SERVER__'] =  os.environ['ECE_SERVER']  
		rimpiazza['__ECE_MODEL__'] =  os.environ['ECE_MODEL']  
		rimpiazza['__ECE_THUMB__'] =  os.environ['ECE_THUMB']  
		if jsonToClean['__IF_EDITORIAL__']:
			rimpiazza['__IF_EDITORIAL__'] =  'EDITORIAL' 
		else:
			rimpiazza['__IF_EDITORIAL__'] =  '' 

		
		# use these three lines to do the replacement
		rep = dict((re.escape(k), v) for k, v in rimpiazza.items()) 
		pattern = re.compile("|".join(rep.keys()))
		text = pattern.sub(lambda m: rep[re.escape(m.group(0))], eceTemplate)
		result = text.strip()

	print ( '------------------------ END  preparaKeyframes -------------- ' )
	logger.debug ( '------------------------ END  preparaKeyframes -------------- ' )
	return result

def preparaKeyframesProgrammeVideo( jsonToClean ):
	# qui in jsonToClean sono sicuro di trovare jsonToClean['message']['transcoderMetadata']
	# perche' altimenti non sareiu arrivato a questa funzione

	awsJson = jsonToClean['message']['transcoderMetadata']
	print ( '------------------------ INIT  preparaKeyframesProgrammeVideo -------------- ' )
	logger.debug ( '------------------------ INIT  preparaKeyframesProgrammeVideo -------------- ' )
	result = ''
	if 'keyFrames'in awsJson and not awsJson['keyFrames'] is None and len( awsJson['keyFrames'] ) > 0:
		# devi creare i keyframe nella sezione giusta
		print ( 'prendo : ' + str(awsJson['keyFrames'] ) )
		logger.debug ( 'prendo : ' + str(awsJson['keyFrames'] ))
		for kf in awsJson['keyFrames']:
			# kf e nella forma :
			#{
			#  "timestamp":"timestamp1",
			#	  "url" : "imageS3Url1"
			#}

			# prendo la immagine da  S3
			kUrl = kf['url']
			kTitle = str(kf['timestamp']) + '_' + jsonToClean['message']['_id']
			print ('title = ' + kTitle )
			resultBool = False
			eceId = -1
			[ resultBool, eceId ] = importaImgFromAws( kUrl, jsonToClean['section'], kTitle )

			if not resultBool:
				print ('passo da resultBool == FALSE ' )
				return ''

			# apro il template opportuno 
			fin = codecs.open( os.environ['RESOURCE_DIR'] + "template_relation_keyframe" + ".xml" , 'r', 'utf-8' )
			eceTemplate = fin.read()
			fin.close()
			# a cui devo rimpiazzare __ECE_ID__ __ECE_SERVER__ ECE_MODEL __IF_EDITORIAL__
			rimpiazza = {}
			rimpiazza['__ECE_ID__'] =  eceId 
			rimpiazza['__ECE_SERVER__'] =  os.environ['ECE_SERVER']  
			rimpiazza['__ECE_MODEL__'] =  os.environ['ECE_MODEL']  
			rimpiazza['__ECE_THUMB__'] =  os.environ['ECE_THUMB'] 
			if '__IF_EDITORIAL__' in jsonToClean or 'editorialKeyframe' in jsonToClean:
				rimpiazza['__IF_EDITORIAL__'] =  'EDITORIAL' 
			else:
				rimpiazza['__IF_EDITORIAL__'] =  '' 

			
			# use these three lines to do the replacement
			rep = dict((re.escape(k), v) for k, v in rimpiazza.items()) 
			pattern = re.compile("|".join(rep.keys()))
			text = pattern.sub(lambda m: rep[re.escape(m.group(0))], eceTemplate)
			result = result + ' ' + text.strip()

	print ( '------------------------ END  preparaKeyframesProgrammeVideo -------------- ' )
	logger.debug ( '------------------------ END  preparaKeyframesProgrammeVideo -------------- ' )
	return result



def preparaKeyframesTranscodableVideo( jsonToClean ):
	# qui in jsonToClean sono sicuro di trovare jsonToClean['message']['transcoderMetadata']
	# perche' altimenti non sareiu arrivato a questa funzione

	awsJson = jsonToClean['message']['transcoderMetadata']
	print ( '------------------------ INIT  preparaKeyframesTranscodableVideo -------------- ' )
	logger.debug ( '------------------------ INIT  preparaKeyframesTranscodableVideo -------------- ' )
	result = ''
	if 'keyFrames'in awsJson and not awsJson['keyFrames'] is None and len( awsJson['keyFrames'] ) > 0:
		# devi creare i keyframe nella sezione giusta
		print ( 'prendo : ' + str(awsJson['keyFrames'] ) )
		logger.debug ( 'prendo : ' + str(awsJson['keyFrames'] ))
		for kf in awsJson['keyFrames']:
			# kf e nella forma :
			#{
			#  "timestamp":"timestamp1",
			#	  "url" : "imageS3Url1"
			#}

			# prendo la immagine da  S3
			kUrl = kf['url']
			kTitle = str(kf['timestamp']) + '_' + jsonToClean['message']['_id']
			print ('title = ' + kTitle )
			resultBool = False
			eceId = -1
			[ resultBool, eceId ] = importaImgFromAws( kUrl, jsonToClean['section'], kTitle )

			if not resultBool:
				print ('passo da resultBool == FALSE ' )
				return ''

			# apro il template opportuno 
			fin = codecs.open( os.environ['RESOURCE_DIR'] + "template_relation_keyframe" + ".xml" , 'r', 'utf-8' )
			eceTemplate = fin.read()
			fin.close()
			# a cui devo rimpiazzare __ECE_ID__ __ECE_SERVER__ ECE_MODEL __IF_EDITORIAL__
			rimpiazza = {}
			rimpiazza['__ECE_ID__'] =  eceId 
			rimpiazza['__ECE_SERVER__'] =  os.environ['ECE_SERVER']  
			rimpiazza['__ECE_MODEL__'] =  os.environ['ECE_MODEL']  
			rimpiazza['__ECE_THUMB__'] =  os.environ['ECE_THUMB']  
			if '__IF_EDITORIAL__' in jsonToClean or 'editorialKeyframe' in jsonToClean:
				rimpiazza['__IF_EDITORIAL__'] =  'EDITORIAL' 
			else:
				rimpiazza['__IF_EDITORIAL__'] =  '' 

			
			# use these three lines to do the replacement
			rep = dict((re.escape(k), v) for k, v in rimpiazza.items()) 
			pattern = re.compile("|".join(rep.keys()))
			text = pattern.sub(lambda m: rep[re.escape(m.group(0))], eceTemplate)
			result = result + ' ' + text.strip()

	print ( '------------------------ END  preparaKeyframesTranscodableVideo -------------- ' )
	logger.debug ( '------------------------ END  preparaKeyframesTranscodableVideo -------------- ' )
	return result

def prendiXmlProgramme ( eceId ):

	print ( '------------------------ INIT prendiXmlProgramme -------------- ' )
	logger.debug ( '------------------------ INIT prendiXmlProgramme -------------- ' )
	result = ''
	auth = '%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])
	base64string = b64encode(auth.encode())
	base64string = base64string.decode("ascii")
	headers = { 'Authorization' : 'Basic %s' %  base64string }

	link = os.environ['ECE_SERVER']  + eceId
	print( 'link : ' + link  )
	logger.debug( 'link : ' + link  )

	try:
		request = urllib.request.Request(link, headers=headers)
		resultResponse = urllib.request.urlopen(request)
		xml = minidom.parse(resultResponse)
		result = xml.toxml()

	except Exception as e:
		print ( 'PROBLEMI in prendiXmlProgramme : ' + str(e) )
		logger.debug ( 'PROBLEMI in prendiXmlProgramme : ' + str(e) )
		return result
	print ( '------------------------ END prendiXmlProgramme -------------- ' )
	logger.debug ( '------------------------ END prendiXmlProgramme -------------- ' )
	return result

def cambiaActivationDate( pId, data ):
        print ( '------------------------ INIT cambiaActivationDate -------------- ' )
        logger.debug ( '------------------------ INIT cambiaActivationDate -------------- ' )
        result = False

        # qui devo aggiungere alle relation di questo programme
        # l asset eceId
        xmlStr =  prendiXmlProgramme( pId )
        if len( xmlStr) < 1:
                return False
        # con questo split ho l'ultima posizione del file e posso aggiungere un ulteriore
        # link di relazione
        #print ( xmlStr.split('</entry>')[0] )
        entry =  xmlStr.split('</entry>')[0]

        text ='<dcterms:available>' + '2999-01-01T22:55:41.000Z' + '</dcterms:available>'
        text ='<dcterms:available>' + data + '</dcterms:available>'

        #print ( text )

        xmlStr = entry + text.strip() + '</entry>'

        tree = ET.fromstring(xmlStr)
        #ET.dump(root)
        tree = ET.ElementTree(tree)
        nome_file = creaNomeFile( 'programmeActive' )
        tree.write(nome_file)
        result = putId(pId, nome_file)

        print ( '------------------------ END cambiaActivationDate -------------- ' )
        logger.debug ( '------------------------ END cambiaActivationDate -------------- ' )
        return result



def addRelProgrammeVideo( programmeEceId, pVideoEceId, startTimeInMs ):
	print ( '------------------------ INIT addRelProgrammeVideo -------------- ' )
	logger.debug ( '------------------------ INIT addRelProgrammeVideo -------------- ' )
	print ( '- aggiungo EceId : ' + pVideoEceId + ' al EceId : ' + programmeEceId )
	logger.debug ( '- aggiungo EceId : ' + pVideoEceId + ' al EceId : ' + programmeEceId )
	result = False

	# qui devo aggiungere alle relation di questo programme 
	# l asset eceId
	xmlStr =  prendiXmlProgramme( programmeEceId )
	if len( xmlStr) < 1:
		return False
	# con questo split ho l'ultima posizione del file e posso aggiungere un ulteriore
	# link di relazione 
	#print ( xmlStr.split('</entry>')[0] )
	entry =  xmlStr.split('</entry>')[0] 
	
	# apro il template opportuno 
	fin = codecs.open( os.environ['RESOURCE_DIR'] + "template_relation_programmevideo" + ".xml" , 'r', 'utf-8' )
	eceTemplate = fin.read()
	fin.close()
	# a cui devo rimpiazzare __ECE_ID__ __ECE_SERVER__ ECE_MODEL __IF_EDITORIAL__
	rimpiazza = {}
	rimpiazza['__ECE_ID__'] =  pVideoEceId 
	rimpiazza['__ECE_SERVER__'] =  os.environ['ECE_SERVER']  
	rimpiazza['__ECE_MODEL__'] =  os.environ['ECE_MODEL']  
	
	# use these three lines to do the replacement
	rep = dict((re.escape(k), v) for k, v in rimpiazza.items()) 
	pattern = re.compile("|".join(rep.keys()))
	text = pattern.sub(lambda m: rep[re.escape(m.group(0))], eceTemplate)
	
	#print ( text ) 

	xmlStr = entry + text.strip() + '</entry>'
		
	tree = ET.fromstring(xmlStr)
	#ET.dump(root)
	# qui per far funzionre il player con gli offset giusti
	# deve mettere nel campo startTimeInMs del programme il proprio valore 
	# del campo startTimeInMs che e' stato passato come parametro
	print( prendiField( tree, 'startTimeInMs'))
	tree = cambiaField( tree, 'startTimeInMs', str(startTimeInMs))
	print( prendiField( tree, 'startTimeInMs'))

	print  ( prendiListaRelated(  tree, 'related' )  )
	listaRelated = prendiListaRelated(  tree, 'related' ) 

	# adesso sulla lista dei related gli va a mettere l'offset giusto
	# partendo dal valore appena settato nella startTimeInMs
	resultBool = sistemaOffsetAndPVId( listaRelated, str(startTimeInMs ), pVideoEceId)
	if not resultBool:
		logger.warning(' WARNING : non sono riuscito a settare gli OFFSET ')

	#print ( ordinaListaRelated( listaRelated ) )
	tree = rimuoviListaRelated( tree , 'related' )
	tree = aggiungiListaRelated( tree , listaRelated ) 
	tree = ET.ElementTree(tree)
	nome_file = creaNomeFile( 'programmeVideo' )
	tree.write(nome_file)
	result = putId(programmeEceId, nome_file)


	#result = putIdFromStr( programmeEceId, xmlStr)

	print ( '------------------------ END addRelProgrammeVideo -------------- ' )
	logger.debug ( '------------------------ END addRelProgrammeVideo -------------- ' )
	return result

def addRelSegmentOrder( programmeEceId, segmentEceId, order ):
	print ( '------------------------ INIT addRelSegmentOrder -------------- ' )
	logger.debug ( '------------------------ INIT addRelSegmentOrder -------------- ' )
	print ( '- aggiungo EceId : ' + segmentEceId + ' al EceId : ' + programmeEceId )
	logger.debug ( '- aggiungo EceId : ' + segmentEceId + ' al EceId : ' + programmeEceId )
	result = False

	# qui devo aggiungere alle relation di questo programme 
	# l asset eceId
	xmlStr =  prendiXmlProgramme( programmeEceId )
	if len( xmlStr) < 1:
		return False
	# con questo split ho l'ultima posizione del file e posso aggiungere un ulteriore
	# link di relazione 
	#print ( xmlStr.split('</entry>')[0] )
	entry =  xmlStr.split('</entry>')[0] 
	
	# apro il template opportuno 
	fin = codecs.open( os.environ['RESOURCE_DIR'] + "template_relation_segment" + ".xml" , 'r', 'utf-8' )
	eceTemplate = fin.read()
	fin.close()
	# a cui devo rimpiazzare __ECE_ID__ __ECE_SERVER__ ECE_MODEL __IF_EDITORIAL__
	rimpiazza = {}
	rimpiazza['__ECE_ID__'] =  segmentEceId 
	rimpiazza['__ECE_SERVER__'] =  os.environ['ECE_SERVER']  
	rimpiazza['__ECE_MODEL__'] =  os.environ['ECE_MODEL']  
	rimpiazza['__SEGMENT_ORDER__'] = str(order )
	
	# use these three lines to do the replacement
	rep = dict((re.escape(k), v) for k, v in rimpiazza.items()) 
	pattern = re.compile("|".join(rep.keys()))
	text = pattern.sub(lambda m: rep[re.escape(m.group(0))], eceTemplate)
	
	#print ( text ) 

	xmlStr = entry + text.strip() + '</entry>'

	
	tree = ET.fromstring(xmlStr)
	#ET.dump(root)
	print  ( prendiListaRelated(  tree, 'related' )  )
	listaRelated = prendiListaRelated(  tree, 'related' ) 
	#print ( ordinaListaRelated( listaRelated ) )
	tree = rimuoviListaRelated( tree , 'related' )
	tree = aggiungiListaRelated( tree , listaRelated ) 
	tree = ET.ElementTree(tree)
	nome_file = creaNomeFile('segmentedProgrammeVideo'  )
	tree.write(nome_file)
	result = putId(programmeEceId, nome_file)

	print ( '------------------------ END addRelSegmentOrder -------------- ' )
	logger.debug ( '------------------------ END addRelSegmentOrder -------------- ' )
	return result



def addRelSegment( programmeEceId, segmentEceId ):
	print ( '------------------------ INIT addRelSegment -------------- ' )
	logger.debug ( '------------------------ INIT addRelSegment -------------- ' )
	print ( '- aggiungo EceId : ' + segmentEceId + ' al EceId : ' + programmeEceId )
	logger.debug ( '- aggiungo EceId : ' + segmentEceId + ' al EceId : ' + programmeEceId )
	result = False

	# qui devo aggiungere alle relation di questo programme 
	# l asset eceId
	xmlStr =  prendiXmlProgramme( programmeEceId )
	if len( xmlStr) < 1:
		return False
	# con questo split ho l'ultima posizione del file e posso aggiungere un ulteriore
	# link di relazione 
	#print ( xmlStr.split('</entry>')[0] )
	entry =  xmlStr.split('</entry>')[0] 
	
	# apro il template opportuno 
	fin = codecs.open( os.environ['RESOURCE_DIR'] + "template_relation_segment" + ".xml" , 'r', 'utf-8' )
	eceTemplate = fin.read()
	fin.close()
	# a cui devo rimpiazzare __ECE_ID__ __ECE_SERVER__ ECE_MODEL __IF_EDITORIAL__
	rimpiazza = {}
	rimpiazza['__ECE_ID__'] =  segmentEceId 
	rimpiazza['__ECE_SERVER__'] =  os.environ['ECE_SERVER']  
	rimpiazza['__ECE_MODEL__'] =  os.environ['ECE_MODEL']  
	
	# use these three lines to do the replacement
	rep = dict((re.escape(k), v) for k, v in rimpiazza.items()) 
	pattern = re.compile("|".join(rep.keys()))
	text = pattern.sub(lambda m: rep[re.escape(m.group(0))], eceTemplate)
	
	#print ( text ) 

	xmlStr = entry + text.strip() + '</entry>'
	
	result = putIdFromStr( programmeEceId, xmlStr)

	print ( '------------------------ END addRelSegment -------------- ' )
	logger.debug ( '------------------------ END addRelSegment -------------- ' )
	return result

def addRelTranscodableVideo( programmeEceId, transcodableEceId ):
	print ( '------------------------ INIT addRelTranscodableVideo -------------- ' )
	logger.debug ( '------------------------ INIT addRelTranscodableVideo -------------- ' )
	print ( '- aggiungo EceId : ' + transcodableEceId + ' al EceId : ' + programmeEceId )
	logger.debug ( '- aggiungo EceId : ' + transcodableEceId + ' al EceId : ' + programmeEceId )
	result = False

	# qui devo aggiungere alle relation di questo programme 
	# l asset eceId
	xmlStr =  prendiXmlProgramme( programmeEceId )
	if len( xmlStr) < 1:
		return False
	# con questo split ho l'ultima posizione del file e posso aggiungere un ulteriore
	# link di relazione 
	#print ( xmlStr.split('</entry>')[0] )
	entry =  xmlStr.split('</entry>')[0] 
	
	# apro il template opportuno 
	fin = codecs.open( os.environ['RESOURCE_DIR'] + "template_relation_transcodablevideo" + ".xml" , 'r', 'utf-8' )
	eceTemplate = fin.read()
	fin.close()
	# a cui devo rimpiazzare __ECE_ID__ __ECE_SERVER__ ECE_MODEL __IF_EDITORIAL__
	rimpiazza = {}
	rimpiazza['__ECE_ID__'] =  transcodableEceId 
	rimpiazza['__ECE_SERVER__'] =  os.environ['ECE_SERVER']  
	rimpiazza['__ECE_MODEL__'] =  os.environ['ECE_MODEL']  
	
	# use these three lines to do the replacement
	rep = dict((re.escape(k), v) for k, v in rimpiazza.items()) 
	pattern = re.compile("|".join(rep.keys()))
	text = pattern.sub(lambda m: rep[re.escape(m.group(0))], eceTemplate)
	
	#print ( text ) 

	xmlStr = entry + text.strip() + '</entry>'
	
	result = putIdFromStr( programmeEceId, xmlStr)

	print ( '------------------------ END addRelTranscodableVideo -------------- ' )
	logger.debug ( '------------------------ END addRelTranscodableVideo -------------- ' )
	return result



def addRelEditorialKeyframe( assetEceId, keyframeEceId ):

	print ( '------------------------ INIT addRelEditorialKeyframe -------------- ' )
	logger.debug ( '------------------------ INIT addRelEditorialKeyframe -------------- ' )
	print ( '- aggiungo EceId : ' + keyframeEceId + ' al EceId : ' + assetEceId )
	logger.debug ( '- aggiungo EceId : ' + keyframeEceId + ' al EceId : ' + assetEceId )
	result = False

	# qui devo aggiungere alle relation di questo programme 
	# l asset eceId
	xmlStr =  prendiXmlProgramme( assetEceId )
	if len( xmlStr) < 1:
		return False
	# con questo split ho l'ultima posizione del file e posso aggiungere un ulteriore
	# link di relazione 
	#print ( xmlStr.split('</entry>')[0] )
	entry =  xmlStr.split('</entry>')[0] 
	
	# apro il template opportuno 
	fin = codecs.open( os.environ['RESOURCE_DIR'] + "template_relation_keyframe" + ".xml" , 'r', 'utf-8' )
	eceTemplate = fin.read()
	fin.close()
	# a cui devo rimpiazzare __ECE_ID__ __ECE_SERVER__ ECE_MODEL __IF_EDITORIAL__
	rimpiazza = {}
	rimpiazza['__ECE_ID__'] =  keyframeEceId 
	rimpiazza['__ECE_SERVER__'] =  os.environ['ECE_SERVER']  
	rimpiazza['__ECE_MODEL__'] =  os.environ['ECE_MODEL']  
	rimpiazza['__ECE_THUMB__'] =  os.environ['ECE_THUMB']  
	rimpiazza['__IF_EDITORIAL__'] =  'EDITORIAL' 

	
	# use these three lines to do the replacement
	rep = dict((re.escape(k), v) for k, v in rimpiazza.items()) 
	pattern = re.compile("|".join(rep.keys()))
	text = pattern.sub(lambda m: rep[re.escape(m.group(0))], eceTemplate)
	
	#print ( text ) 

	xmlStr = entry + text.strip() + '</entry>'
	
	tree = ET.fromstring(xmlStr)
	#ET.dump(root)
	print  ( prendiListaRelated(  tree, 'related' )  )
	listaRelated = prendiListaRelated(  tree, 'related' ) 
	# in listaRelated ho la liste delle relazioni di questo asset
	#print ( ordinaEditorial( listaRelated ) )
	tree = rimuoviListaRelated( tree , 'related' )
	tree = aggiungiEditorialList( tree , listaRelated ) 
	tree = ET.ElementTree(tree)
	nome_file = creaNomeFile( 'keyframe' )
	tree.write(nome_file)
	result = putId(assetEceId, nome_file)

	print ( '------------------------ END addRelEditorialKeyframe -------------- ' )
	logger.debug ( '------------------------ END addRelEditorialKeyframe -------------- ' )
	return result


def sistemaTcOffsetAndPVId( eceId, startTimeInMs, pvId):

	result = False

	print ( '------------------------ INIT sistemaTcOffsetAndPVId -------------- ' )
	logger.debug ( '------------------------ INIT sistemaTcOffsetAndPVId -------------- ' )
	print ( '- sistemo tc_offset a : ' + eceId + ' con valore di partenza : ' + startTimeInMs )
	logger.debug( '- sistemo tc_offset a : ' + eceId + ' con valore di partenza : ' + startTimeInMs )

	[ successGetId, tree ] = getEceId( eceId )
	print ( successGetId )
	print ( tree )
	#ET.dump(root)
	print( prendiField( tree, 'clipsMarkIn' ))
	tc_offset = trasformaTcOffset(int(prendiField( tree, 'clipsMarkIn' )) - int(startTimeInMs))
	tree = cambiaField( tree, 'tc_offset', tc_offset )
	tree = cambiaField( tree, 'programmeVideoId', pvId )

	nome_file = creaNomeFile( 'segment' )
	tree.write(nome_file)
	result = putId(eceId, nome_file)

	print ( '------------------------ END sistemaTcOffsetAndPVId -------------- ' )
	logger.debug ( '------------------------ END sistemaTcOffsetAndPVId -------------- ' )
	return result



#################################################
# 		PRODUCI				#
#################################################


def produciTranscodableVideoDaKafka( message ):

	print ( '------------------------ inizia produciTranscodableVideoDaKafka -------------- ' )
	logger.debug ( '------------------------ inizia produciTranscodableVideoDaKafka -------------- ' )
	# questo mi ritorna tutti gli xml gia' buoni da buttare su ECE
	resultBool = False
	resultJson = {}

	try:

		# prendo il dict e lo trasformo in stringa per pulirlo
		# dalle varie cose inutili e fare i cambiamenti del caso
		messageStr = json.dumps( message )
		xmlStr = pulisciXml( messageStr )
		print ( xmlStr )
		logger.debug( xmlStr )
		# e quindi lo riporto in json
		messageJson = json.loads( xmlStr )

		channel = 'la1'
		if 'channel' in messageJson:
			channel = messageJson['channel']
			
		jsonFlatMessage = flatten( messageJson )
		print ( jsonFlatMessage )

		successGetId = -1
		programme = None
		if 'cms_urn' in jsonFlatMessage and len( jsonFlatMessage['cms_urn']) > 0:
			print ( 'prendo ECE id' )
			logger.debug( 'prendo ECE id' )
			[ successGetId, programmeEceIdTree  ] = getEceId(  jsonFlatMessage['cms_urn'] )
		else:
			print ( 'in produciTranscodableVideoDaKafka non trovo cms_urn' )
			logger.debug( 'in produciTranscodableVideoDaKafka non trovo cms_urn' )
			jsonFlatMessage['cms_urn'] = ''

		sectionId = -1

		if successGetId != -1:
			# qui prendo la sezione in cui si trova il suo programme
			sectionId = prendiSectionDaProgramme( programmeEceIdTree )

		else:
			# qui devo postare / o ricordarmi poi di postare sul topic
			# mam-ece-problems con la chiave #2-no-programme
			print( 'produciTranscodableVideoDaKafka manda mam-ece-problems con la chiave #2-no-programme')
			logger.debug ('produciTranscodableVideoDaKafka manda mam-ece-problems con la chiave #2-no-programme')

		if sectionId == -1:
			# qui costruisco l id della sezione in cui buttarlo
			sectionId = prendiSectionId( jsonFlatMessage )
	

		# ennesimo flusso di scelta della sectionId basato sulla source:
		# se la 'source' nel message e' uguale a ingest-system-sonaps deve
		# andare nella section 10630, se invece source == ingest-media - ingest-systems-cmm 
		# oppure ingest-media-aws deve andare nella 5
		sectionId = prendiSectionDaSource( jsonFlatMessage , sectionId )

		print ('sectionId = ' +  sectionId )
		logger.debug('sectionId = ' +  sectionId )

		# aggiunto se mi arriva direttamente il numero di sezione dalla GUI
		if 'section' in jsonFlatMessage:
			print ( 'section gia assegnata dalla GUI = ' + str( jsonFlatMessage['section'] ))
			jsonFlatMessage['section'] = str(jsonFlatMessage['section'])
		else:
			jsonFlatMessage['section'] = sectionId

		# metto intero messaggio per prendere poi il json aws direttamente da li 
		jsonFlatMessage['message'] = messageJson

		# 		PRODUCI				#
		####### TRANS VIDEO ##############

		jsonFlatMessage = sistemaJsonPerTranscodableVideo( jsonFlatMessage )
		print ( jsonFlatMessage )

		mapJsonMpToEce = mapMptoEce( jsonFlatMessage, listaFields.fieldsMamTranscodableVideo )
		logger.debug( mapJsonMpToEce )
		print ( mapJsonMpToEce )
		# e aggiungo __ECE_MODEL__ dagli os.environ
		mapJsonMpToEce['__ECE_MODEL__'] = os.environ['ECE_MODEL']
		
		# devo arrivare qui con in mapJsonMpToEce il json delle sostituzioni 
		# "giuste" per quel template

		xml4Ece = replaceFielsTemplate( mapJsonMpToEce, "mamtranscodablevideo" )
		logger.debug( xml4Ece )
		print ( xml4Ece )

		if '__MAMURN_MAMTRANSCODABLEVIDEO__' in mapJsonMpToEce:
			resultJson[ mapJsonMpToEce['__MAMURN_MAMTRANSCODABLEVIDEO__'] ] = { 'sectionId' : sectionId, 'xml4Ece' : xml4Ece, 'mljson' : mapJsonMpToEce, 'message' : message }

		#print ( resultJson )
		
		# in resultJson ho la lista degli xml n cui cambiare ancora il MODEL da buttare su
		success = False
		transcodableVideoEceId = ''
		# aggiunto lo strip perche altrimenti con uno spazio iniziale alla stringa arriva 500 internal server error
		[ success, transcodableVideoEceId  ]= createMamStr( jsonFlatMessage['section'], xml4Ece.strip() )

		if success:
			# la  creazone e andata ben aggiungo  questo segment al programme relativo
			# la  creazone e andata ben aggiungo l ECE id al risultato
			resultJson[ mapJsonMpToEce['__MAMURN_MAMTRANSCODABLEVIDEO__'] ][ 'eceId' ] = transcodableVideoEceId
			resultJson[ 'relationData' ] = creaRelationData( transcodableVideoEceId, flatten( message) )

			success = False
			if 'cms_urn' in jsonFlatMessage and len( jsonFlatMessage['cms_urn']) > 0:
				success = addRelTranscodableVideo( jsonFlatMessage['cms_urn'], transcodableVideoEceId )
			else:
				print ( 'in produciTranscodableVideoDaKafka non trovo cms_urn non metto relazione con segment' )
				logger.debug( 'in produciTranscodableVideoDaKafka non trovo cms_urn non metto relazione con segment' )
				# qui devo postare sul topic
				# mam-ece-problems con la chiave #1-no-programmeId
			resultBool = True
			
		else:
			resultJson[ mapJsonMpToEce['__MAMURN_MAMTRANSCODABLEVIDEO__'] ][ 'eceId' ] = -1
			resultBool = False

	except Exception as e:
		print ( 'PROBLEMI in produciTranscodableVideoDaKafka : ' + str(e) )
		logger.debug( 'PROBLEMI in produciTranscodableVideoDaKafka : ' + str(e) )
		return [ False, { 'error' : str(e) } ]

	print ( '------------------------ END produciTranscodableVideoDaKafka -------------- ' )
	logger.debug( '------------------------ END produciTranscodableVideoDaKafka -------------- ' )
	return  [ resultBool, resultJson ]



def produciTranscodableAudioDaKafka( message ):

	print ( '------------------------ inizia produciTranscodableAudioDaKafka -------------- ' )
	logger.debug( '------------------------ inizia produciTranscodableAudioDaKafka -------------- ' )
	# questo mi ritorna tutti gli xml gia' buoni da buttare su ECE
	resultBool = False
	resultJson = {}

	try:

		# prendo il dict e lo trasformo in stringa per pulirlo
		# dalle varie cose inutili e fare i cambiamenti del caso
		messageStr = json.dumps( message )
		xmlStr = pulisciXml( messageStr )
		print ( xmlStr )
		logger.debug( xmlStr )
		# e quindi lo riporto in json
		messageJson = json.loads( xmlStr )

		channel = 'la1'
		if 'channel' in messageJson:
			channel = messageJson['channel']
			
		jsonFlatMessage = flatten( messageJson )

		successGetId = -1
		programme = None
		if 'cms_urn' in jsonFlatMessage and len( jsonFlatMessage['cms_urn']) > 0:
			print ( 'prendo ECE id' )
			logger.debug( 'prendo ECE id' )
			[ successGetId, programmeEceIdTree  ] = getEceId(  jsonFlatMessage['cms_urn'] )
		else:
			print ( 'in produciTranscodableAudioDaKafka non trovo cms_urn' )
			logger.debug( 'in produciTranscodableAudioDaKafka non trovo cms_urn' )
			jsonFlatMessage['cms_urn'] = ''

		sectionId = -1

		if successGetId != -1:
			# qui prendo la sezione in cui si trova il suo programme
			sectionId = prendiSectionDaProgramme( programmeEceIdTree )

		if sectionId == -1:
			# qui costruisco l id della sezione in cui buttarlo
			sectionId = prendiSectionId( jsonFlatMessage )
		
		# ennesimo flusso di scelta della sectionId basato sulla source:
		# se la 'source' nel message e' uguale a ingest-system-sonaps deve
		# andare nella section 10630, se invece source == ingest-media - ingest-systems-cmm 
		# oppure ingest-media-aws deve andare nella 5
		sectionId = prendiSectionDaSource( jsonFlatMessage , sectionId )

		print ('sectionId = ' +  sectionId )
		logger.debug('sectionId = ' +  sectionId )

		# aggiunto se mi arriva direttamente il numero di sezione dalla GUI
		if 'section' in jsonFlatMessage:
			print ( 'section gia assegnata dalla GUI = ' + str( jsonFlatMessage['section'] ))
			jsonFlatMessage['section'] = str(jsonFlatMessage['section'])
		else:
			jsonFlatMessage['section'] = sectionId

		# metto intero messaggio per prendere poi il json aws direttamente da li 
		jsonFlatMessage['message'] = messageJson

		# 		PRODUCI				#
		####### TRANS AUDIO ##############

		jsonFlatMessage = sistemaJsonPerTranscodableAudio( jsonFlatMessage )
		#print ( jsonFlatMessage )
		#logger.debug( jsonFlatMessage )

		mapJsonMpToEce = mapMptoEce( jsonFlatMessage, listaFields.fieldsMamTranscodableVideo )
		print ( mapJsonMpToEce )
		logger.debug( mapJsonMpToEce )
		# e aggiungo __ECE_MODEL__ dagli os.environ
		mapJsonMpToEce['__ECE_MODEL__'] = os.environ['ECE_MODEL']
		
		# devo arrivare qui con in mapJsonMpToEce il json delle sostituzioni 
		# "giuste" per quel template

		xml4Ece = replaceFielsTemplate( mapJsonMpToEce, "mamtranscodableaudio" )
		print ( xml4Ece )
		logger.debug( xml4Ece )

		if '__MAMURN_MAMTRANSCODABLEVIDEO__' in mapJsonMpToEce:
			resultJson[ mapJsonMpToEce['__MAMURN_MAMTRANSCODABLEVIDEO__'] ] = { 'sectionId' : sectionId, 'xml4Ece' : xml4Ece, 'mljson' : mapJsonMpToEce, 'message' : message }

		#print ( resultJson )
		#logger.debug( resultJson )
		
		# in resultJson ho la lista degli xml n cui cambiare ancora il MODEL da buttare su
		success = False
		transcodableAudioEceId = ''
		# aggiunto lo strip perche altrimenti con uno spazio iniziale alla stringa arriva 500 internal server error
		[ success, transcodableAudioEceId  ]= createMamStr( jsonFlatMessage['section'], xml4Ece.strip() )

		if success:
			# la  creazone e andata ben aggiungo  questo segment al programme relativo
			success = False
			if 'cms_urn' in jsonFlatMessage and len( jsonFlatMessage['cms_urn']) > 0:
				success = addRelTranscodableAudio( jsonFlatMessage['cms_urn'], transcodableAudioEceId )
			else:
				print ( 'in produciTranscodableAudioDaKafka non trovo cms_urn non metto relazione con segment' )
				logger.debug( 'in produciTranscodableAudioDaKafka non trovo cms_urn non metto relazione con segment' )
			# la  creazone e andata ben aggiungo l ECE id al risultato
			resultJson[ mapJsonMpToEce['__MAMURN_MAMTRANSCODABLEVIDEO__'] ][ 'eceId' ] = transcodableAudioEceId
			resultJson[ 'relationData' ] = creaRelationData( transcodableAudioEceId, flatten( message) )
			resultBool = True
			
		else:
			resultJson[ mapJsonMpToEce['__MAMURN_MAMTRANSCODABLEVIDEO__'] ][ 'eceId' ] = -1
			resultBool = False

	except Exception as e:
		print ( 'PROBLEMI in produciTranscodableAudioDaKafka : ' + str(e) )
		logger.debug( 'PROBLEMI in produciTranscodableAudioDaKafka : ' + str(e) )
		return [ False, { 'error' : str(e) } ]

	print ( '------------------------ END produciTranscodableAudioDaKafka -------------- ' )
	logger.debug( '------------------------ END produciTranscodableAudioDaKafka -------------- ' )
	return  [ resultBool, resultJson ]



def produciKeyframeDaKafka( message ):

	print ( '------------------------ inizia produciKeyframeDaKafka -------------- ' )
	logger.debug( '------------------------ inizia produciKeyframeDaKafka -------------- ' )
	# questo mi ritorna tutti gli xml gia' buoni da buttare su ECE
	resultBool = False
	resultJson = {}

	try:

		# prendo il dict e lo trasformo in stringa per pulirlo
		# dalle varie cose inutili e fare i cambiamenti del caso
		messageStr = json.dumps( message )
		xmlStr = pulisciXml( messageStr )
		print ( xmlStr )
		logger.debug( xmlStr )
		# e quindi lo riporto in json
		messageJson = json.loads( xmlStr )

		jsonFlatMessage = flatten( messageJson )
		print ( jsonFlatMessage )

		successGetId = -1
		programmeEceIdTree = None
		if 'video_cms_asset_urn' in jsonFlatMessage:
			print ( 'prendo ECE id' )
			logger.debug( 'prendo ECE id' )
			[ successGetId, programmeEceIdTree  ] = getEceId(  jsonFlatMessage['video_cms_asset_urn'] )
		else:
			print ( 'in produciKeyframeDaKafka non trovo cms_urn' )
			print ( 'in produciKeyframeDaKafka lo metto nella sezion incomingKeyframes' )
			logger.debug( 'in produciKeyframeDaKafka non trovo cms_urn' )
			jsonFlatMessage['cms_urn'] = ''

		if not 'url' in jsonFlatMessage:
			print ( 'in produciKeyframeDaKafka non trovo il keyframe' )
			logger.debug( 'in produciKeyframeDaKafka non trovo il keyframe' )
			return [ False, {'error':'non trovato il Keyframe' } ]

		sectionId = -1

		if successGetId != -1:
			# qui prendo la sezione in cui si trova il suo programme
			sectionId = prendiSectionDaProgramme( programmeEceIdTree )

			# e qui ne prendo lo startTime come da MG-96
			#print (jsonFlatMessage)
			# e lo sbatto nel campo rights_activationDate cosi' sotto
			# nella sistemaJsonPerKeyframe lo metteranno al posto giusto
			# programmeEceIdTree.write('./test_programmeEceIdTree.xml')
		if sectionId == -1:
			# qui costruisco l id della sezione in cui buttarlo
			sectionId = prendiSectionId( jsonFlatMessage )

		print ( sectionId )
		logger.debug( sectionId )
		
		jsonFlatMessage['section'] = sectionId
		# metto intero mesaggio per prendere poi il json aws direttamente da li 
		jsonFlatMessage['message'] = messageJson

		# 		PRODUCI				#
		####### KEYFRAME ##############

		resultBool = False
		imageEceId = -1
		title = jsonFlatMessage['url'].split('/')[-1]
		if len(title) < 8:
			title = 'KeyFrame_Title'
		if  'video__id' in jsonFlatMessage:
			title = jsonFlatMessage['video__id'] + '_' + title
		[ resultBool, imageEceId ] = importaImgFromAws( jsonFlatMessage['url'], jsonFlatMessage['section'], title )

		if not resultBool:
			return [ False, { 'error' : "in produciKeyframeDaKafka non riesco ad importare img" } ]

		else:
			# la  creazone e andata ben aggiungo  questo segment al programme relativo
			success = False
			if 'video_cms_asset_urn' in jsonFlatMessage:
				success = addRelEditorialKeyframe( jsonFlatMessage['video_cms_asset_urn'], imageEceId )
			else:
				print ( 'in produciKeyframeDaKafka non trovo cms_urn non metto relazione ' )
				logger.debug( 'in produciKeyframeDaKafka non trovo cms_urn non metto relazione ' )
				jsonFlatMessage['video_cms_asset_urn'] = ''
			# la  creazone e andata ben aggiungo l ECE id al risultato

		if success:
			resultBool = True
			resultJson = jsonFlatMessage
		else:
			resultJson = { 'error': 'not success in produciKeyframeDaKafka' }
			resultBool = False

	except Exception as e:
		print ( 'PROBLEMI in produciKeyframeDaKafka : ' + str(e) )
		logger.debug( 'PROBLEMI in produciKeyframeDaKafka : ' + str(e) )
		return [ False, { 'error' : str(e) } ]

	print ( '------------------------ END produciKeyframeDaKafka -------------- ' )
	logger.debug( '------------------------ END produciKeyframeDaKafka -------------- ' )
	return  [ resultBool, resultJson ]




def produciProgrammeVideoDaKafka( message ):

	print ( '------------------------ inizia produciProgrammeVideoDaKafka -------------- ' )
	logger.debug( '------------------------ inizia produciProgrammeVideoDaKafka -------------- ' )
	# questo mi ritorna tutti gli xml gia' buoni da buttare su ECE
	resultBool = False
	resultJson = {}

	try:

		# prendo il dict e lo trasformo in stringa per pulirlo
		# dalle varie cose inutili e fare i cambiamenti del caso
		messageStr = json.dumps( message )
		xmlStr = pulisciXml( messageStr )
		print ( xmlStr )
		logger.debug( xmlStr )
		# e quindi lo riporto in json
		messageJson = json.loads( xmlStr )

		channel = 'la1'
		if 'channel' in messageJson:
			channel = messageJson['channel']
			
		jsonFlatMessage = flatten( messageJson )
		print(jsonFlatMessage)

		# per sistemare il titolo
		jsonFlatMessage['title'] = jsonFlatMessage['title'].split('la1 - ')[-1].strip()
		jsonFlatMessage['title'] = jsonFlatMessage['title'].split('la2 - ')[-1].strip()

		successGetId = -1
		programme = None
		if 'cms_urn' in jsonFlatMessage and len( jsonFlatMessage['cms_urn']) > 0:
			print ( 'prendo ECE id' )
			logger.debug( 'prendo ECE id' )
			[ successGetId, programmeEceIdTree  ] = getEceId(  jsonFlatMessage['cms_urn'] )
		else:
			print ( 'in produciProgrammeVideoDaKafka non trovo cms_urn' )
			logger.debug( 'in produciProgrammeVideoDaKafka non trovo cms_urn' )
			jsonFlatMessage['cms_urn'] = ''

		sectionId = -1

		if successGetId != -1:
			print( 'in successGetId ------------------------- ' )
			logger.debug( 'in successGetId ------------------------- ' )
			# qui prendo la sezione in cui si trova il suo programme
			sectionId = prendiSectionDaProgramme( programmeEceIdTree )

			# e qui ne prendo lo startTime come da MG-96
			#print (jsonFlatMessage)
			# e lo sbatto nel campo rights_activationDate cosi' sotto
			# nella sistemaJson lo metteranno al posto giusto
			# programmeEceIdTree.write('./test_programmeEceIdTree.xml')
			jsonFlatMessage['rights_activationDate'] = prendiDateUpdate( programmeEceIdTree )
			print( jsonFlatMessage['rights_activationDate'] )
			 
			tmpTitle = prendiField( programmeEceIdTree, 'title' )
			if len(tmpTitle) > 0:
				jsonFlatMessage['title'] = tmpTitle
			print( jsonFlatMessage['title'] ) 
	
		if sectionId == -1:
			# qui costruisco l id della sezione in cui buttarlo
			sectionId = prendiSectionId( jsonFlatMessage )

		

		# ennesimo flusso di scelta della sectionId basato sulla source:
		# se la 'source' nel message e' uguale a ingest-system-sonaps deve
		# andare nella section 10630, se invece source == ingest-media - ingest-systems-cmm 
		# oppure ingest-media-aws deve andare nella 5
		sectionId = prendiSectionDaSource( jsonFlatMessage , sectionId )

		print ('sectionId = ' +  sectionId )
		logger.debug('sectionId = ' +  sectionId )

		# in questo modo i json che arrivano dalla GUI con section gia settata
		# non vengono sovrascritti mentre a tutti gli altri viene settata la sezione 
		# appena calcolata
		if 'section' in jsonFlatMessage:
			print ( 'section gia assegnata dalla GUI = ' + str( jsonFlatMessage['section'] ))
			jsonFlatMessage['section'] = str(jsonFlatMessage['section'])
		else:
			jsonFlatMessage['section'] = sectionId

		# metto intero mesaggio per prendere poi il json aws direttamente da li 
		jsonFlatMessage['message'] = messageJson

		# 		PRODUCI				#
		# 		PROGRAMMEVIDEO			#

		jsonFlatMessage = sistemaJsonPerProgrammeVideo( jsonFlatMessage )
		print ( jsonFlatMessage )
		#logger.debug( jsonFlatMessage )
		#print ( jsonFlatMessage )

		mapJsonMpToEce = mapMptoEce( jsonFlatMessage, listaFields.fieldsMamProgrammeVideo )
		logger.debug( mapJsonMpToEce )
		print ( mapJsonMpToEce )
		# e aggiungo __ECE_MODEL__ dagli os.environ
		mapJsonMpToEce['__ECE_MODEL__'] = os.environ['ECE_MODEL']

		# devo arrivare qui con in mapJsonMpToEce il json delle sostituzioni 
		# "giuste" per quel template

		xml4Ece = replaceFielsTemplate( mapJsonMpToEce, "mamprogrammevideo" )
		#logger.debug( xml4Ece )
		#print ( xml4Ece )

		if '__MAMURN_MAMPROGRAMMEVIDEO__' in mapJsonMpToEce:
			resultJson[ mapJsonMpToEce['__MAMURN_MAMPROGRAMMEVIDEO__'] ] = { 'sectionId' : sectionId, 'xml4Ece' : xml4Ece, 'mljson' : mapJsonMpToEce, 'message' : message }

		#logger.debug( resultJson )
		
		# in resultJson ho la lista degli xml n cui cambiare ancora il MODEL da buttare su
		success = False
		programmeVideoEceId = ''
		# aggiunto lo strip perche altrimenti con uno spazio iniziale alla stringa arriva 500 internal server error
		[ success, programmeVideoEceId  ]= createMamStr( jsonFlatMessage['section'], xml4Ece.strip() )

		if success:
			# la  creazone e andata ben aggiungo  questo segment al programme relativo
			success = False
			if 'cms_urn' in jsonFlatMessage and len( jsonFlatMessage['cms_urn']) > 0:
				success = addRelProgrammeVideo( jsonFlatMessage['cms_urn'], programmeVideoEceId, jsonFlatMessage['clips_markIn'] )
			else:
				print ( 'in produciProgrammeVideoDaKafka non trovo cms_urn non metto relazione con segment' )
				logger.debug( 'in produciProgrammeVideoDaKafka non trovo cms_urn non metto relazione con segment' )

			# la  creazione e andata ben aggiungo l ECE id al risultato
			resultJson[ mapJsonMpToEce['__MAMURN_MAMPROGRAMMEVIDEO__'] ][ 'eceId' ] = programmeVideoEceId
			resultJson[ 'relationData' ] = creaRelationData( programmeVideoEceId, flatten( message) )
			resultBool = True
			
		else:
			resultJson[ mapJsonMpToEce['__MAMURN_MAMPROGRAMMEVIDEO__'] ][ 'eceId' ] = -1
			resultBool = False

	except Exception as e:
		print ( 'PROBLEMI in produciProgrammeVideoDaKafka : ' + str(e) )
		logger.debug( 'PROBLEMI in produciProgrammeVideoDaKafka : ' + str(e) )
		return [ False, { 'error' : str(e) } ]

	print ( '------------------------ END produciProgrammeVideoDaKafka -------------- ' )
	logger.debug( '------------------------ END produciProgrammeVideoDaKafka -------------- ' )
	return  [ resultBool, resultJson ]



def produciSegmentVideoDaKafka( message ):

	print ( '------------------------ inizia produciSegmentVideoDaKafka -------------- ' )
	logger.debug( '------------------------ inizia produciSegmentVideoDaKafka -------------- ' )
	# questo mi ritorna tutti gli xml gia' buoni da buttare su ECE
	resultBool = False
	resultJson = {}

	try:

		# prendo il dict e lo trasformo in stringa per pulirlo
		# dalle varie cose inutili e fare i cambiamenti del caso
		messageStr = json.dumps( message )
		xmlStr = pulisciXml( messageStr )
		print ( xmlStr )
		logger.debug( xmlStr )
		# e quindi lo riporto in json
		messageJson = json.loads( xmlStr )

		channel = 'la1'
		if 'channel' in messageJson:
			channel = messageJson['channel']
			
		jsonFlatMessage = flatten( messageJson )

		successGetId = -1
		programme = None
		if 'cms_urn' in jsonFlatMessage and len( jsonFlatMessage['cms_urn']) > 0:
			# qui ne porendo il porogramme di riferimento
			[ successGetId, programmeEceIdTree  ] = getEceId(  jsonFlatMessage['cms_urn'] )
		else:
			print ( 'in produciSegmentVideoDaKafka non trovo cms_urn' )
			logger.debug( 'in produciSegmentVideoDaKafka non trovo cms_urn' )
			jsonFlatMessage['cms_urn'] = ''

		sectionId = -1


		if successGetId != -1:
			# qui prendo la sezione in cui si trova il suo programme
			sectionId = prendiSectionDaProgramme( programmeEceIdTree )
			# e qui ne prendo lo startTime come da MG-96
			#print (jsonFlatMessage)
			# e lo sbatto nel campo rights_activationDate cosi' sotto
			# nella sistemaJsonPerSegmentedVideo lo metteranno al posto giusto
			# programmeEceIdTree.write('./test_programmeEceIdTree.xml')
			jsonFlatMessage['rights_activationDate'] = prendiDateUpdate( programmeEceIdTree )
			print( 'rights_activationDate : ' + jsonFlatMessage['rights_activationDate'] )
			
			# per arrivare alla sistemaJson con il valore eventualmente giusto di offset
			# vado a vedere se nel programmeEceIdTree qualche anima pia ha settato 
			# il valore di startTimeInMs
			jsonFlatMessage['startTimeInMs'] = prendiStartTimeInMs( programmeEceIdTree )
			print( 'startTimeInMs : ' + jsonFlatMessage['startTimeInMs'] )

			# stessa identica cosa per il valore del ProgrammeVideoId da mettere 
			# nel segment 
			# devo andare a vedere se nel programme c'e' gia' il PVideo
			# nelle relation allora devo prendere quel PVid
			# altrimenti lascio il valore nullo e ci pensara' poi il
			# Pvideo ad aggiornarlo quando verra' attaccato al Programme
			listaRelated = prendiListaRelated( programmeEceIdTree, 'related' )
			jsonFlatMessage['programmeVideoId'] = prendiPVideoIdDaRel( listaRelated )
		
		if sectionId == -1:
			# qui costruisco l id della sezione in cui buttarlo
			sectionId = prendiSectionId( jsonFlatMessage )

		# ennesimo flusso di scelta della sectionId basato sulla source:
		# se la 'source' nel message e' uguale a ingest-system-sonaps deve
		# andare nella section 10630, se invece source == ingest-media - ingest-systems-cmm 
		# oppure ingest-media-aws deve andare nella 5
		sectionId = prendiSectionDaSource( jsonFlatMessage , sectionId )

		print ('sectionId = ' +  sectionId )
		logger.debug('sectionId = ' +  sectionId )

		# in questo modo i json che arrivano dalla GUI con section gia settata
		# non vengono sovrascritti mentre a tutti gli altri viene settata la sezione 
		# appena calcolata
		if 'section' in jsonFlatMessage:
			print ( 'section gia assegnata dalla GUI = ' + str( jsonFlatMessage['section'] ))
			jsonFlatMessage['section'] = str(jsonFlatMessage['section'])
		else:
			jsonFlatMessage['section'] = sectionId

		# 		PRODUCI				#
		####### SEGMENTED P VIDEO ##############


		jsonFlatMessage = sistemaJsonPerSegmentedVideo( jsonFlatMessage )
		print ( jsonFlatMessage )
		logger.debug( jsonFlatMessage )

		#esempio di jsonFlatMessage = {'channel': 'la1', 'source': 'OpenMedia', 'day': '08/06/2020', 'order': 10, 'urn': '9dc1ae82efcdde8895a93611861ef35e', 'rights_geoblocked': False, 'clips': [{'markOut': 1591640706094, 'title': 'RSI La1 - TG 20:00 08.06.2020 - Roma, sgombera Casa Pound', 'markIn': 1591640530117}], 'lastupdate': '2020-06-08T06:30:50Z', 'composedEvent': False, 'title': 'RSI La1 - TG 20:00 08.06.2020 - Roma, sgombera Casa Pound', '_id': '9dc1ae82efcdde8895a93611861ef35e', 'awsKeyframe_status': 'qualcosa', 'rights_expireDate': 'null', 'assetType': 'segmentCut', 'rights_activationDate': 'null', 'state': 'awsError', 'cms_urn': 'urn:rsi:escenic:13067754', '_rev': '3-0353946fe44f020ec507cfd0b0b02af5', 'awsKeyframe_url': 'http://s3sourceimage', 'normalized': True}

		mapJsonMpToEce = mapMptoEce( jsonFlatMessage, listaFields.fieldsMamSegmentVideo )
		print ( mapJsonMpToEce )
		logger.debug( mapJsonMpToEce )

			# e aggiungo __ECE_MODEL__ dagli os.environ
		mapJsonMpToEce['__ECE_MODEL__'] = os.environ['ECE_MODEL']
		
		# devo arrivare qui con in mapJsonMpToEce il json delle sostituzioni 
		# "giuste" per quel template

		xml4Ece = replaceFielsTemplate( mapJsonMpToEce, "mamsegmentedprogrammevideo" )
		print ( xml4Ece )
		logger.debug( xml4Ece )

		if '__MAMURN_MAMSEGMENTVIDEO__' in mapJsonMpToEce:
			resultJson[ mapJsonMpToEce['__MAMURN_MAMSEGMENTVIDEO__'] ] = { 'sectionId' : sectionId, 'xml4Ece' : xml4Ece, 'mljson' : mapJsonMpToEce, 'message' : message }

		#print ( resultJson )
		#logger.debug( resultJson )
		
		# in resultJson ho la lista degli xml n cui cambiare ancora il MODEL da buttare su
		success = False
		segmentEceId = ''
		# aggiunto lo strip perche altrimenti con uno spazio iniziale alla stringa arriva 500 internal server error
		[ success, segmentEceId  ]= createMamStr( sectionId, xml4Ece.strip() )

		if 'order' in jsonFlatMessage:
			order = int(jsonFlatMessage['order'])
		else:
			order = 0
		if success:
			# prendo il valore di ordine del segmento
			# la  creazone e andata ben aggiungo  questo segment al programme relativo
			print ( jsonFlatMessage ) 
			success = False
			if 'cms_urn' in jsonFlatMessage and len( jsonFlatMessage['cms_urn']) > 0:
				#success = addRelSegment( jsonFlatMessage['cms_urn'], segmentEceId )
				success = addRelSegmentOrder( jsonFlatMessage['cms_urn'], segmentEceId , order)
			else:
				print ( 'in produciSegmentVideoDaKafka non trovo cms_urn non metto relazione con segment' )
				logger.debug( 'in produciSegmentVideoDaKafka non trovo cms_urn non metto relazione con segment' )
			# la  creazone e andata ben aggiungo l ECE id al risultato
			resultJson[ mapJsonMpToEce['__MAMURN_MAMSEGMENTVIDEO__'] ][ 'eceId' ] = segmentEceId
			resultJson[ 'relationData' ] = creaRelationData( segmentEceId, flatten( message) )
			resultBool = True
			
		else:
			resultJson[ mapJsonMpToEce['__MAMURN_MAMSEGMENTVIDEO__'] ][ 'eceId' ] = -1
			resultBool = False

	except Exception as e:
		print ( 'PROBLEMI in produciSegmentVideoDaKafka : ' + str(e) )
		logger.debug( 'PROBLEMI in produciSegmentVideoDaKafka : ' + str(e) )
		return [ False, { 'error' : str(e) } ]

	print ( '------------------------ END produciSegmentVideoDaKafka -------------- ' )
	logger.debug( '------------------------ END produciSegmentVideoDaKafka -------------- ' )
	return  [ resultBool, resultJson ]


	
def produciProgrammeDaKafka( message ):

	print ( '------------------------ inizia produciProgrammeDaKafka -------------- ' )
	logger.debug( '------------------------ inizia produciProgrammeDaKafka -------------- ' )
	# questo mi ritorna tutti gli xml gia' buoni da buttare su ECE
	resultBool = False
	resultJson = {}

	try:

		# prendo il dict e lo trasformo in stringa per pulirlo
		# dalle varie cose inutili e fare i cambiamenti del caso
		messageStr = json.dumps( message )
		xmlStr = pulisciXml( messageStr )
		# e quindi lo riporto in json
		messageJson = json.loads( xmlStr )

		channel = 'la1'
		if 'channel' in messageJson:
			channel = messageJson['channel']
		
		jsonFlatRundown = flatten( messageJson )
		#print ( jsonFlatRundown )
		#logger.debug( jsonFlatRundown )
		jsonFlatRundown = sistemaJsonPerProgramme( jsonFlatRundown )
		print ( jsonFlatRundown )
		logger.debug( jsonFlatRundown )

		mapJsonMpToEce = mapMptoEce( jsonFlatRundown, listaFields.fieldsMamProgramme )

		#print ( mapJsonMpToEce )
		#logger.debug( mapJsonMpToEce )

		# qui costruisco l id della sezione in cui buttarlo
		sectionId = prendiSectionId( mapJsonMpToEce )
		
		print ( sectionId )
		logger.debug( sectionId )

		# e aggiungo __ECE_MODEL__ dagli os.environ
		mapJsonMpToEce['__ECE_MODEL__'] = os.environ['ECE_MODEL']

		# 		PRODUCI				#
		####### PROGRAMME ##############

		xml4Ece = replaceFielsTemplate( mapJsonMpToEce, "mamprogramme" )
		#print ( xml4Ece )
		#logger.debug( xml4Ece )

		if '__MAMURN_MAMPROGRAMME__' in mapJsonMpToEce:
			resultJson[ mapJsonMpToEce['__MAMURN_MAMPROGRAMME__'] ] = { 'sectionId' : sectionId, 'xml4Ece' : xml4Ece, 'mljson' : mapJsonMpToEce, 'message' : message }

		#print ( resultJson )
		
		# in resultJson ho la lista degli xml n cui cambiare ancora il MODEL da buttare su
		success = False
		[ success, eceId  ]= createMamStr( sectionId, xml4Ece )
		
		if success:
			# la  creazione e andata ben aggiungo l ECE id al risultato
			resultJson[ mapJsonMpToEce['__MAMURN_MAMPROGRAMME__'] ][ 'eceId' ] = eceId
			resultJson[ 'relationData' ] = creaRelationDataProgramme( eceId, flatten( message), sectionId )
			resultBool = True
			
		else:
			resultJson[ mapJsonMpToEce['__MAMURN_MAMPROGRAMME__'] ][ 'eceId' ] = -1
			resultBool = False

	except Exception as e:
		print ( 'PROBLEMI in produciProgrammeDaKafka : ' + str(e) )
		logger.debug( 'PROBLEMI in produciProgrammeDaKafka : ' + str(e) )
		return [ False, { 'error' : str(e) } ]

	print ( '------------------------ END produciProgrammeDaKafka -------------- ' )
	logger.debug( '------------------------ END produciProgrammeDaKafka -------------- ' )
	return  [ resultBool, resultJson ]

#################################################
# 		PRODUCI				#
#################END#############################

def preparaJsonRelationProgramme(  eceId,flatMsg, sectionId ):
	result = {}

	tmpjson = listaFields.fieldsRelationPerGiu

	for key,value in tmpjson.items() :
		if value in flatMsg:
			result[ key ] = flatMsg[ value ] 
	result['escenicId'] = int(eceId)
	result['SectionId'] = int(sectionId)
	print ( result )
	logger.debug( 'json per Giu :: ' + str( result ))

	return result

def creaRelationDataProgramme( eceId, flatMsg, sectionId ):
	result = {}
	# deve creare un json come questo:
	#testData = {"urn":"rsi:mam:9ea7cdb0142fef040426ebbddb41b296",
		#"cms":{
		#"urn":"rsi:escenic:12682029"
		##},
		#"sourceSystem":{
		#"urn":"rsi:mp:102867428"
		#}
	#}
	#testChiave = "rsi:mam:9ea7cdb0142fef040426ebbddb41b296-rsi:escenic:12682029" 

	jsonPerGiu = preparaJsonRelationProgramme( eceId, flatMsg, sectionId )
	
	aggiuntaRotturadiCazzo = 'rundown'
	if 'Video' in flatMsg['contentType']:
		aggiuntaRotturadiCazzo = 'video'
	elif 'Audio' in  flatMsg['contentType']:
		aggiuntaRotturadiCazzo = 'audio'
	 
	if 'sourceSystem_urn' in flatMsg:
		result = { "chiave" : "rsi:mam:" + aggiuntaRotturadiCazzo + ':'+ flatMsg['_id'] + "-" + "rsi:escenic:"   + flatMsg['contentType'] + ':' + eceId,
			   "value" : { "urn":"rsi:mam:" + aggiuntaRotturadiCazzo + ':' + flatMsg['_id'],
					"cms" :  {
						"urn":"rsi:escenic:"   + flatMsg['contentType'] + ':' + eceId ,
						"content": jsonPerGiu,
					},
					"sourceSystem":{ 
					"urn":flatMsg[ 'sourceSystem_urn' ]
					}
				}
			}
	else:
		result = { "chiave" : "rsi:mam:" + aggiuntaRotturadiCazzo + ':'+ flatMsg['_id'] + "-" + "rsi:escenic:"  + flatMsg['contentType'] + ':' + eceId,
			   "value" : { "urn":"rsi:mam:" +aggiuntaRotturadiCazzo + ':' + flatMsg['_id'],
					"cms" :  {
						"urn":"rsi:escenic:" + flatMsg['contentType'] + ':' + eceId ,
						"content": jsonPerGiu
					},
				}
			}

	print( 'relationData :: ' + str( result ) )
	logger.debug( 'relationData :: ' + str(result ) )
	
	return result

def creaRelationData( eceId, flatMsg ):
	result = {}
	# deve creare un json come questo:
	#testData = {"urn":"rsi:mam:9ea7cdb0142fef040426ebbddb41b296",
		#"cms":{
		#"urn":"rsi:escenic:12682029"
		##},
		#"sourceSystem":{
		#"urn":"rsi:mp:102867428"
		#}
	#}
	#testChiave = "rsi:mam:9ea7cdb0142fef040426ebbddb41b296-rsi:escenic:12682029" 
	aggiuntaRotturadiCazzo = 'rundown'
	if 'Video' in flatMsg['contentType']:
		aggiuntaRotturadiCazzo = 'video'
	elif 'Audio' in  flatMsg['contentType']:
		aggiuntaRotturadiCazzo = 'audio'
	 
	if 'sourceSystem_urn' in flatMsg:
		result = { "chiave" : "rsi:mam:" + aggiuntaRotturadiCazzo + ':'+ flatMsg['_id'] + "-" + "rsi:escenic:"   + flatMsg['contentType'] + ':' + eceId,
			   "value" : { "urn":"rsi:mam:" + aggiuntaRotturadiCazzo + ':' + flatMsg['_id'],
					"cms" :  {
						"urn":"rsi:escenic:"   + flatMsg['contentType'] + ':' + eceId 
					},
					"sourceSystem":{ 
					"urn":flatMsg[ 'sourceSystem_urn' ]
					}
				}
			}
	else:
		result = { "chiave" : "rsi:mam:" + aggiuntaRotturadiCazzo + ':'+ flatMsg['_id'] + "-" + "rsi:escenic:"  + flatMsg['contentType'] + ':' + eceId,
			   "value" : { "urn":"rsi:mam:" +aggiuntaRotturadiCazzo + ':' + flatMsg['_id'],
					"cms" :  {
						"urn":"rsi:escenic:" + flatMsg['contentType'] + ':' + eceId 
					},
				}
			}

	print( 'relationData :: ' + str( result ) )
	logger.debug( 'relationData :: ' + str(result ) )
	
	return result




def PutJsonToRest( dati, path, nomefile ):

	logger.debug( '------------------------ inizia PutJsonToRest -------------- ' )
	#print( 'dati : ' + str(dati)  )
	try:
		prod_ece = 'http://internal.publishing.production.rsi.ch/webservice/escenic/content/'
		stag_ece = 'https://sslstagingapp.rsi.ch/rsi-api/intlay/test/writefblands'
		prod_ece = 'http://www.rsi.ch/rsi-api/fblikesandshares/writefblands'
		

		
		#link = os.environ['ECE_SERVER']  + str(idx)
		link = os.environ['ECE_REST']


		logger.debug( 'path : ' + path )
		logger.debug( 'nomefile : ' + nomefile )
		headers = {
		'rsiauthentication': 'fb14nd5ru135',
		'pathfromheader': path,
		'nomefilefromheader': nomefile
		}    


		r = requests.post(link, headers=headers, json=dati)
		logger.debug( 'risultato post su :' + prod_ece + ' con autentication ' ) 
		logger.debug( r.status_code  )
		resultJson = r.json() 
		logger.debug( resultJson )
	
	except Exception as e:
		logger.debug( 'PROBLEMI in PutJsonToRest : ' + str(e) )
		return False

	logger.debug('------------------------ finisce PutJsonToRest -------------- ')
	return True

def StampaTutti():

	for i in range(100,234):
		path = "../Resources/message_" +  str(i)
		print ( path)
		
		fin = codecs.open(path,'r','utf-8')
		stampaCampi( fin.read() )
		fin.close()

	exit(0)

def stampaCampi( dictIn ):

	print (type( dictIn))
	dictIn = json.loads( dictIn )
	print (dictIn['channel'])

	for dic in dictIn['rundown']:
		
		dicWork = flatten( dic )
		for key,value in dicWork.items():
			#print ( key,value)
			if  'press' in key or 'title' in key:
				print ( key,value)
		print ()

def stampaCampiCaveSorted( dictIn ):

        print (type( dictIn))
        dictIn = json.loads( dictIn )

        dicWork = flatten( dictIn )
        for key in sorted(dicWork):
                
                print ( '\"XXX\":\"' + key  + '\",')
        print ()


def stampaCampiCave( dictIn ):

        print (type( dictIn))
        dictIn = json.loads( dictIn )

        dicWork = flatten( dictIn )
        for key,value in dicWork.items():
                #print ( key,value)
                print ( '\"XXX\":\"' + key  + '\",')
                #if  'press' in key or 'title' in key:
                        #print ( key,value)
        print ()


def buttasu( filePath, section ):

	print( '------------------------ inizia buttasu -------------- '  )

	result = [False, '']

	# che deve fare la  curl --include -u perucccl:perucccl -X POST -H "Content-Type: application/atom+xml"  
	# http://internal.publishing.production.rsi.ch/webservice/escenic/section/__CREATE_SECTION__/content-items 
	# --upload-file Resources/template_mamProgramme.xml
	# dove __CREATE_SECTION__ deve essere cambiato con section e il file template deve essere aggiornato con i valori
	# dei parametri
	# 5909 e la temp

	try:
		#link =  os.environ['IMG_CREATE_URL'].replace('__CREATE_SECTION__', section )

		link =  "http://internal.publishing.staging.rsi.ch/webservice/escenic/section/" + section  + "/content-items"
		print( 'link : ' + link  )
		
		# apro il template file
		auth = '%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])
		base64string = b64encode(auth.encode())
		base64string = base64string.decode("ascii")

		headers = { 'Authorization' : 'Basic %s' % base64string,
				'Content-Type': 'application/atom+xml'
			}    

	        #dati=open( os.environ['VID_RESOURCE_TEMPLATE'] ,'r').read()
		dati=open( filePath,'r').read()
		dati = dati.replace('__ECE_MODEL__', 'http://internal.publishing.staging.rsi.ch/webservice/publication/rsi/escenic/model/' )
		#dati = dati.replace('__CREATE_SECTION__', section )
		#dati = dati.replace('__TITOLO_PICTURE__', titolo )
		#dati = dati.replace('__BINARY_LOCATION__', binaryUrl )

		print( ' data  = ' + dati  )
		dati = ( dati ).encode()

		request = urllib.request.Request(url=link, data=dati, headers=headers, method='POST')
		resultResponse = urllib.request.urlopen(request)
		print ( resultResponse.status )
		#print ( resultResponse.status )
		#print ( resultResponse.headers )
		#print ( resultResponse.headers )
		#print ( resultResponse.headers.split('rsi.ch/webservice/escenic/content/' )[-1] )
		#print ( resultResponse.reason )
		print ( resultResponse.getheader('Location') )
		#print ( resultResponse.getheader('ocation') )
		#for lis  in resultResponse.getheaders():
			#print ( lis )
			#print (type(lis))

	
	except Exception as e:
		print( 'PROBLEMI in buttasu: ' + str(e)  )
		return [False, '' ]

	print('------------------------ finisce buttasu-------------- ' )
	return result

def prendiXmlField( doc, name ):

	content = doc.getElementsByTagName('content')[0]
	payload = list(filter(lambda x: x.nodeType == minidom.Node.ELEMENT_NODE, content.childNodes))[0]
	fields = list(filter(lambda x: x.nodeType == minidom.Node.ELEMENT_NODE, payload.childNodes))
	print ( str(len(fields)))
	for fiel in fields:
		if name in fiel.getAttribute('name') and len(name) == len(fiel.getAttribute('name') ):
			print( fiel.getAttribute('name') )
			#value = list(filter(lambda x: x.nodeType == minidom.Node.ELEMENT_NODE, fiel.childNodes))
			#print( value[0].childNodes[0].nodeValue )
			value = fiel.firstChild
			if not ( value.childNodes[0].nodeValue is None ):
				print( value.childNodes[0].nodeValue )
				return value.childNodes[0].nodeValue 
			else:
				return ''

	return ''
	#fields = content.getAttribute('field')
	#print ( fields )
	exit(0)
	

def prendiXmlFieldCustom( doc, name ):

	payload = list(filter(lambda x: x.nodeType == minidom.Node.ELEMENT_NODE, doc.childNodes))[0]
	fields = list(filter(lambda x: x.nodeType == minidom.Node.ELEMENT_NODE, payload.childNodes))
	print ( str(len(fields)))
	for fiel in fields:
		if name in fiel.getAttribute('name') and len(name) == len(fiel.getAttribute('name') ):
			print( fiel.getAttribute('name') )
			#value = list(filter(lambda x: x.nodeType == minidom.Node.ELEMENT_NODE, fiel.childNodes))
			#print( value[0].childNodes[0].nodeValue )
			value = fiel.firstChild
			if not ( value.childNodes[0].nodeValue is None ):
				print( value.childNodes[0].nodeValue )
				return value.childNodes[0].nodeValue 
			else:
				return 0

	return 0
	#fields = content.getAttribute('field')
	#print ( fields )
	exit(0)
	


def parseTest(id, filePath ):

	print( '------------------------INIT parseTest -------------- '  )

	try:
		#link =  os.environ['IMG_CREATE_URL'].replace('__CREATE_SECTION__', section )

	        #dati=open( os.environ['VID_RESOURCE_TEMPLATE'] ,'r').read()
		#dati=open( filePath,'r').read()
		
		doc = minidom.parse( filePath )
		#print( doc.toxml() )
		#exit(0)

		prendiXmlField( doc, 'title' )
		prendiXmlField( doc, 'startTime' )

		exit(0)
		
		tree = ET.parse( filePath )
		bodyVal = prendiBodyVal( tree, 'body' )
		print(bodyVal)
		exit(0)
		tree.write( filePath )
		putId( id, filePath )
		
		
	except Exception as e:
		print( 'PROBLEMI in buttasu: ' + str(e)  )
		return [False, '' ]

	print('------------------------ finisce buttasu-------------- ' )
	return True


def CreateProgrammeVideo( binaryUrl, titolo, section ):

	logger.debug( '------------------------ inizia CreateProgrammeVideo -------------- ' )

	result = [False, '']

	# che deve fare la  curl --include -u perucccl:perucccl -X POST -H "Content-Type: application/atom+xml"  
	# http://internal.publishing.production.rsi.ch/webservice/escenic/section/__CREATE_SECTION__/content-items 
	# --upload-file Resources/template_picture.xml
	# dove __CREATE_SECTION__ deve essere cambiato con section e il file template deve essere aggiornato con i valori
	# dei parametri

	try:
		link =  os.environ['IMG_CREATE_URL'].replace('__CREATE_SECTION__', section )
		logger.debug( 'link : ' + link )
		
		# apro il template file
		
		
		logger.debug( 'binaryUrl : ' + binaryUrl )
		logger.debug( 'titolo : ' + titolo )

		base64string = base64.encodestring('%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])).replace('\n', '')

		headers = { 'Authorization' : 'Basic %s' % base64string,
				'Content-Type': 'application/atom+xml'
			}    

		dati=open( os.environ['VID_RESOURCE_TEMPLATE'] ,'r').read()
		#dati = dati.replace('__CREATE_SECTION__', section )
		dati = dati.replace('__TITOLO_PICTURE__', titolo )
		dati = dati.replace('__BINARY_LOCATION__', binaryUrl )

		logger.debug( ' data  = ' + dati )

		response = requests.request('POST', link, headers=headers, data=dati)

		logger.debug( 'response status: ' + str(response.status_code ))
		
		if 201 == response.status_code:
			if 'Location' in response.headers:
				result[0] = True
				result[1] = response.headers['Location'].split('/')[-1]
			else:
				result[1] = 'Error in risposta create Img'
				result[0] = False
			
	
	except Exception as e:
		logger.debug( 'PROBLEMI in CreateProgrammeVideo : ' + str(e) )
		return [False, '' ]

	logger.debug('------------------------ finisce CreateProgrammeVideo -------------- ')
	return result

def prendiS3Img( fileS3Url ):
	result = None
	print('------------------------ INIT prendiS3Img -------------- ')

	try:
		print( 'link : ' + fileS3Url  )
		link = fileS3Url
		
		request = urllib.request.Request(url=link,  method='GET')
		#request.add_unredirected_header('Authorization', 'Bearer %s' % 'n2qefrD42juHkzAqrVaStvBUgsZHjN')
		resultResponse = urllib.request.urlopen(request)
		print ( resultResponse.status )
		#result = resultResponse.data
		print ( resultResponse.headers )
		#print ( resultResponse.headers )
		#print ( resultResponse.headers.split('rsi.ch/webservice/escenic/content/' )[-1] )
		print ( resultResponse.reason )
		result = resultResponse.read()




	except Exception as e:
		print ( 'PROBLEMI in prendiS3Img : ' + str(e) )
		logger.debug ( 'PROBLEMI in prendiS3Img : ' + str(e) )
		return [ False, { 'error' : str(e) } ]
	print('------------------------ END prendiS3Img -------------- ')
	return result

def importaImgFromAws( fileS3Url, section, titolo ):

	logger.debug('------------------------ INIT importaImg -------------- ')
	print('------------------------ INIT importaImg -------------- ')

	resultBool = False
	resultJson = {}
	#section = os.environ['IMPORT_SECTION']
	section = section

	binData = prendiS3Img( fileS3Url ) 
	

	binary = uploadBinaryFromBin( binData )
	print( binary )
	logger.debug( binary )
	if not binary[0] :
		# non son riuscito a fare la load del file ....
		# continuo senza cambiare img con id
		print ( ' not binary' )
		logger.debug ( ' not binary' )
		return binary
	else:
		# ho fatto upload binary e in binary[1] ho la location url del binary
		# da passare alla creazione del content picture
		print ( ' binary ok ' )
		logger.debug ( ' binary ok ' )
		print ( section )
		# CLAD Brutto FIX
		if section == -1 or len(section ) < 4:
			section = '15097'
		result_crea = createImg( binary[1] , titolo, section )
		# in result_crea ho [ True, EceId ]
		print ( result_crea ) 
		logger.debug ( result_crea ) 
		return result_crea

	logger.debug('------------------------ END importaImg -------------- ')
	print('------------------------ END importaImg -------------- ')

	return [ False , 'boh ? ' ]



def importaImg( file_path, section ):

	logger.debug('------------------------ INIT importaImg -------------- ')

	resultBool = False
	resultJson = {}
	#section = os.environ['IMPORT_SECTION']
	section = section

	# dove trovo filenames del tipo : GP765997_P00000011_1.jpg
	# con ancora tutto il path
	# e a me interessano il primo campo = LegacyId
	# e il numero prima del . che rappresenta quanti ce ne sono per quel LegacyId

	# in img ho la lista di file_path che passo per upload del binary
	logger.debug( ' file path = ' + file_path )

	binary = uploadBinaryFromFile( file_path )
	print( binary )
	if not binary[0] :
		# non son riuscito a fare la load del file ....
		# continuo senza cambiare img con id
		print ( ' not binary' )
		return binary
	else:
		# ho fatto upload binary e in binary[1] ho la location url del binary
		# da passare alla creazione del content picture
		print ( ' binary ok ' )
		result_crea = createImg( binary[1] , 'titolo', section )
		print ( result_crea ) 
		return result_crea

	logger.debug('------------------------ END importaImg -------------- ')

	return [ False , 'boh ? ' ]




def createImg( binaryUrl, titolo, section ):

	logger.debug( '------------------------ inizia createImg -------------- ' )
	print( '------------------------ inizia createImg -------------- ' )

	result = [False, '']

	# che deve fare la  curl --include -u perucccl:perucccl -X POST -H "Content-Type: application/atom+xml"  
	# http://internal.publishing.production.rsi.ch/webservice/escenic/section/__CREATE_SECTION__/content-items 
	# --upload-file Resources/template_picture.xml
	# dove __CREATE_SECTION__ deve essere cambiato con section e il file template deve essere aggiornato con i valori
	# dei parametri

	#try:
	link =  os.environ['IMG_CREATE_URL'].replace('__CREATE_SECTION__', section )
	logger.debug( 'link : ' + link )
	print ( 'link : ' + link )
	logger.debug ( 'link : ' + link )
	
	# apro il template file
	
	
	print ( 'binaryUrl : ' + binaryUrl )
	logger.debug ( 'binaryUrl : ' + binaryUrl )
	print ( 'titolo : ' + titolo )
	logger.debug ( 'titolo : ' + titolo )

	auth = '%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])
	base64string = b64encode(auth.encode())
	base64string = base64string.decode("ascii")

	headers = { 'Authorization' : 'Basic %s' % base64string,
			'Content-Type': 'application/atom+xml'
		}    

	print (  os.environ['IMG_RESOURCE_TEMPLATE'] )
	logger.debug (  os.environ['IMG_RESOURCE_TEMPLATE'] )
	dati=open( os.environ['IMG_RESOURCE_TEMPLATE'] ,'r').read()
	print ( dati )
	logger.debug ( dati )

	
	#dati = dati.replace('__CREATE_SECTION__', section )
	dati = dati.replace('__TITOLO_PICTURE__', titolo )
	dati = dati.replace('__ECE_MODEL__', os.environ['ECE_MODEL'] )
	dati = dati.replace('__BINARY_LOCATION__', binaryUrl )

	print ( ' data da mandare --> ' + dati )
	logger.debug ( ' data  = ' + dati )
	dati = ( dati ).encode()

	try:
		request = urllib.request.Request(url=link, data=dati, headers=headers, method='POST')
		resultResponse = urllib.request.urlopen(request)
		print ( resultResponse.status )
		logger.debug ( resultResponse.status )
	except Exception as e:
		print ( 'PROBLEMI in createImg : ' + str(e) )
		logger.debug ( 'PROBLEMI in prendiS3Img : ' + str(e) )
		return [ False, { 'error' : str(e) } ]
	if  201 == resultResponse.status:
		#print ( 'tutto ok ' )
		print ( resultResponse.getheader('Location') )
		logger.debug ( resultResponse.getheader('Location') )
		#print ( resultResponse.getheader('Location').split('rsi.ch/webservice/escenic/content/' )[-1]  )
		return [ True, resultResponse.getheader('Location').split('rsi.ch/webservice/escenic/content/' )[-1] ]
	else:
		return [ False, resultResponse.status ]
	#print ( resultResponse.headers )
	#print ( resultResponse.headers )
	#print ( resultResponse.headers.split('rsi.ch/webservice/escenic/content/' )[-1] )
	#print ( resultResponse.reason )
	#print ( resultResponse.getheader('ocation') )
	#for lis  in resultResponse.getheaders():
		#print ( lis )
		#print (type(lis))
	
	'''
	except Exception as e:
		logger.debug( 'PROBLEMI in createImg : ' + str(e) )
		return [False, '' ]
	'''

	logger.debug('------------------------ finisce createImg -------------- ')
	print('------------------------ finisce createImg -------------- ')
	return result

def uploadBinaryFromBin( binData ):

	logger.debug(" ---------------------- INIT uploadBinaryFromBin CLAD ------------------ " )
	print(" ---------------------- INIT uploadBinaryFromBin CLAD ------------------ " )
	result = [False, '']
	url = os.environ['ECE_BINARY']
	logger.debug( 'url per upload : ' + url )
	print( 'url per upload : ' + url )

	try:

		link =  url
		print( 'link : ' + link  )

		auth = '%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])
		base64string = b64encode(auth.encode())
		base64string = base64string.decode("ascii")

		#xmlStr = urllib.parse.urlencode(xmlStr).encode("utf-8")
		
		headers = { 'Authorization' : 'Basic %s' %  base64string ,
			  'Content-Type':'image/jpeg'}

		dati=binData
	
		#print( ' data  = ' + dati  )
		# probabilmente non necessario
		#dati = ( dati ).encode()

		request = urllib.request.Request(url=link, data=dati, headers=headers, method='POST')
		resultResponse = urllib.request.urlopen(request)
		print ( resultResponse.status )
		if  201 == resultResponse.status:
			#print ( 'tutto ok ' )
			print ( 'Location = ' + resultResponse.getheader('Location') )
			#print ( resultResponse.getheader('Location').split('rsi.ch/webservice/escenic/content/' )[-1]  )
			return [ True, resultResponse.getheader('Location').split('rsi.ch/webservice/escenic/content/' )[-1] ]
		else:
			return [ False, resultResponse.status ]
		#print ( resultResponse.headers )
		#print ( resultResponse.headers )
		#print ( resultResponse.headers.split('rsi.ch/webservice/escenic/content/' )[-1] )
		#print ( resultResponse.reason )
		#print ( resultResponse.getheader('ocation') )
		#for lis  in resultResponse.getheaders():
			#print ( lis )
			#print (type(lis))

	except Exception as e:
		print( 'PROBLEMI in uploadBinaryFromBin: ' + str(e)  )
		logger.error('ERROR: EXCEPT in uploadBinaryFromBin  = ' + str(e))
		return [False, str(e) ]


	logger.debug(" ---------------------- FINE uploadBinaryFromBin ------------------ " )
	print(" ---------------------- FINE uploadBinaryFromBin ------------------ " )
	return result


def uploadBinaryFromFile( file_path ):

	logger.debug(" ---------------------- INIT uploadBinaryFromFile CLAD ------------------ " )
	result = [False, '']
	url = os.environ['ECE_BINARY']
	logger.debug( 'url per upload : ' + url )
	logger.debug( 'file path per upload : ' + file_path )
	print( 'url per upload : ' + url )
	print( 'file path per upload : ' + file_path  )

	try:

		link =  url
		print( 'link : ' + link  )

		auth = '%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])
		base64string = b64encode(auth.encode())
		base64string = base64string.decode("ascii")

		#xmlStr = urllib.parse.urlencode(xmlStr).encode("utf-8")
		
		headers = { 'Authorization' : 'Basic %s' %  base64string ,
			  'Content-Type':'image/jpeg'}

		dati=open(file_path,'rb').read()
	
		#print( ' data  = ' + dati  )
		# probabilmente non necessario
		#dati = ( dati ).encode()

		request = urllib.request.Request(url=link, data=dati, headers=headers, method='POST')
		resultResponse = urllib.request.urlopen(request)
		print ( resultResponse.status )
		if  201 == resultResponse.status:
			#print ( 'tutto ok ' )
			print ( resultResponse.getheader('Location') )
			#print ( resultResponse.getheader('Location').split('rsi.ch/webservice/escenic/content/' )[-1]  )
			return [ True, resultResponse.getheader('Location').split('rsi.ch/webservice/escenic/content/' )[-1] ]
		else:
			return [ False, resultResponse.status ]
		#print ( resultResponse.headers )
		#print ( resultResponse.headers )
		#print ( resultResponse.headers.split('rsi.ch/webservice/escenic/content/' )[-1] )
		#print ( resultResponse.reason )
		#print ( resultResponse.getheader('ocation') )
		#for lis  in resultResponse.getheaders():
			#print ( lis )
			#print (type(lis))

	except Exception as e:
		print( 'PROBLEMI in uploadBinaryFromFile: ' + str(e)  )
		logger.error('ERROR: EXCEPT in uploadBinaryFromFile  = ' + str(e))
		return [False, str(e) ]


	logger.debug(" ---------------------- FINE uploadBinaryFromFile ------------------ " )
	return result

	
def createMamStr( section, xmlStr ):

	print( '------------------------ inizia createMamStr -------------- '  )
	logger.debug( '------------------------ inizia createMamStr -------------- '  )

	result = [False, '']

	# che deve fare la  curl --include -u perucccl:perucccl -X POST -H "Content-Type: application/atom+xml"  
	# http://internal.publishing.production.rsi.ch/webservice/escenic/section/__CREATE_SECTION__/content-items 
	# --upload-file Resources/template_mamProgramme.xml
	# dove __CREATE_SECTION__ deve essere cambiato con section e il file template deve essere aggiornato con i valori
	# dei parametri

	try:

		link =  os.environ['CREATE_URL'].replace('__CREATE_SECTION__', section )
		#link =  "http://internal.publishing.staging.rsi.ch/webservice/escenic/section/5909/content-items"
		print( 'link : ' + link  )
		logger.debug( 'link : ' + link  )

		auth = '%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])
		base64string = b64encode(auth.encode())
		base64string = base64string.decode("ascii")

		#xmlStr = urllib.parse.urlencode(xmlStr).encode("utf-8")
		
		headers = { 'Authorization' : 'Basic %s' %  base64string ,
			  'Content-Type':'application/atom+xml'}

		

		dati = xmlStr.replace('__ECE_MODEL__', os.environ['ECE_MODEL'] )
		
		print( ' data  = ' + dati  )
		logger.debug( ' data  = ' + dati  )
		dati = ( dati ).encode()

		request = urllib.request.Request(url=link, data=dati, headers=headers, method='POST')
		resultResponse = urllib.request.urlopen(request)
		print ( resultResponse.status )
		logger.debug ( resultResponse.status )
		#print ( resultResponse.getheader('Location') )
		
		if  201 == resultResponse.status:
			#print ( 'tutto ok ' )
			#print ( resultResponse.getheader('Location') )
			#print ( resultResponse.getheader('Location').split('rsi.ch/webservice/escenic/content/' )[-1]  )
			return [ True, resultResponse.getheader('Location').split('rsi.ch/webservice/escenic/content/' )[-1] ]
		else:
			return [ False, resultResponse.status ]
		#print ( resultResponse.headers )
		#print ( resultResponse.headers )
		#print ( resultResponse.headers.split('rsi.ch/webservice/escenic/content/' )[-1] )
		#print ( resultResponse.reason )
		#print ( resultResponse.getheader('ocation') )
		#for lis  in resultResponse.getheaders():
			#print ( lis )
			#print (type(lis))
		

	except Exception as e:
		print( 'PROBLEMI in createMamStr: ' + str(e)  )
		logger.debug( 'PROBLEMI in createMamStr: ' + str(e)  )
		return [False, str(e) ]

	print('------------------------ finisce createMamStr-------------- ' )
	logger.debug('------------------------ finisce createMamStr-------------- ' )
	return result


def cambiaMamProgramme( idStr , updateFields ):
		
	logger.debug(' --------------------------- init cambiaMamProgramme ----------------------')
		
	auth = '%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])
	base64string = b64encode(auth.encode())
	base64string = base64string.decode("ascii")
	headers = { 'Authorization' : 'Basic %s' % base64string,
			'Content-Type': 'application/atom+xml'
		}    
	link = os.environ['ECE_SERVER']  + str(idStr)
	print (idStr )
	print (link )

	try:
		request = urllib.request.Request(link, headers=headers)
		resultResponse = urllib.request.urlopen(request)

		tree = ET.parse(resultResponse)
		#tree.write('8568391.xml')
		# esempio di modifica del file xml 
		#tmp_tree =  CambiaField( tree, "title", 'CAMBIATOOOO' )
		#tree = tmp_tree
		#tmp_tree =  CambiaField( tree, "leadtext", 'INSERITOOOOOO' )
		#tree = tmp_tree
		#tree.write('xml_cambiato.xml')
		#exit(0)
		
		#ET.dump(tree)
		for key, value in updateFields.items():
			print(key, value)
			if value is None:
				value = 'None'
			else:
				if  isinstance( value, ( int ) ):
					logger.debug(value)
					value = str( value )
				
			if 'None' in value:
				continue

			# come fatto qui sotto posso prendere tutti i fields
			tmp_tree =  CambiaField( tree, key, value )
			tree = tmp_tree

		print('Dump CLAD')
		#NON CAMBIAAAAAAA
		#tree.write('8568391_cambiato.xml')
		result.close()
		logger.debug(' --------------------------- fine cambiaMamProgramme ----------------------')
		return tree

	except urllib.error.HTTPError as e:
		logger.debug(' EXCEPTIOOOONNNNNN - ritorno none !!!! ')
		return 'None' 
	
	logger.debug(' --------------------------- fine cambiaMamProgramme ----------------------')
	return 'None' 

if __name__ == "__main__":

	

	os.environ['ECE_USER'] = 'TSMM'
	os.environ['ECE_PWD'] = '8AKjwWXiWAFTxb2UM3pZ'


	os.environ['ECE_MODEL'] = 'http://internal.publishing.staging.rsi.ch/webservice/publication/rsi/escenic/model/'
	os.environ['IMG_CREATE_URL'] = 'http://internal.publishing.staging.rsi.ch/webservice/escenic/section/__CREATE_SECTION__/content-items'
	os.environ['ECE_SERVER'] = 'http://internal.publishing.staging.rsi.ch/webservice/escenic/content/'
	os.environ['ECE_SECTION'] = 'http://internal.publishing.staging.rsi.ch/webservice/escenic/section/'
	os.environ['ECE_BRAND'] = 'http://internal.publishing.staging.rsi.ch/webservice-extensions/srg/sectionIdForBrand/?publicationName=rsi&channel=__CHANNEL__&brand=__BRAND__'

	os.environ['CREATE_URL'] =  "http://internal.publishing.staging.rsi.ch/webservice/escenic/section/__CREATE_SECTION__/content-items"
	os.environ['UPDATE_FILE'] =  "/home/perucccl/Webservices/STAGING/newMamServices/Resources/_cambiamento_"


	#Dump_Link("http://internal.publishing.staging.rsi.ch/webservice/publication/rsi/escenic/model/mamSegmentedProgrammeVideo", 'mamSegmentedProgrammeVideo_model.xml')
	#Dump_Link("http://internal.publishing.staging.rsi.ch/webservice/escenic/content/13202466", 'transcodable_13202466.xml')
	#Dump_Link("http://internal.publishing.staging.rsi.ch/webservice/escenic/content/13237155", 'pv_13237155.xml')
	#Dump_Link("http://internal.publishing.production.rsi.ch/webservice/escenic/content/13418244", '/home/perucccl/p_13418244.xml')
	#exit(0)
	
	Environment_Variables = {'FILE_LOG_DIR':'/home/perucccl/Webservices/STAGING/newMamServices/LOGS/FILES','BINARY_PATH_4_TV':'http://internal.publishing.staging.rsi.ch/webservice/escenic/binary/13202466/2020/7/6/14/Giappone+sotto+l%2527acqua.mp4','KAFKA_IMPORT_PATH' : '/home/perucccl/Webservices/STAGING/newMamServices/KAFKA', 'ECE_IMPORT_PATH':'/home/perucccl/Webservices/STAGING/newMamServices/ECE','PUBLISH_ECE' : 'http://publishing.staging.rsi.ch/rsi-api/mam/blackhole/publish/content/','COUCH_DB' : 'testnuovo', 'COUCH_HOST' : 'rsis-zp-mongo1', 'COUCH_PORT':'5984', 'COUCH_USER':'admin', 'COUCH_PWD':'78-AjhQ','IMPORT_SECTION': '9736','CREATE_URL':'http://internal.publishing.staging.rsi.ch/webservice/escenic/section/__CREATE_SECTION__/content-items','IMG_CREATE_URL':'http://internal.publishing.staging.rsi.ch/webservice/escenic/section/__CREATE_SECTION__/content-items', 'VID_RESOURCE_TEMPLATE':'/home/perucccl/Webservices/STAGING/CreateTranscodable/Resources/template_pVideo.xml.stag', 'IMG_RESOURCE_TEMPLATE':'/home/perucccl/Webservices/STAGING/newMamServices/Resources/template_picture.xml','LOCK_URL' : 'http://internal.publishing.production.rsi.ch/webservice/escenic/lock/article/','LOCK_NAME' : 'template_lock.xml','LOCK_ID' : '11868353' ,'REST_POST_URL': 'http://publishing.staging.rsi.ch/rsi-api/intlay/mockup/importkeyframe/keyframes.json','REST_GET_URL': 'http://publishing.staging.rsi.ch/rsi-api/intlay/mockup/importkeyframe/keyframes.json', 'FTP_ARCHIVE_DIR' : '/mnt/rsi_import/keyframe_traffic/archived/',  'FTP_DIR' : '/mnt/rsi_import/keyframe_traffic/test/','VERSION' : '3.2','ECE_USER' : 'TSMM', 'ECE_PWD':'8AKjwWXiWAFTxb2UM3pZ', 'ECE_MODEL':'http://internal.publishing.staging.rsi.ch/webservice/publication/rsi/escenic/model/','ECE_SERVER' : 'http://internal.publishing.staging.rsi.ch/webservice/escenic/content/','ECE_BINARY' : 'http://internal.publishing.staging.rsi.ch/webservice/escenic/binary','ECE_BRAND' : 'http://internal.publishing.staging.rsi.ch/webservice-extensions/srg/sectionIdForBrand/?publicationName=rsi&channel=__CHANNEL__&brand=__BRAND__','ECE_THUMB' : 'http://internal.publishing.staging.rsi.ch/webservice/thumbnail/article/', 'ECE_SECTION' : 'http://internal.publishing.staging.rsi.ch/webservice/escenic/section/', 'UPDATE_FILE' : '/home/perucccl/Webservices/STAGING/newMamServices/Resources/_cambiamento_','ARCHIVE_NAME' : '/home/perucccl/STAGING/APICoreX/Resources/_LivestreamingArchive_','LOCK_RESOURCE_DIR':'/home/perucccl/Webservices/STAGING/newMamServices/Resources/','RESOURCE_DIR':'/home/perucccl/Webservices/STAGING/newMamServices/Resources/','DB_NAME' : '/home/perucccl/Webservices/STAGING/CreateTranscodable/Resources/_ImportKeyFramesDb_', 'LESSOPEN': '||/usr/bin/lesspipe.sh %s', 'SSH_CLIENT': '146.159.126.207 57239 22', 'SELINUX_USE_CUR RENT_RANGE': '', 'LOGNAME': 'perucccl', 'USER': 'perucccl', 'HOME': '/home/perucccl', 'PATH': '/usr/local/bin:/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/sbin:/home/perucccl/bin', 'LANG': 'en_US.UTF-8', 'TERM': 'xterm', 'SHELL': '/bin/bash', 'SHLVL': '1', 'G_BROKEN_FILENAME S': '1', 'HISTSIZE': '1000', 'SELINUX_ROLE_REQUESTED': '', '_': '/usr/bin/python', 'SSH_CONNECTION': '146.159.126.207 57239 10.72.112.35 22', 'SSH_TTY': '/dev/pts/1', 'HOSTNAME': 'rsis-prod-web1.media.int', 'SELINUX_LEVE L_REQUESTED': '', 'HISTCONTROL': 'ignoredups', 'no_proxy': 'amazonaws.com,rsis-zp-mongo1,localhost,127.0.0.1,.media.int,rsis-tifone-t1,rsis-tifone-t2,rsis-tifone-t,.rsi.ch,10.102.7.38:8180,10.101.8.27:8180,.twitter.com', 'MAIL': '/var/spool/mail/perucccl', 'LS_COLORS': 'rs=0:di=01;34:ln=01;36:mh=00: pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:mi=01;05;37;41:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=34;42:st=37;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arj=01;31:*.taz=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.zip=01;31:*.z=01;31:*.Z=01;31:* .dz=01;31:*.gz=01;31:*.lz=01;31:*.xz=01;31:*.bz2=01;31:*.tbz=01;31:*.tbz2=01;31:*.bz=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.rar=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.jpg=01;35:*.jpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:* .pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob=01;35:*.qt =01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.axv=01;35:*.anx=01;35:*.ogv=01;35:*.ogx=01;35:*.aac=01;36:*.au=01;36:*.f lac=01;36:*.mid=01;36:*.midi=01;36:*.mka=01;36:*.mp3=01;36:*.mpc=01;36:*.ogg=01;36:*.ra=01;36:*.wav=01;36:*.axa=01;36:*.oga=01;36:*.spx=01;36:*.xspf=01;36:'} 

	# PROD
	#Environment_Variables =	{'FILE_LOG_DIR':'/opt/mam-ece-services/LOGS/FILES','BINARY_PATH_4_TV':'http://internal.publishing.production.rsi.ch/webservice/escenic/binary/13390764/2020/9/7/14/000+VIDEO+16-9.mp4','PUBLISH_ECE' : 'http://internal.publishing.production.rsi.ch/rsi-api/mam/blackhole/publish/content/','COUCH_DB' : 'importkeyframe', 'COUCH_HOST' : 'rsis-zp-mongo1', 'COUCH_PORT':'5984', 'COUCH_USER':'admin', 'COUCH_PWD':'78-AjhQ','IMPORT_SECTION': '9376','CREATE_URL':'http://internal.publishing.production.rsi.ch/webservice/escenic/section/__CREATE_SECTION__/content-items','IMG_CREATE_URL':'http://internal.publishing.production.rsi.ch/webservice/escenic/section/__CREATE_SECTION__/content-items','VID_RESOURCE_TEMPLATE':'/home/perucccl/Webservices/PRODUCTION/CreateTranscodable/Resources/template_pVideo.xml.prod', 'IMG_RESOURCE_TEMPLATE':'/opt/mam-ece-services/Resources/template_picture.xml','LOCK_URL' : 'http://internal.publishing.production.rsi.ch/webservice/escenic/lock/article/','LOCK_NAME' : 'template_lock.xml','LOCK_ID' : '11868353' ,'REST_POST_URL': 'http://publishing.rsi.ch/rsi-api/intlay/mockup/importkeyframe/keyframes.json','REST_GET_URL': 'https://www.rsi.ch/rsi-api/intlay/mockup/importkeyframe/keyframes.json', 'FTP_ARCHIVE_DIR' : '/mnt/rsi_import/keyframe_traffic/archived/', 'FTP_DIR' : '/mnt/rsi_import/keyframe_traffic/prod/','VERSION' : '3.2','ECE_USER' : 'TSMM', 'ECE_PWD':'8AKjwWXiWAFTxb2UM3pZ','ECE_MODEL':'http://internal.publishing.production.rsi.ch/webservice/publication/rsi/escenic/model/', 'ECE_SERVER' : 'http://internal.publishing.production.rsi.ch/webservice/escenic/content/','ECE_BINARY' : 'http://internal.publishing.production.rsi.ch/webservice/escenic/binary','ECE_BRAND' : 'http://internal.publishing.production.rsi.ch/webservice-extensions/srg/sectionIdForBrand/?publicationName=rsi&channel=__CHANNEL__&brand=__BRAND__','ECE_THUMB' : 'http://internal.publishing.production.rsi.ch/webservice/thumbnail/article/', 'ECE_SECTION' : 'http://internal.publishing.production.rsi.ch/webservice/escenic/section/','UPDATE_FILE' : '/opt/mam-ece-services/Resources/_cambiamento_','ARCHIVE_NAME' : '/home/perucccl/APICoreXPROD/Resources/_LivestreamingArchive_','LOCK_RESOURCE_DIR':'/opt/mam-ece-services/Resources/','RESOURCE_DIR':'/opt/mam-ece-services/Resources/','DB_NAME' : '/home/perucccl/Webservices/PRODUCTION/CreateTranscodable/Resources/_ImportKeyFramesDb_', 'LESSOPEN': '||/usr/bin/lesspipe.sh %s', 'SSH_CLIENT': '146.159.126.207 57239 22', 'SELINUX_USE_CURRENT_RANGE': '', 'L OGNAME': 'perucccl', 'USER': 'perucccl', 'HOME': '/home/perucccl', 'PATH': '/usr/local/bin:/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/sbin:/home/perucccl/bin', 'LANG': 'en_US.UTF-8', 'TERM': 'xterm', 'SHELL': '/bin/bash', 'SHLVL': '1', 'G_BROKEN_FILENAMES': '1', 'HISTSIZE' : '1000', 'SELINUX_ROLE_REQUESTED': '', '_': '/usr/bin/python', 'SSH_CONNECTION': '146.159.126.207 57239 10.72.112.35 22', 'SSH_TTY': '/dev/pts/1', 'SELINUX_LEVEL_REQUESTED': '', ' HISTCONTROL': 'ignoredups', 'no_proxy': 'amazonaws.com,rsis-zp-mongo1,localhost,127.0.0.1,.media.int,rsis-tifone-t1,rsis-tifone-t2,rsis-tifone-t,rsis-tifone-01,rsis-tifone-02,.rsi.ch,10.102.7.38:8180,10.101.8.27:8180,.twitter.com', 'MAIL': '/var/spool/mail/perucccl', 'LS_COLORS': 'rs=0:di=01;34:ln=0 1;36:mh=00:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:mi=01;05;37;41:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=34;42:st=37;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arj=01;31:*.taz=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.zip=01;31:*.z=01;31: *.Z=01;31:*.dz=01;31:*.gz=01;31:*.lz=01;31:*.xz=01;31:*.bz2=01;31:*.tbz=01;31:*.tbz2=01;31:*.bz=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.rar=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.jpg=01;35:*.jpeg=01;35:*.gif=01;35:*.bmp=01;35:*.  pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob =01;35:*.qt=01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.axv=01;35:*.anx=01;35:*.ogv=01;35:*.ogx=01;35:*.aac=01;36:*.a u=01;36:*.flac=01;36:*.mid=01;36:*.midi=01;36:*.mka=01;36:*.mp3=01;36:*.mpc=01;36:*.ogg=01;36:*.ra=01;36:*.wav=01;36:*.axa=01;36:*.oga=01;36:*.spx=01;36:*.xspf=01;36:'}

	# STAG

	#Environment_Variables = {'FILE_LOG_DIR':'/opt/mam-ece-services/LOGS/FILES','BINARY_PATH_4_TV':'http://internal.publishing.staging.rsi.ch/webservice/escenic/binary/13202466/2020/7/6/14/Giappone+sotto+l%2527acqua.mp4','PUBLISH_ECE' : 'http://internal.publishing.staging.rsi.ch/rsi-api/mam/blackhole/publish/content/','COUCH_DB' : 'testnuovo', 'COUCH_HOST' : 'rsis-zp-mongo1', 'COUCH_PORT':'5984', 'COUCH_USER':'admin', 'COUCH_PWD':'78-AjhQ','IMPORT_SECTION': '9736','CREATE_URL':'http://internal.publishing.staging.rsi.ch/webservice/escenic/section/__CREATE_SECTION__/content-items','IMG_CREATE_URL':'http://internal.publishing.staging.rsi.ch/webservice/escenic/section/__CREATE_SECTION__/content-items', 'VID_RESOURCE_TEMPLATE':'/home/perucccl/Webservices/STAGING/CreateTranscodable/Resources/template_pVideo.xml.stag', 'IMG_RESOURCE_TEMPLATE':'/opt/mam-ece-services/Resources/template_picture.xml','LOCK_URL' : 'http://internal.publishing.staging.rsi.ch/webservice/escenic/lock/article/','LOCK_NAME' : 'template_lock.xml','LOCK_ID' : '11868353' ,'REST_POST_URL': 'http://publishing.staging.rsi.ch/rsi-api/intlay/mockup/importkeyframe/keyframes.json','REST_GET_URL': 'http://publishing.staging.rsi.ch/rsi-api/intlay/mockup/importkeyframe/keyframes.json', 'FTP_ARCHIVE_DIR' : '/mnt/rsi_import/keyframe_traffic/archived/',  'FTP_DIR' : '/mnt/rsi_import/keyframe_traffic/test/','VERSION' : '3.2','ECE_USER' : 'TSMM', 'ECE_PWD':'8AKjwWXiWAFTxb2UM3pZ', 'ECE_MODEL':'http://internal.publishing.staging.rsi.ch/webservice/publication/rsi/escenic/model/','ECE_SERVER' : 'http://internal.publishing.staging.rsi.ch/webservice/escenic/content/','ECE_BINARY' : 'http://internal.publishing.staging.rsi.ch/webservice/escenic/binary','ECE_BRAND' : 'http://internal.publishing.staging.rsi.ch/webservice-extensions/srg/sectionIdForBrand/?publicationName=rsi&channel=__CHANNEL__&brand=__BRAND__','ECE_THUMB' : 'http://internal.publishing.staging.rsi.ch/webservice/thumbnail/article/', 'ECE_SECTION' : 'http://internal.publishing.staging.rsi.ch/webservice/escenic/section/', 'UPDATE_FILE' : '/opt/mam-ece-services/Resources/_cambiamento_','ARCHIVE_NAME' : '/home/perucccl/STAGING/APICoreX/Resources/_LivestreamingArchive_','LOCK_RESOURCE_DIR':'/opt/mam-ece-services/Resources/','RESOURCE_DIR':'/opt/mam-ece-services/Resources/','DB_NAME' : '/home/perucccl/Webservices/STAGING/CreateTranscodable/Resources/_ImportKeyFramesDb_', 'LESSOPEN': '||/usr/bin/lesspipe.sh %s', 'SSH_CLIENT': '146.159.126.207 57239 22', 'SELINUX_USE_CUR RENT_RANGE': '', 'LOGNAME': 'perucccl', 'USER': 'perucccl', 'HOME': '/home/perucccl', 'PATH': '/usr/local/bin:/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/sbin:/home/perucccl/bin', 'LANG': 'en_US.UTF-8', 'TERM': 'xterm', 'SHELL': '/bin/bash', 'SHLVL': '1', 'G_BROKEN_FILENAME S': '1', 'HISTSIZE': '1000', 'SELINUX_ROLE_REQUESTED': '', '_': '/usr/bin/python', 'SSH_CONNECTION': '146.159.126.207 57239 10.72.112.35 22', 'SSH_TTY': '/dev/pts/1', 'HOSTNAME': 'rsis-prod-web1.media.int', 'SELINUX_LEVE L_REQUESTED': '', 'HISTCONTROL': 'ignoredups', 'no_proxy': 'amazonaws.com,rsis-zp-mongo1,localhost,127.0.0.1,.media.int,rsis-tifone-t1,rsis-tifone-t2,rsis-tifone-t,.rsi.ch,10.102.7.38:8180,10.101.8.27:8180,.twitter.com', 'MAIL': '/var/spool/mail/perucccl', 'LS_COLORS': 'rs=0:di=01;34:ln=01;36:mh=00: pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:mi=01;05;37;41:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=34;42:st=37;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arj=01;31:*.taz=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.zip=01;31:*.z=01;31:*.Z=01;31:* .dz=01;31:*.gz=01;31:*.lz=01;31:*.xz=01;31:*.bz2=01;31:*.tbz=01;31:*.tbz2=01;31:*.bz=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.rar=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.jpg=01;35:*.jpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:* .pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob=01;35:*.qt =01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.axv=01;35:*.anx=01;35:*.ogv=01;35:*.ogx=01;35:*.aac=01;36:*.au=01;36:*.f lac=01;36:*.mid=01;36:*.midi=01;36:*.mka=01;36:*.mp3=01;36:*.mpc=01;36:*.ogg=01;36:*.ra=01;36:*.wav=01;36:*.axa=01;36:*.oga=01;36:*.spx=01;36:*.xspf=01;36:'} 

	for param in Environment_Variables.keys():
		os.environ[param] = Environment_Variables[ param ]


	'''
	with open('/home/perucccl/listaProgrammeId.txt', 'r') as filehandle:
		listaPId = json.load(filehandle)
	with open('/home/perucccl/listaMinori.txt', 'r') as filehandle:
		listaMinori = json.load(filehandle)

	print( 'tutti = ' + str(len(listaPId)))
	print( 'da fare  = ' + str(len(listaMinori)))

	for lis in listaMinori:
		print ('lavoro su : ' + lis)
		cambiaActivationDate( lis, '2999-01-01T22:55:41.000Z' )
	exit(0)

	for lis in listaPId:
		print ('lavoro su : ' + lis)
		Dump_Link("http://internal.publishing.production.rsi.ch/webservice/escenic/content/" + lis,  '/home/perucccl/tmpFILES/' + lis + '.xml')
		putId( lis, '/home/perucccl/tmpFILES/' + lis + '.xml')
	exit(0)
	'''





	#putId( '13418244', '/home/perucccl/p_13418244.xml')
	#exit(0)
	# FOR REFACTORING TESTING PURPOSES
	#servers = ['rsis-bs-kafka1:9092','rsis-bs-kafka2:9092','rsis-bs-kafka3:9092']
	#topic = 'mam-rundown-cms-delete'
	#topic = 'mam-create-programme-video'
	#topic = 'mam-create-segment-video'
	#topic = 'mam-create-keyframe'
	#topic = 'mam-create-programme'
	#consumerPtr= pyKafkaClass.Consumer(servers, topic, 'refactor' )

	# MAIN

	messagePV =  {'normalized': True, 'clips': [{'markOut': 1600277697680, 'title': 'la1 - Il Quotidiano', 'markIn': 1600275611000}], '_rev': '14-14e26d1d7c3b24f9d4cb862efe3bfd3b', 'channel': 'la1', 'cms': {'urn': 'rsi:escenic:programme:13237271', 'louise': 'EP978678'}, 'source': 'Veda', 'title': 'la1 - Il Quotidiano', 'composedEvent': False, 'assetType': 'rundownCut', 'urn': 'rsi:mam:video:b9f1c949ef3ec9b9dc98ac85bd479701', 'QoS': 'RSI_VIDEO_HD720', 'rights': {'expireDate': '64060655711000', 'geoblocked': False, 'activationDate': '1600277711000'}, 'logo': 'RSI_LOGO_STANDARD', '_id': 'b9f1c949ef3ec9b9dc98ac85bd479701', 'lastupdate': '2020-09-16T19:48:05Z', 'geoblocked': False, 'transcoderMetadata': {'urn': 'rsi:mam:video:b9f1c949ef3ec9b9dc98ac85bd479701', 'transcodedAssets': [{'duration': 2086684, 'width': 1280, 'bitrate': 3400000, 'backup_url': 'https://mam.rsi.ch/download-asset/cnNpLW1hbS10cmFuc2NvZGVkLWFzc2V0cy1tYXN0ZXIvcnNpLXd3LzIwMjAvMDkvMTYvYjlmMWM5NDllZjNlYzliOWRjOThhYzg1YmQ0Nzk3MDFfMjAyMDA5MTZfMTcwMDExXzcyMC5tcDQ=', 'uri': 's3://origin/rsi-ww/2020/09/16/b9f1c949ef3ec9b9dc98ac85bd479701_20200916_170011_720.mp4', 'height': 720}, {'duration': 2086684, 'width': 640, 'bitrate': 1200000, 'backup_url': 'https://mam.rsi.ch/download-asset/cnNpLW1hbS10cmFuc2NvZGVkLWFzc2V0cy1tYXN0ZXIvcnNpLXd3LzIwMjAvMDkvMTYvYjlmMWM5NDllZjNlYzliOWRjOThhYzg1YmQ0Nzk3MDFfMjAyMDA5MTZfMTcwMDExXzM2MC5tcDQ=', 'uri': 's3://origin/rsi-ww/2020/09/16/b9f1c949ef3ec9b9dc98ac85bd479701_20200916_170011_360.mp4', 'height': 360}, {'duration': 2086684, 'width': 480, 'bitrate': 300000, 'backup_url': 'https://mam.rsi.ch/download-asset/cnNpLW1hbS10cmFuc2NvZGVkLWFzc2V0cy1tYXN0ZXIvcnNpLXd3LzIwMjAvMDkvMTYvYjlmMWM5NDllZjNlYzliOWRjOThhYzg1YmQ0Nzk3MDFfMjAyMDA5MTZfMTcwMDExXzI3Mi5tcDQ=', 'uri': 's3://origin/rsi-ww/2020/09/16/b9f1c949ef3ec9b9dc98ac85bd479701_20200916_170011_272.mp4', 'height': 272}], 'title': 'la1 - Il Quotidiano', 'status': 'COMPLETED'}, 'day': '2020-09-16', 'status': 'COMPLETED'}


	messageS =  {'normalized': True, 'clips': [{'markOut': 1600276599032, 'title': 'RSI La1 - QUOTIDIANO  16.09.2020 - Grave operaio caduto ad Agno', 'markIn': 1600276566499}], '_rev': '4-12d6acde5a313963fdada269564b0a80', 'channel': 'la1', 'cms': {'urn': 'rsi:escenic:programme:13237271', 'louise': 'null'}, 'awsKeyframe': {'url': 'https://rsi-mam-poster-frames-master.s3.eu-west-1.amazonaws.com/1600276571499.jpg', 'status': 'SUCCEEDED'}, 'source': 'OpenMedia', 'title': 'RSI La1 - QUOTIDIANO  16.09.2020 - Grave operaio caduto ad Agno', 'composedEvent': False, 'assetType': 'segmentCut', 'urn': 'rsi:mam:video:b9f1c949ef3ec9b9dc98ac85bd47f5f8', 'QoS': 'RSI_VIDEO_HD720', 'rights': {'expireDate': 'null', 'geoblocked': False, 'activationDate': 'null'}, 'logo': 'RSI_LOGO_STANDARD', '_id': 'b9f1c949ef3ec9b9dc98ac85bd47f5f8', 'lastupdate': '2020-09-17T11:49:32Z', 'geoblocked': False, 'day': '2020-09-16', 'user': {'name': 'Perucca, Claudio (RSI)', 'id': '2802889d-8256-4ea8-bd17-4750e55e58c2'}, 'status': 'COMPLETED', 'order': 5}

	#buttasu('./test_butta.xml', '4161')
	print('CLAD')
	#exit(0)
	#prendiS3Img( 'https://rsi-mam-poster-frames-staging.s3.eu-west-1.amazonaws.com/1595921623470.jpg')
	#print ( gestisciCreateUpdateKeyframeMessageDaKafka( messageK ) )
	#########putId( '13221494', './pv_13221494.xml')
	#print ( gestisciCreateUpdateTest( messagePUpdate , consumerPtr) )
	#print ( gestisciCreateUpdateTranscodableVideoMessageDaKafka( messageTV ) )
	#print ( gestisciCreateUpdateTranscodableAudioMessageDaKafka( messageTA ) )
	#Dump_Link("http://internal.publishing.staging.rsi.ch/webservice/escenic/content/13221494", "pv_13221494.xml")

	#putId( '13221494', './pv_13221494.xml')
	#exit(0)
	print ( gestisciCreateUpdateProgrammeVideoMessageDaKafka( messagePV) )

	#print ( gestisciCreateUpdateSegmentVideoMessageDaKafka( messageS ) )
	#print ( gestisciCreateUpdateProgrammeMessageDaKafka( messageP ) )
	#print ( gestisciCreateUpdateProgrammeMessageDaKafka( messagePUpdate ) )
	exit(0)
	'''

	tree = ET.parse( '/home/perucccl/Webservices/STAGING/newMamServices/programme_13184505_1.xml' )
	print (tree )
	print  ( prendiListaRelated(  tree, 'related' )  )
	listaRelated = prendiListaRelated(  tree, 'related' ) 
	print ( ordinaListaRelated( listaRelated ) )
	tree = rimuoviListaRelated( tree , 'related' )
	tree = aggiungiListaRelated( tree , listaRelated ) 
	tree.write('./test_rimuovi_1.xml')
	putId(13184505, './test_rimuovi_1.xml')
	Dump_Link("http://internal.publishing.staging.rsi.ch/webservice/escenic/content/13218613", "programme_13218613.xml")
	exit(0)
	'''
	#prendiS3Img( 'https://rsi-mam-poster-frames-staging.s3.eu-west-1.amazonaws.com/1595921623470.jpg')
	#putId( '13221494', './pv_13221494.xml')
	#exit(0)
	#print ( gestisciCreateUpdateProgrammeVideoMessageDaKafka( messagePV ) )
	#print ( gestisciCreateUpdateKeyframeMessageDaKafka( messageK ) )
	#print ( gestisciCreateUpdateProgrammeMessageDaKafka( messageP ) )
	#exit(0)

	#print ( gestisciCreateUpdateSegmentVideoMessageDaKafka( messageS ) )
	#exit(0)
	#print ( gestisciCreateUpdateProgrammeMessageDaKafka( messageUpdate ) )
	#exit(0)

	with open('listid20200729.txt', 'r') as filehandle:
	    basicList = json.load(filehandle)
	
	for lis in basicList:

		print (lis['id'].split('article:')[-1])
		deleteId(lis['id'].split('article:')[-1])
	exit(0)


	
	message = json.loads( message )

	prendiS3Img( 'https://rsi-mam-poster-frames-staging.s3.eu-west-1.amazonaws.com/1595921623470.jpg')
	
	#putId( '13084205', './programme_13084205.xml')

	#buttasu('Dummy_test', 'dummy_Section')
	#exit(0)
	print ( gestisciCreateUpdateProgrammeVideoMessageDaKafka( message ) )
	exit(0)

	importaImg( './CPTHEBEST_P0000333_1.jpg', '5909' ) 
	exit(0)
	postLockId( 13080332 ) 
	exit(0)
	lockEceId( 13080332 )
	exit(0)
	#deleteId( 12696840 )
	#exit(0)
	#putId( 12682010, "./x.xml")
	#exit(0)
	
	#getEceIdFromCmsJson(cmsJson)
	updateProgrammeDaKafka( message )

	exit(0)

	tree = ET.parse( '/home/perucccl/Webservices/STAGING/newMamServices/Resources/_test_exp_in.xml' )
	print (tree )
	creaEntryDateExpires( tree, 'CLAD' )
	exit(0)

	path = "./Resources/Messages/message_5860"
	fin = codecs.open(path,'r','utf-8')
	xmlStr = fin.read()

	produciProgrammeDaKafka( xmlStr )
	
	fin.close()
	exit(0)

	#fin = open('./dictCave.json', 'r')
	#dictCave = json.loads( fin.read())
	#print  (dictCave )

	#print (flatten( dictCave ))
	#exit(0)
	#print  (togliAccenti( "caioâ5ã5ä5è5é5ê5ë555ë5ì5íî5ï5ò5óô5öù5úû5ü" ) )
	#exit(0)

	#Put_Id("12681998", './Resources/test_2.0.xml')
	#Dump_Link("http://internal.publishing.staging.rsi.ch/webservice/escenic/content/12682003", '12682003.xml')
	#Dump_Link("http://internal.publishing.staging.rsi.ch/webservice/publication/rsi/escenic/model/mamProgramme", 'model_mamProgramme.xml')
	#Dump_ET_Link("12588738", 'test.xml')

	'''
	cambiaMamProgramme( '12682003', 'dummyfields')
	exit(0)

	buttasu('Dummy_test', 'dummy_Section')
	exit(0)
	'''

	'''
	path = "./Resources/Messages/message_4536"
	fin = codecs.open(path,'r','utf-8')
	xmlStr = fin.read()
	xmlStr = pulisciXml( xmlStr)
	stampaCampiCaveSorted( xmlStr )
	fin.close()
	exit(0)
	'''

	'''
	StampaTutti()
	fin = codecs.open("../Resources/message_212",'r','utf-8')
	stampaCampi( fin.read() )
	fin.close()
	exit(0)

	print ( produciProgrammeDaKafka( fin.read() ) )
	exit(0)
	'''

	path = "./Resources/Messages/message_4536"
	listJsonMpRundown = json.loads(fin.read())
	fin.close()
	print ( listJsonMpRundown )
	exit(0)
	
	listJsonFlatRundown = flatterMpRundown( listJsonMpRundown )
	#print ( listJsonFlatRundown )

	#mapListJsonMpToEce = mapMptoEce( listJsonFlatRundown, listaFields.fieldsMamProgramme )
	#for key,value in mapListJsonMpToEce[0].items():
		#print ( key,value )

	exit(0)


	xml4Ece = replaceFielsTemplate( mapJsonMpToEce, "mamprogramme" )
	print ( xml4Ece )
	
	exit(0)

	lista =  Prendi_Flag_Social_New( '11464993' )
	logger.debug(lista)
	for lis in lista.items():
		logger.debug(lis)
	logger.debug(lista)
	exit(0)

	lista_parameters = Prendi_Section_Parameters( '22742' )
	print ( lista_parameters )
	exit(0)
	
	lista =  Prendi_Flag_Social( '10501717' )
	logger.debug(lista)
	for lis in lista.items():
		logger.debug(lis)
	logger.debug(lista)
	exit(0)

	#Get_Item_Content('8845033')
	#exit(0)

	Put_Id( 8732965, './_test_cambiamento_')
	exit(0)

	#logger.debug(Prendi_Social_TimeCtrl('8730301'))
	# logger.debug(Prendi_Social_TimeCtrl('8568391'))
	
	#logger.debug(Check_Data_Range( datetime.utcnow(), '8568391'))
	#logger.debug('Adesso CLAD')
	#logger.debug(Prendi_Altro_Content( '8730301'))
	#exit(0)

	UpdateMonitor( '10378235', 'dummy', 'dummy' )
	exit(0)

	lista =  Prendi_Flag_Social( '9845290' )
	logger.debug(lista)
	for lis in lista.items():
		logger.debug(lis)
	logger.debug(lista)
	exit(0)
	lista_da_passare = { 'id':'1234567:8568391'}
	lista_da_passare = {'contenttype': 'story', 'lead': {'picture': None, 'caption': '', 'link': 'http://www.rsi.ch/temp/test-push-to-social-Civi-8568391.html', 'name': 'test push to social Civi', 'description': 'subhead'}, 'title': 'test story title', 'lastmodifieddate': '2017-01-10T13:23:23Z', 'state': 'published', 'da_fare': [], 'id': 'article:8568391'}

	Controlla_Flag_Social( [ lista_da_passare])
	#logger.debug('in main')
	#Prendi_Altro_Content( '8568391')
	exit(0)
	#programmeId  = GetSection_and_State( 117530 )
	#logger.debug(programmeId)
	#logger.debug()
	#logger.debug(State_Id_xx)
	#print ( )
	#logger.debug(Edited_Id_xx)


	'''
	import Resources.listaUpdate as dictUpdate
	result = {}
	for key,value in sorted(dictUpdate.fields.items()):
		print ( key, value )
		result[value] = key

	print (result)
		

	'''
	exit(0)
	exit(0)



