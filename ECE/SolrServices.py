
# -*- coding: utf-8 -*-
import os
import logging
import urllib.request, urllib.error, urllib.parse
import base64
from http.client import HTTPSConnection
from base64 import b64encode
from xml.dom import minidom
import xml.etree.ElementTree as ET
import urllib
import json

# definizione dei namespaces per parsaqre gli atom
namespaces = { 'atom':'{http://www.w3.org/2005/Atom}',
               'dcterms' : '{http://purl.org/dc/terms/}',
                'mam' : '{http://www.vizrt.com/2010/mam}',
                'opensearch' : '{http://a9.com/-/spec/opensearch/1.1/}',
                'vaext' : '{http://www.vizrt.com/atom-ext}',
                'vdf' : '{http://www.vizrt.com/types}',
                'ece' : '{http://www.escenic.com/2007/content-engine}',
                'playout' : '{http://ns.vizrt.com/ardome/playout}' }

logger = logging.getLogger('pySolr')

		
def deleteId( idx ):


	print('------------------------ inizia deleteId -------------- ')
	logger.debug('------------------------ inizia deleteId -------------- ')
	try:
		auth = '%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])
		base64string = b64encode(auth.encode())
		base64string = base64string.decode("ascii")

		headers = { 'Authorization' : 'Basic %s' % base64string,
				'If-Match': '*',
				'Content-Type': 'application/atom+xml'
			}    

		link = os.environ['ECE_SERVER']  + str(idx)
		print ( link )
		logger.debug ( link )

		request = urllib.request.Request(url=link, headers=headers, method='DELETE')
		resultResponse = urllib.request.urlopen(request)
		print ( resultResponse.status )
		logger.debug ( resultResponse.status )
		print ( resultResponse.headers )
		logger.debug ( resultResponse.headers )
		print ( resultResponse.reason )
		logger.debug ( resultResponse.reason )
		
	
	except Exception as e:
		logger.debug ( 'PROBLEMI in deleteId : ' + str(e) )
		print ( 'PROBLEMI in deleteId : ' + str(e) )
		return False

	logger.debug('------------------------ finisce deleteId -------------- ')
	return True


def solrPrendiContent( sectionId, contentType, zuluDateFrom, zuluDateTo ):


	dateFrom = zuluDateFrom.replace(':','%3A')
	dateTo = zuluDateTo.replace(':','%3A')
	try:
		auth = '%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])
		base64string = b64encode(auth.encode())
		base64string = base64string.decode("ascii")


		headers = { 'Authorization' : 'Basic %s' % base64string,
				'If-Match': '*',
				'Content-Type': 'application/atom+xml'
			}    

		# base64string = base64.encodestring('%s:%s' % ('TSMM', '8AKjwWXiWAFTxb2UM3pZ')).replace('\n', '')

		# prende items con
		# state:published
		# creationdate:[NOW-1DAY TO NOW]
		# contenttype:story OR contenttype:programme OR contenttype:shortStory
		# http://10.102.7.38:8180/solr/collection1/select?q=*%3A*&fq=contenttype%3Astory+OR+contenttype%3Aprogramme+OR+contenttype%3AshortStory&fq=creationdate%3A%5BNOW-1DAY+TO+NOW%5D&fq=state%3Apublished&wt=json&indent=true


		# questa con la publishdate
		# link_template = "http://10.102.7.38:8180/solr/collection1/select?q=*%3A*&fq=contenttype%3Astory+OR+contenttype%3Aprogramme+OR+contenttype%3AshortStory&fq=creationdate%3A%5BNOW-1HOUR+TO+NOW%5D&fq=state%3Apublished&start=__START__&rows=100&fl=id+title+state+lastmodifieddate+contenttype&wt=json&indent=true"

		# questa con la lastmodifieddate
		# prende story - programme - shortStory
		# dell'ultima ora
		#link_template = "http://10.102.7.38:8180/solr/collection1/select?q=*%3A*&fq=contenttype%3Astory+OR+contenttype%3Aprogramme+OR+contenttype%3AshortStory&fq=lastmodifieddate%3A%5BNOW-1HOUR+TO+NOW%5D&fq=state%3Apublished&start=__START__&rows=100&fl=id+title+state+lastmodifieddate+contenttype+creationdate+publishdate&wt=json&indent=true"
		#link_template = "http://10.102.7.38:8180/solr/collection1/select?q=*%3A*&fq=contenttype%3Astory+OR+contenttype%3Aprogramme+OR+contenttype%3AshortStory&fq=lastmodifieddate%3A%5BNOW-5MINUTES+TO+NOW%5D&fq=state%3Apublished&start=__START__&rows=100&fl=id+title+state+lastmodifieddate+contenttype+creationdate+publishdate&wt=json&indent=true"
		#link_template = http://10.102.7.38:8180/solr/collection1/select?q=*%3A*&fq=contenttype%3Aprogramme&fq=lastmodifieddate%3A+%5B+2021-04-01T00%3A00%3A00Z+TO+2021-04-03T00%3A00%3A00Z%5D&fq=state%3Apublished&wt=json&indent=true
		link_template = "http://internal.publishing.production.rsi.ch:8180/solr/collection1/select?q=*%3A*&fq=state%3Apublished&fq=contenttype%3A" + contentType + "&fq=lastmodifieddate%3A+%5B+" + dateFrom + "+TO+" +  dateTo + "%5D" +"&start=__START__&rows=100&fl=id&wt=json&indent=true"

		link = link_template.replace('__SECTION_ID__', sectionId).replace('__START__', '0')

		request = urllib.request.Request(url=link, headers=headers, method='GET')
		resultResponse = urllib.request.urlopen(request)
		print ( resultResponse.status )
		logger.debug ( resultResponse.status )
		#print ( resultResponse.headers )
		#logger.debug ( resultResponse.headers )
		#print ( resultResponse.reason )
		#logger.debug ( resultResponse.reason )
		str_response = resultResponse.read().decode('utf-8')
		response = json.loads(str_response)
		#print(response)

		logger.debug(' solrPrendiContent: ' + str(response['response']['numFound']) + ' ' +  "documents found")
		print( ' solrPrendiContent: ' + str(response['response']['numFound']) + ' ' +  "documents found")
		logger.debug( ' solrPrendiContent: ' + str(response['response']['numFound']) + ' ' +  "documents found")

		#print( len(response['response']['docs']))

		lista_content_id_data = response['response']['docs']
		#print( len(lista_content_id_data))

		totresult = int(response['response']['numFound'])
		items_per_page = int(100)

		if  totresult > items_per_page:
			#print( ' giro sui next e prev ')
			# devi  girare sui next per prendere gli altri
			for x in range(int(float(totresult)/float(items_per_page))):
				__start__ = (x+1) * items_per_page
				print( str(x+1) + ' ' +  str(__start__) )

				#logger.debug(' giro per prenderli tutti')
				#e qui faccio la request sul campo next
				link_next = link_template.replace('__SECTION_ID__', sectionId).replace('__START__', str(__start__))
				#logger.debug(link_next)
				#logger.debug(link_next)
				request = urllib.request.Request(url=link_next, headers=headers, method='GET')
				resultResponse = urllib.request.urlopen(request)
				print ( resultResponse.reason )
				logger.debug ( resultResponse.reason )
				str_response = resultResponse.read().decode('utf-8')
				response = json.loads(str_response)
				#connection = urllib2.urlopen(link_next)
				#response = eval(connection.read() )

				#logger.debug('Ricevuti : ' + str(len(response['response']['docs'])))
				#logger.debug( response['response']['docs'])

				lista_content_id_data = lista_content_id_data +  response['response']['docs']

				#lista_content_id_data.append( response['response']['docs'] )
				#print( len(lista_content_id_data))

	except Exception as e:
		logger.debug ( 'PROBLEMI in solrPrendiContent : ' + str(e) )
		print ( 'PROBLEMI in solrPrendiContent : ' + str(e) )
		return []

	print( ' solrPrendiContent: content per section ' + sectionId + ' contenType = ' + contentType + ' from : ' + zuluDateFrom + ' to : ' + zuluDateTo + '  = % d ' % len(lista_content_id_data))
	logger.debug( ' solrPrendiContent: content per section ' + sectionId + ' contenType = ' + contentType + ' from : ' + zuluDateFrom + ' to : ' + zuluDateTo + '  = % d ' % len(lista_content_id_data))

	return lista_content_id_data



if __name__ == "__main__":


	os.environ['ECE_USER'] = 'TSMM'
	os.environ['ECE_PWD'] = '8AKjwWXiWAFTxb2UM3pZ'


	os.environ['ECE_MODEL'] = 'http://internal.publishing.staging.rsi.ch/webservice/publication/rsi/escenic/model/'
	os.environ['IMG_CREATE_URL'] = 'http://internal.publishing.staging.rsi.ch/webservice/escenic/section/__CREATE_SECTION__/content-items'
	os.environ['ECE_SERVER'] = 'http://internal.publishing.staging.rsi.ch/webservice/escenic/content/'
	os.environ['ECE_SECTION'] = 'http://internal.publishing.staging.rsi.ch/webservice/escenic/section/'
	os.environ['ECE_BRAND'] = 'http://internal.publishing.staging.rsi.ch/webservice-extensions/srg/sectionIdForBrand/?publicationName=rsi&channel=__CHANNEL__&brand=__BRAND__'

	os.environ['CREATE_URL'] =  "http://internal.publishing.staging.rsi.ch/webservice/escenic/section/__CREATE_SECTION__/content-items"
	os.environ['UPDATE_FILE'] =  "/home/perucccl/Webservices/STAGING/newMamServices/Resources/_cambiamento_"
	#Dump_Link("http://internal.publishing.staging.rsi.ch/webservice/escenic/content/13089906", 'Programme_13146153.xml')
	#Dump_Link("http://internal.publishing.staging.rsi.ch/webservice/escenic/content/13084699", 'transcodable_13084699.xml')
	#Dump_Link("http://internal.publishing.staging.rsi.ch/webservice/publication/rsi/escenic/model/mamSegmentedProgrammeVideo", 'mamSegmentedProgrammeVideo_model.xml')
	#exit(0)
	
	Environment_Variables = {'COUCH_DB' : 'testnuovo', 'COUCH_HOST' : 'rsis-zp-mongo1', 'COUCH_PORT':'5984', 'COUCH_USER':'admin', 'COUCH_PWD':'78-AjhQ','IMPORT_SECTION': '9736','CREATE_URL':'http://internal.publishing.staging.rsi.ch/webservice/escenic/section/__CREATE_SECTION__/content-items','IMG_CREATE_URL':'http://internal.publishing.staging.rsi.ch/webservice/escenic/section/__CREATE_SECTION__/content-items', 'VID_RESOURCE_TEMPLATE':'/home/perucccl/Webservices/STAGING/CreateTranscodable/Resources/template_pVideo.xml.stag', 'IMG_RESOURCE_TEMPLATE':'/home/perucccl/Webservices/STAGING/newMamServices/Resources/template_picture.xml','LOCK_URL' : 'http://internal.publishing.production.rsi.ch/webservice/escenic/lock/article/','LOCK_NAME' : 'template_lock.xml','LOCK_ID' : '11868353' ,'REST_POST_URL': 'http://publishing.staging.rsi.ch/rsi-api/intlay/mockup/importkeyframe/keyframes.json','REST_GET_URL': 'http://publishing.staging.rsi.ch/rsi-api/intlay/mockup/importkeyframe/keyframes.json', 'FTP_ARCHIVE_DIR' : '/mnt/rsi_import/keyframe_traffic/archived/',  'FTP_DIR' : '/mnt/rsi_import/keyframe_traffic/test/','VERSION' : '3.2','ECE_USER' : 'TSMM', 'ECE_PWD':'8AKjwWXiWAFTxb2UM3pZ', 'ECE_MODEL':'http://internal.publishing.staging.rsi.ch/webservice/publication/rsi/escenic/model/','ECE_SERVER' : 'http://internal.publishing.staging.rsi.ch/webservice/escenic/content/','ECE_BINARY' : 'http://internal.publishing.staging.rsi.ch/webservice/escenic/binary','ECE_BRAND' : 'http://internal.publishing.staging.rsi.ch/webservice-extensions/srg/sectionIdForBrand/?publicationName=rsi&channel=__CHANNEL__&brand=__BRAND__','ECE_THUMB' : 'http://internal.publishing.staging.rsi.ch/webservice/thumbnail/article/', 'ECE_SECTION' : 'http://internal.publishing.staging.rsi.ch/webservice/escenic/section/', 'UPDATE_FILE' : '/home/perucccl/Webservices/STAGING/newMamServices/Resources/_cambiamento_','ARCHIVE_NAME' : '/home/perucccl/STAGING/APICoreX/Resources/_LivestreamingArchive_','LOCK_RESOURCE_DIR':'/home/perucccl/Webservices/STAGING/newMamServices/Resources/','RESOURCE_DIR':'/home/perucccl/Webservices/STAGING/newMamServices/Resources/','DB_NAME' : '/home/perucccl/Webservices/STAGING/CreateTranscodable/Resources/_ImportKeyFramesDb_', 'http_proxy': 'http://gateway.zscloud.net:10268', 'LESSOPEN': '||/usr/bin/lesspipe.sh %s', 'SSH_CLIENT': '146.159.126.207 57239 22', 'SELINUX_USE_CUR RENT_RANGE': '', 'LOGNAME': 'perucccl', 'USER': 'perucccl', 'HOME': '/home/perucccl', 'PATH': '/usr/local/bin:/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/sbin:/home/perucccl/bin', 'LANG': 'en_US.UTF-8', 'TERM': 'xterm', 'SHELL': '/bin/bash', 'SHLVL': '1', 'G_BROKEN_FILENAME S': '1', 'HISTSIZE': '1000', 'https_proxy': 'http://gateway.zscloud.net:10268', 'SELINUX_ROLE_REQUESTED': '', '_': '/usr/bin/python', 'SSH_CONNECTION': '146.159.126.207 57239 10.72.112.35 22', 'SSH_TTY': '/dev/pts/1', 'HOSTNAME': 'rsis-prod-web1.media.int', 'SELINUX_LEVE L_REQUESTED': '', 'HISTCONTROL': 'ignoredups', 'no_proxy': 'amazonaws.com,rsis-zp-mongo1,localhost,127.0.0.1,.media.int,rsis-tifone-t1,rsis-tifone-t2,rsis-tifone-t,.rsi.ch,10.102.7.38:8180,10.101.8.27:8180,.twitter.com', 'MAIL': '/var/spool/mail/perucccl', 'LS_COLORS': 'rs=0:di=01;34:ln=01;36:mh=00: pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:mi=01;05;37;41:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=34;42:st=37;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arj=01;31:*.taz=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.zip=01;31:*.z=01;31:*.Z=01;31:* .dz=01;31:*.gz=01;31:*.lz=01;31:*.xz=01;31:*.bz2=01;31:*.tbz=01;31:*.tbz2=01;31:*.bz=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.rar=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.jpg=01;35:*.jpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:* .pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob=01;35:*.qt =01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.axv=01;35:*.anx=01;35:*.ogv=01;35:*.ogx=01;35:*.aac=01;36:*.au=01;36:*.f lac=01;36:*.mid=01;36:*.midi=01;36:*.mka=01;36:*.mp3=01;36:*.mpc=01;36:*.ogg=01;36:*.ra=01;36:*.wav=01;36:*.axa=01;36:*.oga=01;36:*.spx=01;36:*.xspf=01;36:'} 
	for param in Environment_Variables.keys():
		os.environ[param] = Environment_Variables[ param ]



	#lista = solrPrendiContent('4', 'mamProgramme', '2021-04-03T00%3A00%3A00Z')
	lista = solrPrendiContent('4', 'mamProgramme', '2021-04-01T00:00:00Z',  '2021-04-03T00:00:00Z')
	with open('listid.txt', 'w') as filehandle:
	    json.dump(lista, filehandle)
	exit(0)
	
	'''
	'''

	with open('listid.txt', 'r') as filehandle:
	    basicList = json.load(filehandle)
	
	for lis in basicList:

		print( (lis['id'].split('article:')[-1]))
		deleteId(lis['id'].split('article:')[-1])
	exit(0)


