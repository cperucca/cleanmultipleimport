import os 

# -*- coding: utf-8 -*-

import logging
import urllib.request, urllib.error, urllib.parse
import base64
from xml.dom import minidom
import xml.etree.ElementTree as ET
import codecs
import operator
import os.path
import stat
import shutil
import glob
import json
from datetime import datetime, timedelta
import ast
import os
import copy
from http.client import HTTPSConnection
from base64 import b64encode
import sys

# libreria per flatterare il json
import collections
# importa la lista dei fields da importare mappato con la loro posizione nel template
import re

# definizione dei namespaces per parsare gli atom
namespaces = { 'atom':'{http://www.w3.org/2005/Atom}',
	       'dcterms' : '{http://purl.org/dc/terms/}',
		'mam' : '{http://www.vizrt.com/2010/mam}',
		'age' : '{http://purl.org/atompub/age/1.0}',
		'opensearch' : '{http://a9.com/-/spec/opensearch/1.1/}',
		'vaext' : '{http://www.vizrt.com/atom-ext}',
		'app' : '{http://www.w3.org/2007/app}',
		'vdf' : '{http://www.vizrt.com/types}',
		'metadata' : '{http://xmlns.escenic.com/2010/atom-metadata}',
		#'': '{http://www.w3.org/1999/xhtml}',
		'playout' : '{http://ns.vizrt.com/ardome/playout}' }

_UPDATE_FUNC_ = 0
_CREATE_FUNC_ = 1
_SISTEMA_FUNC_ = 2

# importa il pySettaVars
import Resources.pySettaVars as pySettaVars

logger = logging.getLogger('pyECE')

for entry in namespaces:
	#logger.debug(entry, namespaces[entry])
	ET._namespace_map[namespaces[entry].replace('{','').replace('}','')] = entry

def Dump_Link ( link, filename ):

	auth = '%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])
	base64string = b64encode(auth.encode())
	base64string = base64string.decode("ascii")
	headers = { 'Authorization' : 'Basic %s' %  base64string }

	print (link )
	print (filename)

	try:
		request = urllib.request.Request(link, headers=headers)
		resultResponse = urllib.request.urlopen(request)
		xml = minidom.parse(resultResponse)
		fout = codecs.open(filename, 'w', 'utf-8')
		fout.write( xml.toxml() )
		fout.close()

	except Exception as e:
		print ( 'PROBLEMI in prendiSection : ' + str(e) )
		pass 
	return

def Dump_ET_Link ( idStr, filename ):

	auth = '%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])
	base64string = b64encode(auth.encode())
	base64string = base64string.decode("ascii")
	headers = { 'Authorization' : 'Basic %s' %  base64string }

	link = os.environ['ECE_SERVER']  + str(idStr)

	print (idStr )
	print (link )
	print (filename)
	try:
		request = urllib.request.Request(link, headers=headers)
		resultResponse = urllib.request.urlopen(request)
		tree = ET.parse(resultResponse)
		#print ( ET.tostring( tree.getroot() ))
		tree.write(filename)

	except Exception as e:
		print ( 'PROBLEMI in prendiSection : ' + str(e) )
		pass

	return


def Put_Link( link, filename ):
	
	logger.debug('------------------------ inizia Put_Link -------------- ')

	auth = '%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])
	base64string = b64encode(auth.encode())
	base64string = base64string.decode("ascii")
	headers = { 'Authorization' : 'Basic %s' %  base64string }
	file = open(filename)
	dati = file.read()

	request = urllib.request.Request(link, data=dati)

	request.add_header("Authorization", "Basic %s" % base64string)
	request.add_header('If-Match', '*')
	request.add_header('Content-Type', 'application/atom+xml')
	request.get_method = lambda: 'PUT'
	result = urllib.request.urlopen(request)

	#logger.debug(result.read())
	return
	xml = minidom.parse(result)
	fout = codecs.open(filename, 'w', 'utf-8')
	fout.write( xml.toxml() )
	fout.close()


	return

def Put_IdProd( idx, filename ):

	logger.debug('------------------------ inizia Put_Id -------------- ')
	try:
		auth = '%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])
		base64string = b64encode(auth.encode())
		base64string = base64string.decode("ascii")
		headers = { 'Authorization' : 'Basic %s' %  base64string }

		file = open(filename)
		dati = file.read()
		link = os.environ['ECE_SERVER']  + str(idx)

		prod_ece = 'http://internal.publishing.production.rsi.ch/webservice/escenic/content/'
		link = prod_ece  + str(idx)
		request = urllib.request.Request(link, data=dati)

		request.add_header("Authorization", "Basic %s" % base64string)
		request.add_header('If-Match', '*')
		request.add_header('Content-Type', 'application/atom+xml')
		request.get_method = lambda: 'PUT'
		result = urllib.request.urlopen(request)

		logger.debug('Put_Id' )
		logger.debug(result.read())
		
	
	except Exception as e:
		print ( 'PROBLEMI in Put_Id : ' + str(e) )
		return False

	logger.debug('------------------------ finisce Put_Id -------------- ')
	return True




def Put_Id( idx, filename ):

	print('------------------------ inizia Put_Id -------------- ')
	try:
		auth = '%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])
		base64string = b64encode(auth.encode())
		base64string = base64string.decode("ascii")

		headers = { 'Authorization' : 'Basic %s' % base64string,
				'If-Match': '*',
				'Content-Type': 'application/atom+xml'
			}    

		file = open(filename)
		dati = file.read()
		dati = ( dati ).encode()
		link = os.environ['ECE_SERVER']  + str(idx)
		print ( link )

		request = urllib.request.Request(url=link, data=dati, headers=headers, method='PUT')
		resultResponse = urllib.request.urlopen(request)

		print ( resultResponse.status )
		print ( resultResponse.headers )
		#print ( resultResponse.headers.split('rsi.ch/webservice/escenic/content/' )[-1] )
		print ( resultResponse.reason )
		
	
	except Exception as e:
		print ( 'PROBLEMI in Put_Id : ' + str(e) )
		return False

	logger.debug('------------------------ finisce Put_Id -------------- ')
	return True





#Dump_Link("http://internal.publishing.production.rsi.ch/webservice/escenic/content/8709154", '8709154.xml')
#Put_Link("http://internal.publishing.production.rsi.ch/webservice/escenic/content/8568391", '../_cambiamento_')
#Dump_Link("http://internal.publishing.production.rsi.ch/webservice/escenic/content/8568391", '8568391.xml')
#Dump_Link("http://internal.publishing.production.rsi.ch/webservice/escenic/content/8730301", '8730301.xml')
#Dump_Link("http://internal.publishing.production.rsi.ch/webservice/escenic/content/8675248", 'gallery_8675248.xml')
#Dump_Link("http://internal.publishing.production.rsi.ch/webservice/escenic/content/8948557", 'gallery_8948557.xml')
#Dump_Link("http://internal.publishing.production.rsi.ch/webservice/escenic/content/9013267", '9013267.xml')
#Dump_ET_Link('8845033', '8845033_et.xml')
#exit(0)

#Dump_Link("http://internal.publishing.production.rsi.ch/webservice/escenic/section/22742", 'jonas_section')
#Dump_Link("http://localhost/webservice/escenic/section/ROOT/subsections", 'jonas_1')
#exit(0)
#Dump_Link("http://internal.publishing.production.rsi.ch/webservice/escenic/section/7963/subsections", 'jonas_tvs')
#Dump_Link("http://internal.publishing.production.rsi.ch/webservice/escenic/section/8599/subsections", 'jonas_tvsplayer')
#Dump_Link("http://internal.publishing.production.rsi.ch/webservice/escenic/section/4/subsections", 'jonas_rsi')


def Dump_Rows ( link, filename ):

	logger.debug(' in Dump Rows ')

	#base64string = base64.encodestring('%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD']45')).replace('\n', '')
	base64string = base64.encodestring('%s:%s' % ('admin', 'admin')).replace('\n', '')

	request = urllib.request.Request(link)
	request.add_header("Authorization", "Basic %s" % base64string)
	result = urllib.request.urlopen(request)
	
	tree = ET.parse(result)


	totresult = int(tree.find( namespaces['opensearch'] + 'totalResults').text)
	items_per_page = int(tree.find( namespaces['opensearch'] + 'itemsPerPage').text)

	logger.debug('  per le ROWS : totalResults %s itemsPerPage %s ' % (totresult , items_per_page))

	request = urllib.request.Request(link)
	request.add_header("Authorization", "Basic %s" % base64string)
	result_xml = urllib.request.urlopen(request)
	xml = minidom.parse(result_xml)
	fout = codecs.open(filename, 'a', 'utf-8')
	fout.write( xml.toxml() )
	if  totresult > items_per_page:
		#logger.debug(' giro sui next e prev ')
		# devi  girare sui next per prendere gli altri
		for x in range(int(float(totresult)/float(items_per_page))):
			link_next = PrendiLink(tree, 'next')
			#logger.debug(link_next)
			request = urllib.request.Request(link_next )

			request.add_header("Authorization", "Basic %s" % base64string)
			result = urllib.request.urlopen(request)
			tree = ET.parse( result )
			request = urllib.request.Request(link_next)
			request.add_header("Authorization", "Basic %s" % base64string)
			result_xml = urllib.request.urlopen(request)
			xml = minidom.parse(result_xml)
			fout = codecs.open(filename, 'a', 'utf-8')
			fout.write( xml.toxml() )
			fout.close()

	exit(0)

	return

def PrendiState( entry , rel):

	list =  entry.findall(namespaces['app'] + "control/" + namespaces['vaext'] + 'state')
	
	for idx , lis in enumerate(list):
		#logger.debug(idx, lis)
		#logger.debug(lis.attrib['name'])
		return lis.attrib['name']
	return None

def PrendiEdited_Time( entry , rel):

	list =  entry.findall(namespaces['app'] + "edited/" )
	
	#logger.debug(list)

	for idx , lis in enumerate(list):
		#logger.debug(idx, lis.text)
		#logger.debug(lis.attrib['name'])
		return lis.text

	return None



def PrendiSezione( entry , rel):

	list =  entry.findall(namespaces['atom'] + "link")
	
	#logger.debug(list)

	for idx , lis in enumerate(list):
		#logger.debug(idx, lis)
		#logger.debug(lis.attrib['rel'])
		if rel in lis.attrib['rel']:
			#logger.debug(lis.attrib['href'])
			#logger.debug(lis.attrib['title'])
			return lis.attrib['title']
	return None

def PrendiTipo( entry ):

	list =  entry.findall(namespaces['atom'] + "content")
	for lis in list:
		#logger.debug(lis)
		payload =  lis.findall(namespaces['vdf'] + "payload")
		tipo = payload[0].attrib['model'].split('/')[-1]
		return tipo

	return None


def PrendiPayload( entry , rel):

	list =  entry.findall(namespaces['atom'] + "payload")
	
	logger.debug(len(list))

	for idx , lis in enumerate(list):
		logger.debug(idx, lis)
		logger.debug(lis.attrib['rel'])
		if rel in lis.attrib['rel']:
			logger.debug(lis.attrib['href'])
			logger.debug(lis.attrib['title'])
			#return lis.attrib['href']
	return None

def prendiGroup( entry , rel):

	result = []
	for idx , lis in enumerate(list):
		#logger.debug(idx, lis)
		#logger.debug(lis.attrib['rel'])
		if rel in lis.attrib['rel']:
			result.append( lis )
	return result


def prendiListaRelated( entry , rel):

	result = []
	list =  entry.findall(namespaces['atom'] + "link")
	for idx , lis in enumerate(list):
		#logger.debug(idx, lis)
		#logger.debug(lis.attrib['rel'])
		if rel in lis.attrib['rel']:
			result.append( lis )
	return result

def ordinaEditorial( listaRel ):
	
	print ( '------------------------ INIT ordinaEditorial -------------- ' )
	# questa ritorna una stringa con la lista delle related ordinata
	# per il valore che trova in custom
	order = {}
	result = ''
	count  = 0
	primo = True
	for res in listaRel:
		# in res ho la relazione  
		val = res.get(namespaces['metadata'] +'group')
		if 'EDITORIALKEYFRAMES' in val  and primo:
			print  (ET.tostring(res))
			order[ count ] = listaRel[-1]
			primo = False
		else:
			print  (ET.tostring(res))
			order[ count ] = res
		count += 1

	print ( order ) 
	for i in sorted (order.keys()) :  
		#print(i)
		print  (ET.tostring(order[ i ] ))
		#exit(0)
		result = result + str(ET.tostring(order[ i ]), 'utf-8' )

	print ( '------------------------ END ordinaEditorial -------------- ' )
	return result
		

def aggiungiEditorialList( entry,listaRel ):
	
	print ( '------------------------ INIT aggiungiEditorialList -------------- ' )
	# questa ritorna una stringa con la lista delle related ordinata
	# per il valore che trova in custom
	order = {}
	result = ''
	count  = 0
	for res in listaRel[:-1]:
		# in res ho la relazione  
		val = res.get(namespaces['metadata'] +'group')
		if 'EDITORIALKEYFRAMES' in val:
			listaRel.pop( count )
		count +=1

	for rel in listaRel:
		#print(i)
		print  (ET.tostring(rel ))
		#exit(0)
		entry.append(rel)

	print ( '------------------------ END aggiungiEditorialList -------------- ' )
	return entry
		

def ordinaListaRelated( listaRel ):
	
	print ( '------------------------ INIT ordinaListaRelated -------------- ' )
	# questa ritorna una stringa con la lista delle related ordinata
	# per il valore che trova in custom
	order = {}
	result = ''
	for res in listaRel:
		# in res ho la relazione 
		#print ( prendiFieldCustom( res, 'custom' ) )
		order[ int(prendiFieldCustom( res, 'custom' )) ] = res

	#print ( order ) 
	for i in sorted (order.keys()) :  
		#print(i)
		#print  (ET.tostring(order[ i ] ))
		#exit(0)
		result = result + str(ET.tostring(order[ i ]), 'utf-8' )

	print ( '------------------------ END ordinaListaRelated -------------- ' )
	return result
		
def rimuoviListaRelated( entry , rel):
	print ( '------------------------ INIT rimuoviListaRelated -------------- ' )
	# questa rimuove dal xml la lista delle relazioni 

	list =  entry.findall(namespaces['atom'] + "link")
	for idx,lis in enumerate(list):
		print(idx, lis)
		#logger.debug(lis.attrib['rel'])
		if rel in lis.attrib['rel']:
			print('rimuovo qualcosa')
			entry.remove( lis )
	print ( '------------------------ END rimuoviListaRelated -------------- ' )
	return entry
	
def aggiungiListaRelated( entry , listaRel):
	print ( '------------------------ INIT aggiungiListaRelated -------------- ' )
	
	order = {}
	result = ''
	for res in listaRel:
		# in res ho la relazione 
		#print ( prendiFieldCustom( res, 'custom' ) )
		order[ int(prendiFieldCustom( res, 'custom' )) ] = res

	#print ( order ) 
	for i in sorted (order.keys()) :  
		#print(i) 
		#print  (ET.tostring(order[ i ] ))
		#exit(0)
		entry.append(order[ i ])

	print ( '------------------------ END aggiungiListaRelated -------------- ' )
	return entry


def PrendiLinkRelated( entry , rel):

	list =  entry.findall(namespaces['atom'] + "link")
	
	#logger.debug(len(list))

	for idx , lis in enumerate(list):
		#logger.debug(idx, lis)
		#logger.debug(lis.attrib['rel'])
		if rel in lis.attrib['rel']:
			#logger.debug(lis.attrib['href'])
			#logger.debug(lis.attrib['title'])
			return lis
			
	return None


def PrendiLinkRelatedId( entry , rel):

	list =  entry.findall(namespaces['atom'] + "link")
	
	#logger.debug(len(list))

	for idx , lis in enumerate(list):
		#logger.debug(idx, lis)
		#logger.debug(lis.attrib['rel'])
		if rel in lis.attrib['rel']:
			#logger.debug(lis.attrib['href'])
			#logger.debug(lis.attrib['title'])
			return lis.attrib['href'].split('/')[-1]
			
	return None

def Trova_KeyFrame( Id_xx ):


	logger.debug(' ------------------- INIZIO Trova_KeyFrame ------------------------- ')
	
	base64string = base64.encodestring('%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])).replace('\n', '')

	link = os.environ['ECE_SERVER']  + str(Id_xx)

	request = urllib.request.Request(link)


	request.add_header("Authorization", "Basic %s" % base64string)
	try:
		result = urllib.request.urlopen(request)
	

		tree = ET.parse(result)

		id_keyframe =  PrendiLinkKeyframe( tree , 'related')




	except urllib.error.HTTPError as e:
		logger.debug(' EXCEPTIOOOONNNNNN - ritorno none !!!! ')
		return [ u'None' , u'None']

	logger.debug(' ------------------- FINE Trova_KeyFrame ------------------------- ')
	return id_keyframe

def PrendiLinkKeyframe( entry , rel):
	

	list =  entry.findall(namespaces['atom'] + "link")

        #logger.debug(len(list))
	id_keyframe = ''
	for idx , lis in enumerate(list):
		#logger.debug(idx, lis)
		#logger.debug(lis.attrib['rel'])
		if rel in lis.attrib['rel']:
			#for idx2, attr in enumerate(lis.attrib):
				#logger.debug(idx2, attr)
			logger.debug(lis.attrib[namespaces['metadata'] + 'group'])
			group =  lis.attrib[namespaces['metadata'] + 'group']
 
			if 'KEYFRAMES' in group:
				# ho trovato un keyframe
				_payload = lis.findall( namespaces['vdf'] + "payload")
				logger.debug(_payload[0].attrib['model'])
				if 'EDITORIAL' in group:
				
					logger.debug(lis.attrib['href'])
					logger.debug(lis.attrib['title'])
					#  se editorial ritorno con ID
					return lis.attrib['href'].split('/')[-1]
				else:
					# se normale me lo ricordo ma cerco editorial
					id_keyframe = lis.attrib['href'].split('/')[-1] 

	return id_keyframe


def PrendiLinkLeadId( entry , rel):
	
	# al momento prende sempre e solo la prima
	# e assume che sia una immagine

	list =  entry.findall(namespaces['atom'] + "link")

	#logger.debug(len(list))

	for idx , lis in enumerate(list):
		#logger.debug(idx, lis)
		#logger.debug(lis.attrib['rel'])
		if rel in lis.attrib['rel']:
			#for idx2, attr in enumerate(lis.attrib):
				#logger.debug(idx2, attr)
			#logger.debug(lis.attrib[namespaces['metadata'] + 'group'])
			group =  lis.attrib[namespaces['metadata'] + 'group']
		if 'lead' in group:
				# ho trovato una relazione in lead
				# devo verificare che sia una picture
				_payload = lis.findall( namespaces['vdf'] + "payload")
				#logger.debug(_payload[0].attrib['model'])
				if 'picture' in _payload[0].attrib['model']:
				
					#logger.debug(lis.attrib['href'])
					#logger.debug(lis.attrib['title'])
					# e qui ne prendo ID
					return lis.attrib['href'].split('/')[-1]
				else:
					# devo prendere la immagine del video
					# altrimenti il video stesso
					logger.debug('Trovato VIDEO ? ')
					# devo prendere l id della picture che mi interessa
					# poi dovro prendere il valore del video stesso e buttarlo
					# su
					id_keyframe = Trova_KeyFrame( lis.attrib['href'].split('/')[-1] )
					return id_keyframe

	return None


def PrendiLink( entry , rel):

	list =  entry.findall(namespaces['atom'] + "link")
	
	#logger.debug(len(list))

	for idx , lis in enumerate(list):
		#logger.debug(idx, lis)
		#logger.debug(lis.attrib['rel'])
		if rel in lis.attrib['rel']:
			#logger.debug(lis.attrib['href'])
			#logger.debug(lis.attrib['title'])
			
			return lis.attrib['href']
	return None

def prendiFieldCustom( entry , rel):

	#print (lis)
	payload =  entry.findall(namespaces['vdf'] + "payload")
	for pay in payload:
		fields = pay.findall(namespaces['vdf'] + "field")
		#print (fields)

		for idx , fiel in enumerate(fields):
			#print(idx, fiel)
			#print(fiel.attrib['name'])
			if rel in fiel.attrib['name']:
				if not (fiel.find(namespaces['vdf'] + "value") is None) :
					return fiel.find(namespaces['vdf'] + "value").text
				else:
					return 0

	return 0

def prendiField( entry , rel):

	list =  entry.findall(namespaces['atom'] + "content")
	for lis in list:
		#print (lis)
		payload =  lis.findall(namespaces['vdf'] + "payload")
		for pay in payload:
			fields = pay.findall(namespaces['vdf'] + "field")
			#print (fields)

			for idx , fiel in enumerate(fields):
				#print(idx, fiel)
				#print(fiel.attrib['name'])
				if rel in fiel.attrib['name']:
					if not (fiel.find(namespaces['vdf'] + "value") is None) :
						return fiel.find(namespaces['vdf'] + "value").text
					else:
						return ''

	return ''



def PrendiField( entry , rel):

	list =  entry.findall(namespaces['atom'] + "content")
	for lis in list:
		#print (lis)
		payload =  lis.findall(namespaces['vdf'] + "payload")
		for pay in payload:
			fields = pay.findall(namespaces['vdf'] + "field")
			#print (fields)

			for idx , fiel in enumerate(fields):
				#print(idx, fiel)
				#print(fiel.attrib['name'])
				if rel in fiel.attrib['name']:
					if not (fiel.find(namespaces['vdf'] + "value") is None) :
						return fiel.find(namespaces['vdf'] + "value").text
					else:
						return None

	return None

def PrendiFieldSocial( entry , rel):

	list =  entry.findall(namespaces['atom'] + "content")
	for lis in list:
		#logger.debug(lis)
		payload =  lis.findall(namespaces['vdf'] + "payload")
		for pay in payload:
			fields = pay.findall(namespaces['vdf'] + "field")
			#logger.debug(fields)

			for idx , fiel in enumerate(fields):
				#logger.debug(idx, fiel)
				#logger.debug(fiel.attrib['name'])
				if rel in fiel.attrib['name']:
					if not (fiel.find(namespaces['vdf'] + "value") is None) :
						return fiel.find(namespaces['vdf'] + "value").text
					else:
						return None

	return None




def processElem(elem):

	if elem.text is not None:
		logger.debug(elem.text)
	for child in elem:
		processElem(child)
		if child.tail is not None:
			logger.debug(' scrivo la coda ' )
			logger.debug(child.tail)


def PrendiFieldTest( entry , rel):

	list =  entry.findall(namespaces['atom'] + "content")
	for lis in list:
		#logger.debug(lis)
		payload =  lis.findall(namespaces['vdf'] + "payload")
		for pay in payload:
			fields = pay.findall(namespaces['vdf'] + "field")
			#logger.debug(fields)

			for idx , fiel in enumerate(fields):
				logger.debug(idx, fiel)
				logger.debug(fiel.attrib['name'])
				if rel in fiel.attrib['name']:
					logger.debug(' trovato ' + rel )
					if not (fiel.find(namespaces['vdf'] + "value") is None) :
						val =  fiel.find(namespaces['vdf'] + "value")
						processElem( val )
						return fiel.find(namespaces['vdf'] + "value").text
					else:
						return 'None'

	return 'None'



def PrendiTimeCtrl( entry ):

	
	activation = PrendiActivation(entry, ' activation_time')
	#logger.debug('activation ' + activation)
	published = PrendiPublished( entry, 'updated')
	#logger.debug('published ' + published)
	expires = PrendiExpires( entry, 'expiration' )
	#logger.debug('expires ' + expires)

	return [ activation, published, expires ]

def PrendiPublished( entry , name):

	list =  entry.findall(namespaces['atom'] + name )
	for lis in list:
		#logger.debug(lis.text)
		return lis.text

	return None




def PrendiExpires( entry , rel):

	list =  entry.findall(namespaces['age'] + "expires")
	for lis in list:
		#logger.debug(lis.text)
		return lis.text

	return None



def PrendiActivation( entry , rel):

	list =  entry.findall(namespaces['dcterms'] + "available")
	for lis in list:
		#logger.debug(lis.text)
		return lis.text

	return None


def CambiaBodyFile( filein ):

	print( '--------------- INIT CambiaBodyFile ---------------------')
	rimpiazza = {'<html:':'<', '</html:':'</','xmlns:html=':'xmlns=' }

	rep = dict((re.escape(k), v) for k, v in rimpiazza.items()) 
	pattern = re.compile("|".join(rep.keys()))

	linesOk = ""
	with open(filein) as infile:
		lines = infile.readlines()
		#print (lines )
		for line in lines:
			txtLine = pattern.sub(lambda m: rep[re.escape(m.group(0))], line )
			print(" ------ " +  txtLine )
		linesOk = linesOk + " " + txtLine

	
	print ( linesOk)
	exit(0)

	infile.close()
	with open(filein, 'w') as outfile:
		for lineout in lines:
			outfile.write(lineout)	
	outfile.close()
	print( '--------------- END CambiaBodyFile ---------------------')
	

def CambiaState( entry , value):

	list =  entry.findall(namespaces['app'] + "control/" + namespaces['vaext'] + 'state')
	
	#logger.debug(' CambiaState ------ ')
	#logger.debug(list)

	for idx , lis in enumerate(list):
		#logger.debug(idx, lis)
		lis.text = value
		return entry

	#logger.debug(' CambiaState ------ ')
	return None



def CambiaField( entry , rel, value):

	list =  entry.findall(namespaces['atom'] + "content")
	for lis in list:
		payload =  lis.findall(namespaces['vdf'] + "payload")
		for pay in payload:
			fields = pay.findall(namespaces['vdf'] + "field")
			#logger.debug(fields)

			for idx , fiel in enumerate(fields):
				#logger.debug(idx, fiel)
				#logger.debug(fiel.attrib['name'])
				if rel in fiel.attrib['name']:
					if fiel.find(namespaces['vdf'] + "value") is None:
						fiel.append(ET.Element(namespaces['vdf'] + "value"))
					fiel.find(namespaces['vdf'] + "value").text = value
					#logger.debug(fiel.attrib['name'], fiel.find(namespaces['vdf'] + "value").text)
					return entry

	return entry

def Change_Item_Content( title, description, alttext, leadtext, tree ):


	if leadtext is not None:
		# allora devo riempirlo con il leadtext
		logger.debug("leadtext is not None = " + leadtext)
		CambiaField( tree, "alttext", leadtext )
		return tree
	#logger.debug("alttext vuoto")
	else:
		if description is not None:
			# allora devo riempirlo con la descrizione
			logger.debug("description is not None = " + description)
			CambiaField( tree, "alttext", description )
			return tree
		else:
			# allora devo riempirlo con il titolo
			logger.debug("riempio con il titolo = " + title)
			CambiaField( tree, "alttext", title )
			return tree

	return tree


def Get_Item_Content( Id_xx ):

	base64string = base64.encodestring('%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])).replace('\n', '')
	link = os.environ['ECE_SERVER']  + str(Id_xx)
	request = urllib.request.Request(link)
	request.add_header("Authorization", "Basic %s" % base64string)
	try:
		result = urllib.request.urlopen(request)
		tree = ET.parse(result)
		#tree.write('x.xml')
		#ET.dump(tree)
		tipo = PrendiTipo( tree )

		logger.debug(tipo)

		return

		if 'keyframe' in tipo:
			#se e' un keyframe si chiama name
			campo_titolo = "name"
		else:
			#altrimenti si chiama title
			campo_titolo = "title"
		title =  PrendiField( tree, campo_titolo)
		logger.debug(title)
		description =  PrendiField( tree, "description")
		logger.debug(description)
		alttext =  PrendiField( tree, "alttext")
		logger.debug(alttext)
		leadtext =  PrendiField( tree, "leadtext")
		logger.debug(leadtext)
		#programmeId =  PrendiField( tree, "programmeId")
		#logger.debug(programmeId)

		result.close()
	except urllib.error.HTTPError as e:
		logger.debug(' EXCEPTIOOOONNNNNN - ritorno none !!!! ')
		return [ u'None' , u'None', u'None']


	return [ title , description, alttext, leadtext, tree ]


def Prendi_Url( Id_xx ):
		
	base64string = base64.encodestring('%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])).replace('\n', '')

	link = os.environ['ECE_SERVER']  + str(Id_xx)

	request = urllib.request.Request(link)

	request.add_header("Authorization", "Basic %s" % base64string)
	try:
		result = urllib.request.urlopen(request)
	

		tree = ET.parse(result)
		#tree.write('prendi_URL_ok.xml')
		#ET.dump(tree)
		Url =  PrendiLink( tree, "alternate")
		#print ('URL = ' +  Url )

		result.close()
	except urllib.request.HTTPError as e:
		logger.debug(' EXCEPTIOOOONNNNNN - ritorno none !!!! ')
		return [ u'None' , u'None', u'None']
		
	
	return Url

def Check_Data_Range( adesso, Id_xx ):

	# questa server per verificare di essere nel range tra activation
	# e expiration 
	# verifico anche la publishing perche talvolta e piu alta della activation date

	[ activation, published, expires ] =  Prendi_Social_TimeCtrl(Id_xx)
	# logger.debug(activation, published, expires)

	data_limite = adesso
	active = False

	# logger.debug(' adesso = ' + str(adesso))

	# in caso non sia settata alcuna data di attivazione la Prendi_Social_TimeCtrl
	# mi torna None per il campo activate 
	if activation is None:
		# mettendo il seguente aassegnamento sono sicuro poi di confrontare
		# la data attuale con la data  di pubblicazione e dovrei passare sempre
		active = True
	else :
		data_limite = datetime.strptime( activation, '%Y-%m-%dT%X.000Z' )

	# logger.debug('data_limite = ' + str(data_limite))
	
	if adesso >= data_limite:	
		# logger.debug(' Data attuale nel Range ')
		return True
	
	# logger.debug(' Data attuale FUORI Range ')
	return False

	



def Prendi_Social_TimeCtrl( Id_xx ):
	
	flagDiRitorno = {}
		
	base64string = base64.encodestring('%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])).replace('\n', '')

	link = os.environ['ECE_SERVER']  + str(Id_xx)

	request = urllib.request.Request(link)


	request.add_header("Authorization", "Basic %s" % base64string)
	try:
		result = urllib.request.urlopen(request)
	

		tree = ET.parse(result)
		timeCtrl = PrendiTimeCtrl( tree )

		result.close()
	except urllib.error.HTTPError as e:
		logger.debug(' EXCEPTIOOOONNNNNN - ritorno none !!!! ')
		return [ None , None]
		
	
	return timeCtrl


def VerificaGeo( entry ):

	result = []

	list =  entry.findall(namespaces['atom'] + "content")
	for lis in list:
		#logger.debug(lis)
		payload =  lis.findall(namespaces['vdf'] + "payload")
		for pay in payload:
			fields = pay.findall(namespaces['vdf'] + "field")
			#logger.debug(fields)

			for idx , fiel in enumerate(fields):
				#logger.debug(idx, fiel)
				#logger.debug(fiel.attrib['name'])
				if 'stream' == fiel.attrib['name']:
					#logger.debug('passo di qui')
					# qui ho preso il field fb-livestreaming-account
					# e adesso devo prendere tutti gli account che ho messo
					_list = fiel.findall(namespaces['vdf'] + "list")
					if len( _list ) > 0 :
						_list = _list[0]
					else:
						return False
					stre_payloads =  _list.findall(namespaces['vdf'] + "payload")
					#logger.debug('stre_payloads ' )
					#logger.debug(stre_payloads)
					for stre in stre_payloads:
						#logger.debug('giro _payloads')
						#logger.debug(stre)
						stre_field = stre.find(namespaces['vdf'] + "field")
						#logger.debug(stre_field.attrib['name'])
						if 'stream' in stre_field.attrib['name']:
							if not (stre_field.find(namespaces['vdf'] + "value") is None) :
								#logger.debug(stre_field.find(namespaces['vdf'] + "value").text)
								if 'ch-lh.akamaihd' in stre_field.find(namespaces['vdf'] + "value").text:
									#logger.debug( ' Ritorno GEO = True ' )
									return True
								#result.append(acc_field.find(namespaces['vdf'] + "value").text)	

	return False

def PrendiAccounts( entry , type ):

	result = {}
	account = {}
	list =  entry.findall(namespaces['atom'] + "content")
	for lis in list:
		#logger.debug(lis)
		payload =  lis.findall(namespaces['vdf'] + "payload")
		for pay in payload:
			fields = pay.findall(namespaces['vdf'] + "field")
			#logger.debug(fields)

			for idx , fiel in enumerate(fields):
				#logger.debug(idx, fiel)
				#logger.debug(fiel.attrib['name'])
				if type in fiel.attrib['name']:
					#logger.debug('passo di qui')
					# qui ho preso il field fb-livestreaming-account
					# e adesso devo prendere tutti gli account che ho messo
					_list = fiel.findall(namespaces['vdf'] + "list")
					if len( _list ) < 1 :
						return result
						
					else:

						# faccio cosi perche la lista e unica
						_list = _list[0]
						acc_payloads =  _list.findall(namespaces['vdf'] + "payload")
						
						#print ( ' acc_payloads lungo : ' + str( len ( acc_payloads)) )
						for acc in acc_payloads:
							#print ( '----------------------------giro _payloads' )
							acc_fblivesocencchan = acc.find(namespaces['vdf'] + "field")
							acc_fields = acc_fblivesocencchan.findall(namespaces['vdf'] + "field")
							#print ( ' acc_payloads lungo : ' + str( len ( acc_payloads)) )
							
							for acc_f in acc_fields:
								if not (acc_f.find(namespaces['vdf'] + "value") is None) :
									#print (  acc_f.attrib['name']  + ' = ' + acc_f.find(namespaces['vdf'] + "value").text )
									account[ acc_f.attrib['name'] ] =  acc_f.find(namespaces['vdf'] + "value").text	

							result[ account['livesocialid']] =   {'SocialDetails' : account}
							account = {}
	return result

def PrendiSocialTime( entry ):


	start = ''
	end = ''

	list =  entry.findall(namespaces['atom'] + "content")
	for lis in list:
		#logger.debug(lis)
		payload =  lis.findall(namespaces['vdf'] + "payload")
		for pay in payload:
			fields = pay.findall(namespaces['vdf'] + "field")
			#logger.debug(fields)

			for idx , fiel in enumerate(fields):
				#logger.debug(idx, fiel)
				#logger.debug(fiel.attrib['name'])
				if 'socialStartTime' in fiel.attrib['name'] :
					if not (fiel.find(namespaces['vdf'] + "value") is None) :
						# ho trovato il time nella forma 
						# <vdf:value>2017-04-24T10:30:00Z</vdf:value>
						start =  fiel.find(namespaces['vdf'] + "value").text
	
				if 'socialEndTime' in fiel.attrib['name'] :
					if not (fiel.find(namespaces['vdf'] + "value") is None) :
						# ho trovato il time nella forma 
						# <vdf:value>2017-04-24T10:30:00Z</vdf:value>
						end =  fiel.find(namespaces['vdf'] + "value").text



	return [ start, end ]



def PrendiTime( entry ):


	start = ''
	end = ''

	list =  entry.findall(namespaces['atom'] + "content")
	for lis in list:
		#logger.debug(lis)
		payload =  lis.findall(namespaces['vdf'] + "payload")
		for pay in payload:
			fields = pay.findall(namespaces['vdf'] + "field")
			#logger.debug(fields)

			for idx , fiel in enumerate(fields):
				#logger.debug(idx, fiel)
				#logger.debug(fiel.attrib['name'])
				if 'startTime' in fiel.attrib['name'] :
					if not (fiel.find(namespaces['vdf'] + "value") is None) :
						# ho trovato il time nella forma 
						# <vdf:value>2017-04-24T10:30:00Z</vdf:value>
						start =  fiel.find(namespaces['vdf'] + "value").text
	
				if 'endTime' in fiel.attrib['name'] :
					if not (fiel.find(namespaces['vdf'] + "value") is None) :
						# ho trovato il time nella forma 
						# <vdf:value>2017-04-24T10:30:00Z</vdf:value>
						end =  fiel.find(namespaces['vdf'] + "value").text


	return [ start, end ]



def Prendi_Section_Parameters( Id_xx ):
	
	logger = logging.getLogger('pySub_ECE')
	logger.debug(' ---------- INIT Prendi_Section_Parameters -------------- ')
	lista_value_social = [ 'title','subtitle', 'fbVideoTitle', 'fbVideoDesc', 'ytVideoTitle','ytVideoDesc' ]


	base64string = base64.encodestring('%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])).replace('\n', '')
	#link = os.environ['ECE_SERVER']  + str(Id_xx)
	# cambiato per andare a prendere pub2 senza pwd
	print ( os.environ['ECE_SECTION'] )

	link = os.environ['ECE_SECTION']  + str(Id_xx)
	logger.debug( link )
	#print ( 'Clad' )
	request = urllib.request.Request(link)
	request.add_header("Authorization", "Basic %s" % base64string)

	num_posts = 0
	account = ''
	nomefile = ''
	path = ''

	try:
	
		result = urllib.request.urlopen(request)
		#print ( result.read()  )

		tree = ET.parse(result)
		#tree.write('22742.xml')
		#ET.dump(tree)
		print ( 'scritto' )
		#return
		parametri = PrendiParam( tree )
		print ( parametri )
		parametri = parametri.splitlines()
		for para in parametri:
			if 'mostSocial.account' in para:
				print ( 'trovato account' )
				account = para.split('=')[-1]
			if 'mostSocial.num_posts' in para:
				print ( 'trovato num_posts' )
				num_posts = para.split('=')[-1]
			if 'mostSocial.nomeFile' in para:
				print ( 'trovato num_posts' )
				nomefile = para.split('=')[-1]
			if 'mostSocial.path' in para:
				print ( 'trovato num_posts' )
				path = para.split('=')[-1]
		
		

		logger.debug(' ---------- FINE Prendi_Section_Parameters -------------- ')
		result.close()
	except urllib.error.HTTPError as e:
		logger.debug(' EXCEPTIOOOONNNNNN - ritorno none !!!! ')
		logger.debug(' ---------- FINE Prendi_Section_Parameters -------------- ')

		return { u'num_posts' : u'None' , u'account' : u'None', u'nomefile': u'None', u'path': u'None'}
		
	
	return { u'num_posts' : num_posts , u'account' : account , u'nomefile' : nomefile, u'path' : path }


def PrendiParam( entry ):

	rel = 'sectionParameters'

	list =  entry.findall(namespaces['atom'] + "content")
	for lis in list:
		#logger.debug(lis)
		#print ( lis )
		payload =  lis.findall(namespaces['vdf'] + "payload")
		for pay in payload:
			fields = pay.findall(namespaces['vdf'] + "field")
			#logger.debug(fields)
			#print ( fields )

			for idx , fiel in enumerate(fields):
				#print ( idx, fiel )
				#print ( fiel.attrib['name'] )
				if rel in fiel.attrib['name']:
					if not (fiel.find(namespaces['vdf'] + "value") is None) :
						return fiel.find(namespaces['vdf'] + "value").text
					else:
						return None

	return None

def putIdFromStr( idx, xmlStr ):

	print('------------------------ inizia putIdFromStr -------------- ')
	logger.debug('------------------------ inizia putIdFromStr -------------- ')
	try:
		auth = '%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])
		base64string = b64encode(auth.encode())
		base64string = base64string.decode("ascii")

		headers = { 'Authorization' : 'Basic %s' % base64string,
				'If-Match': '*',
				'Content-Type': 'application/atom+xml'
			}    

		dati = xmlStr
		dati = ( dati ).encode()
		link = os.environ['ECE_SERVER']  + str(idx)
		print ( link )
		logger.debug ( link )

		request = urllib.request.Request(url=link, data=dati, headers=headers, method='PUT')
		resultResponse = urllib.request.urlopen(request)

		print ( resultResponse.status )
		logger.debug ( resultResponse.status )
		print ( resultResponse.headers )
		logger.debug ( resultResponse.headers )
		#print ( resultResponse.headers.split('rsi.ch/webservice/escenic/content/' )[-1] )
		print ( resultResponse.reason )
		logger.debug ( resultResponse.reason )
		
	
	except Exception as e:
		print ( 'PROBLEMI in putIdFromStr : ' + str(e) )
		logger.debug ( 'PROBLEMI in putIdFromStr : ' + str(e) )
		return False

	logger.debug('------------------------ finisce putIdFromStr -------------- ')
	return True

		
def putId( idx, filename ):

	print('------------------------ inizia putId -------------- ')
	logger.debug('------------------------ inizia putId -------------- ')
	try:
		auth = '%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])
		base64string = b64encode(auth.encode())
		base64string = base64string.decode("ascii")

		headers = { 'Authorization' : 'Basic %s' % base64string,
				'If-Match': '*',
				'Content-Type': 'application/atom+xml'
			}    

		file = open(filename)
		dati = file.read()
		
		print( dati)
		dati = ( dati ).encode()
		print( dati)
		link = os.environ['ECE_SERVER']  + str(idx)
		print ( link )
		logger.debug ( link )


		request = urllib.request.Request(url=link, data=dati, headers=headers, method='PUT')
		resultResponse = urllib.request.urlopen(request)

		print ( resultResponse.status )
		logger.debug ( resultResponse.status )
		print ( resultResponse.headers )
		logger.debug ( resultResponse.headers )
		#print ( resultResponse.headers.split('rsi.ch/webservice/escenic/content/' )[-1] )
		print ( resultResponse.reason )
		logger.debug ( resultResponse.reason )
		
	
	except Exception as e:
		print ( 'PROBLEMI in putId : ' + str(e) )
		logger.debug ( 'PROBLEMI in putId : ' + str(e) )
		return False

	logger.debug('------------------------ finisce putId -------------- ')
	return True
		
def putToState( eceId, state ):

	print('------------------------ inizia putToState -------------- ')
	logger.debug('------------------------ inizia putToState -------------- ')
	successGetId = -1
	try:
		[ successGetId, treeEceId  ] = getEceId(  eceId )

		if successGetId == -1:
			return False

		nome_file = creaNomeFile( 'digas_da_pubblicare' )
		tree4Ece = CambiaState( treeEceId, state )
		tree4Ece.write(nome_file)
		if not putId( eceId, nome_file):
			raise Exception("Errore in putId")	
		else:
			print ( " updatato : " + eceId )
	
	except Exception as e:
		print ( 'PROBLEMI in putToState : ' + str(e) )
		logger.debug ( 'PROBLEMI in putToState : ' + str(e) )
		return False

	logger.debug('------------------------ finisce putToState -------------- ')
	print('------------------------ finisce putToState -------------- ')
	return True

def deleteId( idx ):

	print('------------------------ inizia deleteId -------------- ')
	logger.debug('------------------------ inizia deleteId -------------- ')
	try:
		auth = '%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])
		base64string = b64encode(auth.encode())
		base64string = base64string.decode("ascii")

		headers = { 'Authorization' : 'Basic %s' % base64string,
				'If-Match': '*',
				'Content-Type': 'application/atom+xml'
			}    

		link = os.environ['ECE_SERVER']  + str(idx)
		print ( link )
		logger.debug ( link )

		request = urllib.request.Request(url=link, headers=headers, method='DELETE')
		resultResponse = urllib.request.urlopen(request)

		print ( resultResponse.status )
		logger.debug ( resultResponse.status )
		print ( resultResponse.headers )
		logger.debug ( resultResponse.headers )
		print ( resultResponse.reason )
		logger.debug ( resultResponse.reason )
		
	
	except Exception as e:
		logger.debug ( 'PROBLEMI in deleteId : ' + str(e) )
		print ( 'PROBLEMI in deleteId : ' + str(e) )
		return False

	logger.debug('------------------------ finisce deleteId -------------- ')
	return True






def flatten(d, parent_key='', sep='_'):
	items = []
	for k, v in d.items():
		new_key = parent_key + sep + k if parent_key else k
		try:
			items.extend(flatten(v, new_key, sep=sep).items())
		except:
			items.append((new_key, v))

	return dict(items)

def flatterRundown( jsonRundown ):
	# funzione che fa il parsing dell xml del rundown e ne restituisce un json con
	# i campi e i loro valori
	# quando poi si passera al json si presume che venga uguale
	result = []

	for jrun in jsonMpRundown:
		try:
			#print ( jsonMpRundown )
			result.append(flatten(jrun))
		except:
			logger.warning("problemi in flatterMpRundown")
			return []

	return result
		

def getEceId( eceId ):
	print ( '------------------------ inizia getEceId -------------- ' )
	logger.debug ( '------------------------ inizia getEceId -------------- ' )
	result=[ False, None ]

	try:
		link = os.environ['ECE_SERVER']  + str(eceId)
		print( 'link : ' + link  )
		logger.debug( 'link : ' + link  )
		
		auth = '%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])
		base64string = b64encode(auth.encode())
		base64string = base64string.decode("ascii")

		headers = { 'Authorization' : 'Basic %s' % base64string,
				'Content-Type': 'application/atom+xml'
			}    

		request = urllib.request.Request(url=link, headers=headers, method='GET')
		resultResponse = urllib.request.urlopen(request)
		print ( resultResponse.status )
		logger.debug ( resultResponse.status )
		#print ( resultResponse.headers )
		#print ( resultResponse.headers )
		#print ( resultResponse.headers.split('rsi.ch/webservice/escenic/content/' )[-1] )
		#print ( resultResponse.reason )
		#print ( resultResponse.getheader('Location') )

		tree = ET.parse(resultResponse)
		#print ( tree )
		#print ( type(tree))
		result = tree
		#tree.write('newx.xml')
		#exit(0)
		#ET.dump(tree)

		#title =  PrendiField( tree, "title")
		#logger.debug(title)
		#programmeId =  PrendiField( tree, "programmeId")
		#logger.debug(programmeId)

	except urllib.error.HTTPError as e:
		print(' EXCEPTIOOOONNNNNN - ritorno none !!!! ')
		logger.debug(' EXCEPTIOOOONNNNNN - ritorno none !!!! ')
		return [ False, str(e) ]

	print ( '------------------------ END getEceId -------------- ' )
	logger.debug ( '------------------------ END getEceId -------------- ' )

	return [ True, result ]

def creaEntryDateExpires( entry, value ):

	# scritta perche' altrimenti non trovando la entry expires
	# non avremmo fatto la update
	# invece cosi aggiungiamo la entry 
	exp = ET.Element(namespaces['age'] + "expires")
	exp.text = value

	root = entry.getroot(  )
	root.insert( 1, exp )

	#entry.write('test_expires.xml')
	 
	return entry

def cambiaDateExpires( entry , dummyRel, value):

	list =  entry.findall(namespaces['age'] + "expires")
	if len( list ) < 1:
		# non ho trovato la entry : la creo io
		creaEntryDateExpires( entry, value )
	else:
		for lis in list:
			print  (lis.text )
			lis.text = value
	return entry

def prendiTitle( entry ):

	result = ''
	list =  entry.findall(namespaces['atom'] + "title")
	for lis in list:
		date_time_obj = datetime. strptime(lis.text, "%Y-%m-%dT%H:%M:%S.%fZ")
		return str(int(datetime.timestamp(date_time_obj)))
	
	return result




def prendiDateUpdate( entry ):

	result = ''
	list =  entry.findall(namespaces['atom'] + "updated")
	for lis in list:
		return lis.text
	
	return result



def cambiaDateUpdate( entry , dummyRel, value):

	list =  entry.findall(namespaces['atom'] + "updated")
	for lis in list:
		print  (lis.text )
		lis.text = value
	return entry


def cambiaField( entry , rel, value):

	list =  entry.findall(namespaces['atom'] + "content")
	for lis in list:
		payload =  lis.findall(namespaces['vdf'] + "payload")
		for pay in payload:
			fields = pay.findall(namespaces['vdf'] + "field")
			#logger.debug(fields)

			for idx , fiel in enumerate(fields):
				#logger.debug(idx, fiel)
				#logger.debug(fiel.attrib['name'])
				if rel in fiel.attrib['name'] and len( rel) == len(fiel.attrib['name']):
					if fiel.find(namespaces['vdf'] + "value") is None:
						fiel.append(ET.Element(namespaces['vdf'] + "value"))
					fiel.find(namespaces['vdf'] + "value").text = value
					#logger.debug(fiel.attrib['name'], fiel.find(namespaces['vdf'] + "value").text)
					return entry

	return entry

def creaNomeFile( contentType ):

	dateTimeObj = datetime.now()
	timestampStr = dateTimeObj.strftime("%d.%m.%Y_%H.%M.%S.%f")
	result = os.environ['FILE_LOG_DIR'] + '/' + contentType + '_' + timestampStr + '.xml'
	return result
	

	
def deleteEceDaKafka( message ):

	print ( '------------------------ inizia deleteEceDaKafka -------------- ' )
	logger.debug ( '------------------------ inizia deleteEceDaKafka -------------- ' )
	# questo mi ritorna tutti gli xml gia' buoni da buttare su ECE
	result = False

	try:
	
	 	# posso passare direttamente il valore del json con ece id da 
		# cambiare perche se sono qui vuol dire che e presente
		eceId = None
		#
		[ eceId, treeToUpdate  ]= getEceIdFromCmsJson( message['cms'] )
		print ( 'tornato da getEceIdFromCmsJson ' )
		logger.debug ( 'tornato da getEceIdFromCmsJson ' )
		if eceId is None:
			return False

		if not deleteId( eceId):
			raise Exception("Errore in deleteId")	
		else:
			print ( " updatato : " + eceId )
			logger.debug ( " updatato : " + eceId )
			result = True

	except Exception as e:
		print ( 'PROBLEMI in deleteProgrammeDaKafka : ' + str(e) )
		logger.debug ( 'PROBLEMI in deleteProgrammeDaKafka : ' + str(e) )
		return False

	print ( '------------------------ END deleteEceDaKafka -------------- ' )
	logger.debug ( '------------------------ END deleteEceDaKafka -------------- ' )
	return result

def buttasu( filePath, section ):

	print( '------------------------ inizia buttasu -------------- '  )

	result = [False, '']

	# che deve fare la  curl --include -u perucccl:perucccl -X POST -H "Content-Type: application/atom+xml"  
	# http://internal.publishing.production.rsi.ch/webservice/escenic/section/__CREATE_SECTION__/content-items 
	# --upload-file Resources/template_mamProgramme.xml
	# dove __CREATE_SECTION__ deve essere cambiato con section e il file template deve essere aggiornato con i valori
	# dei parametri
	# 5909 e la temp

	try:
		#link =  os.environ['IMG_CREATE_URL'].replace('__CREATE_SECTION__', section )

		link =  "http://internal.publishing.staging.rsi.ch/webservice/escenic/section/" + section  + "/content-items"
		print( 'link : ' + link  )
		
		# apro il template file
		auth = '%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])
		base64string = b64encode(auth.encode())
		base64string = base64string.decode("ascii")

		headers = { 'Authorization' : 'Basic %s' % base64string,
				'Content-Type': 'application/atom+xml'
			}    

	        #dati=open( os.environ['VID_RESOURCE_TEMPLATE'] ,'r').read()
		dati=open( filePath,'r').read()
		dati = dati.replace('__ECE_MODEL__', 'http://internal.publishing.staging.rsi.ch/webservice/publication/rsi/escenic/model/' )
		#dati = dati.replace('__CREATE_SECTION__', section )
		#dati = dati.replace('__TITOLO_PICTURE__', titolo )
		#dati = dati.replace('__BINARY_LOCATION__', binaryUrl )

		print( ' data  = ' + dati  )
		dati = ( dati ).encode()

		request = urllib.request.Request(url=link, data=dati, headers=headers, method='POST')
		resultResponse = urllib.request.urlopen(request)
		print ( resultResponse.status )
		#print ( resultResponse.status )
		#print ( resultResponse.headers )
		#print ( resultResponse.headers )
		#print ( resultResponse.headers.split('rsi.ch/webservice/escenic/content/' )[-1] )
		#print ( resultResponse.reason )
		print ( resultResponse.getheader('Location') )
		#print ( resultResponse.getheader('ocation') )
		#for lis  in resultResponse.getheaders():
			#print ( lis )
			#print (type(lis))

	
	except Exception as e:
		print( 'PROBLEMI in buttasu: ' + str(e)  )
		return [False, '' ]

	print('------------------------ finisce buttasu-------------- ' )
	return result


def CreateProgrammeVideo( binaryUrl, titolo, section ):

	logger.debug( '------------------------ inizia CreateProgrammeVideo -------------- ' )

	result = [False, '']

	# che deve fare la  curl --include -u perucccl:perucccl -X POST -H "Content-Type: application/atom+xml"  
	# http://internal.publishing.production.rsi.ch/webservice/escenic/section/__CREATE_SECTION__/content-items 
	# --upload-file Resources/template_picture.xml
	# dove __CREATE_SECTION__ deve essere cambiato con section e il file template deve essere aggiornato con i valori
	# dei parametri

	try:
		link =  os.environ['IMG_CREATE_URL'].replace('__CREATE_SECTION__', section )
		logger.debug( 'link : ' + link )
		
		# apro il template file
		
		
		logger.debug( 'binaryUrl : ' + binaryUrl )
		logger.debug( 'titolo : ' + titolo )

		base64string = base64.encodestring('%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])).replace('\n', '')

		headers = { 'Authorization' : 'Basic %s' % base64string,
				'Content-Type': 'application/atom+xml'
			}    

		dati=open( os.environ['VID_RESOURCE_TEMPLATE'] ,'r').read()
		#dati = dati.replace('__CREATE_SECTION__', section )
		dati = dati.replace('__TITOLO_PICTURE__', titolo )
		dati = dati.replace('__BINARY_LOCATION__', binaryUrl )

		logger.debug( ' data  = ' + dati )

		response = requests.request('POST', link, headers=headers, data=dati)

		logger.debug( 'response status: ' + str(response.status_code ))
		
		if 201 == response.status_code:
			if 'Location' in response.headers:
				result[0] = True
				result[1] = response.headers['Location'].split('/')[-1]
			else:
				result[1] = 'Error in risposta create Img'
				result[0] = False
			
	
	except Exception as e:
		logger.debug( 'PROBLEMI in CreateProgrammeVideo : ' + str(e) )
		return [False, '' ]

	logger.debug('------------------------ finisce CreateProgrammeVideo -------------- ')
	return result

def prendiS3Img( fileS3Url ):
	result = None
	print('------------------------ INIT prendiS3Img -------------- ')

	try:
		print( 'link : ' + fileS3Url  )
		link = fileS3Url
		
		request = urllib.request.Request(url=link,  method='GET')
		#request.add_unredirected_header('Authorization', 'Bearer %s' % 'n2qefrD42juHkzAqrVaStvBUgsZHjN')
		resultResponse = urllib.request.urlopen(request)
		print ( resultResponse.status )
		#result = resultResponse.data
		print ( resultResponse.headers )
		#print ( resultResponse.headers )
		#print ( resultResponse.headers.split('rsi.ch/webservice/escenic/content/' )[-1] )
		print ( resultResponse.reason )
		result = resultResponse.read()




	except Exception as e:
		print ( 'PROBLEMI in prendiS3Img : ' + str(e) )
		logger.debug ( 'PROBLEMI in prendiS3Img : ' + str(e) )
		return [ False, { 'error' : str(e) } ]
	print('------------------------ END prendiS3Img -------------- ')
	return result

def importaImgFromAws( fileS3Url, section, titolo ):

	logger.debug('------------------------ INIT importaImg -------------- ')
	print('------------------------ INIT importaImg -------------- ')

	resultBool = False
	resultJson = {}
	#section = os.environ['IMPORT_SECTION']
	section = section

	binData = prendiS3Img( fileS3Url ) 
	

	binary = uploadBinaryFromBin( binData )
	print( binary )
	logger.debug( binary )
	if not binary[0] :
		# non son riuscito a fare la load del file ....
		# continuo senza cambiare img con id
		print ( ' not binary' )
		logger.debug ( ' not binary' )
		return binary
	else:
		# ho fatto upload binary e in binary[1] ho la location url del binary
		# da passare alla creazione del content picture
		print ( ' binary ok ' )
		logger.debug ( ' binary ok ' )
		print ( section )
		# CLAD Brutto FIX
		if section == -1 or len(section ) < 4:
			section = '15097'
		result_crea = createImg( binary[1] , titolo, section )
		# in result_crea ho [ True, EceId ]
		print ( result_crea ) 
		logger.debug ( result_crea ) 
		return result_crea

	logger.debug('------------------------ END importaImg -------------- ')
	print('------------------------ END importaImg -------------- ')

	return [ False , 'boh ? ' ]



def importaImg( file_path, section ):

	logger.debug('------------------------ INIT importaImg -------------- ')

	resultBool = False
	resultJson = {}
	#section = os.environ['IMPORT_SECTION']
	section = section

	# dove trovo filenames del tipo : GP765997_P00000011_1.jpg
	# con ancora tutto il path
	# e a me interessano il primo campo = LegacyId
	# e il numero prima del . che rappresenta quanti ce ne sono per quel LegacyId

	# in img ho la lista di file_path che passo per upload del binary
	logger.debug( ' file path = ' + file_path )

	binary = uploadBinaryFromFile( file_path )
	print( binary )
	if not binary[0] :
		# non son riuscito a fare la load del file ....
		# continuo senza cambiare img con id
		print ( ' not binary' )
		return binary
	else:
		# ho fatto upload binary e in binary[1] ho la location url del binary
		# da passare alla creazione del content picture
		print ( ' binary ok ' )
		result_crea = createImg( binary[1] , 'titolo', section )
		print ( result_crea ) 
		return result_crea

	logger.debug('------------------------ END importaImg -------------- ')

	return [ False , 'boh ? ' ]


def createTA( binaryUrl, titolo, section ):

	logger.debug( '------------------------ inizia createTA -------------- ' )
	print( '------------------------ inizia createTA -------------- ' )

	result = [False, '']

	# che deve fare la  curl --include -u perucccl:perucccl -X POST -H "Content-Type: application/atom+xml"  
	# http://internal.publishing.production.rsi.ch/webservice/escenic/section/__CREATE_SECTION__/content-items 
	# --upload-file Resources/template_picture.xml
	# dove __CREATE_SECTION__ deve essere cambiato con section e il file template deve essere aggiornato con i valori
	# dei parametri

	#try:
	link =  os.environ['AUDIO_CREATE_URL'].replace('__CREATE_SECTION__', section )
	logger.debug( 'link : ' + link )
	print ( 'link : ' + link )
	logger.debug ( 'link : ' + link )
	
	# apro il template file
	
	
	print ( 'binaryUrl : ' + binaryUrl )
	logger.debug ( 'binaryUrl : ' + binaryUrl )
	print ( 'titolo : ' + titolo )
	logger.debug ( 'titolo : ' + titolo )

	auth = '%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])
	base64string = b64encode(auth.encode())
	base64string = base64string.decode("ascii")

	headers = { 'Authorization' : 'Basic %s' % base64string,
			'Content-Type': 'application/atom+xml'
		}    

	print (  os.environ['AUDIO_RESOURCE_TEMPLATE'] )
	logger.debug (  os.environ['AUDIO_RESOURCE_TEMPLATE'] )
	dati=open( os.environ['AUDIO_RESOURCE_TEMPLATE'] ,'r').read()
	print ( dati )
	logger.debug ( dati )

	
	#dati = dati.replace('__CREATE_SECTION__', section )
	dati = dati.replace('__TITLE_MAMTRANSCODABLEVIDEO__', titolo )
	dati = dati.replace('__ECE_MODEL__', os.environ['ECE_MODEL'] )
	dati = dati.replace('__BINARY_LOCATION__', binaryUrl )

	print ( ' data da mandare --> ' + dati )
	logger.debug ( ' data  = ' + dati )
	dati = ( dati ).encode()

	try:
		request = urllib.request.Request(url=link, data=dati, headers=headers, method='POST')
		resultResponse = urllib.request.urlopen(request)
		print ( resultResponse.status )
		logger.debug ( resultResponse.status )
	except Exception as e:
		print ( 'PROBLEMI in createTA : ' + str(e) )
		logger.debug ( 'PROBLEMI in prendiS3TA : ' + str(e) )
		return [ False, { 'error' : str(e) } ]
	if  201 == resultResponse.status:
		#print ( 'tutto ok ' )
		print ( resultResponse.getheader('Location') )
		logger.debug ( resultResponse.getheader('Location') )
		#print ( resultResponse.getheader('Location').split('rsi.ch/webservice/escenic/content/' )[-1]  )
		return [ True, resultResponse.getheader('Location').split('rsi.ch/webservice/escenic/content/' )[-1] ]
	else:
		return [ False, resultResponse.status ]
	#print ( resultResponse.headers )
	#print ( resultResponse.headers )
	#print ( resultResponse.headers.split('rsi.ch/webservice/escenic/content/' )[-1] )
	#print ( resultResponse.reason )
	#print ( resultResponse.getheader('ocation') )
	#for lis  in resultResponse.getheaders():
		#print ( lis )
		#print (type(lis))
	
	'''
	except Exception as e:
		logger.debug( 'PROBLEMI in createTA : ' + str(e) )
		return [False, '' ]
	'''

	logger.debug('------------------------ finisce createTA -------------- ')
	print('------------------------ finisce createTA -------------- ')
	return result



def createImg( binaryUrl, titolo, section ):

	logger.debug( '------------------------ inizia createImg -------------- ' )
	print( '------------------------ inizia createImg -------------- ' )

	result = [False, '']

	# che deve fare la  curl --include -u perucccl:perucccl -X POST -H "Content-Type: application/atom+xml"  
	# http://internal.publishing.production.rsi.ch/webservice/escenic/section/__CREATE_SECTION__/content-items 
	# --upload-file Resources/template_picture.xml
	# dove __CREATE_SECTION__ deve essere cambiato con section e il file template deve essere aggiornato con i valori
	# dei parametri

	#try:
	link =  os.environ['IMG_CREATE_URL'].replace('__CREATE_SECTION__', section )
	logger.debug( 'link : ' + link )
	print ( 'link : ' + link )
	logger.debug ( 'link : ' + link )
	
	# apro il template file
	
	
	print ( 'binaryUrl : ' + binaryUrl )
	logger.debug ( 'binaryUrl : ' + binaryUrl )
	print ( 'titolo : ' + titolo )
	logger.debug ( 'titolo : ' + titolo )

	auth = '%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])
	base64string = b64encode(auth.encode())
	base64string = base64string.decode("ascii")

	headers = { 'Authorization' : 'Basic %s' % base64string,
			'Content-Type': 'application/atom+xml'
		}    

	print (  os.environ['IMG_RESOURCE_TEMPLATE'] )
	logger.debug (  os.environ['IMG_RESOURCE_TEMPLATE'] )
	dati=open( os.environ['IMG_RESOURCE_TEMPLATE'] ,'r').read()
	print ( dati )
	logger.debug ( dati )

	
	#dati = dati.replace('__CREATE_SECTION__', section )
	dati = dati.replace('__TITOLO_PICTURE__', titolo )
	dati = dati.replace('__ECE_MODEL__', os.environ['ECE_MODEL'] )
	dati = dati.replace('__BINARY_LOCATION__', binaryUrl )

	print ( ' data da mandare --> ' + dati )
	logger.debug ( ' data  = ' + dati )
	dati = ( dati ).encode()

	try:
		request = urllib.request.Request(url=link, data=dati, headers=headers, method='POST')
		resultResponse = urllib.request.urlopen(request)
		print ( resultResponse.status )
		logger.debug ( resultResponse.status )
	except Exception as e:
		print ( 'PROBLEMI in createImg : ' + str(e) )
		logger.debug ( 'PROBLEMI in prendiS3Img : ' + str(e) )
		return [ False, { 'error' : str(e) } ]
	if  201 == resultResponse.status:
		#print ( 'tutto ok ' )
		print ( resultResponse.getheader('Location') )
		logger.debug ( resultResponse.getheader('Location') )
		#print ( resultResponse.getheader('Location').split('rsi.ch/webservice/escenic/content/' )[-1]  )
		return [ True, resultResponse.getheader('Location').split('rsi.ch/webservice/escenic/content/' )[-1] ]
	else:
		return [ False, resultResponse.status ]
	#print ( resultResponse.headers )
	#print ( resultResponse.headers )
	#print ( resultResponse.headers.split('rsi.ch/webservice/escenic/content/' )[-1] )
	#print ( resultResponse.reason )
	#print ( resultResponse.getheader('ocation') )
	#for lis  in resultResponse.getheaders():
		#print ( lis )
		#print (type(lis))
	
	'''
	except Exception as e:
		logger.debug( 'PROBLEMI in createImg : ' + str(e) )
		return [False, '' ]
	'''

	logger.debug('------------------------ finisce createImg -------------- ')
	print('------------------------ finisce createImg -------------- ')
	return result

def uploadBinaryFromBin( binData ):

	logger.debug(" ---------------------- INIT uploadBinaryFromBin CLAD ------------------ " )
	print(" ---------------------- INIT uploadBinaryFromBin CLAD ------------------ " )
	result = [False, '']
	url = os.environ['ECE_BINARY']
	logger.debug( 'url per upload : ' + url )
	print( 'url per upload : ' + url )

	try:

		link =  url
		print( 'link : ' + link  )

		auth = '%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])
		base64string = b64encode(auth.encode())
		base64string = base64string.decode("ascii")

		#xmlStr = urllib.parse.urlencode(xmlStr).encode("utf-8")
		
		headers = { 'Authorization' : 'Basic %s' %  base64string ,
			  'Content-Type':'image/jpeg'}

		dati=binData
	
		#print( ' data  = ' + dati  )
		# probabilmente non necessario
		#dati = ( dati ).encode()

		request = urllib.request.Request(url=link, data=dati, headers=headers, method='POST')
		resultResponse = urllib.request.urlopen(request)
		print ( resultResponse.status )
		if  201 == resultResponse.status:
			#print ( 'tutto ok ' )
			print ( 'Location = ' + resultResponse.getheader('Location') )
			#print ( resultResponse.getheader('Location').split('rsi.ch/webservice/escenic/content/' )[-1]  )
			return [ True, resultResponse.getheader('Location').split('rsi.ch/webservice/escenic/content/' )[-1] ]
		else:
			return [ False, resultResponse.status ]
		#print ( resultResponse.headers )
		#print ( resultResponse.headers )
		#print ( resultResponse.headers.split('rsi.ch/webservice/escenic/content/' )[-1] )
		#print ( resultResponse.reason )
		#print ( resultResponse.getheader('ocation') )
		#for lis  in resultResponse.getheaders():
			#print ( lis )
			#print (type(lis))

	except Exception as e:
		print( 'PROBLEMI in uploadBinaryFromBin: ' + str(e)  )
		logger.error('ERROR: EXCEPT in uploadBinaryFromBin  = ' + str(e))
		return [False, str(e) ]


	logger.debug(" ---------------------- FINE uploadBinaryFromBin ------------------ " )
	print(" ---------------------- FINE uploadBinaryFromBin ------------------ " )
	return result


def uploadBinaryFromFile( file_path ):

	logger.debug(" ---------------------- INIT uploadBinaryFromFile CLAD ------------------ " )
	result = [False, '']
	url = os.environ['ECE_BINARY']
	logger.debug( 'url per upload : ' + url )
	logger.debug( 'file path per upload : ' + file_path )
	print( 'url per upload : ' + url )
	print( 'file path per upload : ' + file_path  )

	try:

		link =  url
		print( 'link : ' + link  )

		auth = '%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])
		base64string = b64encode(auth.encode())
		base64string = base64string.decode("ascii")

		#xmlStr = urllib.parse.urlencode(xmlStr).encode("utf-8")
		
		headers = { 'Authorization' : 'Basic %s' %  base64string ,
			  'Content-Type':'image/jpeg'}

		dati=open(file_path,'rb').read()
	
		#print( ' data  = ' + dati  )
		# probabilmente non necessario
		#dati = ( dati ).encode()

		request = urllib.request.Request(url=link, data=dati, headers=headers, method='POST')
		resultResponse = urllib.request.urlopen(request)
		print ( resultResponse.status )
		if  201 == resultResponse.status:
			#print ( 'tutto ok ' )
			print ( resultResponse.getheader('Location') )
			#print ( resultResponse.getheader('Location').split('rsi.ch/webservice/escenic/content/' )[-1]  )
			return [ True, resultResponse.getheader('Location').split('rsi.ch/webservice/escenic/content/' )[-1] ]
		else:
			return [ False, resultResponse.status ]
		#print ( resultResponse.headers )
		#print ( resultResponse.headers )
		#print ( resultResponse.headers.split('rsi.ch/webservice/escenic/content/' )[-1] )
		#print ( resultResponse.reason )
		#print ( resultResponse.getheader('ocation') )
		#for lis  in resultResponse.getheaders():
			#print ( lis )
			#print (type(lis))

	except Exception as e:
		print( 'PROBLEMI in uploadBinaryFromFile: ' + str(e)  )
		logger.error('ERROR: EXCEPT in uploadBinaryFromFile  = ' + str(e))
		return [False, str(e) ]


	logger.debug(" ---------------------- FINE uploadBinaryFromFile ------------------ " )
	return result

def uploadBinaryFromFileType( file_path, mameType ):

	logger.debug(" ---------------------- INIT uploadBinaryFromFile CLAD ------------------ " )
	result = [False, '']
	url = os.environ['ECE_BINARY']
	logger.debug( 'url per upload : ' + url )
	logger.debug( 'file path per upload : ' + file_path )
	print( 'url per upload : ' + url )
	print( 'file path per upload : ' + file_path  )

	try:

		link =  url
		print( 'link : ' + link  )

		auth = '%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])
		base64string = b64encode(auth.encode())
		base64string = base64string.decode("ascii")

		#xmlStr = urllib.parse.urlencode(xmlStr).encode("utf-8")
		
		headers = { 'Authorization' : 'Basic %s' %  base64string ,
			  'Content-Type': mameType}

		dati=open(file_path,'rb').read()
	
		#print( ' data  = ' + dati  )
		# probabilmente non necessario
		#dati = ( dati ).encode()

		request = urllib.request.Request(url=link, data=dati, headers=headers, method='POST')
		resultResponse = urllib.request.urlopen(request)
		print ( resultResponse.status )
		if  201 == resultResponse.status:
			#print ( 'tutto ok ' )
			print ( resultResponse.getheader('Location') )
			#print ( resultResponse.getheader('Location').split('rsi.ch/webservice/escenic/content/' )[-1]  )
			return [ True, resultResponse.getheader('Location').split('rsi.ch/webservice/escenic/content/' )[-1] ]
		else:
			return [ False, resultResponse.status ]
		#print ( resultResponse.headers )
		#print ( resultResponse.headers )
		#print ( resultResponse.headers.split('rsi.ch/webservice/escenic/content/' )[-1] )
		#print ( resultResponse.reason )
		#print ( resultResponse.getheader('ocation') )
		#for lis  in resultResponse.getheaders():
			#print ( lis )
			#print (type(lis))

	except Exception as e:
		print( 'PROBLEMI in uploadBinaryFromFile: ' + str(e)  )
		logger.error('ERROR: EXCEPT in uploadBinaryFromFile  = ' + str(e))
		return [False, str(e) ]


	logger.debug(" ---------------------- FINE uploadBinaryFromFile ------------------ " )
	return result


	
def createMamStr( section, xmlStr ):

	print( '------------------------ inizia createMamStr -------------- '  )
	logger.debug( '------------------------ inizia createMamStr -------------- '  )

	result = [False, '']

	# che deve fare la  curl --include -u perucccl:perucccl -X POST -H "Content-Type: application/atom+xml"  
	# http://internal.publishing.production.rsi.ch/webservice/escenic/section/__CREATE_SECTION__/content-items 
	# --upload-file Resources/template_mamProgramme.xml
	# dove __CREATE_SECTION__ deve essere cambiato con section e il file template deve essere aggiornato con i valori
	# dei parametri

	try:

		link =  os.environ['CREATE_URL'].replace('__CREATE_SECTION__', section )
		#link =  "http://internal.publishing.staging.rsi.ch/webservice/escenic/section/5909/content-items"
		print( 'link : ' + link  )
		logger.debug( 'link : ' + link  )

		auth = '%s:%s' % (os.environ['ECE_USER'], os.environ['ECE_PWD'])
		base64string = b64encode(auth.encode())
		base64string = base64string.decode("ascii")

		#xmlStr = urllib.parse.urlencode(xmlStr).encode("utf-8")
		
		headers = { 'Authorization' : 'Basic %s' %  base64string ,
			  'Content-Type':'application/atom+xml'}

		

		dati = xmlStr.replace('__ECE_MODEL__', os.environ['ECE_MODEL'] )
		
		print( ' data  = ' + dati  )
		logger.debug( ' data  = ' + dati  )
		dati = ( dati ).encode()

		request = urllib.request.Request(url=link, data=dati, headers=headers, method='POST')
		resultResponse = urllib.request.urlopen(request)
		print ( resultResponse.status )
		logger.debug ( resultResponse.status )
		#print ( resultResponse.getheader('Location') )
		
		if  201 == resultResponse.status:
			#print ( 'tutto ok ' )
			#print ( resultResponse.getheader('Location') )
			#print ( resultResponse.getheader('Location').split('rsi.ch/webservice/escenic/content/' )[-1]  )
			return [ True, resultResponse.getheader('Location').split('rsi.ch/webservice/escenic/content/' )[-1] ]
		else:
			return [ False, resultResponse.status ]
		#print ( resultResponse.headers )
		#print ( resultResponse.headers )
		#print ( resultResponse.headers.split('rsi.ch/webservice/escenic/content/' )[-1] )
		#print ( resultResponse.reason )
		#print ( resultResponse.getheader('ocation') )
		#for lis  in resultResponse.getheaders():
			#print ( lis )
			#print (type(lis))
		

	except Exception as e:
		print( 'PROBLEMI in createMamStr: ' + str(e)  )
		logger.debug( 'PROBLEMI in createMamStr: ' + str(e)  )
		return [False, str(e) ]

	print('------------------------ finisce createMamStr-------------- ' )
	logger.debug('------------------------ finisce createMamStr-------------- ' )
	return result

def spostaAudio(  file_path ):
	
	logger.debug('------------------------ INIT spostaAudio -------------- ')
	try:
		file_name = os.path.basename( file_path )
		logger.debug( "mv " + file_path + " to -> " + os.environ['IMPORT_ARCHIVE_DIR'] + file_name )
		shutil.move( file_path,  os.environ['IMPORT_ARCHIVE_DIR'] + file_name )
		
	except Exception as e:
		logger.error('ERROR: EXCEPT in spostaAudio  = ' + str(e))
		logger.error('ERROR: EXCEPT in spostaAudio per immagine = ' + file_path)
		pass

	logger.debug('------------------------ END spostaAudio -------------- ')



def importaAudio(  dict_da_importare, items_per_db ):
	perDb = items_per_db
	resultBool = True
	
	for key, value in dict_da_importare.items():
		print (key)
		title  = os.path.basename(key)
		title = title.replace('.MP3','').replace('.mp3', '')
		logger.debug( 'title = ' + title )
		localBool = False
		[ localBool, locationBin ]= uploadBinaryFromFileType( key , 'audio/mpeg')
		if not localBool:
			return[ False, locationBin]
		logger.debug( ' locationBin = ' + locationBin )
		localBool = False
		[ localBool, eceId ] = createTA( locationBin, title, os.environ['IMPORT_SECTION'] )
		if localBool:

			'''
			# questo sarebbe pe rpubblicarli in automatico e far cosi' partire il transcoding
			# del file ---- ma per i digas NON ci serve quindi lo TOLGO

			# siamo riusciti a crearlo in draft, adesso dobbiamo metterlo in published
			# quindi lo prendiamo e lo ripubblichiamo dopo avergli cambiato stato
			localBool = putToState( eceId, 'published' )
			if not localBool:
				resultBool = False
				perDb[ key ] = value
				continue
			'''


			spostaAudio( key )
			continue

		resultBool = False
		perDb[ key ] = value

	return [ resultBool, perDb ]


if __name__ == "__main__":

	

	os.environ['ECE_USER'] = 'TSMM'
	os.environ['ECE_PWD'] = '8AKjwWXiWAFTxb2UM3pZ'


	os.environ['ECE_MODEL'] = 'http://internal.publishing.staging.rsi.ch/webservice/publication/rsi/escenic/model/'
	os.environ['IMG_CREATE_URL'] = 'http://internal.publishing.staging.rsi.ch/webservice/escenic/section/__CREATE_SECTION__/content-items'
	os.environ['ECE_SERVER'] = 'http://internal.publishing.staging.rsi.ch/webservice/escenic/content/'
	os.environ['ECE_SECTION'] = 'http://internal.publishing.staging.rsi.ch/webservice/escenic/section/'
	os.environ['ECE_BRAND'] = 'http://internal.publishing.staging.rsi.ch/webservice-extensions/srg/sectionIdForBrand/?publicationName=rsi&channel=__CHANNEL__&brand=__BRAND__'

	os.environ['CREATE_URL'] =  "http://internal.publishing.staging.rsi.ch/webservice/escenic/section/__CREATE_SECTION__/content-items"
	os.environ['UPDATE_FILE'] =  "/home/perucccl/Webservices/STAGING/newMamServices/Resources/_cambiamento_"
	#Dump_Link("http://internal.publishing.staging.rsi.ch/webservice/escenic/content/13234172", '13234172.xml')
	#exit(0)
	#putId( '13221720', './Programme_13221720.xml')
	#Dump_Link("http://internal.publishing.staging.rsi.ch/webservice/publication/rsi/escenic/model/mamSegmentedProgrammeVideo", 'mamSegmentedProgrammeVideo_model.xml')
	#Dump_Link("http://internal.publishing.staging.rsi.ch/webservice/escenic/content/13202466", 'transcodable_13202466.xml')
	#Dump_Link("http://internal.publishing.staging.rsi.ch/webservice/escenic/content/13217516", 'transcodableAudio_13217516.xml')
	#Dump_Link("http://internal.publishing.staging.rsi.ch/webservice/escenic/content/13226238", 'pv_13226238.xml')
	#exit(0)
	
	Environment_Variables = {'FILE_LOG_DIR':'/home/perucccl/Webservices/STAGING/newMamServices/LOGS/FILES','BINARY_PATH_4_TV':'http://internal.publishing.staging.rsi.ch/webservice/escenic/binary/13202466/2020/7/6/14/Giappone+sotto+l%2527acqua.mp4','KAFKA_IMPORT_PATH' : '/home/perucccl/Webservices/STAGING/newMamServices/KAFKA', 'ECE_IMPORT_PATH':'/home/perucccl/Webservices/STAGING/newMamServices/ECE','PUBLISH_ECE' : 'http://publishing.staging.rsi.ch/rsi-api/mam/blackhole/publish/content/','COUCH_DB' : 'testnuovo', 'COUCH_HOST' : 'rsis-zp-mongo1', 'COUCH_PORT':'5984', 'COUCH_USER':'admin', 'COUCH_PWD':'78-AjhQ','IMPORT_SECTION': '9736','CREATE_URL':'http://internal.publishing.staging.rsi.ch/webservice/escenic/section/__CREATE_SECTION__/content-items','IMG_CREATE_URL':'http://internal.publishing.staging.rsi.ch/webservice/escenic/section/__CREATE_SECTION__/content-items', 'VID_RESOURCE_TEMPLATE':'/home/perucccl/Webservices/STAGING/CreateTranscodable/Resources/template_pVideo.xml.stag', 'IMG_RESOURCE_TEMPLATE':'/home/perucccl/Webservices/STAGING/newMamServices/Resources/template_picture.xml','LOCK_URL' : 'http://internal.publishing.production.rsi.ch/webservice/escenic/lock/article/','LOCK_NAME' : 'template_lock.xml','LOCK_ID' : '11868353' ,'REST_POST_URL': 'http://publishing.staging.rsi.ch/rsi-api/intlay/mockup/importkeyframe/keyframes.json','REST_GET_URL': 'http://publishing.staging.rsi.ch/rsi-api/intlay/mockup/importkeyframe/keyframes.json', 'FTP_ARCHIVE_DIR' : '/mnt/rsi_import/keyframe_traffic/archived/',  'FTP_DIR' : '/mnt/rsi_import/keyframe_traffic/test/','VERSION' : '3.2','ECE_USER' : 'TSMM', 'ECE_PWD':'8AKjwWXiWAFTxb2UM3pZ', 'ECE_MODEL':'http://internal.publishing.staging.rsi.ch/webservice/publication/rsi/escenic/model/','ECE_SERVER' : 'http://internal.publishing.staging.rsi.ch/webservice/escenic/content/','ECE_BINARY' : 'http://internal.publishing.staging.rsi.ch/webservice/escenic/binary','ECE_BRAND' : 'http://internal.publishing.staging.rsi.ch/webservice-extensions/srg/sectionIdForBrand/?publicationName=rsi&channel=__CHANNEL__&brand=__BRAND__','ECE_THUMB' : 'http://internal.publishing.staging.rsi.ch/webservice/thumbnail/article/', 'ECE_SECTION' : 'http://internal.publishing.staging.rsi.ch/webservice/escenic/section/', 'UPDATE_FILE' : '/home/perucccl/Webservices/STAGING/newMamServices/Resources/_cambiamento_','ARCHIVE_NAME' : '/home/perucccl/STAGING/APICoreX/Resources/_LivestreamingArchive_','LOCK_RESOURCE_DIR':'/home/perucccl/Webservices/STAGING/newMamServices/Resources/','RESOURCE_DIR':'/home/perucccl/Webservices/STAGING/newMamServices/Resources/','DB_NAME' : '/home/perucccl/Webservices/STAGING/CreateTranscodable/Resources/_ImportKeyFramesDb_', 'http_proxy': 'http://gateway.zscloud.net:10268', 'LESSOPEN': '||/usr/bin/lesspipe.sh %s', 'SSH_CLIENT': '146.159.126.207 57239 22', 'SELINUX_USE_CUR RENT_RANGE': '', 'LOGNAME': 'perucccl', 'USER': 'perucccl', 'HOME': '/home/perucccl', 'PATH': '/usr/local/bin:/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/sbin:/home/perucccl/bin', 'LANG': 'en_US.UTF-8', 'TERM': 'xterm', 'SHELL': '/bin/bash', 'SHLVL': '1', 'G_BROKEN_FILENAME S': '1', 'HISTSIZE': '1000', 'https_proxy': 'http://gateway.zscloud.net:10268', 'SELINUX_ROLE_REQUESTED': '', '_': '/usr/bin/python', 'SSH_CONNECTION': '146.159.126.207 57239 10.72.112.35 22', 'SSH_TTY': '/dev/pts/1', 'HOSTNAME': 'rsis-prod-web1.media.int', 'SELINUX_LEVE L_REQUESTED': '', 'HISTCONTROL': 'ignoredups', 'no_proxy': 'amazonaws.com,rsis-zp-mongo1,localhost,127.0.0.1,.media.int,rsis-tifone-t1,rsis-tifone-t2,rsis-tifone-t,.rsi.ch,10.102.7.38:8180,10.101.8.27:8180,.twitter.com', 'MAIL': '/var/spool/mail/perucccl', 'LS_COLORS': 'rs=0:di=01;34:ln=01;36:mh=00: pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:mi=01;05;37;41:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=34;42:st=37;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arj=01;31:*.taz=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.zip=01;31:*.z=01;31:*.Z=01;31:* .dz=01;31:*.gz=01;31:*.lz=01;31:*.xz=01;31:*.bz2=01;31:*.tbz=01;31:*.tbz2=01;31:*.bz=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.rar=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.jpg=01;35:*.jpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:* .pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob=01;35:*.qt =01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.axv=01;35:*.anx=01;35:*.ogv=01;35:*.ogx=01;35:*.aac=01;36:*.au=01;36:*.f lac=01;36:*.mid=01;36:*.midi=01;36:*.mka=01;36:*.mp3=01;36:*.mpc=01;36:*.ogg=01;36:*.ra=01;36:*.wav=01;36:*.axa=01;36:*.oga=01;36:*.spx=01;36:*.xspf=01;36:'} 
	for param in Environment_Variables.keys():
		os.environ[param] = Environment_Variables[ param ]

